package com.escalableapps.cmdb.baseapp.template.dsl.helper;

import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QBarEntity.barEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAUpdateClause;

public final class BarDslHelper {

  public static BooleanBuilder whereCondition( //
      Integer barId //
  ) {
    BarQuery barQuery = new BarQuery();
    barQuery.setBarId(barId);
    return whereCondition(barQuery);
  }

  public static BooleanBuilder whereCondition(BarQuery barQuery) {
    BooleanBuilder query = new BooleanBuilder();
    whereConditionForBarId(query, barQuery);
    whereConditionForBarChar(query, barQuery);
    whereConditionForBarVarchar(query, barQuery);
    whereConditionForBarText(query, barQuery);
    whereConditionForBarSmallint(query, barQuery);
    whereConditionForBarInteger(query, barQuery);
    whereConditionForBarBigint(query, barQuery);
    whereConditionForBarReal(query, barQuery);
    whereConditionForBarDouble(query, barQuery);
    whereConditionForBarDecimal(query, barQuery);
    whereConditionForBarBoolean(query, barQuery);
    whereConditionForBarDate(query, barQuery);
    whereConditionForBarTimestamp(query, barQuery);
    return query;
  }

  private static void whereConditionForBarId(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarId() != null) {
      if (!barQuery.getBarId().exist()) {
        query.and(barEntity.barId.isNull());
      } else {
        query.and(barEntity.barId.eq(getQueryValue(barQuery.getBarId())));
      }
    }
  }

  private static void whereConditionForBarChar(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarChar() != null) {
      if (!barQuery.getBarChar().exist()) {
        query.and(barEntity.barChar.isNull());
      } else {
        query.and(barEntity.barChar.eq(getQueryValue(barQuery.getBarChar())));
      }
    }
  }

  private static void whereConditionForBarVarchar(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarVarchar() != null) {
      if (!barQuery.getBarVarchar().exist()) {
        query.and(barEntity.barVarchar.isNull());
      } else {
        query.and(barEntity.barVarchar.eq(getQueryValue(barQuery.getBarVarchar())));
      }
    }
  }

  private static void whereConditionForBarText(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarText() != null) {
      if (!barQuery.getBarText().exist()) {
        query.and(barEntity.barText.isNull());
      } else {
        query.and(barEntity.barText.eq(getQueryValue(barQuery.getBarText())));
      }
    }
  }

  private static void whereConditionForBarSmallint(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarSmallint() != null) {
      if (!barQuery.getBarSmallint().exist()) {
        query.and(barEntity.barSmallint.isNull());
      } else {
        query.and(barEntity.barSmallint.eq(getQueryValue(barQuery.getBarSmallint())));
      }
    }
  }

  private static void whereConditionForBarInteger(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarInteger() != null) {
      if (!barQuery.getBarInteger().exist()) {
        query.and(barEntity.barInteger.isNull());
      } else {
        query.and(barEntity.barInteger.eq(getQueryValue(barQuery.getBarInteger())));
      }
    }
  }

  private static void whereConditionForBarBigint(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarBigint() != null) {
      if (!barQuery.getBarBigint().exist()) {
        query.and(barEntity.barBigint.isNull());
      } else {
        query.and(barEntity.barBigint.eq(getQueryValue(barQuery.getBarBigint())));
      }
    }
  }

  private static void whereConditionForBarReal(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarReal() != null) {
      if (!barQuery.getBarReal().exist()) {
        query.and(barEntity.barReal.isNull());
      } else {
        query.and(barEntity.barReal.eq(getQueryValue(barQuery.getBarReal())));
      }
    }
  }

  private static void whereConditionForBarDouble(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarDouble() != null) {
      if (!barQuery.getBarDouble().exist()) {
        query.and(barEntity.barDouble.isNull());
      } else {
        query.and(barEntity.barDouble.eq(getQueryValue(barQuery.getBarDouble())));
      }
    }
  }

  private static void whereConditionForBarDecimal(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarDecimal() != null) {
      if (!barQuery.getBarDecimal().exist()) {
        query.and(barEntity.barDecimal.isNull());
      } else {
        query.and(barEntity.barDecimal.eq(getQueryValue(barQuery.getBarDecimal())));
      }
    }
  }

  private static void whereConditionForBarBoolean(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarBoolean() != null) {
      if (!barQuery.getBarBoolean().exist()) {
        query.and(barEntity.barBoolean.isNull());
      } else {
        query.and(barEntity.barBoolean.eq(getQueryValue(barQuery.getBarBoolean())));
      }
    }
  }

  private static void whereConditionForBarDate(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarDate() != null) {
      if (!barQuery.getBarDate().exist()) {
        query.and(barEntity.barDate.isNull());
      } else {
        query.and(barEntity.barDate.eq(getQueryValue(barQuery.getBarDate())));
      }
    } else if (barQuery.getBarDateMin() != null || barQuery.getBarDateMax() != null) {
      if (barQuery.getBarDateMin() != null && barQuery.getBarDateMin().exist()) {
        query.and(barEntity.barDate.goe(barQuery.getBarDateMin().get()));
      }
      if (barQuery.getBarDateMax() != null && barQuery.getBarDateMax().exist()) {
        query.and(barEntity.barDate.loe(barQuery.getBarDateMax().get()));
      }
    }
  }

  private static void whereConditionForBarTimestamp(BooleanBuilder query, BarQuery barQuery) {
    if (barQuery.getBarTimestamp() != null) {
      if (!barQuery.getBarTimestamp().exist()) {
        query.and(barEntity.barTimestamp.isNull());
      } else {
        query.and(barEntity.barTimestamp.eq(getQueryValue(barQuery.getBarTimestamp())));
      }
    } else if (barQuery.getBarTimestampMin() != null || barQuery.getBarTimestampMax() != null) {
      if (barQuery.getBarTimestampMin() != null && barQuery.getBarTimestampMin().exist()) {
        query.and(barEntity.barTimestamp.goe(barQuery.getBarTimestampMin().get()));
      }
      if (barQuery.getBarTimestampMax() != null && barQuery.getBarTimestampMax().exist()) {
        query.and(barEntity.barTimestamp.loe(barQuery.getBarTimestampMax().get()));
      }
    }
  }

  public static OrderSpecifier<?>[] orderBy(List<SortRequest<BarPropertyName>> sorts) {
    List<OrderSpecifier<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForBarId(orders, sort);
      orderByForBarChar(orders, sort);
      orderByForBarVarchar(orders, sort);
      orderByForBarText(orders, sort);
      orderByForBarSmallint(orders, sort);
      orderByForBarInteger(orders, sort);
      orderByForBarBigint(orders, sort);
      orderByForBarReal(orders, sort);
      orderByForBarDouble(orders, sort);
      orderByForBarDecimal(orders, sort);
      orderByForBarBoolean(orders, sort);
      orderByForBarDate(orders, sort);
      orderByForBarTimestamp(orders, sort);
    });
    return orders.stream().toArray(size -> new OrderSpecifier[size]);
  }

  private static void orderByForBarId(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_ID) {
      orders.add(sort.isAscendant() ? barEntity.barId.asc() : barEntity.barId.desc());
    }
  }

  private static void orderByForBarChar(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_CHAR) {
      orders.add(sort.isAscendant() ? barEntity.barChar.asc() : barEntity.barChar.desc());
    }
  }

  private static void orderByForBarVarchar(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_VARCHAR) {
      orders.add(sort.isAscendant() ? barEntity.barVarchar.asc() : barEntity.barVarchar.desc());
    }
  }

  private static void orderByForBarText(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_TEXT) {
      orders.add(sort.isAscendant() ? barEntity.barText.asc() : barEntity.barText.desc());
    }
  }

  private static void orderByForBarSmallint(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_SMALLINT) {
      orders.add(sort.isAscendant() ? barEntity.barSmallint.asc() : barEntity.barSmallint.desc());
    }
  }

  private static void orderByForBarInteger(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_INTEGER) {
      orders.add(sort.isAscendant() ? barEntity.barInteger.asc() : barEntity.barInteger.desc());
    }
  }

  private static void orderByForBarBigint(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_BIGINT) {
      orders.add(sort.isAscendant() ? barEntity.barBigint.asc() : barEntity.barBigint.desc());
    }
  }

  private static void orderByForBarReal(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_REAL) {
      orders.add(sort.isAscendant() ? barEntity.barReal.asc() : barEntity.barReal.desc());
    }
  }

  private static void orderByForBarDouble(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DOUBLE) {
      orders.add(sort.isAscendant() ? barEntity.barDouble.asc() : barEntity.barDouble.desc());
    }
  }

  private static void orderByForBarDecimal(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DECIMAL) {
      orders.add(sort.isAscendant() ? barEntity.barDecimal.asc() : barEntity.barDecimal.desc());
    }
  }

  private static void orderByForBarBoolean(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_BOOLEAN) {
      orders.add(sort.isAscendant() ? barEntity.barBoolean.asc() : barEntity.barBoolean.desc());
    }
  }

  private static void orderByForBarDate(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DATE) {
      orders.add(sort.isAscendant() ? barEntity.barDate.asc() : barEntity.barDate.desc());
    }
  }

  private static void orderByForBarTimestamp(List<OrderSpecifier<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_TIMESTAMP) {
      orders.add(sort.isAscendant() ? barEntity.barTimestamp.asc() : barEntity.barTimestamp.desc());
    }
  }

  public static JPAUpdateClause updateValues(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    updateValuesForBarChar(jpaUpdate, barUpdate);
    updateValuesForBarVarchar(jpaUpdate, barUpdate);
    updateValuesForBarText(jpaUpdate, barUpdate);
    updateValuesForBarSmallint(jpaUpdate, barUpdate);
    updateValuesForBarInteger(jpaUpdate, barUpdate);
    updateValuesForBarBigint(jpaUpdate, barUpdate);
    updateValuesForBarReal(jpaUpdate, barUpdate);
    updateValuesForBarDouble(jpaUpdate, barUpdate);
    updateValuesForBarDecimal(jpaUpdate, barUpdate);
    updateValuesForBarBoolean(jpaUpdate, barUpdate);
    updateValuesForBarDate(jpaUpdate, barUpdate);
    updateValuesForBarTimestamp(jpaUpdate, barUpdate);
    return jpaUpdate;
  }

  private static void updateValuesForBarChar(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarChar() != null) {
      jpaUpdate.set(barEntity.barChar, getUpdateValue(barUpdate.getBarChar()));
    }
  }

  private static void updateValuesForBarVarchar(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarVarchar() != null) {
      jpaUpdate.set(barEntity.barVarchar, getUpdateValue(barUpdate.getBarVarchar()));
    }
  }

  private static void updateValuesForBarText(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarText() != null) {
      jpaUpdate.set(barEntity.barText, getUpdateValue(barUpdate.getBarText()));
    }
  }

  private static void updateValuesForBarSmallint(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarSmallint() != null) {
      jpaUpdate.set(barEntity.barSmallint, getUpdateValue(barUpdate.getBarSmallint()));
    }
  }

  private static void updateValuesForBarInteger(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarInteger() != null) {
      jpaUpdate.set(barEntity.barInteger, getUpdateValue(barUpdate.getBarInteger()));
    }
  }

  private static void updateValuesForBarBigint(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarBigint() != null) {
      jpaUpdate.set(barEntity.barBigint, getUpdateValue(barUpdate.getBarBigint()));
    }
  }

  private static void updateValuesForBarReal(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarReal() != null) {
      jpaUpdate.set(barEntity.barReal, getUpdateValue(barUpdate.getBarReal()));
    }
  }

  private static void updateValuesForBarDouble(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarDouble() != null) {
      jpaUpdate.set(barEntity.barDouble, getUpdateValue(barUpdate.getBarDouble()));
    }
  }

  private static void updateValuesForBarDecimal(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarDecimal() != null) {
      jpaUpdate.set(barEntity.barDecimal, getUpdateValue(barUpdate.getBarDecimal()));
    }
  }

  private static void updateValuesForBarBoolean(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarBoolean() != null) {
      jpaUpdate.set(barEntity.barBoolean, getUpdateValue(barUpdate.getBarBoolean()));
    }
  }

  private static void updateValuesForBarDate(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarDate() != null) {
      jpaUpdate.set(barEntity.barDate, getUpdateValue(barUpdate.getBarDate()));
    }
  }

  private static void updateValuesForBarTimestamp(JPAUpdateClause jpaUpdate, BarUpdate barUpdate) {
    if (barUpdate.getBarTimestamp() != null) {
      jpaUpdate.set(barEntity.barTimestamp, getUpdateValue(barUpdate.getBarTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private BarDslHelper() {
  }
}
