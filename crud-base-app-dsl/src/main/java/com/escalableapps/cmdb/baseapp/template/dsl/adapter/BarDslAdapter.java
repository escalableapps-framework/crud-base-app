package com.escalableapps.cmdb.baseapp.template.dsl.adapter;

import static com.escalableapps.cmdb.baseapp.template.dsl.helper.BarDslHelper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.mapper.BarEntityMapper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QBarEntity.barEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.dsl.mapper.BarEntityMapper;
import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;
import com.escalableapps.cmdb.baseapp.template.dsl.repository.BarDslRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class BarDslAdapter implements BarSpi {

  private final BarDslRepository repository;
  private final JPAQueryFactory queryFactory;

  public BarDslAdapter(BarDslRepository repository, JPAQueryFactory queryFactory) {
    this.repository = repository;
    this.queryFactory = queryFactory;
  }

  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBar:bar={}", bar);

    BarEntity barEntity = repository.save(fromEntity(bar));

    bar.setBarId(barEntity.getBarId());
    log.debug("createBar:bar={}", bar);
    return bar;
  }

  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    BarEntity bar = repository.findById(barId).orElse(null);
    log.debug("findBar:bar={}", bar);

    return Optional.ofNullable(toEntity(bar));
  }

  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);

    List<BarEntity> bars = repository.findAllById(barIds);
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery //
  ) {
    return findBars(barQuery, new PageRequest<>());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);

    List<BarEntity> bars = queryFactory.selectFrom(barEntity) //
        .where(whereCondition(barQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = repository.existsById(barId);

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBars:barQuery={}", barQuery);

    boolean exists = repository.exists(whereCondition(barQuery));

    log.debug("existsBars:exists={}", exists);
    return exists;
  }

  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("countBars:barQuery={}", barQuery);

    long count = repository.count(whereCondition(barQuery));

    log.debug("countBars:count={}", count);
    return count;
  }

  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("countBars:barId={}", barId);
    log.debug("countBars:barUpdate={}", barUpdate);

    JPAUpdateClause jpaUpdate = queryFactory.update(barEntity);
    updateValues(jpaUpdate, barUpdate);
    jpaUpdate.where(whereCondition(barId));
    long updateCount = jpaUpdate.execute();

    log.debug("countBars:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("countBars:barId={}", barId);

    long deleteCount = queryFactory.delete(barEntity) //
        .where(whereCondition(barId)) //
        .execute();

    log.debug("countBars:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
