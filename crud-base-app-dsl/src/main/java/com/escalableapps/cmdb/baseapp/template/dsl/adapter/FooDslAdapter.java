package com.escalableapps.cmdb.baseapp.template.dsl.adapter;

import static com.escalableapps.cmdb.baseapp.template.dsl.helper.FooDslHelper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.mapper.FooEntityMapper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QFooEntity.fooEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.dsl.mapper.FooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;
import com.escalableapps.cmdb.baseapp.template.dsl.repository.FooDslRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class FooDslAdapter implements FooSpi {

  private final FooDslRepository repository;
  private final JPAQueryFactory queryFactory;

  public FooDslAdapter(FooDslRepository repository, JPAQueryFactory queryFactory) {
    this.repository = repository;
    this.queryFactory = queryFactory;
  }

  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoo:foo={}", foo);

    FooEntity fooEntity = repository.save(fromEntity(foo));

    foo.setFooId(fooEntity.getFooId());
    log.debug("createFoo:foo={}", foo);
    return foo;
  }

  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    FooEntity foo = repository.findById(fooId).orElse(null);
    log.debug("findFoo:foo={}", foo);

    return Optional.ofNullable(toEntity(foo));
  }

  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);

    List<FooEntity> foos = repository.findAllById(fooIds);
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery //
  ) {
    return findFoos(fooQuery, new PageRequest<>());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);

    List<FooEntity> foos = queryFactory.selectFrom(fooEntity) //
        .where(whereCondition(fooQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = repository.existsById(fooId);

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoos:fooQuery={}", fooQuery);

    boolean exists = repository.exists(whereCondition(fooQuery));

    log.debug("existsFoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("countFoos:fooQuery={}", fooQuery);

    long count = repository.count(whereCondition(fooQuery));

    log.debug("countFoos:count={}", count);
    return count;
  }

  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("countFoos:fooId={}", fooId);
    log.debug("countFoos:fooUpdate={}", fooUpdate);

    JPAUpdateClause jpaUpdate = queryFactory.update(fooEntity);
    updateValues(jpaUpdate, fooUpdate);
    jpaUpdate.where(whereCondition(fooId));
    long updateCount = jpaUpdate.execute();

    log.debug("countFoos:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("countFoos:fooId={}", fooId);

    long deleteCount = queryFactory.delete(fooEntity) //
        .where(whereCondition(fooId)) //
        .execute();

    log.debug("countFoos:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
