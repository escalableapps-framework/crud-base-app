package com.escalableapps.cmdb.baseapp.template.dsl.persistence;

import static com.escalableapps.cmdb.baseapp.shared.dsl.SqlTypes.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "crud_base_db", name = "look")
public class LookEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @EqualsAndHashCode.Include
  @EmbeddedId
  private LookPk id;

  @Column(name = "look_char", columnDefinition = CHAR)
  private String lookChar;

  @Column(name = "look_varchar", columnDefinition = VARCHAR)
  private String lookVarchar;

  @Column(name = "look_text", columnDefinition = TEXT)
  private String lookText;

  @Column(name = "look_smallint", columnDefinition = SMALLINT)
  private Short lookSmallint;

  @Column(name = "look_integer", columnDefinition = INTEGER)
  private Integer lookInteger;

  @Column(name = "look_bigint", columnDefinition = BIGINT)
  private Long lookBigint;

  @Column(name = "look_real", columnDefinition = REAL)
  private Float lookReal;

  @Column(name = "look_double", columnDefinition = DOUBLE)
  private Double lookDouble;

  @Column(name = "look_decimal", columnDefinition = DECIMAL)
  private BigDecimal lookDecimal;

  @Column(name = "look_boolean", columnDefinition = BOOLEAN)
  private Boolean lookBoolean;

  @Column(name = "look_date", columnDefinition = DATE)
  private LocalDate lookDate;

  @Column(name = "look_timestamp", columnDefinition = TIMESTAMP)
  private OffsetDateTime lookTimestamp;
}
