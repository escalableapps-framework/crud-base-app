package com.escalableapps.cmdb.baseapp.template.dsl.persistence;

import static com.escalableapps.cmdb.baseapp.shared.dsl.SqlTypes.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "crud_base_db", name = "foo")
public class FooEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @EqualsAndHashCode.Include
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "foo_id", columnDefinition = INTEGER)
  private Integer fooId;

  @Column(name = "foo_char", columnDefinition = CHAR)
  private String fooChar;

  @Column(name = "foo_varchar", columnDefinition = VARCHAR)
  private String fooVarchar;

  @Column(name = "foo_text", columnDefinition = TEXT)
  private String fooText;

  @Column(name = "foo_smallint", columnDefinition = SMALLINT)
  private Short fooSmallint;

  @Column(name = "foo_integer", columnDefinition = INTEGER)
  private Integer fooInteger;

  @Column(name = "foo_bigint", columnDefinition = BIGINT)
  private Long fooBigint;

  @Column(name = "foo_real", columnDefinition = REAL)
  private Float fooReal;

  @Column(name = "foo_double", columnDefinition = DOUBLE)
  private Double fooDouble;

  @Column(name = "foo_decimal", columnDefinition = DECIMAL)
  private BigDecimal fooDecimal;

  @Column(name = "foo_boolean", columnDefinition = BOOLEAN)
  private Boolean fooBoolean;

  @Column(name = "foo_date", columnDefinition = DATE)
  private LocalDate fooDate;

  @Column(name = "foo_timestamp", columnDefinition = TIMESTAMP)
  private OffsetDateTime fooTimestamp;
}
