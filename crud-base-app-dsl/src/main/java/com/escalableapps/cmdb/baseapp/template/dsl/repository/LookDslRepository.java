package com.escalableapps.cmdb.baseapp.template.dsl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;

public interface LookDslRepository extends JpaRepository<LookEntity, LookPk>, QuerydslPredicateExecutor<LookEntity> {

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.look_look_id_seq')")
  Long nextVal();
}
