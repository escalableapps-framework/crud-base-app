package com.escalableapps.cmdb.baseapp.template.dsl.helper;

import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QLookEntity.lookEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAUpdateClause;

public final class LookDslHelper {

  public static BooleanBuilder whereCondition( //
      Long lookOrg, //
      Long lookId //
  ) {
    LookQuery lookQuery = new LookQuery();
    lookQuery.setLookOrg(lookOrg);
    lookQuery.setLookId(lookId);
    return whereCondition(lookQuery);
  }

  public static BooleanBuilder whereCondition(LookQuery lookQuery) {
    BooleanBuilder query = new BooleanBuilder();
    whereConditionForLookOrg(query, lookQuery);
    whereConditionForLookId(query, lookQuery);
    whereConditionForLookChar(query, lookQuery);
    whereConditionForLookVarchar(query, lookQuery);
    whereConditionForLookText(query, lookQuery);
    whereConditionForLookSmallint(query, lookQuery);
    whereConditionForLookInteger(query, lookQuery);
    whereConditionForLookBigint(query, lookQuery);
    whereConditionForLookReal(query, lookQuery);
    whereConditionForLookDouble(query, lookQuery);
    whereConditionForLookDecimal(query, lookQuery);
    whereConditionForLookBoolean(query, lookQuery);
    whereConditionForLookDate(query, lookQuery);
    whereConditionForLookTimestamp(query, lookQuery);
    return query;
  }

  private static void whereConditionForLookOrg(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookOrg() != null) {
      if (!lookQuery.getLookOrg().exist()) {
        query.and(lookEntity.id.lookOrg.isNull());
      } else {
        query.and(lookEntity.id.lookOrg.eq(getQueryValue(lookQuery.getLookOrg())));
      }
    }
  }

  private static void whereConditionForLookId(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookId() != null) {
      if (!lookQuery.getLookId().exist()) {
        query.and(lookEntity.id.lookId.isNull());
      } else {
        query.and(lookEntity.id.lookId.eq(getQueryValue(lookQuery.getLookId())));
      }
    }
  }

  private static void whereConditionForLookChar(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookChar() != null) {
      if (!lookQuery.getLookChar().exist()) {
        query.and(lookEntity.lookChar.isNull());
      } else {
        query.and(lookEntity.lookChar.eq(getQueryValue(lookQuery.getLookChar())));
      }
    }
  }

  private static void whereConditionForLookVarchar(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookVarchar() != null) {
      if (!lookQuery.getLookVarchar().exist()) {
        query.and(lookEntity.lookVarchar.isNull());
      } else {
        query.and(lookEntity.lookVarchar.eq(getQueryValue(lookQuery.getLookVarchar())));
      }
    }
  }

  private static void whereConditionForLookText(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookText() != null) {
      if (!lookQuery.getLookText().exist()) {
        query.and(lookEntity.lookText.isNull());
      } else {
        query.and(lookEntity.lookText.eq(getQueryValue(lookQuery.getLookText())));
      }
    }
  }

  private static void whereConditionForLookSmallint(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookSmallint() != null) {
      if (!lookQuery.getLookSmallint().exist()) {
        query.and(lookEntity.lookSmallint.isNull());
      } else {
        query.and(lookEntity.lookSmallint.eq(getQueryValue(lookQuery.getLookSmallint())));
      }
    }
  }

  private static void whereConditionForLookInteger(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookInteger() != null) {
      if (!lookQuery.getLookInteger().exist()) {
        query.and(lookEntity.lookInteger.isNull());
      } else {
        query.and(lookEntity.lookInteger.eq(getQueryValue(lookQuery.getLookInteger())));
      }
    }
  }

  private static void whereConditionForLookBigint(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookBigint() != null) {
      if (!lookQuery.getLookBigint().exist()) {
        query.and(lookEntity.lookBigint.isNull());
      } else {
        query.and(lookEntity.lookBigint.eq(getQueryValue(lookQuery.getLookBigint())));
      }
    }
  }

  private static void whereConditionForLookReal(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookReal() != null) {
      if (!lookQuery.getLookReal().exist()) {
        query.and(lookEntity.lookReal.isNull());
      } else {
        query.and(lookEntity.lookReal.eq(getQueryValue(lookQuery.getLookReal())));
      }
    }
  }

  private static void whereConditionForLookDouble(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookDouble() != null) {
      if (!lookQuery.getLookDouble().exist()) {
        query.and(lookEntity.lookDouble.isNull());
      } else {
        query.and(lookEntity.lookDouble.eq(getQueryValue(lookQuery.getLookDouble())));
      }
    }
  }

  private static void whereConditionForLookDecimal(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookDecimal() != null) {
      if (!lookQuery.getLookDecimal().exist()) {
        query.and(lookEntity.lookDecimal.isNull());
      } else {
        query.and(lookEntity.lookDecimal.eq(getQueryValue(lookQuery.getLookDecimal())));
      }
    }
  }

  private static void whereConditionForLookBoolean(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookBoolean() != null) {
      if (!lookQuery.getLookBoolean().exist()) {
        query.and(lookEntity.lookBoolean.isNull());
      } else {
        query.and(lookEntity.lookBoolean.eq(getQueryValue(lookQuery.getLookBoolean())));
      }
    }
  }

  private static void whereConditionForLookDate(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookDate() != null) {
      if (!lookQuery.getLookDate().exist()) {
        query.and(lookEntity.lookDate.isNull());
      } else {
        query.and(lookEntity.lookDate.eq(getQueryValue(lookQuery.getLookDate())));
      }
    } else if (lookQuery.getLookDateMin() != null || lookQuery.getLookDateMax() != null) {
      if (lookQuery.getLookDateMin() != null && lookQuery.getLookDateMin().exist()) {
        query.and(lookEntity.lookDate.goe(lookQuery.getLookDateMin().get()));
      }
      if (lookQuery.getLookDateMax() != null && lookQuery.getLookDateMax().exist()) {
        query.and(lookEntity.lookDate.loe(lookQuery.getLookDateMax().get()));
      }
    }
  }

  private static void whereConditionForLookTimestamp(BooleanBuilder query, LookQuery lookQuery) {
    if (lookQuery.getLookTimestamp() != null) {
      if (!lookQuery.getLookTimestamp().exist()) {
        query.and(lookEntity.lookTimestamp.isNull());
      } else {
        query.and(lookEntity.lookTimestamp.eq(getQueryValue(lookQuery.getLookTimestamp())));
      }
    } else if (lookQuery.getLookTimestampMin() != null || lookQuery.getLookTimestampMax() != null) {
      if (lookQuery.getLookTimestampMin() != null && lookQuery.getLookTimestampMin().exist()) {
        query.and(lookEntity.lookTimestamp.goe(lookQuery.getLookTimestampMin().get()));
      }
      if (lookQuery.getLookTimestampMax() != null && lookQuery.getLookTimestampMax().exist()) {
        query.and(lookEntity.lookTimestamp.loe(lookQuery.getLookTimestampMax().get()));
      }
    }
  }

  public static OrderSpecifier<?>[] orderBy(List<SortRequest<LookPropertyName>> sorts) {
    List<OrderSpecifier<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForLookOrg(orders, sort);
      orderByForLookId(orders, sort);
      orderByForLookChar(orders, sort);
      orderByForLookVarchar(orders, sort);
      orderByForLookText(orders, sort);
      orderByForLookSmallint(orders, sort);
      orderByForLookInteger(orders, sort);
      orderByForLookBigint(orders, sort);
      orderByForLookReal(orders, sort);
      orderByForLookDouble(orders, sort);
      orderByForLookDecimal(orders, sort);
      orderByForLookBoolean(orders, sort);
      orderByForLookDate(orders, sort);
      orderByForLookTimestamp(orders, sort);
    });
    return orders.stream().toArray(size -> new OrderSpecifier[size]);
  }

  private static void orderByForLookOrg(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ORG) {
      orders.add(sort.isAscendant() ? lookEntity.id.lookOrg.asc() : lookEntity.id.lookOrg.desc());
    }
  }

  private static void orderByForLookId(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ID) {
      orders.add(sort.isAscendant() ? lookEntity.id.lookId.asc() : lookEntity.id.lookId.desc());
    }
  }

  private static void orderByForLookChar(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_CHAR) {
      orders.add(sort.isAscendant() ? lookEntity.lookChar.asc() : lookEntity.lookChar.desc());
    }
  }

  private static void orderByForLookVarchar(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_VARCHAR) {
      orders.add(sort.isAscendant() ? lookEntity.lookVarchar.asc() : lookEntity.lookVarchar.desc());
    }
  }

  private static void orderByForLookText(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TEXT) {
      orders.add(sort.isAscendant() ? lookEntity.lookText.asc() : lookEntity.lookText.desc());
    }
  }

  private static void orderByForLookSmallint(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_SMALLINT) {
      orders.add(sort.isAscendant() ? lookEntity.lookSmallint.asc() : lookEntity.lookSmallint.desc());
    }
  }

  private static void orderByForLookInteger(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_INTEGER) {
      orders.add(sort.isAscendant() ? lookEntity.lookInteger.asc() : lookEntity.lookInteger.desc());
    }
  }

  private static void orderByForLookBigint(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BIGINT) {
      orders.add(sort.isAscendant() ? lookEntity.lookBigint.asc() : lookEntity.lookBigint.desc());
    }
  }

  private static void orderByForLookReal(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_REAL) {
      orders.add(sort.isAscendant() ? lookEntity.lookReal.asc() : lookEntity.lookReal.desc());
    }
  }

  private static void orderByForLookDouble(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DOUBLE) {
      orders.add(sort.isAscendant() ? lookEntity.lookDouble.asc() : lookEntity.lookDouble.desc());
    }
  }

  private static void orderByForLookDecimal(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DECIMAL) {
      orders.add(sort.isAscendant() ? lookEntity.lookDecimal.asc() : lookEntity.lookDecimal.desc());
    }
  }

  private static void orderByForLookBoolean(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BOOLEAN) {
      orders.add(sort.isAscendant() ? lookEntity.lookBoolean.asc() : lookEntity.lookBoolean.desc());
    }
  }

  private static void orderByForLookDate(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DATE) {
      orders.add(sort.isAscendant() ? lookEntity.lookDate.asc() : lookEntity.lookDate.desc());
    }
  }

  private static void orderByForLookTimestamp(List<OrderSpecifier<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TIMESTAMP) {
      orders.add(sort.isAscendant() ? lookEntity.lookTimestamp.asc() : lookEntity.lookTimestamp.desc());
    }
  }

  public static JPAUpdateClause updateValues(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    updateValuesForLookChar(jpaUpdate, lookUpdate);
    updateValuesForLookVarchar(jpaUpdate, lookUpdate);
    updateValuesForLookText(jpaUpdate, lookUpdate);
    updateValuesForLookSmallint(jpaUpdate, lookUpdate);
    updateValuesForLookInteger(jpaUpdate, lookUpdate);
    updateValuesForLookBigint(jpaUpdate, lookUpdate);
    updateValuesForLookReal(jpaUpdate, lookUpdate);
    updateValuesForLookDouble(jpaUpdate, lookUpdate);
    updateValuesForLookDecimal(jpaUpdate, lookUpdate);
    updateValuesForLookBoolean(jpaUpdate, lookUpdate);
    updateValuesForLookDate(jpaUpdate, lookUpdate);
    updateValuesForLookTimestamp(jpaUpdate, lookUpdate);
    return jpaUpdate;
  }

  private static void updateValuesForLookChar(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookChar() != null) {
      jpaUpdate.set(lookEntity.lookChar, getUpdateValue(lookUpdate.getLookChar()));
    }
  }

  private static void updateValuesForLookVarchar(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookVarchar() != null) {
      jpaUpdate.set(lookEntity.lookVarchar, getUpdateValue(lookUpdate.getLookVarchar()));
    }
  }

  private static void updateValuesForLookText(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookText() != null) {
      jpaUpdate.set(lookEntity.lookText, getUpdateValue(lookUpdate.getLookText()));
    }
  }

  private static void updateValuesForLookSmallint(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookSmallint() != null) {
      jpaUpdate.set(lookEntity.lookSmallint, getUpdateValue(lookUpdate.getLookSmallint()));
    }
  }

  private static void updateValuesForLookInteger(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookInteger() != null) {
      jpaUpdate.set(lookEntity.lookInteger, getUpdateValue(lookUpdate.getLookInteger()));
    }
  }

  private static void updateValuesForLookBigint(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBigint() != null) {
      jpaUpdate.set(lookEntity.lookBigint, getUpdateValue(lookUpdate.getLookBigint()));
    }
  }

  private static void updateValuesForLookReal(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookReal() != null) {
      jpaUpdate.set(lookEntity.lookReal, getUpdateValue(lookUpdate.getLookReal()));
    }
  }

  private static void updateValuesForLookDouble(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDouble() != null) {
      jpaUpdate.set(lookEntity.lookDouble, getUpdateValue(lookUpdate.getLookDouble()));
    }
  }

  private static void updateValuesForLookDecimal(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDecimal() != null) {
      jpaUpdate.set(lookEntity.lookDecimal, getUpdateValue(lookUpdate.getLookDecimal()));
    }
  }

  private static void updateValuesForLookBoolean(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBoolean() != null) {
      jpaUpdate.set(lookEntity.lookBoolean, getUpdateValue(lookUpdate.getLookBoolean()));
    }
  }

  private static void updateValuesForLookDate(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDate() != null) {
      jpaUpdate.set(lookEntity.lookDate, getUpdateValue(lookUpdate.getLookDate()));
    }
  }

  private static void updateValuesForLookTimestamp(JPAUpdateClause jpaUpdate, LookUpdate lookUpdate) {
    if (lookUpdate.getLookTimestamp() != null) {
      jpaUpdate.set(lookEntity.lookTimestamp, getUpdateValue(lookUpdate.getLookTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private LookDslHelper() {
  }
}
