package com.escalableapps.cmdb.baseapp.template.dsl.helper;

import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QZooEntity.zooEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAUpdateClause;

public final class ZooDslHelper {

  public static BooleanBuilder whereCondition( //
      Long zooOrg, //
      Integer zooId //
  ) {
    ZooQuery zooQuery = new ZooQuery();
    zooQuery.setZooOrg(zooOrg);
    zooQuery.setZooId(zooId);
    return whereCondition(zooQuery);
  }

  public static BooleanBuilder whereCondition(ZooQuery zooQuery) {
    BooleanBuilder query = new BooleanBuilder();
    whereConditionForZooOrg(query, zooQuery);
    whereConditionForZooId(query, zooQuery);
    whereConditionForZooChar(query, zooQuery);
    whereConditionForZooVarchar(query, zooQuery);
    whereConditionForZooText(query, zooQuery);
    whereConditionForZooSmallint(query, zooQuery);
    whereConditionForZooInteger(query, zooQuery);
    whereConditionForZooBigint(query, zooQuery);
    whereConditionForZooReal(query, zooQuery);
    whereConditionForZooDouble(query, zooQuery);
    whereConditionForZooDecimal(query, zooQuery);
    whereConditionForZooBoolean(query, zooQuery);
    whereConditionForZooDate(query, zooQuery);
    whereConditionForZooTimestamp(query, zooQuery);
    return query;
  }

  private static void whereConditionForZooOrg(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooOrg() != null) {
      if (!zooQuery.getZooOrg().exist()) {
        query.and(zooEntity.id.zooOrg.isNull());
      } else {
        query.and(zooEntity.id.zooOrg.eq(getQueryValue(zooQuery.getZooOrg())));
      }
    }
  }

  private static void whereConditionForZooId(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooId() != null) {
      if (!zooQuery.getZooId().exist()) {
        query.and(zooEntity.id.zooId.isNull());
      } else {
        query.and(zooEntity.id.zooId.eq(getQueryValue(zooQuery.getZooId())));
      }
    }
  }

  private static void whereConditionForZooChar(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooChar() != null) {
      if (!zooQuery.getZooChar().exist()) {
        query.and(zooEntity.zooChar.isNull());
      } else {
        query.and(zooEntity.zooChar.eq(getQueryValue(zooQuery.getZooChar())));
      }
    }
  }

  private static void whereConditionForZooVarchar(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooVarchar() != null) {
      if (!zooQuery.getZooVarchar().exist()) {
        query.and(zooEntity.zooVarchar.isNull());
      } else {
        query.and(zooEntity.zooVarchar.eq(getQueryValue(zooQuery.getZooVarchar())));
      }
    }
  }

  private static void whereConditionForZooText(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooText() != null) {
      if (!zooQuery.getZooText().exist()) {
        query.and(zooEntity.zooText.isNull());
      } else {
        query.and(zooEntity.zooText.eq(getQueryValue(zooQuery.getZooText())));
      }
    }
  }

  private static void whereConditionForZooSmallint(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooSmallint() != null) {
      if (!zooQuery.getZooSmallint().exist()) {
        query.and(zooEntity.zooSmallint.isNull());
      } else {
        query.and(zooEntity.zooSmallint.eq(getQueryValue(zooQuery.getZooSmallint())));
      }
    }
  }

  private static void whereConditionForZooInteger(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooInteger() != null) {
      if (!zooQuery.getZooInteger().exist()) {
        query.and(zooEntity.zooInteger.isNull());
      } else {
        query.and(zooEntity.zooInteger.eq(getQueryValue(zooQuery.getZooInteger())));
      }
    }
  }

  private static void whereConditionForZooBigint(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooBigint() != null) {
      if (!zooQuery.getZooBigint().exist()) {
        query.and(zooEntity.zooBigint.isNull());
      } else {
        query.and(zooEntity.zooBigint.eq(getQueryValue(zooQuery.getZooBigint())));
      }
    }
  }

  private static void whereConditionForZooReal(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooReal() != null) {
      if (!zooQuery.getZooReal().exist()) {
        query.and(zooEntity.zooReal.isNull());
      } else {
        query.and(zooEntity.zooReal.eq(getQueryValue(zooQuery.getZooReal())));
      }
    }
  }

  private static void whereConditionForZooDouble(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooDouble() != null) {
      if (!zooQuery.getZooDouble().exist()) {
        query.and(zooEntity.zooDouble.isNull());
      } else {
        query.and(zooEntity.zooDouble.eq(getQueryValue(zooQuery.getZooDouble())));
      }
    }
  }

  private static void whereConditionForZooDecimal(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooDecimal() != null) {
      if (!zooQuery.getZooDecimal().exist()) {
        query.and(zooEntity.zooDecimal.isNull());
      } else {
        query.and(zooEntity.zooDecimal.eq(getQueryValue(zooQuery.getZooDecimal())));
      }
    }
  }

  private static void whereConditionForZooBoolean(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooBoolean() != null) {
      if (!zooQuery.getZooBoolean().exist()) {
        query.and(zooEntity.zooBoolean.isNull());
      } else {
        query.and(zooEntity.zooBoolean.eq(getQueryValue(zooQuery.getZooBoolean())));
      }
    }
  }

  private static void whereConditionForZooDate(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooDate() != null) {
      if (!zooQuery.getZooDate().exist()) {
        query.and(zooEntity.zooDate.isNull());
      } else {
        query.and(zooEntity.zooDate.eq(getQueryValue(zooQuery.getZooDate())));
      }
    } else if (zooQuery.getZooDateMin() != null || zooQuery.getZooDateMax() != null) {
      if (zooQuery.getZooDateMin() != null && zooQuery.getZooDateMin().exist()) {
        query.and(zooEntity.zooDate.goe(zooQuery.getZooDateMin().get()));
      }
      if (zooQuery.getZooDateMax() != null && zooQuery.getZooDateMax().exist()) {
        query.and(zooEntity.zooDate.loe(zooQuery.getZooDateMax().get()));
      }
    }
  }

  private static void whereConditionForZooTimestamp(BooleanBuilder query, ZooQuery zooQuery) {
    if (zooQuery.getZooTimestamp() != null) {
      if (!zooQuery.getZooTimestamp().exist()) {
        query.and(zooEntity.zooTimestamp.isNull());
      } else {
        query.and(zooEntity.zooTimestamp.eq(getQueryValue(zooQuery.getZooTimestamp())));
      }
    } else if (zooQuery.getZooTimestampMin() != null || zooQuery.getZooTimestampMax() != null) {
      if (zooQuery.getZooTimestampMin() != null && zooQuery.getZooTimestampMin().exist()) {
        query.and(zooEntity.zooTimestamp.goe(zooQuery.getZooTimestampMin().get()));
      }
      if (zooQuery.getZooTimestampMax() != null && zooQuery.getZooTimestampMax().exist()) {
        query.and(zooEntity.zooTimestamp.loe(zooQuery.getZooTimestampMax().get()));
      }
    }
  }

  public static OrderSpecifier<?>[] orderBy(List<SortRequest<ZooPropertyName>> sorts) {
    List<OrderSpecifier<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForZooOrg(orders, sort);
      orderByForZooId(orders, sort);
      orderByForZooChar(orders, sort);
      orderByForZooVarchar(orders, sort);
      orderByForZooText(orders, sort);
      orderByForZooSmallint(orders, sort);
      orderByForZooInteger(orders, sort);
      orderByForZooBigint(orders, sort);
      orderByForZooReal(orders, sort);
      orderByForZooDouble(orders, sort);
      orderByForZooDecimal(orders, sort);
      orderByForZooBoolean(orders, sort);
      orderByForZooDate(orders, sort);
      orderByForZooTimestamp(orders, sort);
    });
    return orders.stream().toArray(size -> new OrderSpecifier[size]);
  }

  private static void orderByForZooOrg(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ORG) {
      orders.add(sort.isAscendant() ? zooEntity.id.zooOrg.asc() : zooEntity.id.zooOrg.desc());
    }
  }

  private static void orderByForZooId(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ID) {
      orders.add(sort.isAscendant() ? zooEntity.id.zooId.asc() : zooEntity.id.zooId.desc());
    }
  }

  private static void orderByForZooChar(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_CHAR) {
      orders.add(sort.isAscendant() ? zooEntity.zooChar.asc() : zooEntity.zooChar.desc());
    }
  }

  private static void orderByForZooVarchar(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_VARCHAR) {
      orders.add(sort.isAscendant() ? zooEntity.zooVarchar.asc() : zooEntity.zooVarchar.desc());
    }
  }

  private static void orderByForZooText(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TEXT) {
      orders.add(sort.isAscendant() ? zooEntity.zooText.asc() : zooEntity.zooText.desc());
    }
  }

  private static void orderByForZooSmallint(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_SMALLINT) {
      orders.add(sort.isAscendant() ? zooEntity.zooSmallint.asc() : zooEntity.zooSmallint.desc());
    }
  }

  private static void orderByForZooInteger(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_INTEGER) {
      orders.add(sort.isAscendant() ? zooEntity.zooInteger.asc() : zooEntity.zooInteger.desc());
    }
  }

  private static void orderByForZooBigint(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BIGINT) {
      orders.add(sort.isAscendant() ? zooEntity.zooBigint.asc() : zooEntity.zooBigint.desc());
    }
  }

  private static void orderByForZooReal(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_REAL) {
      orders.add(sort.isAscendant() ? zooEntity.zooReal.asc() : zooEntity.zooReal.desc());
    }
  }

  private static void orderByForZooDouble(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DOUBLE) {
      orders.add(sort.isAscendant() ? zooEntity.zooDouble.asc() : zooEntity.zooDouble.desc());
    }
  }

  private static void orderByForZooDecimal(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DECIMAL) {
      orders.add(sort.isAscendant() ? zooEntity.zooDecimal.asc() : zooEntity.zooDecimal.desc());
    }
  }

  private static void orderByForZooBoolean(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BOOLEAN) {
      orders.add(sort.isAscendant() ? zooEntity.zooBoolean.asc() : zooEntity.zooBoolean.desc());
    }
  }

  private static void orderByForZooDate(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DATE) {
      orders.add(sort.isAscendant() ? zooEntity.zooDate.asc() : zooEntity.zooDate.desc());
    }
  }

  private static void orderByForZooTimestamp(List<OrderSpecifier<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TIMESTAMP) {
      orders.add(sort.isAscendant() ? zooEntity.zooTimestamp.asc() : zooEntity.zooTimestamp.desc());
    }
  }

  public static JPAUpdateClause updateValues(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    updateValuesForZooChar(jpaUpdate, zooUpdate);
    updateValuesForZooVarchar(jpaUpdate, zooUpdate);
    updateValuesForZooText(jpaUpdate, zooUpdate);
    updateValuesForZooSmallint(jpaUpdate, zooUpdate);
    updateValuesForZooInteger(jpaUpdate, zooUpdate);
    updateValuesForZooBigint(jpaUpdate, zooUpdate);
    updateValuesForZooReal(jpaUpdate, zooUpdate);
    updateValuesForZooDouble(jpaUpdate, zooUpdate);
    updateValuesForZooDecimal(jpaUpdate, zooUpdate);
    updateValuesForZooBoolean(jpaUpdate, zooUpdate);
    updateValuesForZooDate(jpaUpdate, zooUpdate);
    updateValuesForZooTimestamp(jpaUpdate, zooUpdate);
    return jpaUpdate;
  }

  private static void updateValuesForZooChar(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooChar() != null) {
      jpaUpdate.set(zooEntity.zooChar, getUpdateValue(zooUpdate.getZooChar()));
    }
  }

  private static void updateValuesForZooVarchar(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooVarchar() != null) {
      jpaUpdate.set(zooEntity.zooVarchar, getUpdateValue(zooUpdate.getZooVarchar()));
    }
  }

  private static void updateValuesForZooText(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooText() != null) {
      jpaUpdate.set(zooEntity.zooText, getUpdateValue(zooUpdate.getZooText()));
    }
  }

  private static void updateValuesForZooSmallint(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooSmallint() != null) {
      jpaUpdate.set(zooEntity.zooSmallint, getUpdateValue(zooUpdate.getZooSmallint()));
    }
  }

  private static void updateValuesForZooInteger(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooInteger() != null) {
      jpaUpdate.set(zooEntity.zooInteger, getUpdateValue(zooUpdate.getZooInteger()));
    }
  }

  private static void updateValuesForZooBigint(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBigint() != null) {
      jpaUpdate.set(zooEntity.zooBigint, getUpdateValue(zooUpdate.getZooBigint()));
    }
  }

  private static void updateValuesForZooReal(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooReal() != null) {
      jpaUpdate.set(zooEntity.zooReal, getUpdateValue(zooUpdate.getZooReal()));
    }
  }

  private static void updateValuesForZooDouble(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDouble() != null) {
      jpaUpdate.set(zooEntity.zooDouble, getUpdateValue(zooUpdate.getZooDouble()));
    }
  }

  private static void updateValuesForZooDecimal(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDecimal() != null) {
      jpaUpdate.set(zooEntity.zooDecimal, getUpdateValue(zooUpdate.getZooDecimal()));
    }
  }

  private static void updateValuesForZooBoolean(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBoolean() != null) {
      jpaUpdate.set(zooEntity.zooBoolean, getUpdateValue(zooUpdate.getZooBoolean()));
    }
  }

  private static void updateValuesForZooDate(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDate() != null) {
      jpaUpdate.set(zooEntity.zooDate, getUpdateValue(zooUpdate.getZooDate()));
    }
  }

  private static void updateValuesForZooTimestamp(JPAUpdateClause jpaUpdate, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooTimestamp() != null) {
      jpaUpdate.set(zooEntity.zooTimestamp, getUpdateValue(zooUpdate.getZooTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private ZooDslHelper() {
  }
}
