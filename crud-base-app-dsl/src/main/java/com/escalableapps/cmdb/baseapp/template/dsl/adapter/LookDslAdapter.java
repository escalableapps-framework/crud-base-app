package com.escalableapps.cmdb.baseapp.template.dsl.adapter;

import static com.escalableapps.cmdb.baseapp.template.dsl.helper.LookDslHelper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.mapper.LookEntityMapper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QLookEntity.lookEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.dsl.mapper.LookEntityMapper;
import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;
import com.escalableapps.cmdb.baseapp.template.dsl.repository.LookDslRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.LookSpi;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class LookDslAdapter implements LookSpi {

  private final LookDslRepository repository;
  private final JPAQueryFactory queryFactory;

  public LookDslAdapter(LookDslRepository repository, JPAQueryFactory queryFactory) {
    this.repository = repository;
    this.queryFactory = queryFactory;
  }

  @Override
  public Look createLook( //
      Look look //
  ) {
    log.debug("createLook:look={}", look);

    Long lookId = repository.nextVal();
    LookEntity lookEntity = repository.save(fromEntity(look, lookId));

    look.setLookId(lookEntity.getId().getLookId());
    log.debug("createLook:look={}", look);
    return look;
  }

  @Override
  public Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);

    LookEntity look = repository.findById(new LookPk(lookOrg, lookId)).orElse(null);
    log.debug("findLook:look={}", look);

    return Optional.ofNullable(toEntity(look));
  }

  @Override
  public List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  ) {
    log.debug("findLooks:lookOrg={}", lookOrg);
    log.debug("findLooks:lookIds={}", lookIds);

    List<LookEntity> looks = repository.findAllById(lookIds.stream().map(lookId -> new LookPk(lookOrg, lookId)).collect(Collectors.toList()));
    log.debug("findLooks:looks={}", looks);

    return looks.stream().map(LookEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery //
  ) {
    return findLooks(lookQuery, new PageRequest<>());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  ) {
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:paging={}", paging);

    List<LookEntity> looks = queryFactory.selectFrom(lookEntity) //
        .where(whereCondition(lookQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findLooks:looks={}", looks);

    return looks.stream().map(LookEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);

    boolean exists = repository.existsById(new LookPk(lookOrg, lookId));

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("existsLooks:lookQuery={}", lookQuery);

    boolean exists = repository.exists(whereCondition(lookQuery));

    log.debug("existsLooks:exists={}", exists);
    return exists;
  }

  @Override
  public long countLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("countLooks:lookQuery={}", lookQuery);

    long count = repository.count(whereCondition(lookQuery));

    log.debug("countLooks:count={}", count);
    return count;
  }

  @Override
  public long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  ) {
    log.debug("countLooks:lookOrg={}", lookOrg);
    log.debug("countLooks:lookId={}", lookId);
    log.debug("countLooks:lookUpdate={}", lookUpdate);

    JPAUpdateClause jpaUpdate = queryFactory.update(lookEntity);
    updateValues(jpaUpdate, lookUpdate);
    jpaUpdate.where(whereCondition(lookOrg, lookId));
    long updateCount = jpaUpdate.execute();

    log.debug("countLooks:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("countLooks:lookOrg={}", lookOrg);
    log.debug("countLooks:lookId={}", lookId);

    long deleteCount = queryFactory.delete(lookEntity) //
        .where(whereCondition(lookOrg, lookId)) //
        .execute();

    log.debug("countLooks:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
