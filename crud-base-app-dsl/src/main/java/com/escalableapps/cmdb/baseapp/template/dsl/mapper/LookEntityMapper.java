package com.escalableapps.cmdb.baseapp.template.dsl.mapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;

public final class LookEntityMapper {

  public static LookEntity fromEntity(Look look, Long lookId) {
    if (look == null) {
      return null;
    }
    LookPk id = new LookPk();
    id.setLookOrg(look.getLookOrg());
    id.setLookId(lookId);
    LookEntity lookEntity = new LookEntity();
    lookEntity.setId(id);
    lookEntity.setLookChar(look.getLookChar());
    lookEntity.setLookVarchar(look.getLookVarchar());
    lookEntity.setLookText(look.getLookText());
    lookEntity.setLookSmallint(look.getLookSmallint());
    lookEntity.setLookInteger(look.getLookInteger());
    lookEntity.setLookBigint(look.getLookBigint());
    lookEntity.setLookReal(look.getLookReal());
    lookEntity.setLookDouble(look.getLookDouble());
    lookEntity.setLookDecimal(look.getLookDecimal());
    lookEntity.setLookBoolean(look.getLookBoolean());
    lookEntity.setLookDate(look.getLookDate());
    lookEntity.setLookTimestamp(look.getLookTimestamp());
    return lookEntity;
  }

  public static Look toEntity(LookEntity lookEntity) {
    if (lookEntity == null) {
      return null;
    }
    Look look = new Look();
    look.setLookOrg(lookEntity.getId().getLookOrg());
    look.setLookId(lookEntity.getId().getLookId());
    look.setLookChar(lookEntity.getLookChar());
    look.setLookVarchar(lookEntity.getLookVarchar());
    look.setLookText(lookEntity.getLookText());
    look.setLookSmallint(lookEntity.getLookSmallint());
    look.setLookInteger(lookEntity.getLookInteger());
    look.setLookBigint(lookEntity.getLookBigint());
    look.setLookReal(lookEntity.getLookReal());
    look.setLookDouble(lookEntity.getLookDouble());
    look.setLookDecimal(lookEntity.getLookDecimal());
    look.setLookBoolean(lookEntity.getLookBoolean());
    look.setLookDate(lookEntity.getLookDate());
    look.setLookTimestamp(lookEntity.getLookTimestamp());
    return look;
  }

  private LookEntityMapper() {
  }
}
