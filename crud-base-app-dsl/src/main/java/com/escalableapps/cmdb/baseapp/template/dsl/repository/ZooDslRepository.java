package com.escalableapps.cmdb.baseapp.template.dsl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;

public interface ZooDslRepository extends JpaRepository<ZooEntity, ZooPk>, QuerydslPredicateExecutor<ZooEntity> {

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.zoo_zoo_id_seq')")
  Integer nextVal();
}
