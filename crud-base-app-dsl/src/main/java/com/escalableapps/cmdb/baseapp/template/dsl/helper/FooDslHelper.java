package com.escalableapps.cmdb.baseapp.template.dsl.helper;

import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QFooEntity.fooEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAUpdateClause;

public final class FooDslHelper {

  public static BooleanBuilder whereCondition( //
      Integer fooId //
  ) {
    FooQuery fooQuery = new FooQuery();
    fooQuery.setFooId(fooId);
    return whereCondition(fooQuery);
  }

  public static BooleanBuilder whereCondition(FooQuery fooQuery) {
    BooleanBuilder query = new BooleanBuilder();
    whereConditionForFooId(query, fooQuery);
    whereConditionForFooChar(query, fooQuery);
    whereConditionForFooVarchar(query, fooQuery);
    whereConditionForFooText(query, fooQuery);
    whereConditionForFooSmallint(query, fooQuery);
    whereConditionForFooInteger(query, fooQuery);
    whereConditionForFooBigint(query, fooQuery);
    whereConditionForFooReal(query, fooQuery);
    whereConditionForFooDouble(query, fooQuery);
    whereConditionForFooDecimal(query, fooQuery);
    whereConditionForFooBoolean(query, fooQuery);
    whereConditionForFooDate(query, fooQuery);
    whereConditionForFooTimestamp(query, fooQuery);
    return query;
  }

  private static void whereConditionForFooId(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooId() != null) {
      if (!fooQuery.getFooId().exist()) {
        query.and(fooEntity.fooId.isNull());
      } else {
        query.and(fooEntity.fooId.eq(getQueryValue(fooQuery.getFooId())));
      }
    }
  }

  private static void whereConditionForFooChar(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooChar() != null) {
      if (!fooQuery.getFooChar().exist()) {
        query.and(fooEntity.fooChar.isNull());
      } else {
        query.and(fooEntity.fooChar.eq(getQueryValue(fooQuery.getFooChar())));
      }
    }
  }

  private static void whereConditionForFooVarchar(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooVarchar() != null) {
      if (!fooQuery.getFooVarchar().exist()) {
        query.and(fooEntity.fooVarchar.isNull());
      } else {
        query.and(fooEntity.fooVarchar.eq(getQueryValue(fooQuery.getFooVarchar())));
      }
    }
  }

  private static void whereConditionForFooText(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooText() != null) {
      if (!fooQuery.getFooText().exist()) {
        query.and(fooEntity.fooText.isNull());
      } else {
        query.and(fooEntity.fooText.eq(getQueryValue(fooQuery.getFooText())));
      }
    }
  }

  private static void whereConditionForFooSmallint(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooSmallint() != null) {
      if (!fooQuery.getFooSmallint().exist()) {
        query.and(fooEntity.fooSmallint.isNull());
      } else {
        query.and(fooEntity.fooSmallint.eq(getQueryValue(fooQuery.getFooSmallint())));
      }
    }
  }

  private static void whereConditionForFooInteger(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooInteger() != null) {
      if (!fooQuery.getFooInteger().exist()) {
        query.and(fooEntity.fooInteger.isNull());
      } else {
        query.and(fooEntity.fooInteger.eq(getQueryValue(fooQuery.getFooInteger())));
      }
    }
  }

  private static void whereConditionForFooBigint(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooBigint() != null) {
      if (!fooQuery.getFooBigint().exist()) {
        query.and(fooEntity.fooBigint.isNull());
      } else {
        query.and(fooEntity.fooBigint.eq(getQueryValue(fooQuery.getFooBigint())));
      }
    }
  }

  private static void whereConditionForFooReal(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooReal() != null) {
      if (!fooQuery.getFooReal().exist()) {
        query.and(fooEntity.fooReal.isNull());
      } else {
        query.and(fooEntity.fooReal.eq(getQueryValue(fooQuery.getFooReal())));
      }
    }
  }

  private static void whereConditionForFooDouble(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooDouble() != null) {
      if (!fooQuery.getFooDouble().exist()) {
        query.and(fooEntity.fooDouble.isNull());
      } else {
        query.and(fooEntity.fooDouble.eq(getQueryValue(fooQuery.getFooDouble())));
      }
    }
  }

  private static void whereConditionForFooDecimal(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooDecimal() != null) {
      if (!fooQuery.getFooDecimal().exist()) {
        query.and(fooEntity.fooDecimal.isNull());
      } else {
        query.and(fooEntity.fooDecimal.eq(getQueryValue(fooQuery.getFooDecimal())));
      }
    }
  }

  private static void whereConditionForFooBoolean(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooBoolean() != null) {
      if (!fooQuery.getFooBoolean().exist()) {
        query.and(fooEntity.fooBoolean.isNull());
      } else {
        query.and(fooEntity.fooBoolean.eq(getQueryValue(fooQuery.getFooBoolean())));
      }
    }
  }

  private static void whereConditionForFooDate(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooDate() != null) {
      if (!fooQuery.getFooDate().exist()) {
        query.and(fooEntity.fooDate.isNull());
      } else {
        query.and(fooEntity.fooDate.eq(getQueryValue(fooQuery.getFooDate())));
      }
    } else if (fooQuery.getFooDateMin() != null || fooQuery.getFooDateMax() != null) {
      if (fooQuery.getFooDateMin() != null && fooQuery.getFooDateMin().exist()) {
        query.and(fooEntity.fooDate.goe(fooQuery.getFooDateMin().get()));
      }
      if (fooQuery.getFooDateMax() != null && fooQuery.getFooDateMax().exist()) {
        query.and(fooEntity.fooDate.loe(fooQuery.getFooDateMax().get()));
      }
    }
  }

  private static void whereConditionForFooTimestamp(BooleanBuilder query, FooQuery fooQuery) {
    if (fooQuery.getFooTimestamp() != null) {
      if (!fooQuery.getFooTimestamp().exist()) {
        query.and(fooEntity.fooTimestamp.isNull());
      } else {
        query.and(fooEntity.fooTimestamp.eq(getQueryValue(fooQuery.getFooTimestamp())));
      }
    } else if (fooQuery.getFooTimestampMin() != null || fooQuery.getFooTimestampMax() != null) {
      if (fooQuery.getFooTimestampMin() != null && fooQuery.getFooTimestampMin().exist()) {
        query.and(fooEntity.fooTimestamp.goe(fooQuery.getFooTimestampMin().get()));
      }
      if (fooQuery.getFooTimestampMax() != null && fooQuery.getFooTimestampMax().exist()) {
        query.and(fooEntity.fooTimestamp.loe(fooQuery.getFooTimestampMax().get()));
      }
    }
  }

  public static OrderSpecifier<?>[] orderBy(List<SortRequest<FooPropertyName>> sorts) {
    List<OrderSpecifier<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForFooId(orders, sort);
      orderByForFooChar(orders, sort);
      orderByForFooVarchar(orders, sort);
      orderByForFooText(orders, sort);
      orderByForFooSmallint(orders, sort);
      orderByForFooInteger(orders, sort);
      orderByForFooBigint(orders, sort);
      orderByForFooReal(orders, sort);
      orderByForFooDouble(orders, sort);
      orderByForFooDecimal(orders, sort);
      orderByForFooBoolean(orders, sort);
      orderByForFooDate(orders, sort);
      orderByForFooTimestamp(orders, sort);
    });
    return orders.stream().toArray(size -> new OrderSpecifier[size]);
  }

  private static void orderByForFooId(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_ID) {
      orders.add(sort.isAscendant() ? fooEntity.fooId.asc() : fooEntity.fooId.desc());
    }
  }

  private static void orderByForFooChar(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_CHAR) {
      orders.add(sort.isAscendant() ? fooEntity.fooChar.asc() : fooEntity.fooChar.desc());
    }
  }

  private static void orderByForFooVarchar(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_VARCHAR) {
      orders.add(sort.isAscendant() ? fooEntity.fooVarchar.asc() : fooEntity.fooVarchar.desc());
    }
  }

  private static void orderByForFooText(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TEXT) {
      orders.add(sort.isAscendant() ? fooEntity.fooText.asc() : fooEntity.fooText.desc());
    }
  }

  private static void orderByForFooSmallint(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_SMALLINT) {
      orders.add(sort.isAscendant() ? fooEntity.fooSmallint.asc() : fooEntity.fooSmallint.desc());
    }
  }

  private static void orderByForFooInteger(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_INTEGER) {
      orders.add(sort.isAscendant() ? fooEntity.fooInteger.asc() : fooEntity.fooInteger.desc());
    }
  }

  private static void orderByForFooBigint(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BIGINT) {
      orders.add(sort.isAscendant() ? fooEntity.fooBigint.asc() : fooEntity.fooBigint.desc());
    }
  }

  private static void orderByForFooReal(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_REAL) {
      orders.add(sort.isAscendant() ? fooEntity.fooReal.asc() : fooEntity.fooReal.desc());
    }
  }

  private static void orderByForFooDouble(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DOUBLE) {
      orders.add(sort.isAscendant() ? fooEntity.fooDouble.asc() : fooEntity.fooDouble.desc());
    }
  }

  private static void orderByForFooDecimal(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DECIMAL) {
      orders.add(sort.isAscendant() ? fooEntity.fooDecimal.asc() : fooEntity.fooDecimal.desc());
    }
  }

  private static void orderByForFooBoolean(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BOOLEAN) {
      orders.add(sort.isAscendant() ? fooEntity.fooBoolean.asc() : fooEntity.fooBoolean.desc());
    }
  }

  private static void orderByForFooDate(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DATE) {
      orders.add(sort.isAscendant() ? fooEntity.fooDate.asc() : fooEntity.fooDate.desc());
    }
  }

  private static void orderByForFooTimestamp(List<OrderSpecifier<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TIMESTAMP) {
      orders.add(sort.isAscendant() ? fooEntity.fooTimestamp.asc() : fooEntity.fooTimestamp.desc());
    }
  }

  public static JPAUpdateClause updateValues(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    updateValuesForFooChar(jpaUpdate, fooUpdate);
    updateValuesForFooVarchar(jpaUpdate, fooUpdate);
    updateValuesForFooText(jpaUpdate, fooUpdate);
    updateValuesForFooSmallint(jpaUpdate, fooUpdate);
    updateValuesForFooInteger(jpaUpdate, fooUpdate);
    updateValuesForFooBigint(jpaUpdate, fooUpdate);
    updateValuesForFooReal(jpaUpdate, fooUpdate);
    updateValuesForFooDouble(jpaUpdate, fooUpdate);
    updateValuesForFooDecimal(jpaUpdate, fooUpdate);
    updateValuesForFooBoolean(jpaUpdate, fooUpdate);
    updateValuesForFooDate(jpaUpdate, fooUpdate);
    updateValuesForFooTimestamp(jpaUpdate, fooUpdate);
    return jpaUpdate;
  }

  private static void updateValuesForFooChar(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooChar() != null) {
      jpaUpdate.set(fooEntity.fooChar, getUpdateValue(fooUpdate.getFooChar()));
    }
  }

  private static void updateValuesForFooVarchar(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooVarchar() != null) {
      jpaUpdate.set(fooEntity.fooVarchar, getUpdateValue(fooUpdate.getFooVarchar()));
    }
  }

  private static void updateValuesForFooText(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooText() != null) {
      jpaUpdate.set(fooEntity.fooText, getUpdateValue(fooUpdate.getFooText()));
    }
  }

  private static void updateValuesForFooSmallint(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooSmallint() != null) {
      jpaUpdate.set(fooEntity.fooSmallint, getUpdateValue(fooUpdate.getFooSmallint()));
    }
  }

  private static void updateValuesForFooInteger(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooInteger() != null) {
      jpaUpdate.set(fooEntity.fooInteger, getUpdateValue(fooUpdate.getFooInteger()));
    }
  }

  private static void updateValuesForFooBigint(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBigint() != null) {
      jpaUpdate.set(fooEntity.fooBigint, getUpdateValue(fooUpdate.getFooBigint()));
    }
  }

  private static void updateValuesForFooReal(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooReal() != null) {
      jpaUpdate.set(fooEntity.fooReal, getUpdateValue(fooUpdate.getFooReal()));
    }
  }

  private static void updateValuesForFooDouble(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDouble() != null) {
      jpaUpdate.set(fooEntity.fooDouble, getUpdateValue(fooUpdate.getFooDouble()));
    }
  }

  private static void updateValuesForFooDecimal(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDecimal() != null) {
      jpaUpdate.set(fooEntity.fooDecimal, getUpdateValue(fooUpdate.getFooDecimal()));
    }
  }

  private static void updateValuesForFooBoolean(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBoolean() != null) {
      jpaUpdate.set(fooEntity.fooBoolean, getUpdateValue(fooUpdate.getFooBoolean()));
    }
  }

  private static void updateValuesForFooDate(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDate() != null) {
      jpaUpdate.set(fooEntity.fooDate, getUpdateValue(fooUpdate.getFooDate()));
    }
  }

  private static void updateValuesForFooTimestamp(JPAUpdateClause jpaUpdate, FooUpdate fooUpdate) {
    if (fooUpdate.getFooTimestamp() != null) {
      jpaUpdate.set(fooEntity.fooTimestamp, getUpdateValue(fooUpdate.getFooTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private FooDslHelper() {
  }
}
