package com.escalableapps.cmdb.baseapp.template.dsl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;

public interface FooDslRepository extends JpaRepository<FooEntity, Integer>, QuerydslPredicateExecutor<FooEntity> {

}
