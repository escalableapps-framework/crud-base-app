package com.escalableapps.cmdb.baseapp.template.dsl.adapter;

import static com.escalableapps.cmdb.baseapp.template.dsl.helper.ZooDslHelper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.mapper.ZooEntityMapper.*;
import static com.escalableapps.cmdb.baseapp.template.dsl.persistence.QZooEntity.zooEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.dsl.mapper.ZooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.dsl.persistence.*;
import com.escalableapps.cmdb.baseapp.template.dsl.repository.ZooDslRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.ZooSpi;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class ZooDslAdapter implements ZooSpi {

  private final ZooDslRepository repository;
  private final JPAQueryFactory queryFactory;

  public ZooDslAdapter(ZooDslRepository repository, JPAQueryFactory queryFactory) {
    this.repository = repository;
    this.queryFactory = queryFactory;
  }

  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoo:zoo={}", zoo);

    Integer zooId = repository.nextVal();
    ZooEntity zooEntity = repository.save(fromEntity(zoo, zooId));

    zoo.setZooId(zooEntity.getId().getZooId());
    log.debug("createZoo:zoo={}", zoo);
    return zoo;
  }

  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    ZooEntity zoo = repository.findById(new ZooPk(zooOrg, zooId)).orElse(null);
    log.debug("findZoo:zoo={}", zoo);

    return Optional.ofNullable(toEntity(zoo));
  }

  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);

    List<ZooEntity> zoos = repository.findAllById(zooIds.stream().map(zooId -> new ZooPk(zooOrg, zooId)).collect(Collectors.toList()));
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery //
  ) {
    return findZoos(zooQuery, new PageRequest<>());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);

    List<ZooEntity> zoos = queryFactory.selectFrom(zooEntity) //
        .where(whereCondition(zooQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = repository.existsById(new ZooPk(zooOrg, zooId));

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoos:zooQuery={}", zooQuery);

    boolean exists = repository.exists(whereCondition(zooQuery));

    log.debug("existsZoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("countZoos:zooQuery={}", zooQuery);

    long count = repository.count(whereCondition(zooQuery));

    log.debug("countZoos:count={}", count);
    return count;
  }

  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("countZoos:zooOrg={}", zooOrg);
    log.debug("countZoos:zooId={}", zooId);
    log.debug("countZoos:zooUpdate={}", zooUpdate);

    JPAUpdateClause jpaUpdate = queryFactory.update(zooEntity);
    updateValues(jpaUpdate, zooUpdate);
    jpaUpdate.where(whereCondition(zooOrg, zooId));
    long updateCount = jpaUpdate.execute();

    log.debug("countZoos:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("countZoos:zooOrg={}", zooOrg);
    log.debug("countZoos:zooId={}", zooId);

    long deleteCount = queryFactory.delete(zooEntity) //
        .where(whereCondition(zooOrg, zooId)) //
        .execute();

    log.debug("countZoos:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
