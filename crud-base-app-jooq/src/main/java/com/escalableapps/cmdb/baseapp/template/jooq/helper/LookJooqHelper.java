package com.escalableapps.cmdb.baseapp.template.jooq.helper;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Look.LOOK;
import static com.escalableapps.cmdb.baseapp.template.domain.entity.LookPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.Condition;
import org.jooq.OrderField;
import org.jooq.TableField;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.LookRecord;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class LookJooqHelper {

  public static Collection<Serializable> insertValues(Look look) {
    return Arrays.asList( //
        look.getLookOrg(), //
        look.getLookId(), //
        look.getLookChar(), //
        look.getLookVarchar(), //
        look.getLookText(), //
        look.getLookSmallint(), //
        look.getLookInteger(), //
        look.getLookBigint(), //
        look.getLookReal(), //
        look.getLookDouble(), //
        look.getLookDecimal(), //
        look.getLookBoolean(), //
        look.getLookDate(), //
        look.getLookTimestamp() //
    );
  }

  public static List<Condition> whereCondition(LookQuery lookQuery) {
    List<Condition> conditions = new ArrayList<>();
    whereConditionForLookOrg(conditions, lookQuery);
    whereConditionForLookId(conditions, lookQuery);
    whereConditionForLookChar(conditions, lookQuery);
    whereConditionForLookVarchar(conditions, lookQuery);
    whereConditionForLookText(conditions, lookQuery);
    whereConditionForLookSmallint(conditions, lookQuery);
    whereConditionForLookInteger(conditions, lookQuery);
    whereConditionForLookBigint(conditions, lookQuery);
    whereConditionForLookReal(conditions, lookQuery);
    whereConditionForLookDouble(conditions, lookQuery);
    whereConditionForLookDecimal(conditions, lookQuery);
    whereConditionForLookBoolean(conditions, lookQuery);
    whereConditionForLookDate(conditions, lookQuery);
    whereConditionForLookTimestamp(conditions, lookQuery);
    return conditions;
  }

  private static void whereConditionForLookOrg(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookOrg() != null) {
      if (!lookQuery.getLookOrg().exist()) {
        conditions.add(LOOK.LOOK_ORG.isNull());
      } else {
        conditions.add(LOOK.LOOK_ORG.eq(getQueryValue(lookQuery.getLookOrg())));
      }
    }
  }

  private static void whereConditionForLookId(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookId() != null) {
      if (!lookQuery.getLookId().exist()) {
        conditions.add(LOOK.LOOK_ID.isNull());
      } else {
        conditions.add(LOOK.LOOK_ID.eq(getQueryValue(lookQuery.getLookId())));
      }
    }
  }

  private static void whereConditionForLookChar(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookChar() != null) {
      if (!lookQuery.getLookChar().exist()) {
        conditions.add(LOOK.LOOK_CHAR.isNull());
      } else {
        conditions.add(LOOK.LOOK_CHAR.eq(getQueryValue(lookQuery.getLookChar())));
      }
    }
  }

  private static void whereConditionForLookVarchar(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookVarchar() != null) {
      if (!lookQuery.getLookVarchar().exist()) {
        conditions.add(LOOK.LOOK_VARCHAR.isNull());
      } else {
        conditions.add(LOOK.LOOK_VARCHAR.eq(getQueryValue(lookQuery.getLookVarchar())));
      }
    }
  }

  private static void whereConditionForLookText(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookText() != null) {
      if (!lookQuery.getLookText().exist()) {
        conditions.add(LOOK.LOOK_TEXT.isNull());
      } else {
        conditions.add(LOOK.LOOK_TEXT.eq(getQueryValue(lookQuery.getLookText())));
      }
    }
  }

  private static void whereConditionForLookSmallint(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookSmallint() != null) {
      if (!lookQuery.getLookSmallint().exist()) {
        conditions.add(LOOK.LOOK_SMALLINT.isNull());
      } else {
        conditions.add(LOOK.LOOK_SMALLINT.eq(getQueryValue(lookQuery.getLookSmallint())));
      }
    }
  }

  private static void whereConditionForLookInteger(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookInteger() != null) {
      if (!lookQuery.getLookInteger().exist()) {
        conditions.add(LOOK.LOOK_INTEGER.isNull());
      } else {
        conditions.add(LOOK.LOOK_INTEGER.eq(getQueryValue(lookQuery.getLookInteger())));
      }
    }
  }

  private static void whereConditionForLookBigint(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookBigint() != null) {
      if (!lookQuery.getLookBigint().exist()) {
        conditions.add(LOOK.LOOK_BIGINT.isNull());
      } else {
        conditions.add(LOOK.LOOK_BIGINT.eq(getQueryValue(lookQuery.getLookBigint())));
      }
    }
  }

  private static void whereConditionForLookReal(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookReal() != null) {
      if (!lookQuery.getLookReal().exist()) {
        conditions.add(LOOK.LOOK_REAL.isNull());
      } else {
        conditions.add(LOOK.LOOK_REAL.eq(getQueryValue(lookQuery.getLookReal())));
      }
    }
  }

  private static void whereConditionForLookDouble(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookDouble() != null) {
      if (!lookQuery.getLookDouble().exist()) {
        conditions.add(LOOK.LOOK_DOUBLE.isNull());
      } else {
        conditions.add(LOOK.LOOK_DOUBLE.eq(getQueryValue(lookQuery.getLookDouble())));
      }
    }
  }

  private static void whereConditionForLookDecimal(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookDecimal() != null) {
      if (!lookQuery.getLookDecimal().exist()) {
        conditions.add(LOOK.LOOK_DECIMAL.isNull());
      } else {
        conditions.add(LOOK.LOOK_DECIMAL.eq(getQueryValue(lookQuery.getLookDecimal())));
      }
    }
  }

  private static void whereConditionForLookBoolean(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookBoolean() != null) {
      if (!lookQuery.getLookBoolean().exist()) {
        conditions.add(LOOK.LOOK_BOOLEAN.isNull());
      } else {
        conditions.add(LOOK.LOOK_BOOLEAN.eq(getQueryValue(lookQuery.getLookBoolean())));
      }
    }
  }

  private static void whereConditionForLookDate(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookDate() != null) {
      if (!lookQuery.getLookDate().exist()) {
        conditions.add(LOOK.LOOK_DATE.isNull());
      } else {
        conditions.add(LOOK.LOOK_DATE.eq(getQueryValue(lookQuery.getLookDate())));
      }
    } else if (lookQuery.getLookDateMin() != null || lookQuery.getLookDateMax() != null) {
      if (lookQuery.getLookDateMin() != null && lookQuery.getLookDateMin().exist()) {
        conditions.add(LOOK.LOOK_DATE.ge(lookQuery.getLookDateMin().get()));
      }
      if (lookQuery.getLookDateMax() != null && lookQuery.getLookDateMax().exist()) {
        conditions.add(LOOK.LOOK_DATE.le(lookQuery.getLookDateMax().get()));
      }
    }
  }

  private static void whereConditionForLookTimestamp(List<Condition> conditions, LookQuery lookQuery) {
    if (lookQuery.getLookTimestamp() != null) {
      if (!lookQuery.getLookTimestamp().exist()) {
        conditions.add(LOOK.LOOK_TIMESTAMP.isNull());
      } else {
        conditions.add(LOOK.LOOK_TIMESTAMP.eq(getQueryValue(lookQuery.getLookTimestamp())));
      }
    } else if (lookQuery.getLookTimestampMin() != null || lookQuery.getLookTimestampMax() != null) {
      if (lookQuery.getLookTimestampMin() != null && lookQuery.getLookTimestampMin().exist()) {
        conditions.add(LOOK.LOOK_TIMESTAMP.ge(lookQuery.getLookTimestampMin().get()));
      }
      if (lookQuery.getLookTimestampMax() != null && lookQuery.getLookTimestampMax().exist()) {
        conditions.add(LOOK.LOOK_TIMESTAMP.le(lookQuery.getLookTimestampMax().get()));
      }
    }
  }

  public static List<OrderField<?>> orderBy(List<SortRequest<LookPropertyName>> sorts) {
    List<OrderField<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForLookOrg(orders, sort);
      orderByForLookId(orders, sort);
      orderByForLookChar(orders, sort);
      orderByForLookVarchar(orders, sort);
      orderByForLookText(orders, sort);
      orderByForLookSmallint(orders, sort);
      orderByForLookInteger(orders, sort);
      orderByForLookBigint(orders, sort);
      orderByForLookReal(orders, sort);
      orderByForLookDouble(orders, sort);
      orderByForLookDecimal(orders, sort);
      orderByForLookBoolean(orders, sort);
      orderByForLookDate(orders, sort);
      orderByForLookTimestamp(orders, sort);
    });
    return orders;
  }

  private static void orderByForLookOrg(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_ORG) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_ORG.asc() : LOOK.LOOK_ORG.desc());
    }
  }

  private static void orderByForLookId(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_ID) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_ID.asc() : LOOK.LOOK_ID.desc());
    }
  }

  private static void orderByForLookChar(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_CHAR) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_CHAR.asc() : LOOK.LOOK_CHAR.desc());
    }
  }

  private static void orderByForLookVarchar(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_VARCHAR) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_VARCHAR.asc() : LOOK.LOOK_VARCHAR.desc());
    }
  }

  private static void orderByForLookText(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_TEXT) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_TEXT.asc() : LOOK.LOOK_TEXT.desc());
    }
  }

  private static void orderByForLookSmallint(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_SMALLINT) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_SMALLINT.asc() : LOOK.LOOK_SMALLINT.desc());
    }
  }

  private static void orderByForLookInteger(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_INTEGER) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_INTEGER.asc() : LOOK.LOOK_INTEGER.desc());
    }
  }

  private static void orderByForLookBigint(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_BIGINT) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_BIGINT.asc() : LOOK.LOOK_BIGINT.desc());
    }
  }

  private static void orderByForLookReal(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_REAL) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_REAL.asc() : LOOK.LOOK_REAL.desc());
    }
  }

  private static void orderByForLookDouble(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_DOUBLE) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_DOUBLE.asc() : LOOK.LOOK_DOUBLE.desc());
    }
  }

  private static void orderByForLookDecimal(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_DECIMAL) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_DECIMAL.asc() : LOOK.LOOK_DECIMAL.desc());
    }
  }

  private static void orderByForLookBoolean(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_BOOLEAN) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_BOOLEAN.asc() : LOOK.LOOK_BOOLEAN.desc());
    }
  }

  private static void orderByForLookDate(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_DATE) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_DATE.asc() : LOOK.LOOK_DATE.desc());
    }
  }

  private static void orderByForLookTimestamp(List<OrderField<?>> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LOOK_TIMESTAMP) {
      orders.add(sort.isAscendant() ? LOOK.LOOK_TIMESTAMP.asc() : LOOK.LOOK_TIMESTAMP.desc());
    }
  }

  public static Map<TableField<LookRecord, ?>, Object> updateValues(LookUpdate lookUpdate) {
    Map<TableField<LookRecord, ?>, Object> updateValues = new HashMap<>();
    updateValuesForLookChar(updateValues, lookUpdate);
    updateValuesForLookVarchar(updateValues, lookUpdate);
    updateValuesForLookText(updateValues, lookUpdate);
    updateValuesForLookSmallint(updateValues, lookUpdate);
    updateValuesForLookInteger(updateValues, lookUpdate);
    updateValuesForLookBigint(updateValues, lookUpdate);
    updateValuesForLookReal(updateValues, lookUpdate);
    updateValuesForLookDouble(updateValues, lookUpdate);
    updateValuesForLookDecimal(updateValues, lookUpdate);
    updateValuesForLookBoolean(updateValues, lookUpdate);
    updateValuesForLookDate(updateValues, lookUpdate);
    updateValuesForLookTimestamp(updateValues, lookUpdate);
    return updateValues;
  }

  private static void updateValuesForLookChar(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookChar() != null) {
      updateValues.put(LOOK.LOOK_CHAR, getUpdateValue(lookUpdate.getLookChar()));
    }
  }

  private static void updateValuesForLookVarchar(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookVarchar() != null) {
      updateValues.put(LOOK.LOOK_VARCHAR, getUpdateValue(lookUpdate.getLookVarchar()));
    }
  }

  private static void updateValuesForLookText(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookText() != null) {
      updateValues.put(LOOK.LOOK_TEXT, getUpdateValue(lookUpdate.getLookText()));
    }
  }

  private static void updateValuesForLookSmallint(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookSmallint() != null) {
      updateValues.put(LOOK.LOOK_SMALLINT, getUpdateValue(lookUpdate.getLookSmallint()));
    }
  }

  private static void updateValuesForLookInteger(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookInteger() != null) {
      updateValues.put(LOOK.LOOK_INTEGER, getUpdateValue(lookUpdate.getLookInteger()));
    }
  }

  private static void updateValuesForLookBigint(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBigint() != null) {
      updateValues.put(LOOK.LOOK_BIGINT, getUpdateValue(lookUpdate.getLookBigint()));
    }
  }

  private static void updateValuesForLookReal(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookReal() != null) {
      updateValues.put(LOOK.LOOK_REAL, getUpdateValue(lookUpdate.getLookReal()));
    }
  }

  private static void updateValuesForLookDouble(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDouble() != null) {
      updateValues.put(LOOK.LOOK_DOUBLE, getUpdateValue(lookUpdate.getLookDouble()));
    }
  }

  private static void updateValuesForLookDecimal(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDecimal() != null) {
      updateValues.put(LOOK.LOOK_DECIMAL, getUpdateValue(lookUpdate.getLookDecimal()));
    }
  }

  private static void updateValuesForLookBoolean(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBoolean() != null) {
      updateValues.put(LOOK.LOOK_BOOLEAN, getUpdateValue(lookUpdate.getLookBoolean()));
    }
  }

  private static void updateValuesForLookDate(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDate() != null) {
      updateValues.put(LOOK.LOOK_DATE, getUpdateValue(lookUpdate.getLookDate()));
    }
  }

  private static void updateValuesForLookTimestamp(Map<TableField<LookRecord, ?>, Object> updateValues, LookUpdate lookUpdate) {
    if (lookUpdate.getLookTimestamp() != null) {
      updateValues.put(LOOK.LOOK_TIMESTAMP, getUpdateValue(lookUpdate.getLookTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private LookJooqHelper() {
  }
}
