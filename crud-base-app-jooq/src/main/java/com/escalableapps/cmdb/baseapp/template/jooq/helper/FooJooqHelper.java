package com.escalableapps.cmdb.baseapp.template.jooq.helper;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Foo.FOO;
import static com.escalableapps.cmdb.baseapp.template.domain.entity.FooPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.Condition;
import org.jooq.OrderField;
import org.jooq.TableField;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.FooRecord;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class FooJooqHelper {

  public static Collection<Serializable> insertValues(Foo foo) {
    return Arrays.asList( //
        foo.getFooChar(), //
        foo.getFooVarchar(), //
        foo.getFooText(), //
        foo.getFooSmallint(), //
        foo.getFooInteger(), //
        foo.getFooBigint(), //
        foo.getFooReal(), //
        foo.getFooDouble(), //
        foo.getFooDecimal(), //
        foo.getFooBoolean(), //
        foo.getFooDate(), //
        foo.getFooTimestamp() //
    );
  }

  public static List<Condition> whereCondition(FooQuery fooQuery) {
    List<Condition> conditions = new ArrayList<>();
    whereConditionForFooId(conditions, fooQuery);
    whereConditionForFooChar(conditions, fooQuery);
    whereConditionForFooVarchar(conditions, fooQuery);
    whereConditionForFooText(conditions, fooQuery);
    whereConditionForFooSmallint(conditions, fooQuery);
    whereConditionForFooInteger(conditions, fooQuery);
    whereConditionForFooBigint(conditions, fooQuery);
    whereConditionForFooReal(conditions, fooQuery);
    whereConditionForFooDouble(conditions, fooQuery);
    whereConditionForFooDecimal(conditions, fooQuery);
    whereConditionForFooBoolean(conditions, fooQuery);
    whereConditionForFooDate(conditions, fooQuery);
    whereConditionForFooTimestamp(conditions, fooQuery);
    return conditions;
  }

  private static void whereConditionForFooId(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooId() != null) {
      if (!fooQuery.getFooId().exist()) {
        conditions.add(FOO.FOO_ID.isNull());
      } else {
        conditions.add(FOO.FOO_ID.eq(getQueryValue(fooQuery.getFooId())));
      }
    }
  }

  private static void whereConditionForFooChar(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooChar() != null) {
      if (!fooQuery.getFooChar().exist()) {
        conditions.add(FOO.FOO_CHAR.isNull());
      } else {
        conditions.add(FOO.FOO_CHAR.eq(getQueryValue(fooQuery.getFooChar())));
      }
    }
  }

  private static void whereConditionForFooVarchar(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooVarchar() != null) {
      if (!fooQuery.getFooVarchar().exist()) {
        conditions.add(FOO.FOO_VARCHAR.isNull());
      } else {
        conditions.add(FOO.FOO_VARCHAR.eq(getQueryValue(fooQuery.getFooVarchar())));
      }
    }
  }

  private static void whereConditionForFooText(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooText() != null) {
      if (!fooQuery.getFooText().exist()) {
        conditions.add(FOO.FOO_TEXT.isNull());
      } else {
        conditions.add(FOO.FOO_TEXT.eq(getQueryValue(fooQuery.getFooText())));
      }
    }
  }

  private static void whereConditionForFooSmallint(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooSmallint() != null) {
      if (!fooQuery.getFooSmallint().exist()) {
        conditions.add(FOO.FOO_SMALLINT.isNull());
      } else {
        conditions.add(FOO.FOO_SMALLINT.eq(getQueryValue(fooQuery.getFooSmallint())));
      }
    }
  }

  private static void whereConditionForFooInteger(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooInteger() != null) {
      if (!fooQuery.getFooInteger().exist()) {
        conditions.add(FOO.FOO_INTEGER.isNull());
      } else {
        conditions.add(FOO.FOO_INTEGER.eq(getQueryValue(fooQuery.getFooInteger())));
      }
    }
  }

  private static void whereConditionForFooBigint(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooBigint() != null) {
      if (!fooQuery.getFooBigint().exist()) {
        conditions.add(FOO.FOO_BIGINT.isNull());
      } else {
        conditions.add(FOO.FOO_BIGINT.eq(getQueryValue(fooQuery.getFooBigint())));
      }
    }
  }

  private static void whereConditionForFooReal(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooReal() != null) {
      if (!fooQuery.getFooReal().exist()) {
        conditions.add(FOO.FOO_REAL.isNull());
      } else {
        conditions.add(FOO.FOO_REAL.eq(getQueryValue(fooQuery.getFooReal())));
      }
    }
  }

  private static void whereConditionForFooDouble(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooDouble() != null) {
      if (!fooQuery.getFooDouble().exist()) {
        conditions.add(FOO.FOO_DOUBLE.isNull());
      } else {
        conditions.add(FOO.FOO_DOUBLE.eq(getQueryValue(fooQuery.getFooDouble())));
      }
    }
  }

  private static void whereConditionForFooDecimal(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooDecimal() != null) {
      if (!fooQuery.getFooDecimal().exist()) {
        conditions.add(FOO.FOO_DECIMAL.isNull());
      } else {
        conditions.add(FOO.FOO_DECIMAL.eq(getQueryValue(fooQuery.getFooDecimal())));
      }
    }
  }

  private static void whereConditionForFooBoolean(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooBoolean() != null) {
      if (!fooQuery.getFooBoolean().exist()) {
        conditions.add(FOO.FOO_BOOLEAN.isNull());
      } else {
        conditions.add(FOO.FOO_BOOLEAN.eq(getQueryValue(fooQuery.getFooBoolean())));
      }
    }
  }

  private static void whereConditionForFooDate(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooDate() != null) {
      if (!fooQuery.getFooDate().exist()) {
        conditions.add(FOO.FOO_DATE.isNull());
      } else {
        conditions.add(FOO.FOO_DATE.eq(getQueryValue(fooQuery.getFooDate())));
      }
    } else if (fooQuery.getFooDateMin() != null || fooQuery.getFooDateMax() != null) {
      if (fooQuery.getFooDateMin() != null && fooQuery.getFooDateMin().exist()) {
        conditions.add(FOO.FOO_DATE.ge(fooQuery.getFooDateMin().get()));
      }
      if (fooQuery.getFooDateMax() != null && fooQuery.getFooDateMax().exist()) {
        conditions.add(FOO.FOO_DATE.le(fooQuery.getFooDateMax().get()));
      }
    }
  }

  private static void whereConditionForFooTimestamp(List<Condition> conditions, FooQuery fooQuery) {
    if (fooQuery.getFooTimestamp() != null) {
      if (!fooQuery.getFooTimestamp().exist()) {
        conditions.add(FOO.FOO_TIMESTAMP.isNull());
      } else {
        conditions.add(FOO.FOO_TIMESTAMP.eq(getQueryValue(fooQuery.getFooTimestamp())));
      }
    } else if (fooQuery.getFooTimestampMin() != null || fooQuery.getFooTimestampMax() != null) {
      if (fooQuery.getFooTimestampMin() != null && fooQuery.getFooTimestampMin().exist()) {
        conditions.add(FOO.FOO_TIMESTAMP.ge(fooQuery.getFooTimestampMin().get()));
      }
      if (fooQuery.getFooTimestampMax() != null && fooQuery.getFooTimestampMax().exist()) {
        conditions.add(FOO.FOO_TIMESTAMP.le(fooQuery.getFooTimestampMax().get()));
      }
    }
  }

  public static List<OrderField<?>> orderBy(List<SortRequest<FooPropertyName>> sorts) {
    List<OrderField<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForFooId(orders, sort);
      orderByForFooChar(orders, sort);
      orderByForFooVarchar(orders, sort);
      orderByForFooText(orders, sort);
      orderByForFooSmallint(orders, sort);
      orderByForFooInteger(orders, sort);
      orderByForFooBigint(orders, sort);
      orderByForFooReal(orders, sort);
      orderByForFooDouble(orders, sort);
      orderByForFooDecimal(orders, sort);
      orderByForFooBoolean(orders, sort);
      orderByForFooDate(orders, sort);
      orderByForFooTimestamp(orders, sort);
    });
    return orders;
  }

  private static void orderByForFooId(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_ID) {
      orders.add(sort.isAscendant() ? FOO.FOO_ID.asc() : FOO.FOO_ID.desc());
    }
  }

  private static void orderByForFooChar(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_CHAR) {
      orders.add(sort.isAscendant() ? FOO.FOO_CHAR.asc() : FOO.FOO_CHAR.desc());
    }
  }

  private static void orderByForFooVarchar(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_VARCHAR) {
      orders.add(sort.isAscendant() ? FOO.FOO_VARCHAR.asc() : FOO.FOO_VARCHAR.desc());
    }
  }

  private static void orderByForFooText(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_TEXT) {
      orders.add(sort.isAscendant() ? FOO.FOO_TEXT.asc() : FOO.FOO_TEXT.desc());
    }
  }

  private static void orderByForFooSmallint(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_SMALLINT) {
      orders.add(sort.isAscendant() ? FOO.FOO_SMALLINT.asc() : FOO.FOO_SMALLINT.desc());
    }
  }

  private static void orderByForFooInteger(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_INTEGER) {
      orders.add(sort.isAscendant() ? FOO.FOO_INTEGER.asc() : FOO.FOO_INTEGER.desc());
    }
  }

  private static void orderByForFooBigint(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_BIGINT) {
      orders.add(sort.isAscendant() ? FOO.FOO_BIGINT.asc() : FOO.FOO_BIGINT.desc());
    }
  }

  private static void orderByForFooReal(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_REAL) {
      orders.add(sort.isAscendant() ? FOO.FOO_REAL.asc() : FOO.FOO_REAL.desc());
    }
  }

  private static void orderByForFooDouble(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_DOUBLE) {
      orders.add(sort.isAscendant() ? FOO.FOO_DOUBLE.asc() : FOO.FOO_DOUBLE.desc());
    }
  }

  private static void orderByForFooDecimal(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_DECIMAL) {
      orders.add(sort.isAscendant() ? FOO.FOO_DECIMAL.asc() : FOO.FOO_DECIMAL.desc());
    }
  }

  private static void orderByForFooBoolean(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_BOOLEAN) {
      orders.add(sort.isAscendant() ? FOO.FOO_BOOLEAN.asc() : FOO.FOO_BOOLEAN.desc());
    }
  }

  private static void orderByForFooDate(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_DATE) {
      orders.add(sort.isAscendant() ? FOO.FOO_DATE.asc() : FOO.FOO_DATE.desc());
    }
  }

  private static void orderByForFooTimestamp(List<OrderField<?>> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FOO_TIMESTAMP) {
      orders.add(sort.isAscendant() ? FOO.FOO_TIMESTAMP.asc() : FOO.FOO_TIMESTAMP.desc());
    }
  }

  public static Map<TableField<FooRecord, ?>, Object> updateValues(FooUpdate fooUpdate) {
    Map<TableField<FooRecord, ?>, Object> updateValues = new HashMap<>();
    updateValuesForFooChar(updateValues, fooUpdate);
    updateValuesForFooVarchar(updateValues, fooUpdate);
    updateValuesForFooText(updateValues, fooUpdate);
    updateValuesForFooSmallint(updateValues, fooUpdate);
    updateValuesForFooInteger(updateValues, fooUpdate);
    updateValuesForFooBigint(updateValues, fooUpdate);
    updateValuesForFooReal(updateValues, fooUpdate);
    updateValuesForFooDouble(updateValues, fooUpdate);
    updateValuesForFooDecimal(updateValues, fooUpdate);
    updateValuesForFooBoolean(updateValues, fooUpdate);
    updateValuesForFooDate(updateValues, fooUpdate);
    updateValuesForFooTimestamp(updateValues, fooUpdate);
    return updateValues;
  }

  private static void updateValuesForFooChar(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooChar() != null) {
      updateValues.put(FOO.FOO_CHAR, getUpdateValue(fooUpdate.getFooChar()));
    }
  }

  private static void updateValuesForFooVarchar(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooVarchar() != null) {
      updateValues.put(FOO.FOO_VARCHAR, getUpdateValue(fooUpdate.getFooVarchar()));
    }
  }

  private static void updateValuesForFooText(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooText() != null) {
      updateValues.put(FOO.FOO_TEXT, getUpdateValue(fooUpdate.getFooText()));
    }
  }

  private static void updateValuesForFooSmallint(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooSmallint() != null) {
      updateValues.put(FOO.FOO_SMALLINT, getUpdateValue(fooUpdate.getFooSmallint()));
    }
  }

  private static void updateValuesForFooInteger(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooInteger() != null) {
      updateValues.put(FOO.FOO_INTEGER, getUpdateValue(fooUpdate.getFooInteger()));
    }
  }

  private static void updateValuesForFooBigint(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBigint() != null) {
      updateValues.put(FOO.FOO_BIGINT, getUpdateValue(fooUpdate.getFooBigint()));
    }
  }

  private static void updateValuesForFooReal(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooReal() != null) {
      updateValues.put(FOO.FOO_REAL, getUpdateValue(fooUpdate.getFooReal()));
    }
  }

  private static void updateValuesForFooDouble(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDouble() != null) {
      updateValues.put(FOO.FOO_DOUBLE, getUpdateValue(fooUpdate.getFooDouble()));
    }
  }

  private static void updateValuesForFooDecimal(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDecimal() != null) {
      updateValues.put(FOO.FOO_DECIMAL, getUpdateValue(fooUpdate.getFooDecimal()));
    }
  }

  private static void updateValuesForFooBoolean(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBoolean() != null) {
      updateValues.put(FOO.FOO_BOOLEAN, getUpdateValue(fooUpdate.getFooBoolean()));
    }
  }

  private static void updateValuesForFooDate(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDate() != null) {
      updateValues.put(FOO.FOO_DATE, getUpdateValue(fooUpdate.getFooDate()));
    }
  }

  private static void updateValuesForFooTimestamp(Map<TableField<FooRecord, ?>, Object> updateValues, FooUpdate fooUpdate) {
    if (fooUpdate.getFooTimestamp() != null) {
      updateValues.put(FOO.FOO_TIMESTAMP, getUpdateValue(fooUpdate.getFooTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private FooJooqHelper() {
  }
}
