package com.escalableapps.cmdb.baseapp.template.jooq.adapter;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Look.LOOK;
import static com.escalableapps.cmdb.baseapp.template.jooq.helper.LookJooqHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jooq.mapper.LookRecordMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.*;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jooq.mapper.LookRecordMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.LookSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class LookJooqAdapter implements LookSpi {

  private final DSLContext context;

  public LookJooqAdapter(DSLContext context) {
    this.context = context;
  }

  @Override
  public Look createLook( //
      Look look //
  ) {
    log.debug("createLook:look={}", look);

    Long lookId = context.insertInto(//
        LOOK, //
        LOOK.LOOK_ORG, //
        LOOK.LOOK_ID, //
        LOOK.LOOK_CHAR, //
        LOOK.LOOK_VARCHAR, //
        LOOK.LOOK_TEXT, //
        LOOK.LOOK_SMALLINT, //
        LOOK.LOOK_INTEGER, //
        LOOK.LOOK_BIGINT, //
        LOOK.LOOK_REAL, //
        LOOK.LOOK_DOUBLE, //
        LOOK.LOOK_DECIMAL, //
        LOOK.LOOK_BOOLEAN, //
        LOOK.LOOK_DATE, //
        LOOK.LOOK_TIMESTAMP //
    ) //
        .values(insertValues(look)) //
        .returningResult(LOOK.LOOK_ID) //
        .fetchOne().value1();
    look.setLookId(lookId);

    log.debug("createLook:look={}", look);
    return look;
  }

  @Override
  public Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);

    LookRecord look = context.selectFrom(LOOK) //
        .where(LOOK.LOOK_ORG.eq(lookOrg)) //
        .and(LOOK.LOOK_ID.eq(lookId)) //
        .fetchAny();
    log.debug("findLook:look={}", look);

    return Optional.ofNullable(toEntity(look));
  }

  @Override
  public List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  ) {
    log.debug("findLooks:lookOrg={}", lookOrg);
    log.debug("findLooks:lookIds={}", lookIds);

    List<LookRecord> looks = context.selectFrom(LOOK) //
        .where(LOOK.LOOK_ORG.eq(lookOrg)) //
        .and(LOOK.LOOK_ID.in(lookIds)) //
        .fetch();
    log.debug("findLooks:looks={}", looks);

    return looks.stream().map(LookRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery //
  ) {
    return findLooks(lookQuery, new PageRequest<>());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  ) {
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:paging={}", paging);

    List<LookRecord> looks = context.selectFrom(LOOK) //
        .where(whereCondition(lookQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findLooks:looks={}", looks);

    return looks.stream().map(LookRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);

    boolean exists = context.fetchExists( //
        context.selectOne().from(LOOK) //
            .where(LOOK.LOOK_ORG.eq(lookOrg)) //
            .and(LOOK.LOOK_ID.eq(lookId)) //
            .limit(1) //
    );

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("existsLooks:lookQuery={}", lookQuery);

    boolean exists = context.fetchExists( //
        context.selectOne().from(LOOK) //
            .where(whereCondition(lookQuery)) //
            .limit(1) //
    );

    log.debug("existsLooks:exists={}", exists);
    return exists;
  }

  @Override
  public long countLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("lookQuery={}", lookQuery);

    long count = context.selectCount().from(LOOK) //
        .where(whereCondition(lookQuery)) //
        .fetchAny().value1();

    log.debug("count={}", count);
    return count;
  }

  @Override
  public long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  ) {
    log.debug("lookOrg={}", lookOrg);
    log.debug("lookId={}", lookId);
    log.debug("lookUpdate={}", lookUpdate);

    long updateCount = context.update(LOOK) //
        .set(updateValues(lookUpdate)) //
        .where(LOOK.LOOK_ORG.eq(lookOrg)) //
        .and(LOOK.LOOK_ID.eq(lookId)) //
        .execute();

    log.debug("updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("lookOrg={}", lookOrg);
    log.debug("lookId={}", lookId);

    long deleteCount = context.deleteFrom(LOOK) //
        .where(LOOK.LOOK_ORG.eq(lookOrg)) //
        .and(LOOK.LOOK_ID.eq(lookId)) //
        .execute();

    log.debug("deleteCount={}", deleteCount);
    return deleteCount;
  }
}
