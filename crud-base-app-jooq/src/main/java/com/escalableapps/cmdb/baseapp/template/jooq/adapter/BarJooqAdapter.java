package com.escalableapps.cmdb.baseapp.template.jooq.adapter;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Bar.BAR;
import static com.escalableapps.cmdb.baseapp.template.jooq.helper.BarJooqHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jooq.mapper.BarRecordMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.*;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jooq.mapper.BarRecordMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class BarJooqAdapter implements BarSpi {

  private final DSLContext context;

  public BarJooqAdapter(DSLContext context) {
    this.context = context;
  }

  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBar:bar={}", bar);

    Integer barId = context.insertInto(//
        BAR, //
        BAR.BAR_CHAR, //
        BAR.BAR_VARCHAR, //
        BAR.BAR_TEXT, //
        BAR.BAR_SMALLINT, //
        BAR.BAR_INTEGER, //
        BAR.BAR_BIGINT, //
        BAR.BAR_REAL, //
        BAR.BAR_DOUBLE, //
        BAR.BAR_DECIMAL, //
        BAR.BAR_BOOLEAN, //
        BAR.BAR_DATE, //
        BAR.BAR_TIMESTAMP //
    ) //
        .values(insertValues(bar)) //
        .returningResult(BAR.BAR_ID) //
        .fetchOne().value1();
    bar.setBarId(barId);

    log.debug("createBar:bar={}", bar);
    return bar;
  }

  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    BarRecord bar = context.selectFrom(BAR) //
        .where(BAR.BAR_ID.eq(barId)) //
        .fetchAny();
    log.debug("findBar:bar={}", bar);

    return Optional.ofNullable(toEntity(bar));
  }

  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);

    List<BarRecord> bars = context.selectFrom(BAR) //
        .where(BAR.BAR_ID.in(barIds)) //
        .fetch();
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery //
  ) {
    return findBars(barQuery, new PageRequest<>());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);

    List<BarRecord> bars = context.selectFrom(BAR) //
        .where(whereCondition(barQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = context.fetchExists( //
        context.selectOne().from(BAR) //
            .where(BAR.BAR_ID.eq(barId)) //
            .limit(1) //
    );

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBars:barQuery={}", barQuery);

    boolean exists = context.fetchExists( //
        context.selectOne().from(BAR) //
            .where(whereCondition(barQuery)) //
            .limit(1) //
    );

    log.debug("existsBars:exists={}", exists);
    return exists;
  }

  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("barQuery={}", barQuery);

    long count = context.selectCount().from(BAR) //
        .where(whereCondition(barQuery)) //
        .fetchAny().value1();

    log.debug("count={}", count);
    return count;
  }

  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("barId={}", barId);
    log.debug("barUpdate={}", barUpdate);

    long updateCount = context.update(BAR) //
        .set(updateValues(barUpdate)) //
        .where(BAR.BAR_ID.eq(barId)) //
        .execute();

    log.debug("updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("barId={}", barId);

    long deleteCount = context.deleteFrom(BAR) //
        .where(BAR.BAR_ID.eq(barId)) //
        .execute();

    log.debug("deleteCount={}", deleteCount);
    return deleteCount;
  }
}
