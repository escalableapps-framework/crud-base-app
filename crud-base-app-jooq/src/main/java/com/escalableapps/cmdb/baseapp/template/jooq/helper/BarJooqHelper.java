package com.escalableapps.cmdb.baseapp.template.jooq.helper;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Bar.BAR;
import static com.escalableapps.cmdb.baseapp.template.domain.entity.BarPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.Condition;
import org.jooq.OrderField;
import org.jooq.TableField;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.BarRecord;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class BarJooqHelper {

  public static Collection<Serializable> insertValues(Bar bar) {
    return Arrays.asList( //
        bar.getBarChar(), //
        bar.getBarVarchar(), //
        bar.getBarText(), //
        bar.getBarSmallint(), //
        bar.getBarInteger(), //
        bar.getBarBigint(), //
        bar.getBarReal(), //
        bar.getBarDouble(), //
        bar.getBarDecimal(), //
        bar.getBarBoolean(), //
        bar.getBarDate(), //
        bar.getBarTimestamp() //
    );
  }

  public static List<Condition> whereCondition(BarQuery barQuery) {
    List<Condition> conditions = new ArrayList<>();
    whereConditionForBarId(conditions, barQuery);
    whereConditionForBarChar(conditions, barQuery);
    whereConditionForBarVarchar(conditions, barQuery);
    whereConditionForBarText(conditions, barQuery);
    whereConditionForBarSmallint(conditions, barQuery);
    whereConditionForBarInteger(conditions, barQuery);
    whereConditionForBarBigint(conditions, barQuery);
    whereConditionForBarReal(conditions, barQuery);
    whereConditionForBarDouble(conditions, barQuery);
    whereConditionForBarDecimal(conditions, barQuery);
    whereConditionForBarBoolean(conditions, barQuery);
    whereConditionForBarDate(conditions, barQuery);
    whereConditionForBarTimestamp(conditions, barQuery);
    return conditions;
  }

  private static void whereConditionForBarId(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarId() != null) {
      if (!barQuery.getBarId().exist()) {
        conditions.add(BAR.BAR_ID.isNull());
      } else {
        conditions.add(BAR.BAR_ID.eq(getQueryValue(barQuery.getBarId())));
      }
    }
  }

  private static void whereConditionForBarChar(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarChar() != null) {
      if (!barQuery.getBarChar().exist()) {
        conditions.add(BAR.BAR_CHAR.isNull());
      } else {
        conditions.add(BAR.BAR_CHAR.eq(getQueryValue(barQuery.getBarChar())));
      }
    }
  }

  private static void whereConditionForBarVarchar(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarVarchar() != null) {
      if (!barQuery.getBarVarchar().exist()) {
        conditions.add(BAR.BAR_VARCHAR.isNull());
      } else {
        conditions.add(BAR.BAR_VARCHAR.eq(getQueryValue(barQuery.getBarVarchar())));
      }
    }
  }

  private static void whereConditionForBarText(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarText() != null) {
      if (!barQuery.getBarText().exist()) {
        conditions.add(BAR.BAR_TEXT.isNull());
      } else {
        conditions.add(BAR.BAR_TEXT.eq(getQueryValue(barQuery.getBarText())));
      }
    }
  }

  private static void whereConditionForBarSmallint(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarSmallint() != null) {
      if (!barQuery.getBarSmallint().exist()) {
        conditions.add(BAR.BAR_SMALLINT.isNull());
      } else {
        conditions.add(BAR.BAR_SMALLINT.eq(getQueryValue(barQuery.getBarSmallint())));
      }
    }
  }

  private static void whereConditionForBarInteger(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarInteger() != null) {
      if (!barQuery.getBarInteger().exist()) {
        conditions.add(BAR.BAR_INTEGER.isNull());
      } else {
        conditions.add(BAR.BAR_INTEGER.eq(getQueryValue(barQuery.getBarInteger())));
      }
    }
  }

  private static void whereConditionForBarBigint(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarBigint() != null) {
      if (!barQuery.getBarBigint().exist()) {
        conditions.add(BAR.BAR_BIGINT.isNull());
      } else {
        conditions.add(BAR.BAR_BIGINT.eq(getQueryValue(barQuery.getBarBigint())));
      }
    }
  }

  private static void whereConditionForBarReal(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarReal() != null) {
      if (!barQuery.getBarReal().exist()) {
        conditions.add(BAR.BAR_REAL.isNull());
      } else {
        conditions.add(BAR.BAR_REAL.eq(getQueryValue(barQuery.getBarReal())));
      }
    }
  }

  private static void whereConditionForBarDouble(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarDouble() != null) {
      if (!barQuery.getBarDouble().exist()) {
        conditions.add(BAR.BAR_DOUBLE.isNull());
      } else {
        conditions.add(BAR.BAR_DOUBLE.eq(getQueryValue(barQuery.getBarDouble())));
      }
    }
  }

  private static void whereConditionForBarDecimal(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarDecimal() != null) {
      if (!barQuery.getBarDecimal().exist()) {
        conditions.add(BAR.BAR_DECIMAL.isNull());
      } else {
        conditions.add(BAR.BAR_DECIMAL.eq(getQueryValue(barQuery.getBarDecimal())));
      }
    }
  }

  private static void whereConditionForBarBoolean(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarBoolean() != null) {
      if (!barQuery.getBarBoolean().exist()) {
        conditions.add(BAR.BAR_BOOLEAN.isNull());
      } else {
        conditions.add(BAR.BAR_BOOLEAN.eq(getQueryValue(barQuery.getBarBoolean())));
      }
    }
  }

  private static void whereConditionForBarDate(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarDate() != null) {
      if (!barQuery.getBarDate().exist()) {
        conditions.add(BAR.BAR_DATE.isNull());
      } else {
        conditions.add(BAR.BAR_DATE.eq(getQueryValue(barQuery.getBarDate())));
      }
    } else if (barQuery.getBarDateMin() != null || barQuery.getBarDateMax() != null) {
      if (barQuery.getBarDateMin() != null && barQuery.getBarDateMin().exist()) {
        conditions.add(BAR.BAR_DATE.ge(barQuery.getBarDateMin().get()));
      }
      if (barQuery.getBarDateMax() != null && barQuery.getBarDateMax().exist()) {
        conditions.add(BAR.BAR_DATE.le(barQuery.getBarDateMax().get()));
      }
    }
  }

  private static void whereConditionForBarTimestamp(List<Condition> conditions, BarQuery barQuery) {
    if (barQuery.getBarTimestamp() != null) {
      if (!barQuery.getBarTimestamp().exist()) {
        conditions.add(BAR.BAR_TIMESTAMP.isNull());
      } else {
        conditions.add(BAR.BAR_TIMESTAMP.eq(getQueryValue(barQuery.getBarTimestamp())));
      }
    } else if (barQuery.getBarTimestampMin() != null || barQuery.getBarTimestampMax() != null) {
      if (barQuery.getBarTimestampMin() != null && barQuery.getBarTimestampMin().exist()) {
        conditions.add(BAR.BAR_TIMESTAMP.ge(barQuery.getBarTimestampMin().get()));
      }
      if (barQuery.getBarTimestampMax() != null && barQuery.getBarTimestampMax().exist()) {
        conditions.add(BAR.BAR_TIMESTAMP.le(barQuery.getBarTimestampMax().get()));
      }
    }
  }

  public static List<OrderField<?>> orderBy(List<SortRequest<BarPropertyName>> sorts) {
    List<OrderField<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForBarId(orders, sort);
      orderByForBarChar(orders, sort);
      orderByForBarVarchar(orders, sort);
      orderByForBarText(orders, sort);
      orderByForBarSmallint(orders, sort);
      orderByForBarInteger(orders, sort);
      orderByForBarBigint(orders, sort);
      orderByForBarReal(orders, sort);
      orderByForBarDouble(orders, sort);
      orderByForBarDecimal(orders, sort);
      orderByForBarBoolean(orders, sort);
      orderByForBarDate(orders, sort);
      orderByForBarTimestamp(orders, sort);
    });
    return orders;
  }

  private static void orderByForBarId(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_ID) {
      orders.add(sort.isAscendant() ? BAR.BAR_ID.asc() : BAR.BAR_ID.desc());
    }
  }

  private static void orderByForBarChar(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_CHAR) {
      orders.add(sort.isAscendant() ? BAR.BAR_CHAR.asc() : BAR.BAR_CHAR.desc());
    }
  }

  private static void orderByForBarVarchar(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_VARCHAR) {
      orders.add(sort.isAscendant() ? BAR.BAR_VARCHAR.asc() : BAR.BAR_VARCHAR.desc());
    }
  }

  private static void orderByForBarText(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_TEXT) {
      orders.add(sort.isAscendant() ? BAR.BAR_TEXT.asc() : BAR.BAR_TEXT.desc());
    }
  }

  private static void orderByForBarSmallint(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_SMALLINT) {
      orders.add(sort.isAscendant() ? BAR.BAR_SMALLINT.asc() : BAR.BAR_SMALLINT.desc());
    }
  }

  private static void orderByForBarInteger(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_INTEGER) {
      orders.add(sort.isAscendant() ? BAR.BAR_INTEGER.asc() : BAR.BAR_INTEGER.desc());
    }
  }

  private static void orderByForBarBigint(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_BIGINT) {
      orders.add(sort.isAscendant() ? BAR.BAR_BIGINT.asc() : BAR.BAR_BIGINT.desc());
    }
  }

  private static void orderByForBarReal(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_REAL) {
      orders.add(sort.isAscendant() ? BAR.BAR_REAL.asc() : BAR.BAR_REAL.desc());
    }
  }

  private static void orderByForBarDouble(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_DOUBLE) {
      orders.add(sort.isAscendant() ? BAR.BAR_DOUBLE.asc() : BAR.BAR_DOUBLE.desc());
    }
  }

  private static void orderByForBarDecimal(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_DECIMAL) {
      orders.add(sort.isAscendant() ? BAR.BAR_DECIMAL.asc() : BAR.BAR_DECIMAL.desc());
    }
  }

  private static void orderByForBarBoolean(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_BOOLEAN) {
      orders.add(sort.isAscendant() ? BAR.BAR_BOOLEAN.asc() : BAR.BAR_BOOLEAN.desc());
    }
  }

  private static void orderByForBarDate(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_DATE) {
      orders.add(sort.isAscendant() ? BAR.BAR_DATE.asc() : BAR.BAR_DATE.desc());
    }
  }

  private static void orderByForBarTimestamp(List<OrderField<?>> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BAR_TIMESTAMP) {
      orders.add(sort.isAscendant() ? BAR.BAR_TIMESTAMP.asc() : BAR.BAR_TIMESTAMP.desc());
    }
  }

  public static Map<TableField<BarRecord, ?>, Object> updateValues(BarUpdate barUpdate) {
    Map<TableField<BarRecord, ?>, Object> updateValues = new HashMap<>();
    updateValuesForBarChar(updateValues, barUpdate);
    updateValuesForBarVarchar(updateValues, barUpdate);
    updateValuesForBarText(updateValues, barUpdate);
    updateValuesForBarSmallint(updateValues, barUpdate);
    updateValuesForBarInteger(updateValues, barUpdate);
    updateValuesForBarBigint(updateValues, barUpdate);
    updateValuesForBarReal(updateValues, barUpdate);
    updateValuesForBarDouble(updateValues, barUpdate);
    updateValuesForBarDecimal(updateValues, barUpdate);
    updateValuesForBarBoolean(updateValues, barUpdate);
    updateValuesForBarDate(updateValues, barUpdate);
    updateValuesForBarTimestamp(updateValues, barUpdate);
    return updateValues;
  }

  private static void updateValuesForBarChar(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarChar() != null) {
      updateValues.put(BAR.BAR_CHAR, getUpdateValue(barUpdate.getBarChar()));
    }
  }

  private static void updateValuesForBarVarchar(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarVarchar() != null) {
      updateValues.put(BAR.BAR_VARCHAR, getUpdateValue(barUpdate.getBarVarchar()));
    }
  }

  private static void updateValuesForBarText(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarText() != null) {
      updateValues.put(BAR.BAR_TEXT, getUpdateValue(barUpdate.getBarText()));
    }
  }

  private static void updateValuesForBarSmallint(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarSmallint() != null) {
      updateValues.put(BAR.BAR_SMALLINT, getUpdateValue(barUpdate.getBarSmallint()));
    }
  }

  private static void updateValuesForBarInteger(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarInteger() != null) {
      updateValues.put(BAR.BAR_INTEGER, getUpdateValue(barUpdate.getBarInteger()));
    }
  }

  private static void updateValuesForBarBigint(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarBigint() != null) {
      updateValues.put(BAR.BAR_BIGINT, getUpdateValue(barUpdate.getBarBigint()));
    }
  }

  private static void updateValuesForBarReal(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarReal() != null) {
      updateValues.put(BAR.BAR_REAL, getUpdateValue(barUpdate.getBarReal()));
    }
  }

  private static void updateValuesForBarDouble(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarDouble() != null) {
      updateValues.put(BAR.BAR_DOUBLE, getUpdateValue(barUpdate.getBarDouble()));
    }
  }

  private static void updateValuesForBarDecimal(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarDecimal() != null) {
      updateValues.put(BAR.BAR_DECIMAL, getUpdateValue(barUpdate.getBarDecimal()));
    }
  }

  private static void updateValuesForBarBoolean(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarBoolean() != null) {
      updateValues.put(BAR.BAR_BOOLEAN, getUpdateValue(barUpdate.getBarBoolean()));
    }
  }

  private static void updateValuesForBarDate(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarDate() != null) {
      updateValues.put(BAR.BAR_DATE, getUpdateValue(barUpdate.getBarDate()));
    }
  }

  private static void updateValuesForBarTimestamp(Map<TableField<BarRecord, ?>, Object> updateValues, BarUpdate barUpdate) {
    if (barUpdate.getBarTimestamp() != null) {
      updateValues.put(BAR.BAR_TIMESTAMP, getUpdateValue(barUpdate.getBarTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private BarJooqHelper() {
  }
}
