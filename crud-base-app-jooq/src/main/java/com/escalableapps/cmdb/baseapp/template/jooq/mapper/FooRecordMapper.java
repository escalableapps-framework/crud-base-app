package com.escalableapps.cmdb.baseapp.template.jooq.mapper;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.FooRecord;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public final class FooRecordMapper {

  public static Foo toEntity(FooRecord fooRecord) {
    if (fooRecord == null) {
      return null;
    }
    Foo foo = new Foo();
    foo.setFooId(fooRecord.getFooId());
    foo.setFooChar(fooRecord.getFooChar());
    foo.setFooVarchar(fooRecord.getFooVarchar());
    foo.setFooText(fooRecord.getFooText());
    foo.setFooSmallint(fooRecord.getFooSmallint());
    foo.setFooInteger(fooRecord.getFooInteger());
    foo.setFooBigint(fooRecord.getFooBigint());
    foo.setFooReal(fooRecord.getFooReal());
    foo.setFooDouble(fooRecord.getFooDouble());
    foo.setFooDecimal(fooRecord.getFooDecimal());
    foo.setFooBoolean(fooRecord.getFooBoolean());
    foo.setFooDate(fooRecord.getFooDate());
    foo.setFooTimestamp(fooRecord.getFooTimestamp());
    return foo;
  }

  private FooRecordMapper() {
  }
}
