package com.escalableapps.cmdb.baseapp.template.jooq.mapper;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.BarRecord;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public final class BarRecordMapper {

  public static Bar toEntity(BarRecord barRecord) {
    if (barRecord == null) {
      return null;
    }
    Bar bar = new Bar();
    bar.setBarId(barRecord.getBarId());
    bar.setBarChar(barRecord.getBarChar());
    bar.setBarVarchar(barRecord.getBarVarchar());
    bar.setBarText(barRecord.getBarText());
    bar.setBarSmallint(barRecord.getBarSmallint());
    bar.setBarInteger(barRecord.getBarInteger());
    bar.setBarBigint(barRecord.getBarBigint());
    bar.setBarReal(barRecord.getBarReal());
    bar.setBarDouble(barRecord.getBarDouble());
    bar.setBarDecimal(barRecord.getBarDecimal());
    bar.setBarBoolean(barRecord.getBarBoolean());
    bar.setBarDate(barRecord.getBarDate());
    bar.setBarTimestamp(barRecord.getBarTimestamp());
    return bar;
  }

  private BarRecordMapper() {
  }
}
