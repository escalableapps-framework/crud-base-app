package com.escalableapps.cmdb.baseapp.template.jooq.mapper;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.ZooRecord;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public final class ZooRecordMapper {

  public static Zoo toEntity(ZooRecord zooRecord) {
    if (zooRecord == null) {
      return null;
    }
    Zoo zoo = new Zoo();
    zoo.setZooOrg(zooRecord.getZooOrg());
    zoo.setZooId(zooRecord.getZooId());
    zoo.setZooChar(zooRecord.getZooChar());
    zoo.setZooVarchar(zooRecord.getZooVarchar());
    zoo.setZooText(zooRecord.getZooText());
    zoo.setZooSmallint(zooRecord.getZooSmallint());
    zoo.setZooInteger(zooRecord.getZooInteger());
    zoo.setZooBigint(zooRecord.getZooBigint());
    zoo.setZooReal(zooRecord.getZooReal());
    zoo.setZooDouble(zooRecord.getZooDouble());
    zoo.setZooDecimal(zooRecord.getZooDecimal());
    zoo.setZooBoolean(zooRecord.getZooBoolean());
    zoo.setZooDate(zooRecord.getZooDate());
    zoo.setZooTimestamp(zooRecord.getZooTimestamp());
    return zoo;
  }

  private ZooRecordMapper() {
  }
}
