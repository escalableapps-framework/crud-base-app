package com.escalableapps.cmdb.baseapp.template.jooq.adapter;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Zoo.ZOO;
import static com.escalableapps.cmdb.baseapp.template.jooq.helper.ZooJooqHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jooq.mapper.ZooRecordMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.*;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jooq.mapper.ZooRecordMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.ZooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class ZooJooqAdapter implements ZooSpi {

  private final DSLContext context;

  public ZooJooqAdapter(DSLContext context) {
    this.context = context;
  }

  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoo:zoo={}", zoo);

    Integer zooId = context.insertInto(//
        ZOO, //
        ZOO.ZOO_ORG, //
        ZOO.ZOO_CHAR, //
        ZOO.ZOO_VARCHAR, //
        ZOO.ZOO_TEXT, //
        ZOO.ZOO_SMALLINT, //
        ZOO.ZOO_INTEGER, //
        ZOO.ZOO_BIGINT, //
        ZOO.ZOO_REAL, //
        ZOO.ZOO_DOUBLE, //
        ZOO.ZOO_DECIMAL, //
        ZOO.ZOO_BOOLEAN, //
        ZOO.ZOO_DATE, //
        ZOO.ZOO_TIMESTAMP //
    ) //
        .values(insertValues(zoo)) //
        .returningResult(ZOO.ZOO_ID) //
        .fetchOne().value1();
    zoo.setZooId(zooId);

    log.debug("createZoo:zoo={}", zoo);
    return zoo;
  }

  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    ZooRecord zoo = context.selectFrom(ZOO) //
        .where(ZOO.ZOO_ORG.eq(zooOrg)) //
        .and(ZOO.ZOO_ID.eq(zooId)) //
        .fetchAny();
    log.debug("findZoo:zoo={}", zoo);

    return Optional.ofNullable(toEntity(zoo));
  }

  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);

    List<ZooRecord> zoos = context.selectFrom(ZOO) //
        .where(ZOO.ZOO_ORG.eq(zooOrg)) //
        .and(ZOO.ZOO_ID.in(zooIds)) //
        .fetch();
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery //
  ) {
    return findZoos(zooQuery, new PageRequest<>());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);

    List<ZooRecord> zoos = context.selectFrom(ZOO) //
        .where(whereCondition(zooQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = context.fetchExists( //
        context.selectOne().from(ZOO) //
            .where(ZOO.ZOO_ORG.eq(zooOrg)) //
            .and(ZOO.ZOO_ID.eq(zooId)) //
            .limit(1) //
    );

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoos:zooQuery={}", zooQuery);

    boolean exists = context.fetchExists( //
        context.selectOne().from(ZOO) //
            .where(whereCondition(zooQuery)) //
            .limit(1) //
    );

    log.debug("existsZoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("zooQuery={}", zooQuery);

    long count = context.selectCount().from(ZOO) //
        .where(whereCondition(zooQuery)) //
        .fetchAny().value1();

    log.debug("count={}", count);
    return count;
  }

  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("zooOrg={}", zooOrg);
    log.debug("zooId={}", zooId);
    log.debug("zooUpdate={}", zooUpdate);

    long updateCount = context.update(ZOO) //
        .set(updateValues(zooUpdate)) //
        .where(ZOO.ZOO_ORG.eq(zooOrg)) //
        .and(ZOO.ZOO_ID.eq(zooId)) //
        .execute();

    log.debug("updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("zooOrg={}", zooOrg);
    log.debug("zooId={}", zooId);

    long deleteCount = context.deleteFrom(ZOO) //
        .where(ZOO.ZOO_ORG.eq(zooOrg)) //
        .and(ZOO.ZOO_ID.eq(zooId)) //
        .execute();

    log.debug("deleteCount={}", deleteCount);
    return deleteCount;
  }
}
