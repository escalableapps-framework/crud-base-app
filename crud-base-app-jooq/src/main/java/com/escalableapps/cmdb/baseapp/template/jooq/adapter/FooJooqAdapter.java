package com.escalableapps.cmdb.baseapp.template.jooq.adapter;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Foo.FOO;
import static com.escalableapps.cmdb.baseapp.template.jooq.helper.FooJooqHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jooq.mapper.FooRecordMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.*;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jooq.mapper.FooRecordMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class FooJooqAdapter implements FooSpi {

  private final DSLContext context;

  public FooJooqAdapter(DSLContext context) {
    this.context = context;
  }

  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoo:foo={}", foo);

    Integer fooId = context.insertInto(//
        FOO, //
        FOO.FOO_CHAR, //
        FOO.FOO_VARCHAR, //
        FOO.FOO_TEXT, //
        FOO.FOO_SMALLINT, //
        FOO.FOO_INTEGER, //
        FOO.FOO_BIGINT, //
        FOO.FOO_REAL, //
        FOO.FOO_DOUBLE, //
        FOO.FOO_DECIMAL, //
        FOO.FOO_BOOLEAN, //
        FOO.FOO_DATE, //
        FOO.FOO_TIMESTAMP //
    ) //
        .values(insertValues(foo)) //
        .returningResult(FOO.FOO_ID) //
        .fetchOne().value1();
    foo.setFooId(fooId);

    log.debug("createFoo:foo={}", foo);
    return foo;
  }

  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    FooRecord foo = context.selectFrom(FOO) //
        .where(FOO.FOO_ID.eq(fooId)) //
        .fetchAny();
    log.debug("findFoo:foo={}", foo);

    return Optional.ofNullable(toEntity(foo));
  }

  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);

    List<FooRecord> foos = context.selectFrom(FOO) //
        .where(FOO.FOO_ID.in(fooIds)) //
        .fetch();
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery //
  ) {
    return findFoos(fooQuery, new PageRequest<>());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);

    List<FooRecord> foos = context.selectFrom(FOO) //
        .where(whereCondition(fooQuery)) //
        .orderBy(orderBy(paging.getSortRequests())) //
        .offset(paging.getOffset()) //
        .limit(paging.getPageSize()) //
        .fetch();
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooRecordMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = context.fetchExists( //
        context.selectOne().from(FOO) //
            .where(FOO.FOO_ID.eq(fooId)) //
            .limit(1) //
    );

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoos:fooQuery={}", fooQuery);

    boolean exists = context.fetchExists( //
        context.selectOne().from(FOO) //
            .where(whereCondition(fooQuery)) //
            .limit(1) //
    );

    log.debug("existsFoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("fooQuery={}", fooQuery);

    long count = context.selectCount().from(FOO) //
        .where(whereCondition(fooQuery)) //
        .fetchAny().value1();

    log.debug("count={}", count);
    return count;
  }

  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("fooId={}", fooId);
    log.debug("fooUpdate={}", fooUpdate);

    long updateCount = context.update(FOO) //
        .set(updateValues(fooUpdate)) //
        .where(FOO.FOO_ID.eq(fooId)) //
        .execute();

    log.debug("updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("fooId={}", fooId);

    long deleteCount = context.deleteFrom(FOO) //
        .where(FOO.FOO_ID.eq(fooId)) //
        .execute();

    log.debug("deleteCount={}", deleteCount);
    return deleteCount;
  }
}
