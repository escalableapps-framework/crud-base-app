package com.escalableapps.cmdb.baseapp.template.jooq.helper;

import static com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.Zoo.ZOO;
import static com.escalableapps.cmdb.baseapp.template.domain.entity.ZooPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.Condition;
import org.jooq.OrderField;
import org.jooq.TableField;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.ZooRecord;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class ZooJooqHelper {

  public static Collection<Serializable> insertValues(Zoo zoo) {
    return Arrays.asList( //
        zoo.getZooOrg(), //
        zoo.getZooChar(), //
        zoo.getZooVarchar(), //
        zoo.getZooText(), //
        zoo.getZooSmallint(), //
        zoo.getZooInteger(), //
        zoo.getZooBigint(), //
        zoo.getZooReal(), //
        zoo.getZooDouble(), //
        zoo.getZooDecimal(), //
        zoo.getZooBoolean(), //
        zoo.getZooDate(), //
        zoo.getZooTimestamp() //
    );
  }

  public static List<Condition> whereCondition(ZooQuery zooQuery) {
    List<Condition> conditions = new ArrayList<>();
    whereConditionForZooOrg(conditions, zooQuery);
    whereConditionForZooId(conditions, zooQuery);
    whereConditionForZooChar(conditions, zooQuery);
    whereConditionForZooVarchar(conditions, zooQuery);
    whereConditionForZooText(conditions, zooQuery);
    whereConditionForZooSmallint(conditions, zooQuery);
    whereConditionForZooInteger(conditions, zooQuery);
    whereConditionForZooBigint(conditions, zooQuery);
    whereConditionForZooReal(conditions, zooQuery);
    whereConditionForZooDouble(conditions, zooQuery);
    whereConditionForZooDecimal(conditions, zooQuery);
    whereConditionForZooBoolean(conditions, zooQuery);
    whereConditionForZooDate(conditions, zooQuery);
    whereConditionForZooTimestamp(conditions, zooQuery);
    return conditions;
  }

  private static void whereConditionForZooOrg(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooOrg() != null) {
      if (!zooQuery.getZooOrg().exist()) {
        conditions.add(ZOO.ZOO_ORG.isNull());
      } else {
        conditions.add(ZOO.ZOO_ORG.eq(getQueryValue(zooQuery.getZooOrg())));
      }
    }
  }

  private static void whereConditionForZooId(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooId() != null) {
      if (!zooQuery.getZooId().exist()) {
        conditions.add(ZOO.ZOO_ID.isNull());
      } else {
        conditions.add(ZOO.ZOO_ID.eq(getQueryValue(zooQuery.getZooId())));
      }
    }
  }

  private static void whereConditionForZooChar(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooChar() != null) {
      if (!zooQuery.getZooChar().exist()) {
        conditions.add(ZOO.ZOO_CHAR.isNull());
      } else {
        conditions.add(ZOO.ZOO_CHAR.eq(getQueryValue(zooQuery.getZooChar())));
      }
    }
  }

  private static void whereConditionForZooVarchar(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooVarchar() != null) {
      if (!zooQuery.getZooVarchar().exist()) {
        conditions.add(ZOO.ZOO_VARCHAR.isNull());
      } else {
        conditions.add(ZOO.ZOO_VARCHAR.eq(getQueryValue(zooQuery.getZooVarchar())));
      }
    }
  }

  private static void whereConditionForZooText(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooText() != null) {
      if (!zooQuery.getZooText().exist()) {
        conditions.add(ZOO.ZOO_TEXT.isNull());
      } else {
        conditions.add(ZOO.ZOO_TEXT.eq(getQueryValue(zooQuery.getZooText())));
      }
    }
  }

  private static void whereConditionForZooSmallint(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooSmallint() != null) {
      if (!zooQuery.getZooSmallint().exist()) {
        conditions.add(ZOO.ZOO_SMALLINT.isNull());
      } else {
        conditions.add(ZOO.ZOO_SMALLINT.eq(getQueryValue(zooQuery.getZooSmallint())));
      }
    }
  }

  private static void whereConditionForZooInteger(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooInteger() != null) {
      if (!zooQuery.getZooInteger().exist()) {
        conditions.add(ZOO.ZOO_INTEGER.isNull());
      } else {
        conditions.add(ZOO.ZOO_INTEGER.eq(getQueryValue(zooQuery.getZooInteger())));
      }
    }
  }

  private static void whereConditionForZooBigint(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooBigint() != null) {
      if (!zooQuery.getZooBigint().exist()) {
        conditions.add(ZOO.ZOO_BIGINT.isNull());
      } else {
        conditions.add(ZOO.ZOO_BIGINT.eq(getQueryValue(zooQuery.getZooBigint())));
      }
    }
  }

  private static void whereConditionForZooReal(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooReal() != null) {
      if (!zooQuery.getZooReal().exist()) {
        conditions.add(ZOO.ZOO_REAL.isNull());
      } else {
        conditions.add(ZOO.ZOO_REAL.eq(getQueryValue(zooQuery.getZooReal())));
      }
    }
  }

  private static void whereConditionForZooDouble(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooDouble() != null) {
      if (!zooQuery.getZooDouble().exist()) {
        conditions.add(ZOO.ZOO_DOUBLE.isNull());
      } else {
        conditions.add(ZOO.ZOO_DOUBLE.eq(getQueryValue(zooQuery.getZooDouble())));
      }
    }
  }

  private static void whereConditionForZooDecimal(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooDecimal() != null) {
      if (!zooQuery.getZooDecimal().exist()) {
        conditions.add(ZOO.ZOO_DECIMAL.isNull());
      } else {
        conditions.add(ZOO.ZOO_DECIMAL.eq(getQueryValue(zooQuery.getZooDecimal())));
      }
    }
  }

  private static void whereConditionForZooBoolean(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooBoolean() != null) {
      if (!zooQuery.getZooBoolean().exist()) {
        conditions.add(ZOO.ZOO_BOOLEAN.isNull());
      } else {
        conditions.add(ZOO.ZOO_BOOLEAN.eq(getQueryValue(zooQuery.getZooBoolean())));
      }
    }
  }

  private static void whereConditionForZooDate(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooDate() != null) {
      if (!zooQuery.getZooDate().exist()) {
        conditions.add(ZOO.ZOO_DATE.isNull());
      } else {
        conditions.add(ZOO.ZOO_DATE.eq(getQueryValue(zooQuery.getZooDate())));
      }
    } else if (zooQuery.getZooDateMin() != null || zooQuery.getZooDateMax() != null) {
      if (zooQuery.getZooDateMin() != null && zooQuery.getZooDateMin().exist()) {
        conditions.add(ZOO.ZOO_DATE.ge(zooQuery.getZooDateMin().get()));
      }
      if (zooQuery.getZooDateMax() != null && zooQuery.getZooDateMax().exist()) {
        conditions.add(ZOO.ZOO_DATE.le(zooQuery.getZooDateMax().get()));
      }
    }
  }

  private static void whereConditionForZooTimestamp(List<Condition> conditions, ZooQuery zooQuery) {
    if (zooQuery.getZooTimestamp() != null) {
      if (!zooQuery.getZooTimestamp().exist()) {
        conditions.add(ZOO.ZOO_TIMESTAMP.isNull());
      } else {
        conditions.add(ZOO.ZOO_TIMESTAMP.eq(getQueryValue(zooQuery.getZooTimestamp())));
      }
    } else if (zooQuery.getZooTimestampMin() != null || zooQuery.getZooTimestampMax() != null) {
      if (zooQuery.getZooTimestampMin() != null && zooQuery.getZooTimestampMin().exist()) {
        conditions.add(ZOO.ZOO_TIMESTAMP.ge(zooQuery.getZooTimestampMin().get()));
      }
      if (zooQuery.getZooTimestampMax() != null && zooQuery.getZooTimestampMax().exist()) {
        conditions.add(ZOO.ZOO_TIMESTAMP.le(zooQuery.getZooTimestampMax().get()));
      }
    }
  }

  public static List<OrderField<?>> orderBy(List<SortRequest<ZooPropertyName>> sorts) {
    List<OrderField<?>> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForZooOrg(orders, sort);
      orderByForZooId(orders, sort);
      orderByForZooChar(orders, sort);
      orderByForZooVarchar(orders, sort);
      orderByForZooText(orders, sort);
      orderByForZooSmallint(orders, sort);
      orderByForZooInteger(orders, sort);
      orderByForZooBigint(orders, sort);
      orderByForZooReal(orders, sort);
      orderByForZooDouble(orders, sort);
      orderByForZooDecimal(orders, sort);
      orderByForZooBoolean(orders, sort);
      orderByForZooDate(orders, sort);
      orderByForZooTimestamp(orders, sort);
    });
    return orders;
  }

  private static void orderByForZooOrg(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_ORG) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_ORG.asc() : ZOO.ZOO_ORG.desc());
    }
  }

  private static void orderByForZooId(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_ID) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_ID.asc() : ZOO.ZOO_ID.desc());
    }
  }

  private static void orderByForZooChar(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_CHAR) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_CHAR.asc() : ZOO.ZOO_CHAR.desc());
    }
  }

  private static void orderByForZooVarchar(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_VARCHAR) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_VARCHAR.asc() : ZOO.ZOO_VARCHAR.desc());
    }
  }

  private static void orderByForZooText(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_TEXT) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_TEXT.asc() : ZOO.ZOO_TEXT.desc());
    }
  }

  private static void orderByForZooSmallint(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_SMALLINT) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_SMALLINT.asc() : ZOO.ZOO_SMALLINT.desc());
    }
  }

  private static void orderByForZooInteger(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_INTEGER) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_INTEGER.asc() : ZOO.ZOO_INTEGER.desc());
    }
  }

  private static void orderByForZooBigint(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_BIGINT) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_BIGINT.asc() : ZOO.ZOO_BIGINT.desc());
    }
  }

  private static void orderByForZooReal(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_REAL) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_REAL.asc() : ZOO.ZOO_REAL.desc());
    }
  }

  private static void orderByForZooDouble(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_DOUBLE) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_DOUBLE.asc() : ZOO.ZOO_DOUBLE.desc());
    }
  }

  private static void orderByForZooDecimal(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_DECIMAL) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_DECIMAL.asc() : ZOO.ZOO_DECIMAL.desc());
    }
  }

  private static void orderByForZooBoolean(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_BOOLEAN) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_BOOLEAN.asc() : ZOO.ZOO_BOOLEAN.desc());
    }
  }

  private static void orderByForZooDate(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_DATE) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_DATE.asc() : ZOO.ZOO_DATE.desc());
    }
  }

  private static void orderByForZooTimestamp(List<OrderField<?>> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZOO_TIMESTAMP) {
      orders.add(sort.isAscendant() ? ZOO.ZOO_TIMESTAMP.asc() : ZOO.ZOO_TIMESTAMP.desc());
    }
  }

  public static Map<TableField<ZooRecord, ?>, Object> updateValues(ZooUpdate zooUpdate) {
    Map<TableField<ZooRecord, ?>, Object> updateValues = new HashMap<>();
    updateValuesForZooChar(updateValues, zooUpdate);
    updateValuesForZooVarchar(updateValues, zooUpdate);
    updateValuesForZooText(updateValues, zooUpdate);
    updateValuesForZooSmallint(updateValues, zooUpdate);
    updateValuesForZooInteger(updateValues, zooUpdate);
    updateValuesForZooBigint(updateValues, zooUpdate);
    updateValuesForZooReal(updateValues, zooUpdate);
    updateValuesForZooDouble(updateValues, zooUpdate);
    updateValuesForZooDecimal(updateValues, zooUpdate);
    updateValuesForZooBoolean(updateValues, zooUpdate);
    updateValuesForZooDate(updateValues, zooUpdate);
    updateValuesForZooTimestamp(updateValues, zooUpdate);
    return updateValues;
  }

  private static void updateValuesForZooChar(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooChar() != null) {
      updateValues.put(ZOO.ZOO_CHAR, getUpdateValue(zooUpdate.getZooChar()));
    }
  }

  private static void updateValuesForZooVarchar(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooVarchar() != null) {
      updateValues.put(ZOO.ZOO_VARCHAR, getUpdateValue(zooUpdate.getZooVarchar()));
    }
  }

  private static void updateValuesForZooText(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooText() != null) {
      updateValues.put(ZOO.ZOO_TEXT, getUpdateValue(zooUpdate.getZooText()));
    }
  }

  private static void updateValuesForZooSmallint(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooSmallint() != null) {
      updateValues.put(ZOO.ZOO_SMALLINT, getUpdateValue(zooUpdate.getZooSmallint()));
    }
  }

  private static void updateValuesForZooInteger(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooInteger() != null) {
      updateValues.put(ZOO.ZOO_INTEGER, getUpdateValue(zooUpdate.getZooInteger()));
    }
  }

  private static void updateValuesForZooBigint(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBigint() != null) {
      updateValues.put(ZOO.ZOO_BIGINT, getUpdateValue(zooUpdate.getZooBigint()));
    }
  }

  private static void updateValuesForZooReal(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooReal() != null) {
      updateValues.put(ZOO.ZOO_REAL, getUpdateValue(zooUpdate.getZooReal()));
    }
  }

  private static void updateValuesForZooDouble(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDouble() != null) {
      updateValues.put(ZOO.ZOO_DOUBLE, getUpdateValue(zooUpdate.getZooDouble()));
    }
  }

  private static void updateValuesForZooDecimal(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDecimal() != null) {
      updateValues.put(ZOO.ZOO_DECIMAL, getUpdateValue(zooUpdate.getZooDecimal()));
    }
  }

  private static void updateValuesForZooBoolean(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBoolean() != null) {
      updateValues.put(ZOO.ZOO_BOOLEAN, getUpdateValue(zooUpdate.getZooBoolean()));
    }
  }

  private static void updateValuesForZooDate(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDate() != null) {
      updateValues.put(ZOO.ZOO_DATE, getUpdateValue(zooUpdate.getZooDate()));
    }
  }

  private static void updateValuesForZooTimestamp(Map<TableField<ZooRecord, ?>, Object> updateValues, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooTimestamp() != null) {
      updateValues.put(ZOO.ZOO_TIMESTAMP, getUpdateValue(zooUpdate.getZooTimestamp()));
    }
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private ZooJooqHelper() {
  }
}
