package com.escalableapps.cmdb.baseapp.template.jooq.mapper;

import com.escalableapps.cmdb.baseapp.jooq.crud_base_db.tables.records.LookRecord;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public final class LookRecordMapper {

  public static Look toEntity(LookRecord lookRecord) {
    if (lookRecord == null) {
      return null;
    }
    Look look = new Look();
    look.setLookOrg(lookRecord.getLookOrg());
    look.setLookId(lookRecord.getLookId());
    look.setLookChar(lookRecord.getLookChar());
    look.setLookVarchar(lookRecord.getLookVarchar());
    look.setLookText(lookRecord.getLookText());
    look.setLookSmallint(lookRecord.getLookSmallint());
    look.setLookInteger(lookRecord.getLookInteger());
    look.setLookBigint(lookRecord.getLookBigint());
    look.setLookReal(lookRecord.getLookReal());
    look.setLookDouble(lookRecord.getLookDouble());
    look.setLookDecimal(lookRecord.getLookDecimal());
    look.setLookBoolean(lookRecord.getLookBoolean());
    look.setLookDate(lookRecord.getLookDate());
    look.setLookTimestamp(lookRecord.getLookTimestamp());
    return look;
  }

  private LookRecordMapper() {
  }
}
