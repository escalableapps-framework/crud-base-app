package com.escalableapps.framework.cucumber.model;

import java.util.HashMap;

public class TestContext<E extends Enum<E>> {

  private final HashMap<Enum<E>, Object> context;

  public TestContext() {
    context = new HashMap<>();
  }

  @SuppressWarnings("unchecked")
  public <T> T get(E key) {
    return (T) context.get(key);
  }

  public boolean contains(E key) {
    return context.containsKey(key);
  }

  public void put(E key, Object value) {
    context.put(key, value);
  }

}
