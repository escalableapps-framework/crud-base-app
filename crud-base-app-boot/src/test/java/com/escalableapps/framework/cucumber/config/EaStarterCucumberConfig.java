package com.escalableapps.framework.cucumber.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.escalableapps.framework.cucumber.model.TestContext;

@Configuration
public class EaStarterCucumberConfig {

  @Bean
  @Scope("cucumber-glue")
  public TestContext<?> eaTestContext() {
    return new TestContext<>();
  }

}
