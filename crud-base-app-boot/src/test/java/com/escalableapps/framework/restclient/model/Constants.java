package com.escalableapps.framework.restclient.model;

public interface Constants {

  public interface MediaType {
    public static final String APPLICATION_JSON = "application/json;charset=UTF-8";
  }

  public enum ResponseStatus {
    SUCCESS, FAIL, ERROR;
  }
}
