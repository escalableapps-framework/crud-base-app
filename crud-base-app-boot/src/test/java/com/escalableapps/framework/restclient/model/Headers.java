package com.escalableapps.framework.restclient.model;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.http.HttpHeaders;

public class Headers {

  private final HashMap<String, List<String>> headers;

  public Headers() {
    headers = new HashMap<>();
  }

  private List<String> getHeaderList(String name) {
    List<String> values = headers.get(name);
    if (values == null) {
      values = new ArrayList<>();
      headers.put(name, values);
    }
    return values;
  }

  public Headers add(String name, OffsetDateTime value) {
    add(name, value.format(ISO_OFFSET_DATE_TIME));
    return this;
  }

  public Headers add(String name, LocalDate value) {
    add(name, value.format(ISO_LOCAL_DATE));
    return this;
  }

  public Headers add(String name, Number value) {
    add(name, Objects.toString(value));
    return this;
  }

  public Headers add(String name, String value) {
    if (!isIso88591(name)) {
      throw new IllegalArgumentException(String.format("Non iso-8859-1 characters found in header name: %s", name));
    }
    if (value != null && !isIso88591(value)) {
      throw new IllegalArgumentException(String.format("Non iso-8859-1 characters found in value of header %s: %s", name, value));
    }
    getHeaderList(name).add(value == null ? "" : value);
    return this;
  }

  private boolean isIso88591(String utf) {
    return StandardCharsets.ISO_8859_1.newEncoder().canEncode(utf);
  }

  public List<String> get(String name) {
    List<String> clone = new ArrayList<>();
    clone.addAll(getHeaderList(name));
    return clone;
  }

  public String getFirst(String name) {
    List<String> values = getHeaderList(name);
    return values.size() == 0 ? null : values.get(0);
  }

  public Headers clear(String name) {
    headers.remove(name);
    return this;
  }

  public boolean contains(String name) {
    return headers.containsKey(name);
  }

  public Map<String, List<String>> toMap() {
    HttpHeaders clone = new HttpHeaders();
    headers.forEach((name, values) -> clone.addAll(name, values));
    return clone;
  }

  public static Headers fromMap(Map<String, List<String>> httpHeaders) {
    Headers headers = new Headers();
    httpHeaders.forEach((key, values) -> values.forEach(value -> headers.add(key, value)));
    return headers;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName()).append("(");
    int len = builder.length();
    headers.forEach((key, values) -> values.forEach(value -> {
      if (len != builder.length()) {
        builder.append(", ");
      }
      builder.append(key).append("=").append(value);
    }));
    builder.append(")");
    return builder.toString();
  }
}
