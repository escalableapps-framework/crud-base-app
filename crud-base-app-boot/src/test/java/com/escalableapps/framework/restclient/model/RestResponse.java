package com.escalableapps.framework.restclient.model;

import com.escalableapps.framework.restclient.model.Constants.ResponseStatus;

import lombok.Data;

@Data
public class RestResponse {

  private ResponseStatus responseStatus;

  private int status;

  private Headers headers;

  private String body;

  private String errorMessage;

}
