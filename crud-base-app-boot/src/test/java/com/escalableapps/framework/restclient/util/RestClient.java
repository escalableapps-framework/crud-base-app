package com.escalableapps.framework.restclient.util;

import java.util.HashMap;

import org.apache.http.client.config.RequestConfig;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.DefaultUriBuilderFactory.EncodingMode;
import org.springframework.web.util.UriTemplateHandler;

import com.escalableapps.framework.restclient.model.RestRequest;

public class RestClient {

  private static HashMap<Integer, RestTemplate> restTemplates;

  static {
    restTemplates = new HashMap<>();
  }

  public static RestRequest newRequest() {
    return newRequest(null);
  }

  public static RestRequest newRequest(Integer timeout) {
    RestTemplate restTemplate = restTemplates.get(timeout);
    if (restTemplate == null) {
      synchronized (restTemplates) {
        restTemplate = new RestTemplate(requestFactory(timeout));
        restTemplate.setUriTemplateHandler(handler());

        restTemplates.put(timeout, restTemplate);
      }
    }

    return new RestRequestExecutor(restTemplate);
  }

  private static HttpComponentsClientHttpRequestFactory requestFactory(Integer timeout) {
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory() {
      @Override
      protected RequestConfig createRequestConfig(Object client) {
        RequestConfig.Builder builder = RequestConfig.copy(super.createRequestConfig(client));
        builder.setNormalizeUri(false);
        if (timeout != null) {
          builder.setConnectTimeout(timeout);
          builder.setConnectionRequestTimeout(timeout);
          builder.setSocketTimeout(timeout);
        }
        return builder.build();
      }
    };
    return requestFactory;
  }

  private static UriTemplateHandler handler() {
    DefaultUriBuilderFactory uriFactory = new DefaultUriBuilderFactory();
    uriFactory.setEncodingMode(EncodingMode.NONE);
    return uriFactory;
  }
}