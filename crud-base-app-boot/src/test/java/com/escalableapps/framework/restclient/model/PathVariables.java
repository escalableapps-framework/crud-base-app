package com.escalableapps.framework.restclient.model;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.web.util.UriUtils;

public class PathVariables {

  private final HashMap<String, String> variables;

  public PathVariables() {
    variables = new HashMap<>();
  }

  public PathVariables add(String name, OffsetDateTime value) {
    add(name, value.format(ISO_OFFSET_DATE_TIME));
    return this;
  }

  public PathVariables add(String name, LocalDate value) {
    add(name, value.format(ISO_LOCAL_DATE));
    return this;
  }

  public PathVariables add(String name, Number value) {
    add(name, Objects.toString(value));
    return this;
  }

  public PathVariables add(String name, String value) {
    variables.put(name, value);
    return this;
  }

  public String get(String name) {
    return variables.get(name);
  }

  public PathVariables clear(String name) {
    variables.remove(name);
    return this;
  }

  Map<String, String> toEncodedMap() {
    HashMap<String, String> clone = new HashMap<>();
    variables.forEach((key, value) -> clone.put(key, UriUtils.encode(value, UTF_8)));
    return clone;
  }
}
