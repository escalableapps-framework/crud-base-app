package com.escalableapps.cmdb.baseapp.shared.restclient.util;

import static com.escalableapps.cmdb.baseapp.shared.restclient.model.Constants.MediaType.APPLICATION_JSON;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.escalableapps.cmdb.baseapp.shared.restclient.model.Constants.ResponseStatus;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.Headers;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.RestRequest;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.RestResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class RestRequestExecutor extends RestRequest {

  private final RestTemplate restTemplate;

  protected RestRequestExecutor(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  protected RestResponse execute() {
    if (uri == null) {
      throw new IllegalArgumentException("HTTP request cannot be executed because URI has not been configured");
    }
    executed = true;

    HttpEntity<String> requestEntity = new HttpEntity<>(jsonBody, getRequestHeaders());
    RestResponse response = new RestResponse();
    try {
      ResponseEntity<String> responseEntity = restTemplate.exchange(uri, method, requestEntity, String.class);

      response.setResponseStatus(ResponseStatus.SUCCESS);
      response.setStatus(responseEntity.getStatusCodeValue());
      response.setHeaders(Headers.fromMap(responseEntity.getHeaders()));
      response.setBody(responseEntity.getBody());
    } catch (RestClientResponseException e) {
      response.setResponseStatus(ResponseStatus.FAIL);
      response.setStatus(e.getRawStatusCode());
      response.setHeaders(Headers.fromMap(e.getResponseHeaders()));
      response.setBody(e.getResponseBodyAsString());
    } catch (Exception e) {
      response.setResponseStatus(ResponseStatus.ERROR);
      response.setErrorMessage(e.getMessage());
    } finally {
      if (StringUtils.trimToNull(response.getBody()) == null) {
        response.setBody(null);
      }
    }
    log.info("request={}", this);
    if (ResponseStatus.SUCCESS.equals(response.getResponseStatus())) {
      log.info("response={}", response);
    } else {
      log.error("response={}", response);
    }
    return response;
  }

  private HttpHeaders getRequestHeaders() {
    if (headers == null) {
      headers = new Headers();
    }
    if (headers.get(HttpHeaders.CONTENT_TYPE).isEmpty()) {
      headers.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON);
    }
    return (HttpHeaders) headers.toMap();
  }
}