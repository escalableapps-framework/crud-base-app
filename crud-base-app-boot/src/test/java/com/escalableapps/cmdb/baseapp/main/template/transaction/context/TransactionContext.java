package com.escalableapps.cmdb.baseapp.main.template.transaction.context;

public enum TransactionContext {
  ENDPOINT, METHOD, STATUS, RESPONSE_BODY, IDS, BAR_ID, RESOURCE_PATH
}
