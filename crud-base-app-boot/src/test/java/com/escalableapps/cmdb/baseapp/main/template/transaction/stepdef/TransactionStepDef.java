package com.escalableapps.cmdb.baseapp.main.template.transaction.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.ENDPOINT;
import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.IDS;
import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.METHOD;
import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.RESOURCE_PATH;
import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.RESPONSE_BODY;
import static com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext.STATUS;

import org.json.JSONObject;
import org.junit.Assert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import com.escalableapps.cmdb.baseapp.CrudBaseApp;
import com.escalableapps.cmdb.baseapp.main.template.transaction.context.TransactionContext;
import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.restclient.model.RestResponse;
import com.escalableapps.framework.restclient.util.RestClient;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = CrudBaseApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TransactionStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<TransactionContext> context;

  public TransactionStepDef(TestContext<TransactionContext> context) {
    this.context = context;
  }

  @Dado("los archivos con datos para pruebas en la ruta: {string}")
  public void los_archivos_con_datos_para_pruebas_en_la_ruta(String resourcePath) {
    context.put(RESOURCE_PATH, resourcePath);
  }

  @Dado("el endpoint {string}")
  public void el_endpoint(String endpoint) {
    context.put(ENDPOINT, String.format("http://localhost:%s%s", port, endpoint));
  }

  @Cuando("se efectua la solicitud {string}")
  public void se_efectua_la_solicitud(String method) {
    context.put(METHOD, method);

    String uri = context.get(ENDPOINT);

    RestResponse response = RestClient.newRequest().uri(uri).send(method, null);

    context.put(STATUS, response.getStatus());
    context.put(RESPONSE_BODY, response.getBody());
  }

  @Entonces("responde con el código de error {int}")
  public void responde_con_el_código_de_error(Integer expectedStatus) {
    Integer currentStatus = context.get(STATUS);

    Assert.assertEquals(expectedStatus, currentStatus);
  }

  @Entonces("en el cuerpo de la respuesta la propiedad {string} tiene el id de la entidad creada")
  public void en_el_cuerpo_de_la_respuesta_la_propiedad_tiene_el_id_de_la_entidad_creada(String messagePropertyName) throws Exception {
    String ids = new JSONObject((String) context.get(RESPONSE_BODY)).getString(messagePropertyName);
    context.put(IDS, ids);
  }

  @Entonces("en el cuerpo de la respuesta la propiedad {string} tiene los ids de las entidades creadas")
  public void en_el_cuerpo_de_la_respuesta_la_propiedad_tiene_los_ids_de_las_entidades_creadas(String messagePropertyName) throws Exception {
    String ids = new JSONObject((String) context.get(RESPONSE_BODY)).getString(messagePropertyName);
    context.put(IDS, ids);
  }

  @Entonces("en la base de datos no existe la entidad con el id creado")
  public void en_la_base_de_datos_no_existe_la_entidad_con_el_id_creado() {
    int fooId = Integer.valueOf(context.get(IDS));
    assertFoo(fooId, 404);
  }

  @Entonces("en la base de datos existe la entidad con el id creado")
  public void en_la_base_de_datos_existe_la_entidad_con_el_id_creado() {
    int fooId = Integer.valueOf(context.get(IDS));
    assertFoo(fooId, 204);
  }

  @Entonces("en la base de datos no existe la primera entidad con el id creado")
  public void en_la_base_de_datos_no_existe_la_primera_entidad_con_el_id_creado() {
    int fooId = Integer.valueOf(((String) context.get(IDS)).split(",")[0]);
    assertFoo(fooId, 404);
  }

  @Entonces("en la base de datos existe la primera entidad con el id creado")
  public void en_la_base_de_datos_existe_la_primera_entidad_con_el_id_creado() {
    int fooId = Integer.valueOf(((String) context.get(IDS)).split(",")[0]);
    assertFoo(fooId, 204);
  }

  @Entonces("en la base de datos no existe la segunda entidad con el id creado")
  public void en_la_base_de_datos_no_existe_la_segunda_entidad_con_el_id_creado() {
    int barId = Integer.valueOf(((String) context.get(IDS)).split(",")[1]);
    assertBar(barId, 404);
  }

  @Entonces("en la base de datos existe la segunda entidad con el id creado")
  public void en_la_base_de_datos_existe_la_segunda_entidad_con_el_id_creado() {
    int barId = Integer.valueOf(((String) context.get(IDS)).split(",")[1]);
    assertBar(barId, 204);
  }

  private void assertFoo(int fooId, int expectedStatus) {
    RestResponse response = RestClient.newRequest().uri(String.format("http://localhost:%s/api/v1/foo/%s", port, fooId)).head();
    Assert.assertEquals(expectedStatus, response.getStatus());
  }

  private void assertBar(int barId, int expectedStatus) {
    RestResponse response = RestClient.newRequest().uri(String.format("http://localhost:%s/api/v1/bar/%s", port, barId)).head();
    Assert.assertEquals(expectedStatus, response.getStatus());
  }
}
