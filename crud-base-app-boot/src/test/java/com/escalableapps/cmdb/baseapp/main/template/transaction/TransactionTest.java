package com.escalableapps.cmdb.baseapp.main.template.transaction;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:com/escalableapps/cmdb/baseapp/template/transaction/feature", //
    glue = "classpath:com.escalableapps.cmdb.baseapp.main.template.transaction.stepdef", //
    monochrome = true, //
    strict = true //
)
public class TransactionTest {

}
