package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESOURCE_PATH;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext;
import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.rowmapper.FooRowMapper;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.es.Entonces;

public class FooRestControllerDeleteByParamsStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<FooRestControllerContext> context;
  private final DataSource dataSource;

  private final String findFoosSql;

  public FooRestControllerDeleteByParamsStepDef(TestContext<FooRestControllerContext> context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;

    findFoosSql = ResourceUtils.readResource("classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/sql/findFoos.sql");
  }

  private String getExampleValue(String exampleValue) {
    if (StringUtils.endsWithIgnoreCase(exampleValue, ".txt") || StringUtils.endsWithIgnoreCase(exampleValue, ".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      return ResourceUtils.readResource(resourcePath, exampleValue);
    }
    return exampleValue;
  }

  @Entonces("se verifica que en la base de datos se encuentran los registros {string}")
  public void se_verifica_que_en_la_base_de_datos_se_encuentran_los_registros(String expectedJson) throws JSONException {
    String actualFoos = JsonUtils.asJson(findFoos(null));
    expectedJson = getExampleValue(expectedJson);

    JSONAssert.assertEquals(expectedJson, actualFoos, true);
  }

  private List<Foo> findFoos(Integer fooId) {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    List<Foo> foos = jdbcTemplate.query(findFoosSql, new FooRowMapper());

    return foos;
  }
}
