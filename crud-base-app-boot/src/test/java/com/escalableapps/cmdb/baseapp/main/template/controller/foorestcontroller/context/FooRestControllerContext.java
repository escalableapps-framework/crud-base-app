package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context;

public enum FooRestControllerContext {
  RESOURCE_PATH, ENDPONIT, METHOD, INPUT_JSON, STATUS, SUCCESS_STATUS, HEADERS, RESPONSE_BODY, ID
}
