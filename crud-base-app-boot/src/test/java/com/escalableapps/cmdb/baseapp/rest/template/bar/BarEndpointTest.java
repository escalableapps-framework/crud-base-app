package com.escalableapps.cmdb.baseapp.rest.template.bar;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:rest/template/bar/feature", //
    glue = "classpath:com.escalableapps.cmdb.baseapp.rest.template.bar.stepdef", //
    monochrome = true //
)
public class BarEndpointTest {
}
