package com.escalableapps.cmdb.baseapp.main.template.transaction.service;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.ConflictException;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Bar;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;

@Transactional
public class TransactionBarServiceImpl implements TransactionBarService {

  private BarSpi barSpi;

  public TransactionBarServiceImpl(BarSpi barSpi) {
    this.barSpi = barSpi;
  }

  @Transactional(value = TxType.REQUIRES_NEW)
  @Override
  public int doubleTxWithFirstReversion() {
    Bar barToCreate = createBar();
    Bar barCreated = barSpi.createBar(barToCreate);
    int barId = barCreated.getBarId();
    return barId;
  }

  @Transactional(value = TxType.REQUIRES_NEW)
  @Override
  public void doubleTxWithSecondReversion() {
    Bar barToCreate = createBar();
    Bar barCreated = barSpi.createBar(barToCreate);
    int barId = barCreated.getBarId();
    throw new RuntimeException(String.valueOf(barId));
  }

  @Override
  public void doubleTxWithBothReversion(int fooId) {
    Bar barToCreate = createBar();
    Bar barCreated = barSpi.createBar(barToCreate);
    int barId = barCreated.getBarId();
    throw new ConflictException("%s,%s", fooId, barId);
  }

  private Bar createBar() {
    String barJson = ResourceUtils.readResource("classpath:/com/escalableapps/cmdb/baseapp/template/transaction/data/bar.json");
    Bar barToCreate = JsonUtils.parseAsType(barJson, Bar.class);
    return barToCreate;
  }
}
