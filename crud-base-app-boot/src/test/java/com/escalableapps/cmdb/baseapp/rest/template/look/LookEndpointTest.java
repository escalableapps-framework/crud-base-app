package com.escalableapps.cmdb.baseapp.rest.template.look;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:rest/template/look/feature", //
    glue = "classpath:com.escalableapps.cmdb.baseapp.rest.template.look.stepdef", //
    monochrome = true //
)
public class LookEndpointTest {
}
