package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.ENDPONIT;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESOURCE_PATH;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESPONSE_BODY;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.STATUS;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.restclient.model.PathVariables;
import com.escalableapps.framework.restclient.model.QueryParameters;
import com.escalableapps.framework.restclient.model.RestResponse;
import com.escalableapps.framework.restclient.util.RestClient;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FooRestControllerGetHeadByParamsStepDef {

  private static ResourceLoader resourceLoader;

  private static ResourcePatternResolver resourcePatternResolver;

  static {
    resourceLoader = new DefaultResourceLoader();
    resourcePatternResolver = new PathMatchingResourcePatternResolver();
  }

  @LocalServerPort
  private int port;

  private final TestContext<FooRestControllerContext> context;
  private final DataSource dataSource;

  public FooRestControllerGetHeadByParamsStepDef(TestContext<FooRestControllerContext> context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;
  }

  @Dado("la data inicial para la prueba: {string}")
  public void la_data_inicial_para_la_prueba(String sqlScript) throws Exception {

    Connection connection = null;
    try {
      connection = dataSource.getConnection();

      String resourceUrl = resourcePatternResolver.getResources(sqlScript)[0].getURL().toString();
      Resource resource = resourceLoader.getResource(resourceUrl);

      ScriptUtils.executeSqlScript(connection, resource);
      log.info("{} successfully loaded", sqlScript);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    } finally {
      connectionClose(connection);
    }
  }

  private void connectionClose(Connection connection) {
    try {
      connection.close();
    } catch (Exception e) {
      log.warn(e.getMessage(), e);
    }
  }

  @Cuando("se envia en la solicitud la consulta {string} con los parametros {string}")
  public void se_envia_en_la_solicitud_la_consulta_con_los_parametros(String method, String inputParams) {
    String uri = context.get(ENDPONIT);

    PathVariables pathVariables = new PathVariables().add("port", port);

    List<String> params = Arrays.asList(inputParams.split("\\?|&"));
    QueryParameters query = new QueryParameters();
    params.stream().filter(param -> param.length() > 0).forEach(param -> {
      int equalsPosition = param.indexOf("=");
      String name = param.substring(0, equalsPosition);
      String value = param.substring(equalsPosition + 1);
      query.add(name, value);
    });

    RestResponse response = RestClient.newRequest().uri(uri, pathVariables, query).send(method);

    context.put(STATUS, response.getStatus());
    context.put(RESPONSE_BODY, response.getBody());
  }

  @Cuando("se envia en la solicitud la consulta {string} con el identificador {string}")
  public void se_envia_en_la_solicitud_la_consulta_con_el_identificador(String method, String id) {
    String uri = context.get(ENDPONIT);

    PathVariables pathVariables = new PathVariables().add("port", port).add("id", id);

    RestResponse response = RestClient.newRequest().uri(uri, pathVariables).send(method);

    context.put(STATUS, response.getStatus());
    context.put(RESPONSE_BODY, response.getBody());
  }

  @Entonces("en el cuerpo de la respuesta el json {string}")
  public void en_el_cuerpo_de_la_respuesta_el_json(String expectedResponse) throws JSONException {
    expectedResponse = getExampleValue(expectedResponse);
    String responseBody = context.get(RESPONSE_BODY);

    int status = context.get(STATUS);
    if (status == 200) {
      JSONAssert.assertEquals(expectedResponse, responseBody, true);
    } else {
      JSONAssert.assertEquals(expectedResponse, responseBody, //
          new CustomComparator(JSONCompareMode.NON_EXTENSIBLE, //
              new Customization("timestamp", (o1, o2) -> true) //
          ));

    }
  }

  private String getExampleValue(String exampleValue) {
    if (StringUtils.endsWithIgnoreCase(exampleValue, ".txt") || StringUtils.endsWithIgnoreCase(exampleValue, ".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      return ResourceUtils.readResource(resourcePath, exampleValue);
    }
    return exampleValue;
  }
}
