package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.ENDPONIT;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.HEADERS;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESOURCE_PATH;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESPONSE_BODY;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.STATUS;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.SUCCESS_STATUS;

import java.util.List;

import javax.sql.DataSource;

import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext;
import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.rowmapper.FooRowMapper;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;
import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.restclient.model.PathVariables;
import com.escalableapps.framework.restclient.model.RestResponse;
import com.escalableapps.framework.restclient.util.RestClient;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;

public class FooRestControllerPatchStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<FooRestControllerContext> context;
  private final DataSource dataSource;

  private final String findFooSql;

  public FooRestControllerPatchStepDef(TestContext<FooRestControllerContext> context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;

    findFooSql = ResourceUtils.readResource("classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/sql/findFoo.sql");
  }

  @Cuando("se envia en el cuerpo de la solicitud el json {string} con el metodo {string}")
  public void se_envia_en_el_cuerpo_de_la_solicitud_el_json_con_el_metodo(String inputJson, String method) {
    String uri = context.get(ENDPONIT);
    PathVariables pathVariables = new PathVariables().add("port", port);

    if (inputJson.endsWith(".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      inputJson = ResourceUtils.readResource(resourcePath, inputJson);
    }

    RestResponse response = RestClient.newRequest().uri(uri, pathVariables).send(method, inputJson);

    context.put(SUCCESS_STATUS, response.getResponseStatus());
    context.put(STATUS, response.getStatus());
    context.put(HEADERS, response.getHeaders());
    context.put(RESPONSE_BODY, response.getBody());
  }

  @Entonces("se verifica que en la base de datos se encuentra un registro con el id {int} y con los valores en los campos {string}")
  public void se_verifica_que_en_la_base_de_datos_se_encuentra_un_registro_con_el_id_generado_y_con_los_valores_en_los_campos(Integer id, String expectedJson) throws JSONException {
    String createdFoo = JsonUtils.asJson(findFoo(id));

    if (expectedJson.endsWith(".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      expectedJson = ResourceUtils.readResource(resourcePath, expectedJson);
    }

    JSONAssert.assertEquals(expectedJson, createdFoo, true);
  }

  private Foo findFoo(Integer fooId) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooId", fooId);
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    List<Foo> foos = jdbcTemplate.query(findFooSql, paramMap, new FooRowMapper());

    Foo foo = foos.size() == 0 ? null : foos.get(0);
    return foo;
  }
}
