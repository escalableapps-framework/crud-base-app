package com.escalableapps.cmdb.baseapp.rest.template.bar.stepdef;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.escalableapps.cmdb.baseapp.shared.restclient.model.Headers;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.QueryParameters;

import lombok.Data;

@Component
@Scope("cucumber-glue")
@Data
public class BarTestContext {
  private String pathData;
  private String endpoint;
  private Integer barId;
  private QueryParameters params = new QueryParameters();
  private String requestBody;
  private String method;
  private Integer statusCode;
  private String responseBody;
  private Headers headers;
}
