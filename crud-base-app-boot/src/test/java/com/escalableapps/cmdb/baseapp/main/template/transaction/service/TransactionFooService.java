package com.escalableapps.cmdb.baseapp.main.template.transaction.service;

public interface TransactionFooService {

  void singleTxWithReversion();

  int singleTxWithoutReversion();

  void doubleTxWithFirstReversion();

  String doubleTxWithSecondReversion();

  void doubleTxWithBothReversion();
}