package com.escalableapps.cmdb.baseapp.main.template.transaction.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.cmdb.baseapp.main.template.transaction.service.TransactionFooService;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.ConflictException;

@RestController
@RequestMapping("/tx")
public class TransactionRestController {

  private TransactionFooService transactionFooService;

  public TransactionRestController(TransactionFooService transactionFooService) {
    this.transactionFooService = transactionFooService;
  }

  @GetMapping("/singleTxWithReversion")
  public void singleTxWithReversion() {
    transactionFooService.singleTxWithReversion();
  }

  @GetMapping("/singleTxWithoutReversion")
  public void singleTxWithoutReversion() {
    int id = transactionFooService.singleTxWithoutReversion();
    throw new ConflictException("%s", id);
  }

  @GetMapping("/doubleTxWithFirstReversion")
  public void doubleTxWithFirstReversion() {
    transactionFooService.doubleTxWithFirstReversion();
  }

  @GetMapping("/doubleTxWithSecondReversion")
  public void doubleTxWithSecondReversion() {
    String ids = transactionFooService.doubleTxWithSecondReversion();
    throw new ConflictException("%s", ids);
  }

  @GetMapping("/doubleTxWithBothReversion")
  public void doubleTxWithBothReversion() {
    transactionFooService.doubleTxWithBothReversion();
  }
}
