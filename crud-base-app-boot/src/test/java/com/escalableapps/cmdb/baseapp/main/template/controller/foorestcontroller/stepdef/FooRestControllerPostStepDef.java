package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.ENDPONIT;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.HEADERS;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.ID;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESOURCE_PATH;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESPONSE_BODY;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.SUCCESS_STATUS;

import java.util.List;

import javax.sql.DataSource;

import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext;
import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.rowmapper.FooRowMapper;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;
import com.escalableapps.cmdb.baseapp.template.rest.vo.foo.FooResponse;
import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.restclient.model.Constants.ResponseStatus;
import com.escalableapps.framework.restclient.model.Headers;

import io.cucumber.java.es.Entonces;

public class FooRestControllerPostStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<FooRestControllerContext> context;
  private final DataSource dataSource;

  private final String findFooSql;

  public FooRestControllerPostStepDef(TestContext<FooRestControllerContext> context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;

    findFooSql = ResourceUtils.readResource("classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/sql/findFoo.sql");
  }

  @Entonces("en el cuerpo de la respuesta el json {string} con un id generado")
  public void en_el_cuerpo_de_la_respuesta_el_json_con_un_id_generado(String expectedJson) throws JSONException {
    String responseBody = context.get(RESPONSE_BODY);

    if (expectedJson.endsWith(".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      expectedJson = ResourceUtils.readResource(resourcePath, expectedJson);
    }

    if (ResponseStatus.SUCCESS.equals(context.get(SUCCESS_STATUS))) {
      JSONAssert.assertEquals(expectedJson, responseBody, //
          new CustomComparator(JSONCompareMode.LENIENT, //
              new Customization("fooId", (o1, o2) -> true) //
          ));
      FooResponse foo = JsonUtils.parseAsType(responseBody, FooResponse.class);

      Integer fooId = foo.getFooId();
      Assert.assertNotNull(fooId);
      context.put(ID, fooId);
    } else {
      JSONAssert.assertEquals(expectedJson, responseBody, //
          new CustomComparator(JSONCompareMode.NON_EXTENSIBLE, //
              new Customization("timestamp", (o1, o2) -> true) //
          ));
    }

  }

  @Entonces("se indica la cabecera {string} con el valor {string}")
  public void se_indica_la_cabecera_con_el_valor(String header, String uri) {
    String endpoint = context.get(ENDPONIT) + "/{id}";
    Integer id = context.get(ID);
    String expectedLocation = endpoint.replace("{port}", String.valueOf(port)).replace("{id}", id.toString());

    Headers headers = context.get(HEADERS);
    String actualLocation = headers.getFirst(header);

    Assert.assertEquals(expectedLocation, actualLocation);

  }

  @Entonces("se verifica que en la base de datos se encuentra un registro con el id generado y con los valores en los campos {string}")
  public void se_verifica_que_en_la_base_de_datos_se_encuentra_un_registro_con_el_id_generado_y_con_los_valores_en_los_campos(String expectedJson) throws JSONException {
    if (ResponseStatus.FAIL.equals(context.get(SUCCESS_STATUS))) {
      return;
    }
    Integer id = context.get(ID);
    String createdFoo = JsonUtils.asJson(findFoo(id));

    if (expectedJson.endsWith(".json")) {
      String resourcePath = context.get(RESOURCE_PATH);
      expectedJson = ResourceUtils.readResource(resourcePath, expectedJson);
    }

    JSONAssert.assertEquals(expectedJson, createdFoo, //
        new CustomComparator(JSONCompareMode.LENIENT, //
            new Customization("fooId", (o1, o2) -> true) //
        ));
  }

  private Foo findFoo(Integer fooId) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooId", fooId);
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    List<Foo> foos = jdbcTemplate.query(findFooSql, paramMap, new FooRowMapper());

    Foo foo = foos.size() == 0 ? null : foos.get(0);
    return foo;
  }
}
