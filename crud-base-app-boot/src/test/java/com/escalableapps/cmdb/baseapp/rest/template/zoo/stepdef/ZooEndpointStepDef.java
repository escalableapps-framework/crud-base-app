package com.escalableapps.cmdb.baseapp.rest.template.zoo.stepdef;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.web.util.UriComponentsBuilder;

import com.escalableapps.cmdb.baseapp.TestCrudBaseApp;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.shared.dto.ListOfMaps;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.PathVariables;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.RestResponse;
import com.escalableapps.cmdb.baseapp.shared.restclient.util.RestClient;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Zoo;
import com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.ZooResponse;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;

@CucumberContextConfiguration
@SpringBootTest(classes = TestCrudBaseApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@Slf4j
public class ZooEndpointStepDef {

  @LocalServerPort
  private int port;

  private final ZooTestContext context;
  private final DataSource dataSource;
  private final String findZooSql;
  private final String findZooIdsSql;
  private final ResourceLoader resourceLoader;

  public ZooEndpointStepDef(ZooTestContext context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;
    this.findZooSql = ResourceUtils.readResource("classpath:rest/template/zoo/sql/zoo-test-find.sql");
    this.findZooIdsSql = ResourceUtils.readResource("classpath:rest/template/zoo/sql/zoo-test-findIds.sql");
    resourceLoader = new DefaultResourceLoader();
  }

  @Given("the path of the test scenario files {string}")
  public void thePathOfTheTestScenarioFiles(String pathData) {
    context.setPathData(pathData);
  }

  @Given("the empty zoo table")
  public void theEmptyZooTable() {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource("classpath:rest/template/zoo/sql/zoo-truncate.sql"));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the initial data {string}")
  public void theInitialData(String initialData) {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource(initialData));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the zoo-org value for in the url is {long}")
  public void theZooOrgValueForInTheUrlIs(Long zooOrg) {
    context.setZooOrg(zooOrg);
  }

  @Given("the zoo-id value for in the url is {int}")
  public void theZooIdValueForInTheUrlIs(Integer zooId) {
    context.setZooId(zooId);
  }

  @Given("the endpoint {string}")
  public void theEndpoint(String endpoint) {
    context.setEndpoint(endpoint);
  }

  @Given("the query property {string}=null")
  public void theQueryPropertyNull(String parameter) {
    context.getParams().add(parameter, (String) null);
  }

  @Given("the query property {string}={string}")
  public void theQueryProperty(String parameter, String value) {
    context.getParams().add(parameter, value);
  }

  @Given("the sortProperty={string}")
  public void theSortProperty(String sortProperty) {
    context.getParams().add("sortProperty", sortProperty);
  }

  @Given("the sortAscendant={string}")
  public void theSortAscendant(String sortAscendant) {
    context.getParams().add("sortAscendant", sortAscendant);
  }

  @Given("the request body {string}")
  public void theRequestBody(String requestBodyFile) {
    String requestBody = ResourceUtils.readResource(context.getPathData(), requestBodyFile);
    context.setRequestBody(requestBody);
  }

  @When("a {string} request is made")
  public void aRequestIsMade(String method) {
    context.setMethod(method);
    try {
      String uri = "http://localhost:{port}" + context.getEndpoint();
      PathVariables pathVariables = new PathVariables();
      pathVariables.add("port", port);
      pathVariables.add("zoo-org", context.getZooOrg());
      if (context.getZooId() != null) {
        pathVariables.add("zoo-id", context.getZooId());
      }

      RestResponse response = RestClient.newRequest() //
          .uri(uri, pathVariables, context.getParams()) //
          .send(context.getMethod(), context.getRequestBody());

      context.setHeaders(response.getHeaders());
      context.setStatusCode(response.getStatus());
      context.setResponseBody(response.getBody());
      if ("POST".equals(method)) {
        ZooResponse zoo = JsonUtils.parseAsType(response.getBody(), ZooResponse.class);
        context.setZooId(zoo.getZooId());
      }
    } catch (Exception exception) {
      log.error(exception.getMessage(), exception);
    }
  }

  @Then("I get status code {int}")
  public void iGetStatusCode(Integer expectedStatusCode) {
    Assert.assertEquals(context.getResponseBody(), expectedStatusCode, context.getStatusCode());
  }

  @Then("I get the response body {string} with a new zoo-id value")
  public void iGetTheResponseBodyWithANewZooIdValue(String expectedResponseBodyFile) throws JSONException {
    iGetTheResponseBody(expectedResponseBodyFile);
    Assert.assertNotNull(context.getZooId());
  }

  @Then("I get the response body {string}")
  public void iGetTheResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  private String getExpectedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("zoo-org", context.getZooOrg());
    expectedResponse.put("zoo-id", context.getZooId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get the paged response body {string}")
  public void iGetThePagedResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedPagedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  @SuppressWarnings("unchecked")
  private String getExpectedPagedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    List<Map<String, Object>> expectedResults = (List<Map<String, Object>>) expectedResponse.get("results");
    expectedResults.forEach(result -> {
      result.put("zoo-org", context.getZooOrg());
    });

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get into the database a resource Zoo with the new zoo-id and values like {string}")
  public void iGetIntoTheDatabaseAResourceZooWithTheNewZooIdAndValuesLike(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedEntity(expectedResponseBodyFile);

    List<Zoo> actuals = findZooIntoDatabase(context.getZooOrg(), context.getZooId());

    JSONAssert.assertEquals(expectedResponseBody, JsonUtils.asJson(actuals.get(0)), true);
  }

  private String getExpectedEntity(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("zooOrg", context.getZooOrg());
    expectedResponse.put("zooId", context.getZooId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  private List<Zoo> findZooIntoDatabase(Long zooOrg, Integer zooId) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooIds", Arrays.asList(zooId));

    return getTemplate().query(findZooIdsSql, paramMap, new ZooTestRowMapper());
  }

  @Then("I get the {string} header with the value {string}")
  public void iGetTheHeaderWithTheValue(String headerName, String expectedLocation) {
    String expectedHeaderLocation = buildExpectedLocation(expectedLocation);

    String actualHeaderLocation = context.getHeaders().getFirst(headerName);

    Assert.assertEquals(expectedHeaderLocation, actualHeaderLocation);
  }

  private String buildExpectedLocation(String expectedLocation) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(expectedLocation);
    PathVariables pathVariables = new PathVariables();
    pathVariables.add("port", port);
    pathVariables.add("zoo-org", context.getZooOrg());
    pathVariables.add("zoo-id", context.getZooId());
    return builder.buildAndExpand(pathVariables.toEncodedMap()).toUriString();
  }

  @Then("I get into the database the remaining Zoo resources {string}")
  public void iGetIntoTheDatabaseTheRemainingZooResources(String expectedRemainigRecordFiles) {
    String expectedRemainigRecordsJson = getExpectedEntities(expectedRemainigRecordFiles);
    List<Zoo> expectedRemainigRecords = JsonUtils.parseAsList(expectedRemainigRecordsJson, Zoo.class);
    List<Integer> zooIds = expectedRemainigRecords.stream().map(e -> e.getZooId()).collect(Collectors.toList());

    List<Zoo> actuals = findZoosIntoDatabase(context.getZooOrg(), zooIds);

    Assert.assertEquals(expectedRemainigRecords, actuals);

  }

  private String getExpectedEntities(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    List<Map<String, Object>> expectedResponses = JsonUtils.parseAsType(expectedResponseBody, ListOfMaps.class);
    expectedResponses.forEach(expectedResponse -> {
    expectedResponse.put("zooOrg", context.getZooOrg());
    });

    return JsonUtils.asJsonIncludeNull(expectedResponses);
  }

  private List<Zoo> findZoosIntoDatabase(Long zooOrg, List<Integer> zooIds) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);

    return getTemplate().query(findZooSql, paramMap, new ZooTestRowMapper());
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
