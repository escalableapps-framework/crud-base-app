package com.escalableapps.cmdb.baseapp.shared.restclient.model;

import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

public abstract class RestRequest {

  protected String uri;

  protected HttpMethod method;

  protected Headers headers;

  protected String jsonBody;

  protected boolean executed;

  public RestRequest uri(String uri) {
    return uri(uri, null, null);
  }

  public RestRequest uri(String uri, PathVariables pathVariables) {
    return uri(uri, pathVariables, null);
  }

  public RestRequest uri(String uri, QueryParameters queryParameters) {
    return uri(uri, null, queryParameters);
  }

  public RestRequest uri(String uri, PathVariables pathVariables, QueryParameters queryParameters) {
    pathVariables = pathVariables != null ? pathVariables : new PathVariables();
    queryParameters = queryParameters != null ? queryParameters : new QueryParameters();

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
    builder.queryParams(queryParameters.toEncodedMap());
    this.uri = builder.buildAndExpand(pathVariables.toEncodedMap()).toUriString();

    return this;
  }

  public RestRequest headers(Headers headers) {
    this.headers = headers;

    return this;
  }

  protected abstract RestResponse execute();

  public RestResponse send(String method) {
    this.method = HttpMethod.valueOf(method);
    return execute();
  }

  public RestResponse send(String method, String jsonBody) {
    this.method = HttpMethod.valueOf(method);
    this.jsonBody = jsonBody;
    return execute();
  }

  public RestResponse get() {
    method = HttpMethod.GET;
    return execute();
  }

  public RestResponse head() {
    method = HttpMethod.HEAD;
    return execute();
  }

  public RestResponse post(String jsonBody) {
    method = HttpMethod.POST;
    this.jsonBody = jsonBody;
    return execute();
  }

  public RestResponse put(String jsonBody) {
    method = HttpMethod.PUT;
    this.jsonBody = jsonBody;
    return execute();
  }

  public RestResponse patch(String jsonBody) {
    method = HttpMethod.PATCH;
    this.jsonBody = jsonBody;
    return execute();
  }

  public RestResponse delete(String jsonBody) {
    method = HttpMethod.DELETE;
    this.jsonBody = jsonBody;
    return execute();
  }

  @Override
  public String toString() {
    if (!executed) {
      return "RestRequest has not yet been executed";
    }
    StringBuilder builder = new StringBuilder();
    builder.append("curl '").append(uri).append("'");
    builder.append(" \\\n").append("-X '").append(method.name().toUpperCase()).append("'");
    headers.toMap().forEach((key, values) -> values.forEach(value -> builder.append(" \\\n").append("-H '").append(key).append(": ").append(value).append("'")));
    if (jsonBody != null) {
      builder.append(" \\\n").append("--data-raw '").append(jsonBody).append("'");
    }
    return builder.toString();
  }
}