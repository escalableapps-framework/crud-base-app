package com.escalableapps.cmdb.baseapp.main.template.transaction.service;

public interface TransactionBarService {

  int doubleTxWithFirstReversion();

  void doubleTxWithSecondReversion();

  void doubleTxWithBothReversion(int fooId);
}