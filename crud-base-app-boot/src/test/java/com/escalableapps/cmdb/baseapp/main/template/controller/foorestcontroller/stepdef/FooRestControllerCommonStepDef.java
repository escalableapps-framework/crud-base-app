package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.stepdef;

import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.ENDPONIT;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.RESOURCE_PATH;
import static com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext.STATUS;

import org.junit.Assert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import com.escalableapps.cmdb.baseapp.CrudBaseApp;
import com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.context.FooRestControllerContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = CrudBaseApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class FooRestControllerCommonStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<FooRestControllerContext> context;

  public FooRestControllerCommonStepDef(TestContext<FooRestControllerContext> context) {
    this.context = context;
  }

  @Dado("los archivos con datos para pruebas en la ruta: {string}")
  public void los_archivos_con_datos_para_pruebas_en_la_ruta(String resourcePath) {
    context.put(RESOURCE_PATH, resourcePath);
  }

  @Dado("el endpoint {string}")
  public void el_endpoint(String endpoint) {
    context.put(ENDPONIT, String.format("http://localhost:{port}%s", endpoint));
  }

  @Entonces("se obtiene en la respuesta el codigo de error {int}")
  public void se_obtiene_en_la_respuesta_el_codigo_de_error(Integer expectedStatus) {
    Integer status = context.get(STATUS);
    Assert.assertEquals(expectedStatus, status);
  }
}
