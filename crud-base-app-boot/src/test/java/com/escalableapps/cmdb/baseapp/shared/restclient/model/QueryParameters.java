package com.escalableapps.cmdb.baseapp.shared.restclient.model;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriUtils;

public class QueryParameters {

  private final HashMap<String, List<String>> params;

  public QueryParameters() {
    params = new HashMap<>();
  }

  private List<String> getParamList(String name) {
    List<String> values = params.get(name);
    if (values == null) {
      values = new ArrayList<>();
      params.put(name, values);
    }
    return values;
  }

  public QueryParameters add(String name, OffsetDateTime value) {
    add(name, value.format(ISO_OFFSET_DATE_TIME));
    return this;
  }

  public QueryParameters add(String name, LocalDate value) {
    add(name, value.format(ISO_LOCAL_DATE));
    return this;
  }

  public QueryParameters add(String name, Number value) {
    add(name, Objects.toString(value));
    return this;
  }

  public QueryParameters add(String name, String value) {
    getParamList(name).add(value);
    return this;
  }

  public List<String> get(String name) {
    List<String> clone = new ArrayList<>();
    clone.addAll(getParamList(name));
    return clone;
  }

  public String getFirst(String name) {
    List<String> values = getParamList(name);
    return values.size() == 0 ? null : values.get(0);
  }

  public QueryParameters clear(String name) {
    params.remove(name);
    return this;
  }

  LinkedMultiValueMap<String, String> toEncodedMap() {
    LinkedMultiValueMap<String, String> clone = new LinkedMultiValueMap<>();
    params.forEach((name, values) -> {
      String encodedName = UriUtils.encode(name, UTF_8);
      List<String> encodedValues = values.stream().map(value -> UriUtils.encode(value, UTF_8)).collect(Collectors.toList());
      clone.addAll(encodedName, encodedValues);
    });
    return clone;
  }
}
