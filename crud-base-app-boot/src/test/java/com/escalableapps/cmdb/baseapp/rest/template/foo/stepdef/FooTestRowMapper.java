package com.escalableapps.cmdb.baseapp.rest.template.foo.stepdef;

import java.math.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;

import org.springframework.jdbc.core.RowMapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public class FooTestRowMapper implements RowMapper<Foo> {

  @Override
  public Foo mapRow(ResultSet rs, int rowNum) throws SQLException {
    Foo foo = new Foo();
    foo.setFooId(rs.getObject("fooId", Integer.class));
    foo.setFooChar(rs.getObject("fooChar", String.class));
    foo.setFooVarchar(rs.getObject("fooVarchar", String.class));
    foo.setFooText(rs.getObject("fooText", String.class));
    foo.setFooSmallint(rs.getObject("fooSmallint", Short.class));
    foo.setFooInteger(rs.getObject("fooInteger", Integer.class));
    foo.setFooBigint(rs.getObject("fooBigint", Long.class));
    foo.setFooReal(rs.getObject("fooReal", Float.class));
    foo.setFooDouble(rs.getObject("fooDouble", Double.class));
    foo.setFooDecimal(rs.getObject("fooDecimal", BigDecimal.class));
    foo.setFooBoolean(rs.getObject("fooBoolean", Boolean.class));
    foo.setFooDate(rs.getObject("fooDate", LocalDate.class));
    foo.setFooTimestamp(rs.getObject("fooTimestamp", OffsetDateTime.class));
    return foo;
  }
}
