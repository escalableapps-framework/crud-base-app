package com.escalableapps.cmdb.baseapp.rest.template.foo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:rest/template/foo/feature", //
    glue = "classpath:com.escalableapps.cmdb.baseapp.rest.template.foo.stepdef", //
    monochrome = true //
)
public class FooEndpointTest {
}
