package com.escalableapps.cmdb.baseapp.rest.template.bar.stepdef;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.web.util.UriComponentsBuilder;

import com.escalableapps.cmdb.baseapp.TestCrudBaseApp;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.shared.dto.ListOfMaps;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.PathVariables;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.RestResponse;
import com.escalableapps.cmdb.baseapp.shared.restclient.util.RestClient;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Bar;
import com.escalableapps.cmdb.baseapp.template.rest.vo.bar.BarResponse;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;

@CucumberContextConfiguration
@SpringBootTest(classes = TestCrudBaseApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@Slf4j
public class BarEndpointStepDef {

  @LocalServerPort
  private int port;

  private final BarTestContext context;
  private final DataSource dataSource;
  private final String findBarSql;
  private final String findBarIdsSql;
  private final ResourceLoader resourceLoader;

  public BarEndpointStepDef(BarTestContext context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;
    this.findBarSql = ResourceUtils.readResource("classpath:rest/template/bar/sql/bar-test-find.sql");
    this.findBarIdsSql = ResourceUtils.readResource("classpath:rest/template/bar/sql/bar-test-findIds.sql");
    resourceLoader = new DefaultResourceLoader();
  }

  @Given("the path of the test scenario files {string}")
  public void thePathOfTheTestScenarioFiles(String pathData) {
    context.setPathData(pathData);
  }

  @Given("the empty bar table")
  public void theEmptyBarTable() {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource("classpath:rest/template/bar/sql/bar-truncate.sql"));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the initial data {string}")
  public void theInitialData(String initialData) {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource(initialData));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the bar-id value for in the url is {int}")
  public void theBarIdValueForInTheUrlIs(Integer barId) {
    context.setBarId(barId);
  }

  @Given("the endpoint {string}")
  public void theEndpoint(String endpoint) {
    context.setEndpoint(endpoint);
  }

  @Given("the query property {string}=null")
  public void theQueryPropertyNull(String parameter) {
    context.getParams().add(parameter, (String) null);
  }

  @Given("the query property {string}={string}")
  public void theQueryProperty(String parameter, String value) {
    context.getParams().add(parameter, value);
  }

  @Given("the sortProperty={string}")
  public void theSortProperty(String sortProperty) {
    context.getParams().add("sortProperty", sortProperty);
  }

  @Given("the sortAscendant={string}")
  public void theSortAscendant(String sortAscendant) {
    context.getParams().add("sortAscendant", sortAscendant);
  }

  @Given("the request body {string}")
  public void theRequestBody(String requestBodyFile) {
    String requestBody = ResourceUtils.readResource(context.getPathData(), requestBodyFile);
    context.setRequestBody(requestBody);
  }

  @When("a {string} request is made")
  public void aRequestIsMade(String method) {
    context.setMethod(method);
    try {
      String uri = "http://localhost:{port}" + context.getEndpoint();
      PathVariables pathVariables = new PathVariables();
      pathVariables.add("port", port);
      if (context.getBarId() != null) {
        pathVariables.add("bar-id", context.getBarId());
      }

      RestResponse response = RestClient.newRequest() //
          .uri(uri, pathVariables, context.getParams()) //
          .send(context.getMethod(), context.getRequestBody());

      context.setHeaders(response.getHeaders());
      context.setStatusCode(response.getStatus());
      context.setResponseBody(response.getBody());
      if ("POST".equals(method)) {
        BarResponse bar = JsonUtils.parseAsType(response.getBody(), BarResponse.class);
        context.setBarId(bar.getBarId());
      }
    } catch (Exception exception) {
      log.error(exception.getMessage(), exception);
    }
  }

  @Then("I get status code {int}")
  public void iGetStatusCode(Integer expectedStatusCode) {
    Assert.assertEquals(context.getResponseBody(), expectedStatusCode, context.getStatusCode());
  }

  @Then("I get the response body {string} with a new bar-id value")
  public void iGetTheResponseBodyWithANewBarIdValue(String expectedResponseBodyFile) throws JSONException {
    iGetTheResponseBody(expectedResponseBodyFile);
    Assert.assertNotNull(context.getBarId());
  }

  @Then("I get the response body {string}")
  public void iGetTheResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  private String getExpectedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("bar-id", context.getBarId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get the paged response body {string}")
  public void iGetThePagedResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedPagedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  @SuppressWarnings("unchecked")
  private String getExpectedPagedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    List<Map<String, Object>> expectedResults = (List<Map<String, Object>>) expectedResponse.get("results");
    expectedResults.forEach(result -> {
    });

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get into the database a resource Bar with the new bar-id and values like {string}")
  public void iGetIntoTheDatabaseAResourceBarWithTheNewBarIdAndValuesLike(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedEntity(expectedResponseBodyFile);

    List<Bar> actuals = findBarIntoDatabase(context.getBarId());

    JSONAssert.assertEquals(expectedResponseBody, JsonUtils.asJson(actuals.get(0)), true);
  }

  private String getExpectedEntity(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("barId", context.getBarId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  private List<Bar> findBarIntoDatabase(Integer barId) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("barIds", Arrays.asList(barId));

    return getTemplate().query(findBarIdsSql, paramMap, new BarTestRowMapper());
  }

  @Then("I get the {string} header with the value {string}")
  public void iGetTheHeaderWithTheValue(String headerName, String expectedLocation) {
    String expectedHeaderLocation = buildExpectedLocation(expectedLocation);

    String actualHeaderLocation = context.getHeaders().getFirst(headerName);

    Assert.assertEquals(expectedHeaderLocation, actualHeaderLocation);
  }

  private String buildExpectedLocation(String expectedLocation) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(expectedLocation);
    PathVariables pathVariables = new PathVariables();
    pathVariables.add("port", port);
    pathVariables.add("bar-id", context.getBarId());
    return builder.buildAndExpand(pathVariables.toEncodedMap()).toUriString();
  }

  @Then("I get into the database the remaining Bar resources {string}")
  public void iGetIntoTheDatabaseTheRemainingBarResources(String expectedRemainigRecordFiles) {
    String expectedRemainigRecordsJson = getExpectedEntities(expectedRemainigRecordFiles);
    List<Bar> expectedRemainigRecords = JsonUtils.parseAsList(expectedRemainigRecordsJson, Bar.class);
    List<Integer> barIds = expectedRemainigRecords.stream().map(e -> e.getBarId()).collect(Collectors.toList());

    List<Bar> actuals = findBarsIntoDatabase(barIds);

    Assert.assertEquals(expectedRemainigRecords, actuals);

  }

  private String getExpectedEntities(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    List<Map<String, Object>> expectedResponses = JsonUtils.parseAsType(expectedResponseBody, ListOfMaps.class);
    expectedResponses.forEach(expectedResponse -> {
    });

    return JsonUtils.asJsonIncludeNull(expectedResponses);
  }

  private List<Bar> findBarsIntoDatabase(List<Integer> barIds) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    return getTemplate().query(findBarSql, paramMap, new BarTestRowMapper());
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
