package com.escalableapps.cmdb.baseapp.rest.template.look.stepdef;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.web.util.UriComponentsBuilder;

import com.escalableapps.cmdb.baseapp.TestCrudBaseApp;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;
import com.escalableapps.cmdb.baseapp.shared.domain.util.JsonUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.shared.dto.ListOfMaps;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.PathVariables;
import com.escalableapps.cmdb.baseapp.shared.restclient.model.RestResponse;
import com.escalableapps.cmdb.baseapp.shared.restclient.util.RestClient;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Look;
import com.escalableapps.cmdb.baseapp.template.rest.vo.look.LookResponse;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;

@CucumberContextConfiguration
@SpringBootTest(classes = TestCrudBaseApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@Slf4j
public class LookEndpointStepDef {

  @LocalServerPort
  private int port;

  private final LookTestContext context;
  private final DataSource dataSource;
  private final String findLookSql;
  private final String findLookIdsSql;
  private final ResourceLoader resourceLoader;

  public LookEndpointStepDef(LookTestContext context, DataSource dataSource) {
    this.context = context;
    this.dataSource = dataSource;
    this.findLookSql = ResourceUtils.readResource("classpath:rest/template/look/sql/look-test-find.sql");
    this.findLookIdsSql = ResourceUtils.readResource("classpath:rest/template/look/sql/look-test-findIds.sql");
    resourceLoader = new DefaultResourceLoader();
  }

  @Given("the path of the test scenario files {string}")
  public void thePathOfTheTestScenarioFiles(String pathData) {
    context.setPathData(pathData);
  }

  @Given("the empty look table")
  public void theEmptyLookTable() {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource("classpath:rest/template/look/sql/look-truncate.sql"));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the initial data {string}")
  public void theInitialData(String initialData) {
    try (Connection con = dataSource.getConnection()) {
      ScriptUtils.executeSqlScript(con, resourceLoader.getResource(initialData));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EaFrameworkException(e);
    }
  }

  @Given("the look-org value for in the url is {long}")
  public void theLookOrgValueForInTheUrlIs(Long lookOrg) {
    context.setLookOrg(lookOrg);
  }

  @Given("the look-id value for in the url is {long}")
  public void theLookIdValueForInTheUrlIs(Long lookId) {
    context.setLookId(lookId);
  }

  @Given("the endpoint {string}")
  public void theEndpoint(String endpoint) {
    context.setEndpoint(endpoint);
  }

  @Given("the query property {string}=null")
  public void theQueryPropertyNull(String parameter) {
    context.getParams().add(parameter, (String) null);
  }

  @Given("the query property {string}={string}")
  public void theQueryProperty(String parameter, String value) {
    context.getParams().add(parameter, value);
  }

  @Given("the sortProperty={string}")
  public void theSortProperty(String sortProperty) {
    context.getParams().add("sortProperty", sortProperty);
  }

  @Given("the sortAscendant={string}")
  public void theSortAscendant(String sortAscendant) {
    context.getParams().add("sortAscendant", sortAscendant);
  }

  @Given("the request body {string}")
  public void theRequestBody(String requestBodyFile) {
    String requestBody = ResourceUtils.readResource(context.getPathData(), requestBodyFile);
    context.setRequestBody(requestBody);
  }

  @When("a {string} request is made")
  public void aRequestIsMade(String method) {
    context.setMethod(method);
    try {
      String uri = "http://localhost:{port}" + context.getEndpoint();
      PathVariables pathVariables = new PathVariables();
      pathVariables.add("port", port);
      pathVariables.add("look-org", context.getLookOrg());
      if (context.getLookId() != null) {
        pathVariables.add("look-id", context.getLookId());
      }

      RestResponse response = RestClient.newRequest() //
          .uri(uri, pathVariables, context.getParams()) //
          .send(context.getMethod(), context.getRequestBody());

      context.setHeaders(response.getHeaders());
      context.setStatusCode(response.getStatus());
      context.setResponseBody(response.getBody());
      if ("POST".equals(method)) {
        LookResponse look = JsonUtils.parseAsType(response.getBody(), LookResponse.class);
        context.setLookId(look.getLookId());
      }
    } catch (Exception exception) {
      log.error(exception.getMessage(), exception);
    }
  }

  @Then("I get status code {int}")
  public void iGetStatusCode(Integer expectedStatusCode) {
    Assert.assertEquals(context.getResponseBody(), expectedStatusCode, context.getStatusCode());
  }

  @Then("I get the response body {string} with a new look-id value")
  public void iGetTheResponseBodyWithANewLookIdValue(String expectedResponseBodyFile) throws JSONException {
    iGetTheResponseBody(expectedResponseBodyFile);
    Assert.assertNotNull(context.getLookId());
  }

  @Then("I get the response body {string}")
  public void iGetTheResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  private String getExpectedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("look-org", context.getLookOrg());
    expectedResponse.put("look-id", context.getLookId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get the paged response body {string}")
  public void iGetThePagedResponseBody(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedPagedResponseBody(expectedResponseBodyFile);

    JSONAssert.assertEquals(expectedResponseBody, context.getResponseBody(), true);
  }

  @SuppressWarnings("unchecked")
  private String getExpectedPagedResponseBody(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    List<Map<String, Object>> expectedResults = (List<Map<String, Object>>) expectedResponse.get("results");
    expectedResults.forEach(result -> {
      result.put("look-org", context.getLookOrg());
    });

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  @Then("I get into the database a resource Look with the new look-id and values like {string}")
  public void iGetIntoTheDatabaseAResourceLookWithTheNewLookIdAndValuesLike(String expectedResponseBodyFile) throws JSONException {
    String expectedResponseBody = getExpectedEntity(expectedResponseBodyFile);

    List<Look> actuals = findLookIntoDatabase(context.getLookOrg(), context.getLookId());

    JSONAssert.assertEquals(expectedResponseBody, JsonUtils.asJson(actuals.get(0)), true);
  }

  private String getExpectedEntity(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    Map<String, Object> expectedResponse = JsonUtils.parseAsMap(expectedResponseBody, String.class, Object.class);
    expectedResponse.put("lookOrg", context.getLookOrg());
    expectedResponse.put("lookId", context.getLookId());

    return JsonUtils.asJsonIncludeNull(expectedResponse);
  }

  private List<Look> findLookIntoDatabase(Long lookOrg, Long lookId) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookIds", Arrays.asList(lookId));

    return getTemplate().query(findLookIdsSql, paramMap, new LookTestRowMapper());
  }

  @Then("I get the {string} header with the value {string}")
  public void iGetTheHeaderWithTheValue(String headerName, String expectedLocation) {
    String expectedHeaderLocation = buildExpectedLocation(expectedLocation);

    String actualHeaderLocation = context.getHeaders().getFirst(headerName);

    Assert.assertEquals(expectedHeaderLocation, actualHeaderLocation);
  }

  private String buildExpectedLocation(String expectedLocation) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(expectedLocation);
    PathVariables pathVariables = new PathVariables();
    pathVariables.add("port", port);
    pathVariables.add("look-org", context.getLookOrg());
    pathVariables.add("look-id", context.getLookId());
    return builder.buildAndExpand(pathVariables.toEncodedMap()).toUriString();
  }

  @Then("I get into the database the remaining Look resources {string}")
  public void iGetIntoTheDatabaseTheRemainingLookResources(String expectedRemainigRecordFiles) {
    String expectedRemainigRecordsJson = getExpectedEntities(expectedRemainigRecordFiles);
    List<Look> expectedRemainigRecords = JsonUtils.parseAsList(expectedRemainigRecordsJson, Look.class);
    List<Long> lookIds = expectedRemainigRecords.stream().map(e -> e.getLookId()).collect(Collectors.toList());

    List<Look> actuals = findLooksIntoDatabase(context.getLookOrg(), lookIds);

    Assert.assertEquals(expectedRemainigRecords, actuals);

  }

  private String getExpectedEntities(String expectedResponseBodyFile) {
    String expectedResponseBody = ResourceUtils.readResource(context.getPathData(), expectedResponseBodyFile);
    List<Map<String, Object>> expectedResponses = JsonUtils.parseAsType(expectedResponseBody, ListOfMaps.class);
    expectedResponses.forEach(expectedResponse -> {
    expectedResponse.put("lookOrg", context.getLookOrg());
    });

    return JsonUtils.asJsonIncludeNull(expectedResponses);
  }

  private List<Look> findLooksIntoDatabase(Long lookOrg, List<Long> lookIds) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);

    return getTemplate().query(findLookSql, paramMap, new LookTestRowMapper());
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
