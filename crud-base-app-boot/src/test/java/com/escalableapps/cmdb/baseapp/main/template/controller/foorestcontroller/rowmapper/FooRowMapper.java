package com.escalableapps.cmdb.baseapp.main.template.controller.foorestcontroller.rowmapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.OffsetDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;

public class FooRowMapper implements RowMapper<Foo> {

  @Override
  public Foo mapRow(ResultSet rs, int rowNum) throws SQLException {
    Foo target = new Foo();
    target.setFooId(rs.getObject("fooId", Integer.class));
    target.setFooChar(rs.getObject("fooChar", String.class));
    target.setFooVarchar(rs.getObject("fooVarchar", String.class));
    target.setFooText(rs.getObject("fooText", String.class));
    target.setFooSmallint(rs.getObject("fooSmallint", Short.class));
    target.setFooInteger(rs.getObject("fooInteger", Integer.class));
    target.setFooBigint(rs.getObject("fooBigint", Long.class));
    target.setFooReal(rs.getObject("fooReal", Float.class));
    target.setFooDouble(rs.getObject("fooDouble", Double.class));
    target.setFooDecimal(rs.getObject("fooDecimal", BigDecimal.class));
    target.setFooBoolean(rs.getObject("fooBoolean", Boolean.class));
    target.setFooDate(rs.getObject("fooDate", LocalDate.class));
    target.setFooTimestamp(rs.getObject("fooTimestamp", OffsetDateTime.class));
    return target;
  }
}
