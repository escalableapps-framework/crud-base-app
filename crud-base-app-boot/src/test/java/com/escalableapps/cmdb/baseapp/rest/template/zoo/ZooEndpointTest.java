package com.escalableapps.cmdb.baseapp.rest.template.zoo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:rest/template/zoo/feature", //
    glue = "classpath:com.escalableapps.cmdb.baseapp.rest.template.zoo.stepdef", //
    monochrome = true //
)
public class ZooEndpointTest {
}
