package com.escalableapps.cmdb.baseapp.main;

import java.util.Locale;
import java.util.TimeZone;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.escalableapps.cmdb.baseapp.main.template.transaction.service.TransactionBarService;
import com.escalableapps.cmdb.baseapp.main.template.transaction.service.TransactionBarServiceImpl;
import com.escalableapps.cmdb.baseapp.main.template.transaction.service.TransactionFooServiceImpl;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

@Configuration
public class CrudBaseAppTestConfig {

  static {
    Locale.setDefault(Locale.ENGLISH);
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }

  @Bean
  public TransactionFooServiceImpl transactionFooServiceImpl(FooSpi fooSpi, TransactionBarService barService) {
    return new TransactionFooServiceImpl(fooSpi, barService);
  }

  @Bean
  public TransactionBarServiceImpl transactionBarServiceImpl(BarSpi barSpi) {
    return new TransactionBarServiceImpl(barSpi);
  }
}
