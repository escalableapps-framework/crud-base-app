package com.escalableapps.cmdb.baseapp.shared.restclient.model;

import com.escalableapps.cmdb.baseapp.shared.restclient.model.Constants.ResponseStatus;

import lombok.Data;

@Data
public class RestResponse {

  private ResponseStatus responseStatus;

  private int status;

  private Headers headers;

  private String body;

  private String errorMessage;

}
