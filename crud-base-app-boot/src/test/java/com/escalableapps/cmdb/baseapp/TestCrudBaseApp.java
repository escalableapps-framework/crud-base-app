package com.escalableapps.cmdb.baseapp;

import java.util.Locale;
import java.util.TimeZone;

public class TestCrudBaseApp extends CrudBaseApp {

  static {
    Locale.setDefault(Locale.ENGLISH);
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }

}
