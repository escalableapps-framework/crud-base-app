package com.escalableapps.cmdb.baseapp.main.template.transaction.service;

import javax.transaction.Transactional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.ConflictException;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
public class TransactionFooServiceImpl implements TransactionFooService {

  private FooSpi fooSpi;
  private TransactionBarService barService;

  public TransactionFooServiceImpl(FooSpi fooSpi, TransactionBarService barService) {
    this.fooSpi = fooSpi;
    this.barService = barService;
  }

  @Override
  public void singleTxWithReversion() {
    Foo fooToCreate = new Foo();
    Foo fooCreated = fooSpi.createFoo(fooToCreate);
    int id = fooCreated.getFooId();
    throw new ConflictException("%s", id);
  }

  @Override
  public int singleTxWithoutReversion() {
    Foo fooToCreate = new Foo();
    Foo fooCreated = fooSpi.createFoo(fooToCreate);
    int id = fooCreated.getFooId();
    return id;
  }

  @Override
  public void doubleTxWithFirstReversion() {
    Foo fooToCreate = new Foo();
    Foo fooCreated = fooSpi.createFoo(fooToCreate);
    int fooId = fooCreated.getFooId();
    int barId = barService.doubleTxWithFirstReversion();
    throw new ConflictException("%s,%s", fooId, barId);
  }

  @Override
  public String doubleTxWithSecondReversion() {
    Foo fooToCreate = new Foo();
    Foo fooCreated = fooSpi.createFoo(fooToCreate);
    int fooId = fooCreated.getFooId();
    try {
      barService.doubleTxWithSecondReversion();
    } catch (Exception e) {
      log.warn(e.getMessage(), e);
      return String.format("%s,%s", fooId, e.getMessage());
    }
    return null;
  }

  @Override
  public void doubleTxWithBothReversion() {
    Foo fooToCreate = new Foo();
    Foo fooCreated = fooSpi.createFoo(fooToCreate);
    int fooId = fooCreated.getFooId();
    barService.doubleTxWithBothReversion(fooId);
  }
}
