
INSERT INTO crud_base_db.foo
(foo_id, foo_char, foo_varchar, foo_text, foo_smallint, foo_integer, foo_bigint, foo_real, foo_double, foo_decimal, foo_boolean, foo_date, foo_timestamp) VALUES 
(1, null, null, null, null, null, null, null, null, null, null, null, null),
(2, 'AA', null, null, null, null, null, null, null, null, null, null, null),
(3, null, 'AAAA', null, null, null, null, null, null, null, null, null, null),
(4, null, null, 'A', null, null, null, null, null, null, null, null, null),
(5, null, null, null, 32767, null, null, null, null, null, null, null, null),
(6, null, null, null, null, 2147483647, null, null, null, null, null, null, null),
(7, null, null, null, null, null, 9223372036854775807, null, null, null, null, null, null),
(8, null, null, null, null, null, null, 3.4028235E+38, null, null, null, null, null),
(9, null, null, null, null, null, null, null, 1.7976931348623157E+308, null, null, null, null),
(10, null, null, null, null, null, null, null, null, 99.1111, null, null, null),
(11, null, null, null, null, null, null, null, null, null, true, null, null),
(12, null, null, null, null, null, null, null, null, null, null, '1850-01-01', null),
(13, null, null, null, null, null, null, null, null, null, null, null, '1850-01-01 00:00:00.000+00'),
(14, null, null, null, null, null, null, null, null, null, null, '5874897-12-31', null),
(15, null, null, null, null, null, null, null, null, null, null, null, '2050-12-31 23:59:59.999+00');
