Feature: Delete an Foo resource
  In order to delete an Foo resource
  As a client of the rest api
  I want an endpoint that receives the delete action for an Foo resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/foo/data/delete'
    And the empty foo table
    And the initial data 'classpath:rest/template/foo/sql/foo-initial-delete-data.sql'

  Scenario Outline: Sending an existing foo-id=<id> to the endpoint, the resource Foo is removed from database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is <id>
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Foo resources <remaining records>  

    Examples: 
      | id | remaining records                      |
      |  1 | 'foo-delete-success-remaining-01.json' |
      |  2 | 'foo-delete-success-remaining-02.json' |
      |  3 | 'foo-delete-success-remaining-03.json' |
      |  4 | 'foo-delete-success-remaining-04.json' |
      |  5 | 'foo-delete-success-remaining-05.json' |
      |  6 | 'foo-delete-success-remaining-06.json' |
      |  7 | 'foo-delete-success-remaining-07.json' |
      |  8 | 'foo-delete-success-remaining-08.json' |
      |  9 | 'foo-delete-success-remaining-09.json' |
      | 10 | 'foo-delete-success-remaining-10.json' |
      | 11 | 'foo-delete-success-remaining-11.json' |
      | 12 | 'foo-delete-success-remaining-12.json' |
      | 13 | 'foo-delete-success-remaining-13.json' |
      | 14 | 'foo-delete-success-remaining-14.json' |
      | 15 | 'foo-delete-success-remaining-15.json' |

  Scenario: By sending a non-existent foo-id to the endpoint, no Foo resources are removed from the database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is 10000
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Foo resources 'foo-delete-success-remaining-all.json'
