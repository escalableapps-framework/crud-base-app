Feature: Get an Foo resource by id
  In order to get an Foo resource
  As a client of the rest api
  I want an endpoint that receives the get action for an Foo resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/foo/data/get'
    And the empty foo table
    And the initial data 'classpath:rest/template/foo/sql/foo-initial-get-data.sql'

  Scenario Outline: Sending an existing foo-id=<id> to the endpoint, the resource Foo is fetch from database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is <id>
    When a 'GET' request is made
    Then I get status code 200
    And I get the response body <response body>  

    Examples: 
      | id | response body                            |
      |  1 | 'foo-get-by-id-success-response-01.json' |
      |  2 | 'foo-get-by-id-success-response-02.json' |
      |  3 | 'foo-get-by-id-success-response-03.json' |
      |  4 | 'foo-get-by-id-success-response-04.json' |
      |  5 | 'foo-get-by-id-success-response-05.json' |
      |  6 | 'foo-get-by-id-success-response-06.json' |
      |  7 | 'foo-get-by-id-success-response-07.json' |
      |  8 | 'foo-get-by-id-success-response-08.json' |
      |  9 | 'foo-get-by-id-success-response-09.json' |
      | 10 | 'foo-get-by-id-success-response-10.json' |
      | 11 | 'foo-get-by-id-success-response-11.json' |
      | 12 | 'foo-get-by-id-success-response-12.json' |
      | 13 | 'foo-get-by-id-success-response-13.json' |
      | 14 | 'foo-get-by-id-success-response-14.json' |
      | 15 | 'foo-get-by-id-success-response-15.json' |

  Scenario: By sending a non-existent foo-id to the endpoint, does not fetch any Foo resources from the database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is 10000
    When a 'GET' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameters sortProperty=<sort property> and sortAscendant=<sort ascendant> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/foo"
    And the sortProperty=<sort property>
    And the sortAscendant=<sort ascendant>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | sort property | sort ascendant | response body                             |
      | 'foo-id'      | 'true'         | 'foo-get-sorted-success-response-01.json' |
      | 'foo-id'      | 'false'        | 'foo-get-sorted-success-response-02.json' |

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/foo"
    And the query property <query property>=<property value>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | query property  | property value             | response body                            |
      | 'foo-char'      | null                       | 'foo-get-query-success-response-01.json' |
      | 'foo-char'      | 'AA'                       | 'foo-get-query-success-response-02.json' |
      | 'foo-char'      | 'a'                        | 'foo-get-query-success-response-03.json' |
      | 'foo-varchar'   | null                       | 'foo-get-query-success-response-04.json' |
      | 'foo-varchar'   | 'AAAA'                     | 'foo-get-query-success-response-05.json' |
      | 'foo-varchar'   | 'a'                        | 'foo-get-query-success-response-06.json' |
      | 'foo-text'      | null                       | 'foo-get-query-success-response-07.json' |
      | 'foo-text'      | 'A'                        | 'foo-get-query-success-response-08.json' |
      | 'foo-text'      | 'a'                        | 'foo-get-query-success-response-09.json' |
      | 'foo-smallint'  | null                       | 'foo-get-query-success-response-10.json' |
      | 'foo-smallint'  | '32767'                    | 'foo-get-query-success-response-11.json' |
      | 'foo-smallint'  | '1'                        | 'foo-get-query-success-response-12.json' |
      | 'foo-integer'   | null                       | 'foo-get-query-success-response-13.json' |
      | 'foo-integer'   | '2147483647'               | 'foo-get-query-success-response-14.json' |
      | 'foo-integer'   | '1'                        | 'foo-get-query-success-response-15.json' |
      | 'foo-bigint'    | null                       | 'foo-get-query-success-response-16.json' |
      | 'foo-bigint'    | '9223372036854775807'      | 'foo-get-query-success-response-17.json' |
      | 'foo-bigint'    | '1'                        | 'foo-get-query-success-response-18.json' |
      | 'foo-real'      | null                       | 'foo-get-query-success-response-19.json' |
      | 'foo-real'      | '3.4028235E+38'            | 'foo-get-query-success-response-20.json' |
      | 'foo-real'      | '1'                        | 'foo-get-query-success-response-21.json' |
      | 'foo-double'    | null                       | 'foo-get-query-success-response-22.json' |
      | 'foo-double'    | '1.7976931348623157E+308'  | 'foo-get-query-success-response-23.json' |
      | 'foo-double'    | '1'                        | 'foo-get-query-success-response-24.json' |
      | 'foo-decimal'   | null                       | 'foo-get-query-success-response-25.json' |
      | 'foo-decimal'   | '99.1111'                  | 'foo-get-query-success-response-26.json' |
      | 'foo-decimal'   | '1'                        | 'foo-get-query-success-response-27.json' |
      | 'foo-boolean'   | null                       | 'foo-get-query-success-response-28.json' |
      | 'foo-boolean'   | 'true'                     | 'foo-get-query-success-response-29.json' |
      | 'foo-boolean'   | 'false'                    | 'foo-get-query-success-response-30.json' |
      | 'foo-date'      | null                       | 'foo-get-query-success-response-31.json' |
      | 'foo-date'      | '1850-01-01'               | 'foo-get-query-success-response-32.json' |
      | 'foo-date'      | '2000-01-01'               | 'foo-get-query-success-response-33.json' |
      | 'foo-date'      | '+5874897-12-31'           | 'foo-get-query-success-response-34.json' |
      | 'foo-timestamp' | null                       | 'foo-get-query-success-response-35.json' |
      | 'foo-timestamp' | '1850-01-01T00:00:00Z'     | 'foo-get-query-success-response-36.json' |
      | 'foo-timestamp' | '2000-01-01T00:00:00Z'     | 'foo-get-query-success-response-37.json' |
      | 'foo-timestamp' | '2050-12-31T23:59:59.999Z' | 'foo-get-query-success-response-38.json' |
