Feature: Create an Foo resource
  In order to create an Foo resource
  As a client of the rest api
  I want an endpoint that receives the post action for resource Foo

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/foo/data/post'
    And the empty foo table

  Scenario Outline: Sending the json <request body> to the endpoint, the resource Foo is successfully registered
    Given the endpoint "/api/v1/foo"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 201
    And I get the response body <response body> with a new foo-id value
    And I get into the database a resource Foo with the new foo-id and values like <registered entity>  
    And I get the 'Location' header with the value 'http://localhost:{port}/api/v1/foo/{foo-id}'

    Examples: 
      | request body                       | response body                       | registered entity                 |
      | 'foo-post-success-request-01.json' | 'foo-post-success-response-01.json' | 'foo-post-success-entity-01.json' |
      | 'foo-post-success-request-02.json' | 'foo-post-success-response-02.json' | 'foo-post-success-entity-02.json' |
      | 'foo-post-success-request-03.json' | 'foo-post-success-response-03.json' | 'foo-post-success-entity-03.json' |
      | 'foo-post-success-request-04.json' | 'foo-post-success-response-04.json' | 'foo-post-success-entity-04.json' |
      | 'foo-post-success-request-05.json' | 'foo-post-success-response-05.json' | 'foo-post-success-entity-05.json' |
      | 'foo-post-success-request-06.json' | 'foo-post-success-response-06.json' | 'foo-post-success-entity-06.json' |
      | 'foo-post-success-request-07.json' | 'foo-post-success-response-07.json' | 'foo-post-success-entity-07.json' |
      | 'foo-post-success-request-08.json' | 'foo-post-success-response-08.json' | 'foo-post-success-entity-08.json' |
      | 'foo-post-success-request-09.json' | 'foo-post-success-response-09.json' | 'foo-post-success-entity-09.json' |
      | 'foo-post-success-request-10.json' | 'foo-post-success-response-10.json' | 'foo-post-success-entity-10.json' |
      | 'foo-post-success-request-11.json' | 'foo-post-success-response-11.json' | 'foo-post-success-entity-11.json' |
      | 'foo-post-success-request-12.json' | 'foo-post-success-response-12.json' | 'foo-post-success-entity-12.json' |
      | 'foo-post-success-request-13.json' | 'foo-post-success-response-13.json' | 'foo-post-success-entity-13.json' |
      | 'foo-post-success-request-14.json' | 'foo-post-success-response-14.json' | 'foo-post-success-entity-14.json' |
      | 'foo-post-success-request-15.json' | 'foo-post-success-response-15.json' | 'foo-post-success-entity-15.json' |
      | 'foo-post-success-request-16.json' | 'foo-post-success-response-16.json' | 'foo-post-success-entity-16.json' |
      | 'foo-post-success-request-17.json' | 'foo-post-success-response-17.json' | 'foo-post-success-entity-17.json' |
      | 'foo-post-success-request-18.json' | 'foo-post-success-response-18.json' | 'foo-post-success-entity-18.json' |
      | 'foo-post-success-request-19.json' | 'foo-post-success-response-19.json' | 'foo-post-success-entity-19.json' |
      | 'foo-post-success-request-20.json' | 'foo-post-success-response-20.json' | 'foo-post-success-entity-20.json' |
      | 'foo-post-success-request-21.json' | 'foo-post-success-response-21.json' | 'foo-post-success-entity-21.json' |
      | 'foo-post-success-request-22.json' | 'foo-post-success-response-22.json' | 'foo-post-success-entity-22.json' |
      | 'foo-post-success-request-23.json' | 'foo-post-success-response-23.json' | 'foo-post-success-entity-23.json' |
      | 'foo-post-success-request-24.json' | 'foo-post-success-response-24.json' | 'foo-post-success-entity-24.json' |
      | 'foo-post-success-request-25.json' | 'foo-post-success-response-25.json' | 'foo-post-success-entity-25.json' |
      | 'foo-post-success-request-26.json' | 'foo-post-success-response-26.json' | 'foo-post-success-entity-26.json' |
      | 'foo-post-success-request-27.json' | 'foo-post-success-response-27.json' | 'foo-post-success-entity-27.json' |

  Scenario Outline: Sending the json <request body> to the endpoint, the registration of resource Foo fails
    Given the endpoint "/api/v1/foo"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 400

    Examples: 
      | request body                    |
      | 'foo-post-fail-request-01.json' |
      | 'foo-post-fail-request-02.json' |
      | 'foo-post-fail-request-03.json' |
      | 'foo-post-fail-request-04.json' |
      | 'foo-post-fail-request-05.json' |
      | 'foo-post-fail-request-06.json' |
      | 'foo-post-fail-request-07.json' |
      | 'foo-post-fail-request-08.json' |
      | 'foo-post-fail-request-09.json' |
      | 'foo-post-fail-request-10.json' |
