Feature: Check the availability of an Foo resource by id
  In order to head an Foo resource
  As a client of the rest api
  I want an endpoint that receives the head action for an Foo resource

  Background: 
    Given the empty foo table
    And the initial data 'classpath:rest/template/foo/sql/foo-initial-head-data.sql'

  Scenario Outline: Sending an existing foo-id=<id> to the endpoint, the resource Foo is checked for availability from the database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is <id>
    When a 'HEAD' request is made
    Then I get status code 204

    Examples: 
      | id |
      |  1 |
      |  2 |
      |  3 |
      |  4 |
      |  5 |
      |  6 |
      |  7 |
      |  8 |
      |  9 |
      | 10 |
      | 11 |
      | 12 |
      | 13 |
      | 14 |
      | 15 |

  Scenario: By sending a non-existent foo-id to the endpoint, the availability of the Foo resource is not verified from the database
    Given the endpoint "/api/v1/foo/{foo-id}"
    And the foo-id value for in the url is 10000
    When a 'HEAD' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/foo"
    And the query property <query property>=<property value>
    When a 'HEAD' request is made
    Then I get status code <status code>

    Examples: 
      | query property  | property value             | status code |
      | 'foo-char'      | null                       | 204         |
      | 'foo-char'      | 'AA'                       | 204         |
      | 'foo-char'      | 'a'                        | 404         |
      | 'foo-varchar'   | null                       | 204         |
      | 'foo-varchar'   | 'AAAA'                     | 204         |
      | 'foo-varchar'   | 'a'                        | 404         |
      | 'foo-text'      | null                       | 204         |
      | 'foo-text'      | 'A'                        | 204         |
      | 'foo-text'      | 'a'                        | 404         |
      | 'foo-smallint'  | null                       | 204         |
      | 'foo-smallint'  | '32767'                    | 204         |
      | 'foo-smallint'  | '1'                        | 404         |
      | 'foo-integer'   | null                       | 204         |
      | 'foo-integer'   | '2147483647'               | 204         |
      | 'foo-integer'   | '1'                        | 404         |
      | 'foo-bigint'    | null                       | 204         |
      | 'foo-bigint'    | '9223372036854775807'      | 204         |
      | 'foo-bigint'    | '1'                        | 404         |
      | 'foo-real'      | null                       | 204         |
      | 'foo-real'      | '3.4028235E+38'            | 204         |
      | 'foo-real'      | '1'                        | 404         |
      | 'foo-double'    | null                       | 204         |
      | 'foo-double'    | '1.7976931348623157E+308'  | 204         |
      | 'foo-double'    | '1'                        | 404         |
      | 'foo-decimal'   | null                       | 204         |
      | 'foo-decimal'   | '99.1111'                  | 204         |
      | 'foo-decimal'   | '1'                        | 404         |
      | 'foo-boolean'   | null                       | 204         |
      | 'foo-boolean'   | 'true'                     | 204         |
      | 'foo-boolean'   | 'false'                    | 404         |
      | 'foo-date'      | null                       | 204         |
      | 'foo-date'      | '1850-01-01'               | 204         |
      | 'foo-date'      | '2000-01-01'               | 404         |
      | 'foo-date'      | '+5874897-12-31'           | 204         |
      | 'foo-timestamp' | null                       | 204         |
      | 'foo-timestamp' | '1850-01-01T00:00:00Z'     | 204         |
      | 'foo-timestamp' | '2000-01-01T00:00:00Z'     | 404         |
      | 'foo-timestamp' | '2050-12-31T23:59:59.999Z' | 204         |
