SET SESSION vars.lookOrg = 1;

INSERT INTO crud_base_db.look
(look_org, look_id, look_char, look_varchar, look_text, look_smallint, look_integer, look_bigint, look_real, look_double, look_decimal, look_boolean, look_date, look_timestamp) VALUES 
(current_setting('vars.lookOrg')::bigint, 1, 'A', 'A', 'A', 32767, 2147483647, 9223372036854775807, 3.4028235E+38, 1.7976931348623157E+308, 999999.111111, true, '1850-01-01', '1850-01-01 00:00:00.000000+00'),
(current_setting('vars.lookOrg')::bigint, 2, 'A', 'A', 'A', 32767, 2147483647, 9223372036854775807, 3.4028235E+38, 1.7976931348623157E+308, 999999.111111, true, '5874897-12-31', '1850-01-01 00:00:00.000000+00'),
(current_setting('vars.lookOrg')::bigint, 3, 'A', 'A', 'A', 32767, 2147483647, 9223372036854775807, 3.4028235E+38, 1.7976931348623157E+308, 999999.111111, true, '1850-01-01', '2050-12-31 23:59:59.999000+00');
