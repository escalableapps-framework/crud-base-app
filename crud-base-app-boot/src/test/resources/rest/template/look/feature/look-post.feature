Feature: Create an Look resource
  In order to create an Look resource
  As a client of the rest api
  I want an endpoint that receives the post action for resource Look

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/look/data/post'
    And the empty look table
    And the look-org value for in the url is 1

  Scenario Outline: Sending the json <request body> to the endpoint, the resource Look is successfully registered
    Given the endpoint "/api/v1/lookOrg/{look-org}/look"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 201
    And I get the response body <response body> with a new look-id value
    And I get into the database a resource Look with the new look-id and values like <registered entity>  
    And I get the 'Location' header with the value 'http://localhost:{port}/api/v1/lookOrg/{look-org}/look/{look-id}'

    Examples: 
      | request body                       | response body                       | registered entity                 |
      | 'look-post-success-request-1.json' | 'look-post-success-response-1.json' | 'look-post-success-entity-1.json' |
      | 'look-post-success-request-2.json' | 'look-post-success-response-2.json' | 'look-post-success-entity-2.json' |
      | 'look-post-success-request-3.json' | 'look-post-success-response-3.json' | 'look-post-success-entity-3.json' |

  Scenario Outline: Sending the json <request body> to the endpoint, the registration of resource Look fails
    Given the endpoint "/api/v1/lookOrg/{look-org}/look"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 400

    Examples: 
      | request body                     |
      | 'look-post-fail-request-01.json' |
      | 'look-post-fail-request-02.json' |
      | 'look-post-fail-request-03.json' |
      | 'look-post-fail-request-04.json' |
      | 'look-post-fail-request-05.json' |
      | 'look-post-fail-request-06.json' |
      | 'look-post-fail-request-07.json' |
      | 'look-post-fail-request-08.json' |
      | 'look-post-fail-request-09.json' |
      | 'look-post-fail-request-10.json' |
      | 'look-post-fail-request-11.json' |
      | 'look-post-fail-request-12.json' |
      | 'look-post-fail-request-13.json' |
      | 'look-post-fail-request-14.json' |
      | 'look-post-fail-request-15.json' |
      | 'look-post-fail-request-16.json' |
      | 'look-post-fail-request-17.json' |
      | 'look-post-fail-request-18.json' |
      | 'look-post-fail-request-19.json' |
      | 'look-post-fail-request-20.json' |
      | 'look-post-fail-request-21.json' |
      | 'look-post-fail-request-22.json' |
      | 'look-post-fail-request-23.json' |
      | 'look-post-fail-request-24.json' |
      | 'look-post-fail-request-25.json' |
      | 'look-post-fail-request-26.json' |
      | 'look-post-fail-request-27.json' |
      | 'look-post-fail-request-28.json' |
      | 'look-post-fail-request-29.json' |
