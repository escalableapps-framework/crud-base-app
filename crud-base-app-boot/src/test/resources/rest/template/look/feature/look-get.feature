Feature: Get an Look resource by id
  In order to get an Look resource
  As a client of the rest api
  I want an endpoint that receives the get action for an Look resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/look/data/get'
    And the empty look table
    And the initial data 'classpath:rest/template/look/sql/look-initial-get-data.sql'
    And the look-org value for in the url is 1

  Scenario Outline: Sending an existing look-id=<id> to the endpoint, the resource Look is fetch from database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is <id>
    When a 'GET' request is made
    Then I get status code 200
    And I get the response body <response body>  

    Examples: 
      | id | response body                            |
      |  1 | 'look-get-by-id-success-response-1.json' |
      |  2 | 'look-get-by-id-success-response-2.json' |
      |  3 | 'look-get-by-id-success-response-3.json' |

  Scenario: By sending a non-existent look-id to the endpoint, does not fetch any Look resources from the database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is 10000
    When a 'GET' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameters sortProperty=<sort property> and sortAscendant=<sort ascendant> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/lookOrg/{look-org}/look"
    And the sortProperty=<sort property>
    And the sortAscendant=<sort ascendant>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | sort property | sort ascendant | response body                              |
      | 'look-id'     | 'true'         | 'look-get-sorted-success-response-01.json' |
      | 'look-id'     | 'false'        | 'look-get-sorted-success-response-02.json' |

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/lookOrg/{look-org}/look"
    And the query property <query property>=<property value>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | query property   | property value             | response body                             |
      | 'look-char'      | null                       | 'look-get-query-success-response-01.json' |
      | 'look-char'      | 'A'                        | 'look-get-query-success-response-02.json' |
      | 'look-char'      | 'a'                        | 'look-get-query-success-response-03.json' |
      | 'look-varchar'   | null                       | 'look-get-query-success-response-04.json' |
      | 'look-varchar'   | 'A'                        | 'look-get-query-success-response-05.json' |
      | 'look-varchar'   | 'a'                        | 'look-get-query-success-response-06.json' |
      | 'look-text'      | null                       | 'look-get-query-success-response-07.json' |
      | 'look-text'      | 'A'                        | 'look-get-query-success-response-08.json' |
      | 'look-text'      | 'a'                        | 'look-get-query-success-response-09.json' |
      | 'look-smallint'  | null                       | 'look-get-query-success-response-10.json' |
      | 'look-smallint'  | '32767'                    | 'look-get-query-success-response-11.json' |
      | 'look-smallint'  | '1'                        | 'look-get-query-success-response-12.json' |
      | 'look-integer'   | null                       | 'look-get-query-success-response-13.json' |
      | 'look-integer'   | '2147483647'               | 'look-get-query-success-response-14.json' |
      | 'look-integer'   | '1'                        | 'look-get-query-success-response-15.json' |
      | 'look-bigint'    | null                       | 'look-get-query-success-response-16.json' |
      | 'look-bigint'    | '9223372036854775807'      | 'look-get-query-success-response-17.json' |
      | 'look-bigint'    | '1'                        | 'look-get-query-success-response-18.json' |
      | 'look-real'      | null                       | 'look-get-query-success-response-19.json' |
      | 'look-real'      | '3.4028235E+38'            | 'look-get-query-success-response-20.json' |
      | 'look-real'      | '1'                        | 'look-get-query-success-response-21.json' |
      | 'look-double'    | null                       | 'look-get-query-success-response-22.json' |
      | 'look-double'    | '1.7976931348623157E+308'  | 'look-get-query-success-response-23.json' |
      | 'look-double'    | '1'                        | 'look-get-query-success-response-24.json' |
      | 'look-decimal'   | null                       | 'look-get-query-success-response-25.json' |
      | 'look-decimal'   | '999999.111111'            | 'look-get-query-success-response-26.json' |
      | 'look-decimal'   | '1'                        | 'look-get-query-success-response-27.json' |
      | 'look-boolean'   | null                       | 'look-get-query-success-response-28.json' |
      | 'look-boolean'   | 'true'                     | 'look-get-query-success-response-29.json' |
      | 'look-boolean'   | 'false'                    | 'look-get-query-success-response-30.json' |
      | 'look-date'      | null                       | 'look-get-query-success-response-31.json' |
      | 'look-date'      | '1850-01-01'               | 'look-get-query-success-response-32.json' |
      | 'look-date'      | '2000-01-01'               | 'look-get-query-success-response-33.json' |
      | 'look-date'      | '+5874897-12-31'           | 'look-get-query-success-response-34.json' |
      | 'look-timestamp' | null                       | 'look-get-query-success-response-35.json' |
      | 'look-timestamp' | '1850-01-01T00:00:00Z'     | 'look-get-query-success-response-36.json' |
      | 'look-timestamp' | '2000-01-01T00:00:00Z'     | 'look-get-query-success-response-37.json' |
      | 'look-timestamp' | '2050-12-31T23:59:59.999Z' | 'look-get-query-success-response-38.json' |
