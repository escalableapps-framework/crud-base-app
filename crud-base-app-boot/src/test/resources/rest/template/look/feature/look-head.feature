Feature: Check the availability of an Look resource by id
  In order to head an Look resource
  As a client of the rest api
  I want an endpoint that receives the head action for an Look resource

  Background: 
    Given the empty look table
    And the initial data 'classpath:rest/template/look/sql/look-initial-head-data.sql'
    And the look-org value for in the url is 1

  Scenario Outline: Sending an existing look-id=<id> to the endpoint, the resource Look is checked for availability from the database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is <id>
    When a 'HEAD' request is made
    Then I get status code 204

    Examples: 
      | id |
      |  1 |
      |  2 |
      |  3 |

  Scenario: By sending a non-existent look-id to the endpoint, the availability of the Look resource is not verified from the database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is 10000
    When a 'HEAD' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/lookOrg/{look-org}/look"
    And the query property <query property>=<property value>
    When a 'HEAD' request is made
    Then I get status code <status code>

    Examples: 
      | query property   | property value             | status code |
      | 'look-char'      | null                       | 404         |
      | 'look-char'      | 'A'                        | 204         |
      | 'look-char'      | 'a'                        | 404         |
      | 'look-varchar'   | null                       | 404         |
      | 'look-varchar'   | 'A'                        | 204         |
      | 'look-varchar'   | 'a'                        | 404         |
      | 'look-text'      | null                       | 404         |
      | 'look-text'      | 'A'                        | 204         |
      | 'look-text'      | 'a'                        | 404         |
      | 'look-smallint'  | null                       | 404         |
      | 'look-smallint'  | '32767'                    | 204         |
      | 'look-smallint'  | '1'                        | 404         |
      | 'look-integer'   | null                       | 404         |
      | 'look-integer'   | '2147483647'               | 204         |
      | 'look-integer'   | '1'                        | 404         |
      | 'look-bigint'    | null                       | 404         |
      | 'look-bigint'    | '9223372036854775807'      | 204         |
      | 'look-bigint'    | '1'                        | 404         |
      | 'look-real'      | null                       | 404         |
      | 'look-real'      | '3.4028235E+38'            | 204         |
      | 'look-real'      | '1'                        | 404         |
      | 'look-double'    | null                       | 404         |
      | 'look-double'    | '1.7976931348623157E+308'  | 204         |
      | 'look-double'    | '1'                        | 404         |
      | 'look-decimal'   | null                       | 404         |
      | 'look-decimal'   | '999999.111111'            | 204         |
      | 'look-decimal'   | '1'                        | 404         |
      | 'look-boolean'   | null                       | 404         |
      | 'look-boolean'   | 'true'                     | 204         |
      | 'look-boolean'   | 'false'                    | 404         |
      | 'look-date'      | null                       | 404         |
      | 'look-date'      | '1850-01-01'               | 204         |
      | 'look-date'      | '2000-01-01'               | 404         |
      | 'look-date'      | '+5874897-12-31'           | 204         |
      | 'look-timestamp' | null                       | 404         |
      | 'look-timestamp' | '1850-01-01T00:00:00Z'     | 204         |
      | 'look-timestamp' | '2000-01-01T00:00:00Z'     | 404         |
      | 'look-timestamp' | '2050-12-31T23:59:59.999Z' | 204         |
