Feature: Delete an Look resource
  In order to delete an Look resource
  As a client of the rest api
  I want an endpoint that receives the delete action for an Look resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/look/data/delete'
    And the empty look table
    And the initial data 'classpath:rest/template/look/sql/look-initial-delete-data.sql'
    And the look-org value for in the url is 1

  Scenario Outline: Sending an existing look-id=<id> to the endpoint, the resource Look is removed from database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is <id>
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Look resources <remaining records>  

    Examples: 
      | id | remaining records                      |
      |  1 | 'look-delete-success-remaining-1.json' |
      |  2 | 'look-delete-success-remaining-2.json' |
      |  3 | 'look-delete-success-remaining-3.json' |

  Scenario: By sending a non-existent look-id to the endpoint, no Look resources are removed from the database
    Given the endpoint "/api/v1/lookOrg/{look-org}/look/{look-id}"
    And the look-id value for in the url is 10000
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Look resources 'look-delete-success-remaining-all.json'
