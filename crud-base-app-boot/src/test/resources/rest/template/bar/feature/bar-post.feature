Feature: Create an Bar resource
  In order to create an Bar resource
  As a client of the rest api
  I want an endpoint that receives the post action for resource Bar

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/bar/data/post'
    And the empty bar table

  Scenario Outline: Sending the json <request body> to the endpoint, the resource Bar is successfully registered
    Given the endpoint "/api/v1/bar"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 201
    And I get the response body <response body> with a new bar-id value
    And I get into the database a resource Bar with the new bar-id and values like <registered entity>  
    And I get the 'Location' header with the value 'http://localhost:{port}/api/v1/bar/{bar-id}'

    Examples: 
      | request body                      | response body                      | registered entity                |
      | 'bar-post-success-request-1.json' | 'bar-post-success-response-1.json' | 'bar-post-success-entity-1.json' |
      | 'bar-post-success-request-2.json' | 'bar-post-success-response-2.json' | 'bar-post-success-entity-2.json' |
      | 'bar-post-success-request-3.json' | 'bar-post-success-response-3.json' | 'bar-post-success-entity-3.json' |

  Scenario Outline: Sending the json <request body> to the endpoint, the registration of resource Bar fails
    Given the endpoint "/api/v1/bar"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 400

    Examples: 
      | request body                    |
      | 'bar-post-fail-request-01.json' |
      | 'bar-post-fail-request-02.json' |
      | 'bar-post-fail-request-03.json' |
      | 'bar-post-fail-request-04.json' |
      | 'bar-post-fail-request-05.json' |
      | 'bar-post-fail-request-06.json' |
      | 'bar-post-fail-request-07.json' |
      | 'bar-post-fail-request-08.json' |
      | 'bar-post-fail-request-09.json' |
      | 'bar-post-fail-request-10.json' |
      | 'bar-post-fail-request-11.json' |
      | 'bar-post-fail-request-12.json' |
      | 'bar-post-fail-request-13.json' |
      | 'bar-post-fail-request-14.json' |
      | 'bar-post-fail-request-15.json' |
      | 'bar-post-fail-request-16.json' |
      | 'bar-post-fail-request-17.json' |
      | 'bar-post-fail-request-18.json' |
      | 'bar-post-fail-request-19.json' |
      | 'bar-post-fail-request-20.json' |
      | 'bar-post-fail-request-21.json' |
      | 'bar-post-fail-request-22.json' |
      | 'bar-post-fail-request-23.json' |
      | 'bar-post-fail-request-24.json' |
      | 'bar-post-fail-request-25.json' |
      | 'bar-post-fail-request-26.json' |
      | 'bar-post-fail-request-27.json' |
      | 'bar-post-fail-request-28.json' |
      | 'bar-post-fail-request-29.json' |
