Feature: Check the availability of an Bar resource by id
  In order to head an Bar resource
  As a client of the rest api
  I want an endpoint that receives the head action for an Bar resource

  Background: 
    Given the empty bar table
    And the initial data 'classpath:rest/template/bar/sql/bar-initial-head-data.sql'

  Scenario Outline: Sending an existing bar-id=<id> to the endpoint, the resource Bar is checked for availability from the database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is <id>
    When a 'HEAD' request is made
    Then I get status code 204

    Examples: 
      | id |
      |  1 |
      |  2 |
      |  3 |

  Scenario: By sending a non-existent bar-id to the endpoint, the availability of the Bar resource is not verified from the database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is 10000
    When a 'HEAD' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/bar"
    And the query property <query property>=<property value>
    When a 'HEAD' request is made
    Then I get status code <status code>

    Examples: 
      | query property  | property value             | status code |
      | 'bar-char'      | null                       | 404         |
      | 'bar-char'      | 'A'                        | 204         |
      | 'bar-char'      | 'a'                        | 404         |
      | 'bar-varchar'   | null                       | 404         |
      | 'bar-varchar'   | 'A'                        | 204         |
      | 'bar-varchar'   | 'a'                        | 404         |
      | 'bar-text'      | null                       | 404         |
      | 'bar-text'      | 'A'                        | 204         |
      | 'bar-text'      | 'a'                        | 404         |
      | 'bar-smallint'  | null                       | 404         |
      | 'bar-smallint'  | '32767'                    | 204         |
      | 'bar-smallint'  | '1'                        | 404         |
      | 'bar-integer'   | null                       | 404         |
      | 'bar-integer'   | '2147483647'               | 204         |
      | 'bar-integer'   | '1'                        | 404         |
      | 'bar-bigint'    | null                       | 404         |
      | 'bar-bigint'    | '9223372036854775807'      | 204         |
      | 'bar-bigint'    | '1'                        | 404         |
      | 'bar-real'      | null                       | 404         |
      | 'bar-real'      | '3.4028235E+38'            | 204         |
      | 'bar-real'      | '1'                        | 404         |
      | 'bar-double'    | null                       | 404         |
      | 'bar-double'    | '1.7976931348623157E+308'  | 204         |
      | 'bar-double'    | '1'                        | 404         |
      | 'bar-decimal'   | null                       | 404         |
      | 'bar-decimal'   | '999999.111111'            | 204         |
      | 'bar-decimal'   | '1'                        | 404         |
      | 'bar-boolean'   | null                       | 404         |
      | 'bar-boolean'   | 'true'                     | 204         |
      | 'bar-boolean'   | 'false'                    | 404         |
      | 'bar-date'      | null                       | 404         |
      | 'bar-date'      | '1850-01-01'               | 204         |
      | 'bar-date'      | '2000-01-01'               | 404         |
      | 'bar-date'      | '+5874897-12-31'           | 204         |
      | 'bar-timestamp' | null                       | 404         |
      | 'bar-timestamp' | '1850-01-01T00:00:00Z'     | 204         |
      | 'bar-timestamp' | '2000-01-01T00:00:00Z'     | 404         |
      | 'bar-timestamp' | '2050-12-31T23:59:59.999Z' | 204         |
