Feature: Delete an Bar resource
  In order to delete an Bar resource
  As a client of the rest api
  I want an endpoint that receives the delete action for an Bar resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/bar/data/delete'
    And the empty bar table
    And the initial data 'classpath:rest/template/bar/sql/bar-initial-delete-data.sql'

  Scenario Outline: Sending an existing bar-id=<id> to the endpoint, the resource Bar is removed from database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is <id>
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Bar resources <remaining records>  

    Examples: 
      | id | remaining records                     |
      |  1 | 'bar-delete-success-remaining-1.json' |
      |  2 | 'bar-delete-success-remaining-2.json' |
      |  3 | 'bar-delete-success-remaining-3.json' |

  Scenario: By sending a non-existent bar-id to the endpoint, no Bar resources are removed from the database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is 10000
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Bar resources 'bar-delete-success-remaining-all.json'
