Feature: Get an Bar resource by id
  In order to get an Bar resource
  As a client of the rest api
  I want an endpoint that receives the get action for an Bar resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/bar/data/get'
    And the empty bar table
    And the initial data 'classpath:rest/template/bar/sql/bar-initial-get-data.sql'

  Scenario Outline: Sending an existing bar-id=<id> to the endpoint, the resource Bar is fetch from database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is <id>
    When a 'GET' request is made
    Then I get status code 200
    And I get the response body <response body>  

    Examples: 
      | id | response body                           |
      |  1 | 'bar-get-by-id-success-response-1.json' |
      |  2 | 'bar-get-by-id-success-response-2.json' |
      |  3 | 'bar-get-by-id-success-response-3.json' |

  Scenario: By sending a non-existent bar-id to the endpoint, does not fetch any Bar resources from the database
    Given the endpoint "/api/v1/bar/{bar-id}"
    And the bar-id value for in the url is 10000
    When a 'GET' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameters sortProperty=<sort property> and sortAscendant=<sort ascendant> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/bar"
    And the sortProperty=<sort property>
    And the sortAscendant=<sort ascendant>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | sort property | sort ascendant | response body                             |
      | 'bar-id'      | 'true'         | 'bar-get-sorted-success-response-01.json' |
      | 'bar-id'      | 'false'        | 'bar-get-sorted-success-response-02.json' |

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/bar"
    And the query property <query property>=<property value>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | query property  | property value             | response body                            |
      | 'bar-char'      | null                       | 'bar-get-query-success-response-01.json' |
      | 'bar-char'      | 'A'                        | 'bar-get-query-success-response-02.json' |
      | 'bar-char'      | 'a'                        | 'bar-get-query-success-response-03.json' |
      | 'bar-varchar'   | null                       | 'bar-get-query-success-response-04.json' |
      | 'bar-varchar'   | 'A'                        | 'bar-get-query-success-response-05.json' |
      | 'bar-varchar'   | 'a'                        | 'bar-get-query-success-response-06.json' |
      | 'bar-text'      | null                       | 'bar-get-query-success-response-07.json' |
      | 'bar-text'      | 'A'                        | 'bar-get-query-success-response-08.json' |
      | 'bar-text'      | 'a'                        | 'bar-get-query-success-response-09.json' |
      | 'bar-smallint'  | null                       | 'bar-get-query-success-response-10.json' |
      | 'bar-smallint'  | '32767'                    | 'bar-get-query-success-response-11.json' |
      | 'bar-smallint'  | '1'                        | 'bar-get-query-success-response-12.json' |
      | 'bar-integer'   | null                       | 'bar-get-query-success-response-13.json' |
      | 'bar-integer'   | '2147483647'               | 'bar-get-query-success-response-14.json' |
      | 'bar-integer'   | '1'                        | 'bar-get-query-success-response-15.json' |
      | 'bar-bigint'    | null                       | 'bar-get-query-success-response-16.json' |
      | 'bar-bigint'    | '9223372036854775807'      | 'bar-get-query-success-response-17.json' |
      | 'bar-bigint'    | '1'                        | 'bar-get-query-success-response-18.json' |
      | 'bar-real'      | null                       | 'bar-get-query-success-response-19.json' |
      | 'bar-real'      | '3.4028235E+38'            | 'bar-get-query-success-response-20.json' |
      | 'bar-real'      | '1'                        | 'bar-get-query-success-response-21.json' |
      | 'bar-double'    | null                       | 'bar-get-query-success-response-22.json' |
      | 'bar-double'    | '1.7976931348623157E+308'  | 'bar-get-query-success-response-23.json' |
      | 'bar-double'    | '1'                        | 'bar-get-query-success-response-24.json' |
      | 'bar-decimal'   | null                       | 'bar-get-query-success-response-25.json' |
      | 'bar-decimal'   | '999999.111111'            | 'bar-get-query-success-response-26.json' |
      | 'bar-decimal'   | '1'                        | 'bar-get-query-success-response-27.json' |
      | 'bar-boolean'   | null                       | 'bar-get-query-success-response-28.json' |
      | 'bar-boolean'   | 'true'                     | 'bar-get-query-success-response-29.json' |
      | 'bar-boolean'   | 'false'                    | 'bar-get-query-success-response-30.json' |
      | 'bar-date'      | null                       | 'bar-get-query-success-response-31.json' |
      | 'bar-date'      | '1850-01-01'               | 'bar-get-query-success-response-32.json' |
      | 'bar-date'      | '2000-01-01'               | 'bar-get-query-success-response-33.json' |
      | 'bar-date'      | '+5874897-12-31'           | 'bar-get-query-success-response-34.json' |
      | 'bar-timestamp' | null                       | 'bar-get-query-success-response-35.json' |
      | 'bar-timestamp' | '1850-01-01T00:00:00Z'     | 'bar-get-query-success-response-36.json' |
      | 'bar-timestamp' | '2000-01-01T00:00:00Z'     | 'bar-get-query-success-response-37.json' |
      | 'bar-timestamp' | '2050-12-31T23:59:59.999Z' | 'bar-get-query-success-response-38.json' |
