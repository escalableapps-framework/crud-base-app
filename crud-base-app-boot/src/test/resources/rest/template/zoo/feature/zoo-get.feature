Feature: Get an Zoo resource by id
  In order to get an Zoo resource
  As a client of the rest api
  I want an endpoint that receives the get action for an Zoo resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/zoo/data/get'
    And the empty zoo table
    And the initial data 'classpath:rest/template/zoo/sql/zoo-initial-get-data.sql'
    And the zoo-org value for in the url is 1

  Scenario Outline: Sending an existing zoo-id=<id> to the endpoint, the resource Zoo is fetch from database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is <id>
    When a 'GET' request is made
    Then I get status code 200
    And I get the response body <response body>  

    Examples: 
      | id | response body                            |
      |  1 | 'zoo-get-by-id-success-response-01.json' |
      |  2 | 'zoo-get-by-id-success-response-02.json' |
      |  3 | 'zoo-get-by-id-success-response-03.json' |
      |  4 | 'zoo-get-by-id-success-response-04.json' |
      |  5 | 'zoo-get-by-id-success-response-05.json' |
      |  6 | 'zoo-get-by-id-success-response-06.json' |
      |  7 | 'zoo-get-by-id-success-response-07.json' |
      |  8 | 'zoo-get-by-id-success-response-08.json' |
      |  9 | 'zoo-get-by-id-success-response-09.json' |
      | 10 | 'zoo-get-by-id-success-response-10.json' |
      | 11 | 'zoo-get-by-id-success-response-11.json' |
      | 12 | 'zoo-get-by-id-success-response-12.json' |
      | 13 | 'zoo-get-by-id-success-response-13.json' |
      | 14 | 'zoo-get-by-id-success-response-14.json' |
      | 15 | 'zoo-get-by-id-success-response-15.json' |

  Scenario: By sending a non-existent zoo-id to the endpoint, does not fetch any Zoo resources from the database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is 10000
    When a 'GET' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameters sortProperty=<sort property> and sortAscendant=<sort ascendant> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo"
    And the sortProperty=<sort property>
    And the sortAscendant=<sort ascendant>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | sort property | sort ascendant | response body                             |
      | 'zoo-id'      | 'true'         | 'zoo-get-sorted-success-response-01.json' |
      | 'zoo-id'      | 'false'        | 'zoo-get-sorted-success-response-02.json' |

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo"
    And the query property <query property>=<property value>
    When a 'GET' request is made
    Then I get status code 200
    And I get the paged response body <response body>  

    Examples: 
      | query property  | property value             | response body                            |
      | 'zoo-char'      | null                       | 'zoo-get-query-success-response-01.json' |
      | 'zoo-char'      | 'AA'                       | 'zoo-get-query-success-response-02.json' |
      | 'zoo-char'      | 'a'                        | 'zoo-get-query-success-response-03.json' |
      | 'zoo-varchar'   | null                       | 'zoo-get-query-success-response-04.json' |
      | 'zoo-varchar'   | 'AAAA'                     | 'zoo-get-query-success-response-05.json' |
      | 'zoo-varchar'   | 'a'                        | 'zoo-get-query-success-response-06.json' |
      | 'zoo-text'      | null                       | 'zoo-get-query-success-response-07.json' |
      | 'zoo-text'      | 'A'                        | 'zoo-get-query-success-response-08.json' |
      | 'zoo-text'      | 'a'                        | 'zoo-get-query-success-response-09.json' |
      | 'zoo-smallint'  | null                       | 'zoo-get-query-success-response-10.json' |
      | 'zoo-smallint'  | '32767'                    | 'zoo-get-query-success-response-11.json' |
      | 'zoo-smallint'  | '1'                        | 'zoo-get-query-success-response-12.json' |
      | 'zoo-integer'   | null                       | 'zoo-get-query-success-response-13.json' |
      | 'zoo-integer'   | '2147483647'               | 'zoo-get-query-success-response-14.json' |
      | 'zoo-integer'   | '1'                        | 'zoo-get-query-success-response-15.json' |
      | 'zoo-bigint'    | null                       | 'zoo-get-query-success-response-16.json' |
      | 'zoo-bigint'    | '9223372036854775807'      | 'zoo-get-query-success-response-17.json' |
      | 'zoo-bigint'    | '1'                        | 'zoo-get-query-success-response-18.json' |
      | 'zoo-real'      | null                       | 'zoo-get-query-success-response-19.json' |
      | 'zoo-real'      | '3.4028235E+38'            | 'zoo-get-query-success-response-20.json' |
      | 'zoo-real'      | '1'                        | 'zoo-get-query-success-response-21.json' |
      | 'zoo-double'    | null                       | 'zoo-get-query-success-response-22.json' |
      | 'zoo-double'    | '1.7976931348623157E+308'  | 'zoo-get-query-success-response-23.json' |
      | 'zoo-double'    | '1'                        | 'zoo-get-query-success-response-24.json' |
      | 'zoo-decimal'   | null                       | 'zoo-get-query-success-response-25.json' |
      | 'zoo-decimal'   | '99.1111'                  | 'zoo-get-query-success-response-26.json' |
      | 'zoo-decimal'   | '1'                        | 'zoo-get-query-success-response-27.json' |
      | 'zoo-boolean'   | null                       | 'zoo-get-query-success-response-28.json' |
      | 'zoo-boolean'   | 'true'                     | 'zoo-get-query-success-response-29.json' |
      | 'zoo-boolean'   | 'false'                    | 'zoo-get-query-success-response-30.json' |
      | 'zoo-date'      | null                       | 'zoo-get-query-success-response-31.json' |
      | 'zoo-date'      | '1850-01-01'               | 'zoo-get-query-success-response-32.json' |
      | 'zoo-date'      | '2000-01-01'               | 'zoo-get-query-success-response-33.json' |
      | 'zoo-date'      | '+5874897-12-31'           | 'zoo-get-query-success-response-34.json' |
      | 'zoo-timestamp' | null                       | 'zoo-get-query-success-response-35.json' |
      | 'zoo-timestamp' | '1850-01-01T00:00:00Z'     | 'zoo-get-query-success-response-36.json' |
      | 'zoo-timestamp' | '2000-01-01T00:00:00Z'     | 'zoo-get-query-success-response-37.json' |
      | 'zoo-timestamp' | '2050-12-31T23:59:59.999Z' | 'zoo-get-query-success-response-38.json' |
