Feature: Check the availability of an Zoo resource by id
  In order to head an Zoo resource
  As a client of the rest api
  I want an endpoint that receives the head action for an Zoo resource

  Background: 
    Given the empty zoo table
    And the initial data 'classpath:rest/template/zoo/sql/zoo-initial-head-data.sql'
    And the zoo-org value for in the url is 1

  Scenario Outline: Sending an existing zoo-id=<id> to the endpoint, the resource Zoo is checked for availability from the database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is <id>
    When a 'HEAD' request is made
    Then I get status code 204

    Examples: 
      | id |
      |  1 |
      |  2 |
      |  3 |
      |  4 |
      |  5 |
      |  6 |
      |  7 |
      |  8 |
      |  9 |
      | 10 |
      | 11 |
      | 12 |
      | 13 |
      | 14 |
      | 15 |

  Scenario: By sending a non-existent zoo-id to the endpoint, the availability of the Zoo resource is not verified from the database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is 10000
    When a 'HEAD' request is made
    Then I get status code 404

  Scenario Outline: Sending the query parameter <query property>=<property value> to the endpoint, paged resources are fetch
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo"
    And the query property <query property>=<property value>
    When a 'HEAD' request is made
    Then I get status code <status code>

    Examples: 
      | query property  | property value             | status code |
      | 'zoo-char'      | null                       | 204         |
      | 'zoo-char'      | 'AA'                       | 204         |
      | 'zoo-char'      | 'a'                        | 404         |
      | 'zoo-varchar'   | null                       | 204         |
      | 'zoo-varchar'   | 'AAAA'                     | 204         |
      | 'zoo-varchar'   | 'a'                        | 404         |
      | 'zoo-text'      | null                       | 204         |
      | 'zoo-text'      | 'A'                        | 204         |
      | 'zoo-text'      | 'a'                        | 404         |
      | 'zoo-smallint'  | null                       | 204         |
      | 'zoo-smallint'  | '32767'                    | 204         |
      | 'zoo-smallint'  | '1'                        | 404         |
      | 'zoo-integer'   | null                       | 204         |
      | 'zoo-integer'   | '2147483647'               | 204         |
      | 'zoo-integer'   | '1'                        | 404         |
      | 'zoo-bigint'    | null                       | 204         |
      | 'zoo-bigint'    | '9223372036854775807'      | 204         |
      | 'zoo-bigint'    | '1'                        | 404         |
      | 'zoo-real'      | null                       | 204         |
      | 'zoo-real'      | '3.4028235E+38'            | 204         |
      | 'zoo-real'      | '1'                        | 404         |
      | 'zoo-double'    | null                       | 204         |
      | 'zoo-double'    | '1.7976931348623157E+308'  | 204         |
      | 'zoo-double'    | '1'                        | 404         |
      | 'zoo-decimal'   | null                       | 204         |
      | 'zoo-decimal'   | '99.1111'                  | 204         |
      | 'zoo-decimal'   | '1'                        | 404         |
      | 'zoo-boolean'   | null                       | 204         |
      | 'zoo-boolean'   | 'true'                     | 204         |
      | 'zoo-boolean'   | 'false'                    | 404         |
      | 'zoo-date'      | null                       | 204         |
      | 'zoo-date'      | '1850-01-01'               | 204         |
      | 'zoo-date'      | '2000-01-01'               | 404         |
      | 'zoo-date'      | '+5874897-12-31'           | 204         |
      | 'zoo-timestamp' | null                       | 204         |
      | 'zoo-timestamp' | '1850-01-01T00:00:00Z'     | 204         |
      | 'zoo-timestamp' | '2000-01-01T00:00:00Z'     | 404         |
      | 'zoo-timestamp' | '2050-12-31T23:59:59.999Z' | 204         |
