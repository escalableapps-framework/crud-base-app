Feature: Delete an Zoo resource
  In order to delete an Zoo resource
  As a client of the rest api
  I want an endpoint that receives the delete action for an Zoo resource

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/zoo/data/delete'
    And the empty zoo table
    And the initial data 'classpath:rest/template/zoo/sql/zoo-initial-delete-data.sql'
    And the zoo-org value for in the url is 1

  Scenario Outline: Sending an existing zoo-id=<id> to the endpoint, the resource Zoo is removed from database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is <id>
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Zoo resources <remaining records>  

    Examples: 
      | id | remaining records                      |
      |  1 | 'zoo-delete-success-remaining-01.json' |
      |  2 | 'zoo-delete-success-remaining-02.json' |
      |  3 | 'zoo-delete-success-remaining-03.json' |
      |  4 | 'zoo-delete-success-remaining-04.json' |
      |  5 | 'zoo-delete-success-remaining-05.json' |
      |  6 | 'zoo-delete-success-remaining-06.json' |
      |  7 | 'zoo-delete-success-remaining-07.json' |
      |  8 | 'zoo-delete-success-remaining-08.json' |
      |  9 | 'zoo-delete-success-remaining-09.json' |
      | 10 | 'zoo-delete-success-remaining-10.json' |
      | 11 | 'zoo-delete-success-remaining-11.json' |
      | 12 | 'zoo-delete-success-remaining-12.json' |
      | 13 | 'zoo-delete-success-remaining-13.json' |
      | 14 | 'zoo-delete-success-remaining-14.json' |
      | 15 | 'zoo-delete-success-remaining-15.json' |

  Scenario: By sending a non-existent zoo-id to the endpoint, no Zoo resources are removed from the database
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}"
    And the zoo-id value for in the url is 10000
    When a 'DELETE' request is made
    Then I get status code 204
    And I get into the database the remaining Zoo resources 'zoo-delete-success-remaining-all.json'
