Feature: Create an Zoo resource
  In order to create an Zoo resource
  As a client of the rest api
  I want an endpoint that receives the post action for resource Zoo

  Background: 
    Given the path of the test scenario files 'classpath:rest/template/zoo/data/post'
    And the empty zoo table
    And the zoo-org value for in the url is 1

  Scenario Outline: Sending the json <request body> to the endpoint, the resource Zoo is successfully registered
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 201
    And I get the response body <response body> with a new zoo-id value
    And I get into the database a resource Zoo with the new zoo-id and values like <registered entity>  
    And I get the 'Location' header with the value 'http://localhost:{port}/api/v1/zooOrg/{zoo-org}/zoo/{zoo-id}'

    Examples: 
      | request body                       | response body                       | registered entity                 |
      | 'zoo-post-success-request-01.json' | 'zoo-post-success-response-01.json' | 'zoo-post-success-entity-01.json' |
      | 'zoo-post-success-request-02.json' | 'zoo-post-success-response-02.json' | 'zoo-post-success-entity-02.json' |
      | 'zoo-post-success-request-03.json' | 'zoo-post-success-response-03.json' | 'zoo-post-success-entity-03.json' |
      | 'zoo-post-success-request-04.json' | 'zoo-post-success-response-04.json' | 'zoo-post-success-entity-04.json' |
      | 'zoo-post-success-request-05.json' | 'zoo-post-success-response-05.json' | 'zoo-post-success-entity-05.json' |
      | 'zoo-post-success-request-06.json' | 'zoo-post-success-response-06.json' | 'zoo-post-success-entity-06.json' |
      | 'zoo-post-success-request-07.json' | 'zoo-post-success-response-07.json' | 'zoo-post-success-entity-07.json' |
      | 'zoo-post-success-request-08.json' | 'zoo-post-success-response-08.json' | 'zoo-post-success-entity-08.json' |
      | 'zoo-post-success-request-09.json' | 'zoo-post-success-response-09.json' | 'zoo-post-success-entity-09.json' |
      | 'zoo-post-success-request-10.json' | 'zoo-post-success-response-10.json' | 'zoo-post-success-entity-10.json' |
      | 'zoo-post-success-request-11.json' | 'zoo-post-success-response-11.json' | 'zoo-post-success-entity-11.json' |
      | 'zoo-post-success-request-12.json' | 'zoo-post-success-response-12.json' | 'zoo-post-success-entity-12.json' |
      | 'zoo-post-success-request-13.json' | 'zoo-post-success-response-13.json' | 'zoo-post-success-entity-13.json' |
      | 'zoo-post-success-request-14.json' | 'zoo-post-success-response-14.json' | 'zoo-post-success-entity-14.json' |
      | 'zoo-post-success-request-15.json' | 'zoo-post-success-response-15.json' | 'zoo-post-success-entity-15.json' |
      | 'zoo-post-success-request-16.json' | 'zoo-post-success-response-16.json' | 'zoo-post-success-entity-16.json' |
      | 'zoo-post-success-request-17.json' | 'zoo-post-success-response-17.json' | 'zoo-post-success-entity-17.json' |
      | 'zoo-post-success-request-18.json' | 'zoo-post-success-response-18.json' | 'zoo-post-success-entity-18.json' |
      | 'zoo-post-success-request-19.json' | 'zoo-post-success-response-19.json' | 'zoo-post-success-entity-19.json' |
      | 'zoo-post-success-request-20.json' | 'zoo-post-success-response-20.json' | 'zoo-post-success-entity-20.json' |
      | 'zoo-post-success-request-21.json' | 'zoo-post-success-response-21.json' | 'zoo-post-success-entity-21.json' |
      | 'zoo-post-success-request-22.json' | 'zoo-post-success-response-22.json' | 'zoo-post-success-entity-22.json' |
      | 'zoo-post-success-request-23.json' | 'zoo-post-success-response-23.json' | 'zoo-post-success-entity-23.json' |
      | 'zoo-post-success-request-24.json' | 'zoo-post-success-response-24.json' | 'zoo-post-success-entity-24.json' |
      | 'zoo-post-success-request-25.json' | 'zoo-post-success-response-25.json' | 'zoo-post-success-entity-25.json' |
      | 'zoo-post-success-request-26.json' | 'zoo-post-success-response-26.json' | 'zoo-post-success-entity-26.json' |
      | 'zoo-post-success-request-27.json' | 'zoo-post-success-response-27.json' | 'zoo-post-success-entity-27.json' |

  Scenario Outline: Sending the json <request body> to the endpoint, the registration of resource Zoo fails
    Given the endpoint "/api/v1/zooOrg/{zoo-org}/zoo"
    And the request body <request body>
    When a 'POST' request is made
    Then I get status code 400

    Examples: 
      | request body                    |
      | 'zoo-post-fail-request-01.json' |
      | 'zoo-post-fail-request-02.json' |
      | 'zoo-post-fail-request-03.json' |
      | 'zoo-post-fail-request-04.json' |
      | 'zoo-post-fail-request-05.json' |
      | 'zoo-post-fail-request-06.json' |
      | 'zoo-post-fail-request-07.json' |
      | 'zoo-post-fail-request-08.json' |
      | 'zoo-post-fail-request-09.json' |
      | 'zoo-post-fail-request-10.json' |
