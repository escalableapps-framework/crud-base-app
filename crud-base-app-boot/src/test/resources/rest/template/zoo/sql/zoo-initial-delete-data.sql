SET SESSION vars.zooOrg = 1;

INSERT INTO crud_base_db.zoo
(zoo_org, zoo_id, zoo_char, zoo_varchar, zoo_text, zoo_smallint, zoo_integer, zoo_bigint, zoo_real, zoo_double, zoo_decimal, zoo_boolean, zoo_date, zoo_timestamp) VALUES 
(current_setting('vars.zooOrg')::bigint, 1, null, null, null, null, null, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 2, 'AA', null, null, null, null, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 3, null, 'AAAA', null, null, null, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 4, null, null, 'A', null, null, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 5, null, null, null, 32767, null, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 6, null, null, null, null, 2147483647, null, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 7, null, null, null, null, null, 9223372036854775807, null, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 8, null, null, null, null, null, null, 3.4028235E+38, null, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 9, null, null, null, null, null, null, null, 1.7976931348623157E+308, null, null, null, null),
(current_setting('vars.zooOrg')::bigint, 10, null, null, null, null, null, null, null, null, 99.1111, null, null, null),
(current_setting('vars.zooOrg')::bigint, 11, null, null, null, null, null, null, null, null, null, true, null, null),
(current_setting('vars.zooOrg')::bigint, 12, null, null, null, null, null, null, null, null, null, null, '1850-01-01', null),
(current_setting('vars.zooOrg')::bigint, 13, null, null, null, null, null, null, null, null, null, null, null, '1850-01-01 00:00:00.000+00'),
(current_setting('vars.zooOrg')::bigint, 14, null, null, null, null, null, null, null, null, null, null, '5874897-12-31', null),
(current_setting('vars.zooOrg')::bigint, 15, null, null, null, null, null, null, null, null, null, null, null, '2050-12-31 23:59:59.999+00');
