#language: es
Característica: Actualización parcial del recurso Foo
  A fin de actualizar parcialmente un recurso Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción patch para el recurso foo y actualice los campos indicados del objeto en la base de datos

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/patch"
    Dado la data inicial para la prueba: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/foo4update.sql"

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se actualiza exitosamente el recurso foo
    Dado el endpoint "/api/v1/foo/1"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'PATCH'
    Entonces se obtiene en la respuesta el codigo de error 200
    Y en el cuerpo de la respuesta el json <outputValues>
    Y se verifica que en la base de datos se encuentra un registro con el id 1 y con los valores en los campos <registredData>

    Ejemplos: 
      | inputValues                                | outputValues               | registredData              |
      | '{"fooChar"     : null}'                   | 'fooCharIsNull.json'       | 'fooCharIsNull.json'       |
      | '{"fooVarchar"  : null}'                   | 'fooVarcharIsNull.json'    | 'fooVarcharIsNull.json'    |
      | '{"fooText"     : null}'                   | 'fooTextIsNull.json'       | 'fooTextIsNull.json'       |
      | '{"fooSmallint" : null}'                   | 'fooSmallintIsNull.json'   | 'fooSmallintIsNull.json'   |
      | '{"fooInteger"  : null}'                   | 'fooIntegerIsNull.json'    | 'fooIntegerIsNull.json'    |
      | '{"fooBigint"   : null}'                   | 'fooBigintIsNull.json'     | 'fooBigintIsNull.json'     |
      | '{"fooReal"     : null}'                   | 'fooRealIsNull.json'       | 'fooRealIsNull.json'       |
      | '{"fooDouble"   : null}'                   | 'fooDoubleIsNull.json'     | 'fooDoubleIsNull.json'     |
      | '{"fooDecimal"  : null}'                   | 'fooDecimalIsNull.json'    | 'fooDecimalIsNull.json'    |
      | '{"fooBoolean"  : null}'                   | 'fooBooleanIsNull.json'    | 'fooBooleanIsNull.json'    |
      | '{"fooDate"     : null}'                   | 'fooDateIsNull.json'       | 'fooDateIsNull.json'       |
      | '{"fooTimestamp": null}'                   | 'fooTimestampIsNull.json'  | 'fooTimestampIsNull.json'  |
      | '{"fooChar"     : "FC"}'                   | 'fooCharUpdated.json'      | 'fooCharUpdated.json'      |
      | '{"fooVarchar"  : "FVCH"}'                 | 'fooVarcharUpdated.json'   | 'fooVarcharUpdated.json'   |
      | '{"fooText"     : "LOREM IPSUM DOLOR"}'    | 'fooTextUpdated.json'      | 'fooTextUpdated.json'      |
      | '{"fooSmallint" : 2}'                      | 'fooSmallintUpdated.json'  | 'fooSmallintUpdated.json'  |
      | '{"fooInteger"  : 20}'                     | 'fooIntegerUpdated.json'   | 'fooIntegerUpdated.json'   |
      | '{"fooBigint"   : 200}'                    | 'fooBigintUpdated.json'    | 'fooBigintUpdated.json'    |
      | '{"fooReal"     : 2.2}'                    | 'fooRealUpdated.json'      | 'fooRealUpdated.json'      |
      | '{"fooDouble"   : 20.02}'                  | 'fooDoubleUpdated.json'    | 'fooDoubleUpdated.json'    |
      | '{"fooDecimal"  : 0.0002}'                 | 'fooDecimalUpdated.json'   | 'fooDecimalUpdated.json'   |
      | '{"fooBoolean"  : false}'                  | 'fooBooleanUpdated.json'   | 'fooBooleanUpdated.json'   |
      | '{"fooDate"     : "2019-01-02"}'           | 'fooDateUpdated.json'      | 'fooDateUpdated.json'      |
      | '{"fooTimestamp": "2019-01-02T00:00:00Z"}' | 'fooTimestampUpdated.json' | 'fooTimestampUpdated.json' |
      | 'inputFooAllUpdated.json'                  | 'fooAllUpdated.json'       | 'fooAllUpdated.json'       |

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se produce un error en la actualizacion
    Dado el endpoint "/api/v1/foo/1"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'PATCH'
    Entonces se obtiene en la respuesta el codigo de error 400
    Y en el cuerpo de la respuesta el json <outputValues>
    Y se verifica que en la base de datos se encuentra un registro con el id 1 y con los valores en los campos <registredData>

    Ejemplos: 
      | inputValues                | outputValues                      | registredData       |
      | '{}'                       | 'errorNoPropertiesForUpdate.json' | 'fooUnchanged.json' |
      | '{"fooChar"     : "ABCD"}' | 'errorFooCharBadRequest.json'     | 'fooUnchanged.json' |
