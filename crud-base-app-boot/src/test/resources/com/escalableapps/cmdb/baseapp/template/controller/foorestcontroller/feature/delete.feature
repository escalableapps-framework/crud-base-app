#language: es
Característica: Borrado de un recurso Foo
  A fin de borrar un recurso Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción delete para el recurso foo y borre la entidad en la base de datos

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/delete"
    Dado la data inicial para la prueba: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/foo.sql"

  Esquema del escenario: Al enviar al endpoint la consulta DELETE hacia el identificador de recurso <id>, se obtiene una respuesta del servicio
    Dado el endpoint "/api/v1/foo/{id}"
    Cuando se envia en la solicitud la consulta "DELETE" con el identificador <id>
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y se verifica que en la base de datos se encuentran los registros <remaining>

    Ejemplos: 
      | id      | status | remaining            |
      | '1'     |    204 | 'foo1Deleted.json'   |
      | '2'     |    204 | 'foo2Deleted.json'   |
      | '3'     |    204 | 'foo3Deleted.json'   |
      | '4'     |    204 | 'foo4Deleted.json'   |
      | '5'     |    204 | 'foo5Deleted.json'   |
      | '6'     |    204 | 'foo6Deleted.json'   |
      | '7'     |    204 | 'foo7Deleted.json'   |
      | '8'     |    204 | 'foo8Deleted.json'   |
      | '9'     |    204 | 'foo9Deleted.json'   |
      | '10'    |    204 | 'foo10Deleted.json'  |
      | '11'    |    204 | 'foo11Deleted.json'  |
      | '12'    |    204 | 'foo12Deleted.json'  |
      | '13'    |    204 | 'foo13Deleted.json'  |
      | '14'    |    204 | 'foo14Deleted.json'  |
      | '10000' |    204 | 'fooNotDeleted.json' |
