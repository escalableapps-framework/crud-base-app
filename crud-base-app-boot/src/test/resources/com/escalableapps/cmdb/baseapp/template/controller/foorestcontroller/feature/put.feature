#language: es
Característica: Actualización del recurso Foo
  A fin de actualizar un recurso Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción patch para el recurso foo y actualice el objeto en la base de datos

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/put"
    Dado la data inicial para la prueba: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/foo4update.sql"

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se actualiza exitosamente el recurso foo
    Dado el endpoint "/api/v1/foo/1"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'PUT'
    Entonces se obtiene en la respuesta el codigo de error 200
    Y en el cuerpo de la respuesta el json <outputValues>
    Y se verifica que en la base de datos se encuentra un registro con el id 1 y con los valores en los campos <registredData>

    Ejemplos: 
      | inputValues                                | outputValues                                           | registredData                                          |
      | '{}'                                       | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooChar"     : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooVarchar"  : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooText"     : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooSmallint" : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooInteger"  : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooBigint"   : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooReal"     : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooDouble"   : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooDecimal"  : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooBoolean"  : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooDate"     : null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | '{"fooTimestamp": null}'                   | '{"fooId": 1}'                                         | '{"fooId": 1}'                                         |
      | 'fooCharIsNull.json'                       | 'fooCharIsNull.json'                                   | 'fooCharIsNull.json'                                   |
      | 'fooVarcharIsNull.json'                    | 'fooVarcharIsNull.json'                                | 'fooVarcharIsNull.json'                                |
      | 'fooTextIsNull.json'                       | 'fooTextIsNull.json'                                   | 'fooTextIsNull.json'                                   |
      | 'fooSmallintIsNull.json'                   | 'fooSmallintIsNull.json'                               | 'fooSmallintIsNull.json'                               |
      | 'fooIntegerIsNull.json'                    | 'fooIntegerIsNull.json'                                | 'fooIntegerIsNull.json'                                |
      | 'fooBigintIsNull.json'                     | 'fooBigintIsNull.json'                                 | 'fooBigintIsNull.json'                                 |
      | 'fooRealIsNull.json'                       | 'fooRealIsNull.json'                                   | 'fooRealIsNull.json'                                   |
      | 'fooDoubleIsNull.json'                     | 'fooDoubleIsNull.json'                                 | 'fooDoubleIsNull.json'                                 |
      | 'fooDecimalIsNull.json'                    | 'fooDecimalIsNull.json'                                | 'fooDecimalIsNull.json'                                |
      | 'fooBooleanIsNull.json'                    | 'fooBooleanIsNull.json'                                | 'fooBooleanIsNull.json'                                |
      | 'fooDateIsNull.json'                       | 'fooDateIsNull.json'                                   | 'fooDateIsNull.json'                                   |
      | 'fooTimestampIsNull.json'                  | 'fooTimestampIsNull.json'                              | 'fooTimestampIsNull.json'                              |
      | '{"fooChar"     : "FC"}'                   | '{"fooId": 1, "fooChar"     : "FC"}'                   | '{"fooId": 1, "fooChar"     : "FC"}'                   |
      | '{"fooVarchar"  : "FVCH"}'                 | '{"fooId": 1, "fooVarchar"  : "FVCH"}'                 | '{"fooId": 1, "fooVarchar"  : "FVCH"}'                 |
      | '{"fooText"     : "LOREM IPSUM DOLOR"}'    | '{"fooId": 1, "fooText"     : "LOREM IPSUM DOLOR"}'    | '{"fooId": 1, "fooText"     : "LOREM IPSUM DOLOR"}'    |
      | '{"fooSmallint" : 2}'                      | '{"fooId": 1, "fooSmallint" : 2}'                      | '{"fooId": 1, "fooSmallint" : 2}'                      |
      | '{"fooInteger"  : 20}'                     | '{"fooId": 1, "fooInteger"  : 20}'                     | '{"fooId": 1, "fooInteger"  : 20}'                     |
      | '{"fooBigint"   : 200}'                    | '{"fooId": 1, "fooBigint"   : 200}'                    | '{"fooId": 1, "fooBigint"   : 200}'                    |
      | '{"fooReal"     : 2.2}'                    | '{"fooId": 1, "fooReal"     : 2.2}'                    | '{"fooId": 1, "fooReal"     : 2.2}'                    |
      | '{"fooDouble"   : 20.02}'                  | '{"fooId": 1, "fooDouble"   : 20.02}'                  | '{"fooId": 1, "fooDouble"   : 20.02}'                  |
      | '{"fooDecimal"  : 0.0002}'                 | '{"fooId": 1, "fooDecimal"  : 0.0002}'                 | '{"fooId": 1, "fooDecimal"  : 0.0002}'                 |
      | '{"fooBoolean"  : false}'                  | '{"fooId": 1, "fooBoolean"  : false}'                  | '{"fooId": 1, "fooBoolean"  : false}'                  |
      | '{"fooDate"     : "2019-01-02"}'           | '{"fooId": 1, "fooDate"     : "2019-01-02"}'           | '{"fooId": 1, "fooDate"     : "2019-01-02"}'           |
      | '{"fooTimestamp": "2019-01-02T00:00:00Z"}' | '{"fooId": 1, "fooTimestamp": "2019-01-02T00:00:00Z"}' | '{"fooId": 1, "fooTimestamp": "2019-01-02T00:00:00Z"}' |
      | 'fooCharUpdated.json'                      | 'fooCharUpdated.json'                                  | 'fooCharUpdated.json'                                  |
      | 'fooVarcharUpdated.json'                   | 'fooVarcharUpdated.json'                               | 'fooVarcharUpdated.json'                               |
      | 'fooTextUpdated.json'                      | 'fooTextUpdated.json'                                  | 'fooTextUpdated.json'                                  |
      | 'fooSmallintUpdated.json'                  | 'fooSmallintUpdated.json'                              | 'fooSmallintUpdated.json'                              |
      | 'fooIntegerUpdated.json'                   | 'fooIntegerUpdated.json'                               | 'fooIntegerUpdated.json'                               |
      | 'fooBigintUpdated.json'                    | 'fooBigintUpdated.json'                                | 'fooBigintUpdated.json'                                |
      | 'fooRealUpdated.json'                      | 'fooRealUpdated.json'                                  | 'fooRealUpdated.json'                                  |
      | 'fooDoubleUpdated.json'                    | 'fooDoubleUpdated.json'                                | 'fooDoubleUpdated.json'                                |
      | 'fooDecimalUpdated.json'                   | 'fooDecimalUpdated.json'                               | 'fooDecimalUpdated.json'                               |
      | 'fooBooleanUpdated.json'                   | 'fooBooleanUpdated.json'                               | 'fooBooleanUpdated.json'                               |
      | 'fooDateUpdated.json'                      | 'fooDateUpdated.json'                                  | 'fooDateUpdated.json'                                  |
      | 'fooTimestampUpdated.json'                 | 'fooTimestampUpdated.json'                             | 'fooTimestampUpdated.json'                             |
      | 'inputFooAllUpdated.json'                  | 'fooAllUpdated.json'                                   | 'fooAllUpdated.json'                                   |

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se produce un error en la actualizacion
    Dado el endpoint "/api/v1/foo/1"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'PUT'
    Entonces se obtiene en la respuesta el codigo de error 400
    Y en el cuerpo de la respuesta el json <outputValues>
    Y se verifica que en la base de datos se encuentra un registro con el id 1 y con los valores en los campos <registredData>

    Ejemplos: 
      | inputValues                | outputValues                  | registredData       |
      | '{"fooChar"     : "ABCD"}' | 'errorFooCharBadRequest.json' | 'fooUnchanged.json' |
