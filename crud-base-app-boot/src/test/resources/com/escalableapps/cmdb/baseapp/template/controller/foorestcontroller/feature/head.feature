#language: es
Característica: Consulta la existencia de recursos Foo
  A fin de saber si existe recurso(s) de Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción head con una consulta para el recurso foo y saber si el recurso existe.

  Antecedentes: 
    Dado la data inicial para la prueba: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/foo.sql"

  Esquema del escenario: Al enviar al endpoint la consulta HEAD con los parametros <params>, se obtiene una respuesta del servicio
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en la solicitud la consulta "HEAD" con los parametros <params>
    Entonces se obtiene en la respuesta el codigo de error <status>

    Ejemplos: 
      | params                                                                               | status |
      | '?fooChar=&pageSize=100'                                                             |    204 |
      | '?fooVarchar=&pageSize=100'                                                          |    204 |
      | '?fooText=&pageSize=100'                                                             |    204 |
      | '?fooSmallint=&pageSize=100'                                                         |    204 |
      | '?fooInteger=&pageSize=100'                                                          |    204 |
      | '?fooBigint=&pageSize=100'                                                           |    204 |
      | '?fooReal=&pageSize=100'                                                             |    204 |
      | '?fooDouble=&pageSize=100'                                                           |    204 |
      | '?fooDecimal=&pageSize=100'                                                          |    204 |
      | '?fooBoolean=&pageSize=100'                                                          |    204 |
      | '?fooDate=&pageSize=100'                                                             |    204 |
      | '?fooTimestamp=&pageSize=100'                                                        |    204 |
      | '?fooChar=fc'                                                                        |    204 |
      | '?fooVarchar=fVch'                                                                   |    204 |
      | '?fooText=Lorem Ipsum Dolor'                                                         |    204 |
      | '?fooSmallint=1'                                                                     |    204 |
      | '?fooInteger=10'                                                                     |    204 |
      | '?fooBigint=100'                                                                     |    204 |
      | '?fooReal=1.1'                                                                       |    204 |
      | '?fooDouble=10.01'                                                                   |    204 |
      | '?fooDecimal=0.0001'                                                                 |    204 |
      | '?fooBoolean=true'                                                                   |    204 |
      | '?fooDate=2019-01-01'                                                                |    204 |
      | '?fooDate=2019-01-10'                                                                |    204 |
      | '?fooDateMin=2018-01-01&fooDateMax=2018-12-31'                                       |    404 |
      | '?fooDateMin=2018-01-01&fooDateMax=2019-01-01'                                       |    204 |
      | '?fooDateMin=2018-01-01&fooDateMax=2019-01-09'                                       |    204 |
      | '?fooDateMin=2019-01-01&fooDateMax=2019-01-10'                                       |    204 |
      | '?fooDateMin=2019-01-02&fooDateMax=2019-01-10'                                       |    204 |
      | '?fooDateMin=2019-01-10&fooDateMax=2019-12-31'                                       |    204 |
      | '?fooDateMin=2018-01-11&fooDateMax=2018-12-31'                                       |    404 |
      | '?fooDateMin=2019-01-01'                                                             |    204 |
      | '?fooDateMin=2019-01-02'                                                             |    204 |
      | '?fooDateMax=2019-01-09'                                                             |    204 |
      | '?fooDateMax=2019-01-10'                                                             |    204 |
      | '?fooTimestamp=2019-01-01T00:00:00Z'                                                 |    204 |
      | '?fooTimestamp=2019-01-01T00:00:00-05:00'                                            |    204 |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2018-12-31T23:59:59.999Z'     |    404 |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2019-01-01T00:00:00Z'         |    204 |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2019-01-01T04:59:59.999Z'     |    204 |
      | '?fooTimestampMin=2019-01-01T00:00:00Z&fooTimestampMax=2019-01-01T05:00:00Z'         |    204 |
      | '?fooTimestampMin=2019-01-01T00:00:00.001Z&fooTimestampMax=2019-01-01T05:00:00Z'     |    204 |
      | '?fooTimestampMin=2019-01-01T05:00:00Z&fooTimestampMax=2019-12-31T23:59:59.999Z'     |    204 |
      | '?fooTimestampMin=2019-01-01T05:00:00.001Z&fooTimestampMax=2019-12-31T23:59:59.999Z' |    404 |
      | '?fooTimestampMin=2019-01-01T00:00:00Z'                                              |    204 |
      | '?fooTimestampMin=2019-01-01T00:00:00.001Z'                                          |    204 |
      | '?fooTimestampMax=2019-01-01T04:59:59.999Z'                                          |    204 |
      | '?fooTimestampMax=2019-01-01T05:00:00Z'                                              |    204 |

  Esquema del escenario: Al enviar al endpoint la consulta HEAD hacia el identificador de recurso <id>, se obtiene una respuesta del servicio
    Dado el endpoint "/api/v1/foo/{id}"
    Cuando se envia en la solicitud la consulta "HEAD" con el identificador <id>
    Entonces se obtiene en la respuesta el codigo de error <status>

    Ejemplos: 
      | id      | status |
      | '1'     |    204 |
      | '2'     |    204 |
      | '3'     |    204 |
      | '10000' |    404 |
