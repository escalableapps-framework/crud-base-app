SELECT
  foo.foo_id AS fooId,
  foo.foo_char AS fooChar,
  foo.foo_varchar AS fooVarchar,
  foo.foo_text AS fooText,
  foo.foo_smallint AS fooSmallint,
  foo.foo_integer AS fooInteger,
  foo.foo_bigint AS fooBigint,
  foo.foo_real AS fooReal,
  foo.foo_double AS fooDouble,
  foo.foo_decimal AS fooDecimal,
  foo.foo_boolean AS fooBoolean,
  foo.foo_date AS fooDate,
  foo.foo_timestamp AS fooTimestamp
FROM crud_base_db.foo AS foo
WHERE foo.foo_id = :fooId