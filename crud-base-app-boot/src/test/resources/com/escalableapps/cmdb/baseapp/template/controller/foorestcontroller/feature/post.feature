#language: es
Característica: Creación del recurso Foo
  A fin de crear un recurso Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción post para el recurso foo y cree el objeto en la base de datos

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/post"

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se registra exitosamente el recurso foo
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'POST'
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <outputResource> con un id generado
    Y se indica la cabecera "Location" con el valor "http://localhost:{port}{endpoint}/{id}"
    Y se verifica que en la base de datos se encuentra un registro con el id generado y con los valores en los campos <outputResource>

    Ejemplos: 
      | inputValues                                     | status | outputResource                             |
      | '{}'                                            |    201 | '{}'                                       |
      | '{"fooChar"     : null}'                        |    201 | '{}'                                       |
      | '{"fooVarchar"  : null}'                        |    201 | '{}'                                       |
      | '{"fooText"     : null}'                        |    201 | '{}'                                       |
      | '{"fooSmallint" : null}'                        |    201 | '{}'                                       |
      | '{"fooInteger"  : null}'                        |    201 | '{}'                                       |
      | '{"fooBigint"   : null}'                        |    201 | '{}'                                       |
      | '{"fooReal"     : null}'                        |    201 | '{}'                                       |
      | '{"fooDouble"   : null}'                        |    201 | '{}'                                       |
      | '{"fooDecimal"  : null}'                        |    201 | '{}'                                       |
      | '{"fooBoolean"  : null}'                        |    201 | '{}'                                       |
      | '{"fooDate"     : null}'                        |    201 | '{}'                                       |
      | '{"fooTimestamp": null}'                        |    201 | '{}'                                       |
      | '{"fooTimestamp": null}'                        |    201 | '{}'                                       |
      | '{"fooChar"     : "fc"}'                        |    201 | '{"fooChar"     : "fc"}'                   |
      | '{"fooVarchar"  : "fVch"}'                      |    201 | '{"fooVarchar"  : "fVch"}'                 |
      | '{"fooText"     : "Lorem Ipsum Dolor"}'         |    201 | '{"fooText"     : "Lorem Ipsum Dolor"}'    |
      | '{"fooSmallint" : 1}'                           |    201 | '{"fooSmallint" : 1}'                      |
      | '{"fooInteger"  : 10}'                          |    201 | '{"fooInteger"  : 10}'                     |
      | '{"fooBigint"   : 100}'                         |    201 | '{"fooBigint"   : 100}'                    |
      | '{"fooReal"     : 1.1}'                         |    201 | '{"fooReal"     : 1.1}'                    |
      | '{"fooDouble"   : 10.01}'                       |    201 | '{"fooDouble"   : 10.01}'                  |
      | '{"fooDecimal"  : 0.0001}'                      |    201 | '{"fooDecimal"  : 0.0001}'                 |
      | '{"fooBoolean"  : true}'                        |    201 | '{"fooBoolean"  : true}'                   |
      | '{"fooDate"     : "2019-01-01"}'                |    201 | '{"fooDate"     : "2019-01-01"}'           |
      | '{"fooTimestamp": "2019-01-01T00:00:00Z"}'      |    201 | '{"fooTimestamp": "2019-01-01T00:00:00Z"}' |
      #| '{"fooTimestamp": "2019-01-01T00:00:00-05:00"}' |    201 | '{"fooTimestamp": "2019-01-01T05:00:00Z"}' |
      | 'emoji.json'                                    |    201 | 'emoji.json'                               |
      | 'uft8p1.json'                                   |    201 | 'uft8p1.json'                              |
      | 'uft8p2.json'                                   |    201 | 'uft8p2.json'                              |
      | 'uft8p3.json'                                   |    201 | 'uft8p3.json'                              |
      | 'uft8p4.json'                                   |    201 | 'uft8p4.json'                              |
      | 'uft8p5.json'                                   |    201 | 'uft8p5.json'                              |
      | 'uft8p6.json'                                   |    201 | 'uft8p6.json'                              |
      | 'uft8p7.json'                                   |    201 | 'uft8p7.json'                              |
      | 'uft8p8.json'                                   |    201 | 'uft8p8.json'                              |

  Esquema del escenario: Al enviar al endpoint el json <inputValues>, se produce un error
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en el cuerpo de la solicitud el json <inputValues> con el metodo 'POST'
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <outputResource> con un id generado
    Y se verifica que en la base de datos se encuentra un registro con el id generado y con los valores en los campos <outputResource>

    Ejemplos: 
      | inputValues                | status | outputResource                |
      | '{"fooChar"     : "ABCD"}' |    400 | 'errorFooCharBadRequest.json' |
