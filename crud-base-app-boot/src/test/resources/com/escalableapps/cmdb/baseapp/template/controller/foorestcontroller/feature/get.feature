#language: es
Característica: Consulta de recursos Foo
  A fin de consulta recurso(s) Foo
  Siendo un cliente del api rest
  Quiero un endpoint que reciba la acción get con una consulta para el recurso foo y saber la información del recurso existentes.

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/get"
    Dado la data inicial para la prueba: "classpath:com/escalableapps/cmdb/baseapp/template/controller/foorestcontroller/data/foo.sql"

  Esquema del escenario: Al enviar al endpoint la consulta GET con los parametros <params>, se obtiene una respuesta del servicio
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en la solicitud la consulta "GET" con los parametros <params>
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <response>

    Ejemplos: 
      | params                                                                               | status | response                  |
      | '?fooId=1'                                                                           |    200 | 'fooId1.json'             |
      | '?fooId=2'                                                                           |    200 | 'fooId2.json'             |
      | '?fooId=3'                                                                           |    200 | 'fooId3.json'             |
      | '?fooId='                                                                            |    200 | 'empty.json'              |
      | '?fooChar=&pageSize=100'                                                             |    200 | 'fooCharIsNull.json'      |
      | '?fooVarchar=&pageSize=100'                                                          |    200 | 'fooVarcharIsNull.json'   |
      | '?fooText=&pageSize=100'                                                             |    200 | 'fooTextIsNull.json'      |
      | '?fooSmallint=&pageSize=100'                                                         |    200 | 'fooSmallintIsNull.json'  |
      | '?fooInteger=&pageSize=100'                                                          |    200 | 'fooIntegerIsNull.json'   |
      | '?fooBigint=&pageSize=100'                                                           |    200 | 'fooBigintIsNull.json'    |
      | '?fooReal=&pageSize=100'                                                             |    200 | 'fooRealIsNull.json'      |
      | '?fooDouble=&pageSize=100'                                                           |    200 | 'fooDoubleIsNull.json'    |
      | '?fooDecimal=&pageSize=100'                                                          |    200 | 'fooDecimalIsNull.json'   |
      | '?fooBoolean=&pageSize=100'                                                          |    200 | 'fooBooleanIsNull.json'   |
      | '?fooDate=&pageSize=100'                                                             |    200 | 'fooDateIsNull.json'      |
      | '?fooTimestamp=&pageSize=100'                                                        |    200 | 'fooTimestampIsNull.json' |
      | '?fooChar=fc'                                                                        |    200 | 'fooChar.json'            |
      | '?fooVarchar=fVch'                                                                   |    200 | 'fooVarchar.json'         |
      | '?fooText=Lorem Ipsum Dolor'                                                         |    200 | 'fooText.json'            |
      | '?fooSmallint=1'                                                                     |    200 | 'fooSmallint.json'        |
      | '?fooInteger=10'                                                                     |    200 | 'fooInteger.json'         |
      | '?fooBigint=100'                                                                     |    200 | 'fooBigint.json'          |
      | '?fooReal=1.1'                                                                       |    200 | 'fooReal.json'            |
      | '?fooDouble=10.01'                                                                   |    200 | 'fooDouble.json'          |
      | '?fooDecimal=0.0001'                                                                 |    200 | 'fooDecimal.json'         |
      | '?fooBoolean=true'                                                                   |    200 | 'fooBoolean.json'         |
      | '?fooDate=2019-01-01'                                                                |    200 | 'fooDate_1.json'          |
      | '?fooDate=2019-01-10'                                                                |    200 | 'fooDate_2.json'          |
      | '?fooDateMin=2018-01-01&fooDateMax=2018-12-31'                                       |    200 | 'empty.json'              |
      | '?fooDateMin=2018-01-01&fooDateMax=2019-01-01'                                       |    200 | 'fooDate_1.json'          |
      | '?fooDateMin=2018-01-01&fooDateMax=2019-01-09'                                       |    200 | 'fooDate_1.json'          |
      | '?fooDateMin=2019-01-01&fooDateMax=2019-01-10'                                       |    200 | 'fooDate_both.json'       |
      | '?fooDateMin=2019-01-02&fooDateMax=2019-01-10'                                       |    200 | 'fooDate_2.json'          |
      | '?fooDateMin=2019-01-10&fooDateMax=2019-12-31'                                       |    200 | 'fooDate_2.json'          |
      | '?fooDateMin=2018-01-11&fooDateMax=2018-12-31'                                       |    200 | 'empty.json'              |
      | '?fooDateMin=2019-01-01'                                                             |    200 | 'fooDate_both.json'       |
      | '?fooDateMin=2019-01-02'                                                             |    200 | 'fooDate_2.json'          |
      | '?fooDateMax=2019-01-09'                                                             |    200 | 'fooDate_1.json'          |
      | '?fooDateMax=2019-01-10'                                                             |    200 | 'fooDate_both.json'       |
      | '?fooTimestamp=2019-01-01T00:00:00Z'                                                 |    200 | 'fooTimestamp_1.json'     |
      | '?fooTimestamp=2019-01-01T00:00:00-05:00'                                            |    200 | 'fooTimestamp_2.json'     |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2018-12-31T23:59:59.999Z'     |    200 | 'empty.json'              |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2019-01-01T00:00:00Z'         |    200 | 'fooTimestamp_1.json'     |
      | '?fooTimestampMin=2018-01-01T00:00:00Z&fooTimestampMax=2019-01-01T04:59:59.999Z'     |    200 | 'fooTimestamp_1.json'     |
      | '?fooTimestampMin=2019-01-01T00:00:00Z&fooTimestampMax=2019-01-01T05:00:00Z'         |    200 | 'fooTimestamp_both.json'  |
      | '?fooTimestampMin=2019-01-01T00:00:00.001Z&fooTimestampMax=2019-01-01T05:00:00Z'     |    200 | 'fooTimestamp_2.json'     |
      | '?fooTimestampMin=2019-01-01T05:00:00Z&fooTimestampMax=2019-12-31T23:59:59.999Z'     |    200 | 'fooTimestamp_2.json'     |
      | '?fooTimestampMin=2019-01-01T05:00:00.001Z&fooTimestampMax=2019-12-31T23:59:59.999Z' |    200 | 'empty.json'              |
      | '?fooTimestampMin=2019-01-01T00:00:00Z'                                              |    200 | 'fooTimestamp_both.json'  |
      | '?fooTimestampMin=2019-01-01T00:00:00.001Z'                                          |    200 | 'fooTimestamp_2.json'     |
      | '?fooTimestampMax=2019-01-01T04:59:59.999Z'                                          |    200 | 'fooTimestamp_1.json'     |
      | '?fooTimestampMax=2019-01-01T05:00:00Z'                                              |    200 | 'fooTimestamp_both.json'  |

  Esquema del escenario: Al enviar al endpoint la consulta GET con los parametros <params>, se obtiene una respuesta del servicio ordenada
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en la solicitud la consulta "GET" con los parametros <params>
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <response>

    Ejemplos: 
      | params                                                                                  | status | response                |
      | '?fooDateMin=2019-01-01&fooDateMax=2019-01-10&sortProperty=fooDate&sortAscendant=true'  |    200 | 'fooDate_sortAsc.json'  |
      | '?fooDateMin=2019-01-01&fooDateMax=2019-01-10&sortProperty=fooDate&sortAscendant=false' |    200 | 'fooDate_sortDesc.json' |

  Esquema del escenario: Al enviar al endpoint la consulta GET con los parametros <params>, se obtiene una error en la respuesta
    Dado el endpoint "/api/v1/foo"
    Cuando se envia en la solicitud la consulta "GET" con los parametros <params>
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <response>

    Ejemplos: 
      | params                                                                                      | status | response                    |
      | '?fooDateMin=2019-01-01&fooDateMax=2019-01-10&sortProperty=fooDateTime&sortAscendant=false' |    400 | 'badSortPropertyValue.json' |

  Esquema del escenario: Al enviar al endpoint la consulta GET hacia el identificador de recurso <id>, se obtiene una respuesta del servicio
    Dado el endpoint "/api/v1/foo/{id}"
    Cuando se envia en la solicitud la consulta "GET" con el identificador <id>
    Entonces se obtiene en la respuesta el codigo de error <status>
    Y en el cuerpo de la respuesta el json <response>

    Ejemplos: 
      | id      | status | response                |
      | '1'     |    200 | 'foo1.json'             |
      | '2'     |    200 | 'foo2.json'             |
      | '3'     |    200 | 'foo3.json'             |
      | '10000' |    404 | 'foo10000NotFound.json' |
