#language: es
Característica: Establecer un contexto transaccional a nivel de la capa de servicios
  A fin mantener la transaccionalidad a nivel de la capa de servicios
  Siendo un desarrollador backend
  Quiero una configuración de la transacción que me permita propagar la transacción y manejo de errores sobre la capa de servicio
  
  Regla: El mensaje json de error debe tener el siguiente formato:
  """
  {
  "timestamp": "2000-12-31T00:00:00.000Z",
  "status": 500,
  "error": "Reason of the status code",
  "message": "Some custom message"
  }
  """

  Antecedentes: 
    Dado los archivos con datos para pruebas en la ruta: "classpath:com/escalableapps/cmdb/baseapp/template/transaction/data/"

  Escenario: Habiendose persistido una entidad en base de datos; al producirse una excepcion en la capa de servicios; se revierte la transacción
    Dado el endpoint '/tx/singleTxWithReversion'
    Cuando se efectua la solicitud 'GET'
    Entonces responde con el código de error 409
    Y en el cuerpo de la respuesta la propiedad 'message' tiene el id de la entidad creada
    Y en la base de datos no existe la entidad con el id creado

  Escenario: Habiendose persistido una entidad en base de datos; al producirse una excepcion en la capa de control; no se revierte la transacción
    Dado el endpoint '/tx/singleTxWithoutReversion'
    Cuando se efectua la solicitud 'GET'
    Entonces responde con el código de error 409
    Y en el cuerpo de la respuesta la propiedad 'message' tiene el id de la entidad creada
    Y en la base de datos existe la entidad con el id creado

  Escenario: Habiendose persistido dos entidades en base de datos, la primera dentro de la transacción original y la segunda en una nueva transacción; al producirse una excepcion en la capa de servicios; se revierte la transacción original del servicio pero la segunda transacción persiste
    Dado el endpoint '/tx/doubleTxWithFirstReversion'
    Cuando se efectua la solicitud 'GET'
    Entonces responde con el código de error 409
    Y en el cuerpo de la respuesta la propiedad 'message' tiene los ids de las entidades creadas
    Y en la base de datos no existe la primera entidad con el id creado
    Y en la base de datos existe la segunda entidad con el id creado

  Escenario: Habiendose persistido dos entidades en base de datos, la primera dentro de la transacción original y la segunda en una nueva transacción; al producirse una excepcion en la capa de servicios con la nueva transacción; se persiste la transacción original del servicio pero la segunda transacción se revierte
    Dado el endpoint '/tx/doubleTxWithSecondReversion'
    Cuando se efectua la solicitud 'GET'
    Entonces responde con el código de error 409
    Y en el cuerpo de la respuesta la propiedad 'message' tiene los ids de las entidades creadas
    Y en la base de datos existe la primera entidad con el id creado
    Y en la base de datos no existe la segunda entidad con el id creado

  Escenario: Habiendose persistido dos entidades en base de datos, la ambas dentro de la misma transacción; al producirse una excepcion en la capa de servicios con la nueva transacción; ambas operaciones se revierten
    Dado el endpoint '/tx/doubleTxWithBothReversion'
    Cuando se efectua la solicitud 'GET'
    Entonces responde con el código de error 409
    Y en el cuerpo de la respuesta la propiedad 'message' tiene los ids de las entidades creadas
    Y en la base de datos no existe la primera entidad con el id creado
    Y en la base de datos no existe la segunda entidad con el id creado
