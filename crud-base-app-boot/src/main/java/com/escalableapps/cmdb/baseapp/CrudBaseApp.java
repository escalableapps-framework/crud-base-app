package com.escalableapps.cmdb.baseapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudBaseApp {

  public static void main(String[] args) {
    SpringApplication.run(CrudBaseApp.class, args);
  }
}
