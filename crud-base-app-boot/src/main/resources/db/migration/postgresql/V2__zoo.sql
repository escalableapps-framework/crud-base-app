CREATE TABLE crud_base_db.zoo(
  zoo_org bigint,
  zoo_id serial,
  zoo_char char(2),
  zoo_varchar varchar(4),
  zoo_text text,
  zoo_smallint smallint,
  zoo_integer integer,
  zoo_bigint bigint,
  zoo_real real,
  zoo_double double precision,
  zoo_decimal decimal(6, 4),
  zoo_boolean boolean,
  zoo_date date,
  zoo_timestamp timestamptz(3),
  CONSTRAINT zoo_pk PRIMARY KEY (zoo_org, zoo_id)
);

CREATE TABLE crud_base_db.look(
  look_org bigint,
  look_id bigint,
  look_char char not null,
  look_varchar varchar not null,
  look_text text not null,
  look_smallint smallint not null,
  look_integer integer not null,
  look_bigint bigint not null,
  look_real real not null,
  look_double double precision not null,
  look_decimal decimal not null,
  look_boolean boolean not null,
  look_date date not null,
  look_timestamp timestamptz not null,
  CONSTRAINT look_pk PRIMARY KEY (look_org, look_id)
);