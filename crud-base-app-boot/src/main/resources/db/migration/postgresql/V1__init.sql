CREATE SCHEMA crud_base_db;

CREATE TABLE crud_base_db.foo(
  foo_id serial,
  foo_char char(2),
  foo_varchar varchar(4),
  foo_text text,
  foo_smallint smallint,
  foo_integer integer,
  foo_bigint bigint,
  foo_real real,
  foo_double double precision,
  foo_decimal decimal(6, 4),
  foo_boolean boolean,
  foo_date date,
  foo_timestamp timestamptz(3),
  CONSTRAINT foo_pk PRIMARY KEY (foo_id)
);

CREATE TABLE crud_base_db.bar(
  bar_id serial,
  bar_char char not null,
  bar_varchar varchar not null,
  bar_text text not null,
  bar_smallint smallint not null,
  bar_integer integer not null,
  bar_bigint bigint not null,
  bar_real real not null,
  bar_double double precision not null,
  bar_decimal decimal not null,
  bar_boolean boolean not null,
  bar_date date not null,
  bar_timestamp timestamptz not null,
  CONSTRAINT bar_pk PRIMARY KEY (bar_id)
);