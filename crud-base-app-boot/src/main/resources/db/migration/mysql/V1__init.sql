CREATE TABLE crud_base_db.foo(
  foo_id int NOT NULL AUTO_INCREMENT,
  foo_char char(2),
  foo_varchar varchar(4),
  foo_text text,
  foo_smallint smallint,
  foo_integer integer,
  foo_bigint bigint,
  foo_real real,
  foo_double double precision,
  foo_decimal decimal(6, 4),
  foo_boolean boolean,
  foo_date date,
  foo_timestamp timestamp(3),
  CONSTRAINT foo_pk PRIMARY KEY (foo_id)
);

CREATE TABLE crud_base_db.bar(
  bar_id int NOT NULL AUTO_INCREMENT,
  bar_char char not null,
  bar_varchar varchar(100) not null,
  bar_text text not null,
  bar_smallint smallint not null,
  bar_integer integer not null,
  bar_bigint bigint not null,
  bar_real real not null,
  bar_double double precision not null,
  bar_decimal decimal not null,
  bar_boolean boolean not null,
  bar_date date not null,
  bar_timestamp timestamp not null,
  CONSTRAINT bar_pk PRIMARY KEY (bar_id)
);

CREATE TABLE crud_base_db.zoo(
  zoo_org bigint,
  zoo_id int NOT NULL AUTO_INCREMENT,
  zoo_char char(2),
  zoo_varchar varchar(4),
  zoo_text text,
  zoo_smallint smallint,
  zoo_integer integer,
  zoo_bigint bigint,
  zoo_real real,
  zoo_double double precision,
  zoo_decimal decimal(6, 4),
  zoo_boolean boolean,
  zoo_date date,
  zoo_timestamp timestamp(3),
  CONSTRAINT zoo_pk PRIMARY KEY (zoo_org, zoo_id)
);

CREATE TABLE crud_base_db.look(
  look_org bigint,
  look_id bigint,
  look_char char not null,
  look_varchar varchar(100) not null,
  look_text text not null,
  look_smallint smallint not null,
  look_integer integer not null,
  look_bigint bigint not null,
  look_real real not null,
  look_double double precision not null,
  look_decimal decimal not null,
  look_boolean boolean not null,
  look_date date not null,
  look_timestamp timestamp not null,
  CONSTRAINT look_pk PRIMARY KEY (look_org, look_id)
);