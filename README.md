# Crud Base App
Crud Base App project for Spring Boot

>>>
Contents
 1. [License](#1-license)  
 1.1 [MIT License](#11-mit-license)  
 1.2 [License Summary](#12-lincense-summary)  
 2. [How to run](#2-how-to-run)  
 2.1 [Prerequisites](#21-prerequisites)  
 2.2 [Run from your IDE](#22-run-from-your-ide)  
 2.3 [Run from command line](#23-run-from-command-line)  
 3. [Hexagonal architecture](#3-hexagonal-architecture)
>>>

## 1. License

### 1.1 MIT License
Projects referencing this document are released under the terms of the [MIT License](LICENSE).

### 1.2 License Summary

#### 1.2.1 You can

**Commercial Use**: You may use the work commercially.

**Modify**: You may make changes to the work.

**Distribute**: You may distribute the compiled code and/or source.

**Sublicense**: You may incorporate the work into something that has a more restrictive license.

**Private Use**: You may use the work for private use.

#### 1.2.2 You cannot

**Hold Liable**: The work is provided "as is". You may not hold the author liable.

#### 1.2.3 You must

**Include Copyright**: You must include the copyright notice in all copies or substantial uses of the work.

**Include License**: You must include the license notice in all copies or substantial uses of the work.

## 2. How to run

### 2.1 Prerequisites

In order to run this Work, you must have the following

**Linux**: You must have some version of Linux. In case you need to use Windows, it is recommended to use [Git Bash](https://git-scm.com/downloads) in order to be able to execute the commands indicated in this document.

**JDK**: You must have Java Development Kit version 1.8 installed. `javac --version`

**Maven**: You must have Maven version 3.6 or later installed. `mvn --version`

**IDE for Java**: You must have [Eclipse](https://www.eclipse.org/downloads/packages/), [IntelliJ IDEA](https://www.jetbrains.com/idea/download) or [NetBeans](https://netbeans.apache.org/download/index.html)

**PostgreSQL**: You must have the [PostgreSQL](https://www.postgresql.org/download/) database running on `localhost` and the `cmdb` database must have been created.

### 2.2 Run from your IDE

1. Open the project module `crud-base-app-boot`
2. Open the `com.escalableapps.cmdb.baseapp` package
3. Run the `CrudBaseApp.java` class as an application

### 2.3 Run from command line

1. Change to the project folder `cd your-project-folder`
2. Install the project into maven local repository `mvn clean install -DskipTests`
3. Run the generated jar `java -jar crud-base-app-boot/target/crud-base-app-boot-0.0.1-SNAPSHOT.jar`

## 3. Hexagonal architecture

![](site/hexagonal-dsl.svg)
