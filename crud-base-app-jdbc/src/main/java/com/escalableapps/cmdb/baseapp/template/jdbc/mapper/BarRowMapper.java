package com.escalableapps.cmdb.baseapp.template.jdbc.mapper;

import java.math.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;

import org.springframework.jdbc.core.RowMapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public class BarRowMapper implements RowMapper<Bar> {

  @Override
  public Bar mapRow(ResultSet rs, int rowNum) throws SQLException {
    Bar bar = new Bar();
    bar.setBarId(rs.getObject("barId", Integer.class));
    bar.setBarChar(rs.getObject("barChar", String.class));
    bar.setBarVarchar(rs.getObject("barVarchar", String.class));
    bar.setBarText(rs.getObject("barText", String.class));
    bar.setBarSmallint(rs.getObject("barSmallint", Short.class));
    bar.setBarInteger(rs.getObject("barInteger", Integer.class));
    bar.setBarBigint(rs.getObject("barBigint", Long.class));
    bar.setBarReal(rs.getObject("barReal", Float.class));
    bar.setBarDouble(rs.getObject("barDouble", Double.class));
    bar.setBarDecimal(rs.getObject("barDecimal", BigDecimal.class));
    bar.setBarBoolean(rs.getObject("barBoolean", Boolean.class));
    bar.setBarDate(rs.getObject("barDate", LocalDate.class));
    bar.setBarTimestamp(rs.getObject("barTimestamp", OffsetDateTime.class));
    return bar;
  }
}
