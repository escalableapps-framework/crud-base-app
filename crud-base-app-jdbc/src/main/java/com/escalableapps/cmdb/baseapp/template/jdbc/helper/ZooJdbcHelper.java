package com.escalableapps.cmdb.baseapp.template.jdbc.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class ZooJdbcHelper {

  private static final String ZOO_ORG = "zooOrg";
  private static final String ZOO_ID = "zooId";
  private static final String ZOO_CHAR = "zooChar";
  private static final String ZOO_VARCHAR = "zooVarchar";
  private static final String ZOO_TEXT = "zooText";
  private static final String ZOO_SMALLINT = "zooSmallint";
  private static final String ZOO_INTEGER = "zooInteger";
  private static final String ZOO_BIGINT = "zooBigint";
  private static final String ZOO_REAL = "zooReal";
  private static final String ZOO_DOUBLE = "zooDouble";
  private static final String ZOO_DECIMAL = "zooDecimal";
  private static final String ZOO_BOOLEAN = "zooBoolean";
  private static final String ZOO_DATE = "zooDate";
  private static final String ZOO_DATE_MIN = ZOO_DATE + "Min";
  private static final String ZOO_DATE_MAX = ZOO_DATE + "Max";
  private static final String ZOO_TIMESTAMP = "zooTimestamp";
  private static final String ZOO_TIMESTAMP_MIN = ZOO_TIMESTAMP + "Min";
  private static final String ZOO_TIMESTAMP_MAX = ZOO_TIMESTAMP + "Max";

  private static final String ZOO_ORG_IS_NULL = "zooOrgIsNull";
  private static final String ZOO_ID_IS_NULL = "zooIdIsNull";
  private static final String ZOO_CHAR_IS_NULL = "zooCharIsNull";
  private static final String ZOO_VARCHAR_IS_NULL = "zooVarcharIsNull";
  private static final String ZOO_TEXT_IS_NULL = "zooTextIsNull";
  private static final String ZOO_SMALLINT_IS_NULL = "zooSmallintIsNull";
  private static final String ZOO_INTEGER_IS_NULL = "zooIntegerIsNull";
  private static final String ZOO_BIGINT_IS_NULL = "zooBigintIsNull";
  private static final String ZOO_REAL_IS_NULL = "zooRealIsNull";
  private static final String ZOO_DOUBLE_IS_NULL = "zooDoubleIsNull";
  private static final String ZOO_DECIMAL_IS_NULL = "zooDecimalIsNull";
  private static final String ZOO_BOOLEAN_IS_NULL = "zooBooleanIsNull";
  private static final String ZOO_DATE_IS_NULL = "zooDateIsNull";
  private static final String ZOO_TIMESTAMP_IS_NULL = "zooTimestampIsNull";

  private static final String ZOO_CHAR_UPDATED = "zooCharUpdated";
  private static final String ZOO_VARCHAR_UPDATED = "zooVarcharUpdated";
  private static final String ZOO_TEXT_UPDATED = "zooTextUpdated";
  private static final String ZOO_SMALLINT_UPDATED = "zooSmallintUpdated";
  private static final String ZOO_INTEGER_UPDATED = "zooIntegerUpdated";
  private static final String ZOO_BIGINT_UPDATED = "zooBigintUpdated";
  private static final String ZOO_REAL_UPDATED = "zooRealUpdated";
  private static final String ZOO_DOUBLE_UPDATED = "zooDoubleUpdated";
  private static final String ZOO_DECIMAL_UPDATED = "zooDecimalUpdated";
  private static final String ZOO_BOOLEAN_UPDATED = "zooBooleanUpdated";
  private static final String ZOO_DATE_UPDATED = "zooDateUpdated";
  private static final String ZOO_TIMESTAMP_UPDATED = "zooTimestampUpdated";

  private static final HashMap<ZooPropertyName, String> SORT_PROPERTIES;

  static {
    SORT_PROPERTIES = new HashMap<>();
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_ORG, "zoo.zoo_org");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_ID, "zoo.zoo_id");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_CHAR, "zoo.zoo_char");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_VARCHAR, "zoo.zoo_varchar");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_TEXT, "zoo.zoo_text");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_SMALLINT, "zoo.zoo_smallint");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_INTEGER, "zoo.zoo_integer");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_BIGINT, "zoo.zoo_bigint");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_REAL, "zoo.zoo_real");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_DOUBLE, "zoo.zoo_double");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_DECIMAL, "zoo.zoo_decimal");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_BOOLEAN, "zoo.zoo_boolean");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_DATE, "zoo.zoo_date");
    SORT_PROPERTIES.put(ZooPropertyName.ZOO_TIMESTAMP, "zoo.zoo_timestamp");
  }

  public static MapSqlParameterSource insertValues(Zoo zoo) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue(ZOO_ORG, zoo.getZooOrg());
    paramMap.addValue(ZOO_ID, zoo.getZooId());
    paramMap.addValue(ZOO_CHAR, zoo.getZooChar());
    paramMap.addValue(ZOO_VARCHAR, zoo.getZooVarchar());
    paramMap.addValue(ZOO_TEXT, zoo.getZooText());
    paramMap.addValue(ZOO_SMALLINT, zoo.getZooSmallint());
    paramMap.addValue(ZOO_INTEGER, zoo.getZooInteger());
    paramMap.addValue(ZOO_BIGINT, zoo.getZooBigint());
    paramMap.addValue(ZOO_REAL, zoo.getZooReal());
    paramMap.addValue(ZOO_DOUBLE, zoo.getZooDouble());
    paramMap.addValue(ZOO_DECIMAL, zoo.getZooDecimal());
    paramMap.addValue(ZOO_BOOLEAN, zoo.getZooBoolean());
    paramMap.addValue(ZOO_DATE, zoo.getZooDate());
    paramMap.addValue(ZOO_TIMESTAMP, zoo.getZooTimestamp());
    return paramMap;
  }

  public static MapSqlParameterSource whereCondition(ZooQuery zooQuery) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(ZOO_ORG_IS_NULL, isNullValue(zooQuery.getZooOrg()));
    paramMap.addValue(ZOO_ORG, getQueryValue(zooQuery.getZooOrg()));

    paramMap.addValue(ZOO_ID_IS_NULL, isNullValue(zooQuery.getZooId()));
    paramMap.addValue(ZOO_ID, getQueryValue(zooQuery.getZooId()));

    paramMap.addValue(ZOO_CHAR_IS_NULL, isNullValue(zooQuery.getZooChar()));
    paramMap.addValue(ZOO_CHAR, getQueryValue(zooQuery.getZooChar()));

    paramMap.addValue(ZOO_VARCHAR_IS_NULL, isNullValue(zooQuery.getZooVarchar()));
    paramMap.addValue(ZOO_VARCHAR, getQueryValue(zooQuery.getZooVarchar()));

    paramMap.addValue(ZOO_TEXT_IS_NULL, isNullValue(zooQuery.getZooText()));
    paramMap.addValue(ZOO_TEXT, getQueryValue(zooQuery.getZooText()));

    paramMap.addValue(ZOO_SMALLINT_IS_NULL, isNullValue(zooQuery.getZooSmallint()));
    paramMap.addValue(ZOO_SMALLINT, getQueryValue(zooQuery.getZooSmallint()));

    paramMap.addValue(ZOO_INTEGER_IS_NULL, isNullValue(zooQuery.getZooInteger()));
    paramMap.addValue(ZOO_INTEGER, getQueryValue(zooQuery.getZooInteger()));

    paramMap.addValue(ZOO_BIGINT_IS_NULL, isNullValue(zooQuery.getZooBigint()));
    paramMap.addValue(ZOO_BIGINT, getQueryValue(zooQuery.getZooBigint()));

    paramMap.addValue(ZOO_REAL_IS_NULL, isNullValue(zooQuery.getZooReal()));
    paramMap.addValue(ZOO_REAL, getQueryValue(zooQuery.getZooReal()));

    paramMap.addValue(ZOO_DOUBLE_IS_NULL, isNullValue(zooQuery.getZooDouble()));
    paramMap.addValue(ZOO_DOUBLE, getQueryValue(zooQuery.getZooDouble()));

    paramMap.addValue(ZOO_DECIMAL_IS_NULL, isNullValue(zooQuery.getZooDecimal()));
    paramMap.addValue(ZOO_DECIMAL, getQueryValue(zooQuery.getZooDecimal()));

    paramMap.addValue(ZOO_BOOLEAN_IS_NULL, isNullValue(zooQuery.getZooBoolean()));
    paramMap.addValue(ZOO_BOOLEAN, getQueryValue(zooQuery.getZooBoolean()));

    paramMap.addValue(ZOO_DATE_IS_NULL, isNullValue(zooQuery.getZooDate()));
    paramMap.addValue(ZOO_DATE, getQueryValue(zooQuery.getZooDate()));
    paramMap.addValue(ZOO_DATE_MIN, getQueryValue(zooQuery.getZooDateMin()));
    paramMap.addValue(ZOO_DATE_MAX, getQueryValue(zooQuery.getZooDateMax()));

    paramMap.addValue(ZOO_TIMESTAMP_IS_NULL, isNullValue(zooQuery.getZooTimestamp()));
    paramMap.addValue(ZOO_TIMESTAMP, getQueryValue(zooQuery.getZooTimestamp()));
    paramMap.addValue(ZOO_TIMESTAMP_MIN, getQueryValue(zooQuery.getZooTimestampMin()));
    paramMap.addValue(ZOO_TIMESTAMP_MAX, getQueryValue(zooQuery.getZooTimestampMax()));
    return paramMap;
  }

  public static String orderBy(String sql, List<SortRequest<ZooPropertyName>> sorts) {
    List<String> orders = sorts.stream().map(sort -> {
      boolean asc = sort.isAscendant();
      if (SORT_PROPERTIES.containsKey(sort.getProperty())) {
        return new StringBuilder(SORT_PROPERTIES.get(sort.getProperty())).append(asc ? " ASC" : " DESC").toString();
      } else {
        return null;
      }
    }).filter(Objects::nonNull).collect(Collectors.toList());
    String orderBy = "";
    if (!orders.isEmpty()) {
      orderBy = new StringBuilder("ORDER BY ") //
          .append(orders.stream().collect(Collectors.joining(", "))) //
          .toString();
    }
    return StringUtils.replace(sql, "{{pageRequestOrder}}", orderBy);
  }

  public static MapSqlParameterSource updateValues(ZooUpdate zooUpdate) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(ZOO_CHAR_UPDATED, zooUpdate.getZooChar() != null);
    paramMap.addValue(ZOO_CHAR, getUpdateValue(zooUpdate.getZooChar()));

    paramMap.addValue(ZOO_VARCHAR_UPDATED, zooUpdate.getZooVarchar() != null);
    paramMap.addValue(ZOO_VARCHAR, getUpdateValue(zooUpdate.getZooVarchar()));

    paramMap.addValue(ZOO_TEXT_UPDATED, zooUpdate.getZooText() != null);
    paramMap.addValue(ZOO_TEXT, getUpdateValue(zooUpdate.getZooText()));

    paramMap.addValue(ZOO_SMALLINT_UPDATED, zooUpdate.getZooSmallint() != null);
    paramMap.addValue(ZOO_SMALLINT, getUpdateValue(zooUpdate.getZooSmallint()));

    paramMap.addValue(ZOO_INTEGER_UPDATED, zooUpdate.getZooInteger() != null);
    paramMap.addValue(ZOO_INTEGER, getUpdateValue(zooUpdate.getZooInteger()));

    paramMap.addValue(ZOO_BIGINT_UPDATED, zooUpdate.getZooBigint() != null);
    paramMap.addValue(ZOO_BIGINT, getUpdateValue(zooUpdate.getZooBigint()));

    paramMap.addValue(ZOO_REAL_UPDATED, zooUpdate.getZooReal() != null);
    paramMap.addValue(ZOO_REAL, getUpdateValue(zooUpdate.getZooReal()));

    paramMap.addValue(ZOO_DOUBLE_UPDATED, zooUpdate.getZooDouble() != null);
    paramMap.addValue(ZOO_DOUBLE, getUpdateValue(zooUpdate.getZooDouble()));

    paramMap.addValue(ZOO_DECIMAL_UPDATED, zooUpdate.getZooDecimal() != null);
    paramMap.addValue(ZOO_DECIMAL, getUpdateValue(zooUpdate.getZooDecimal()));

    paramMap.addValue(ZOO_BOOLEAN_UPDATED, zooUpdate.getZooBoolean() != null);
    paramMap.addValue(ZOO_BOOLEAN, getUpdateValue(zooUpdate.getZooBoolean()));

    paramMap.addValue(ZOO_DATE_UPDATED, zooUpdate.getZooDate() != null);
    paramMap.addValue(ZOO_DATE, getUpdateValue(zooUpdate.getZooDate()));

    paramMap.addValue(ZOO_TIMESTAMP_UPDATED, zooUpdate.getZooTimestamp() != null);
    paramMap.addValue(ZOO_TIMESTAMP, getUpdateValue(zooUpdate.getZooTimestamp()));

    return paramMap;
  }

  private static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null || !queryValue.exist() ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue == null || !updateValue.exist() ? null : updateValue.get();
  }

  private ZooJdbcHelper() {
  }
}
