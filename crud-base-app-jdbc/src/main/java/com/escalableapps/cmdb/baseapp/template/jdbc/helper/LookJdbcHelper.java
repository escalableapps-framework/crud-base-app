package com.escalableapps.cmdb.baseapp.template.jdbc.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class LookJdbcHelper {

  private static final String LOOK_ORG = "lookOrg";
  private static final String LOOK_ID = "lookId";
  private static final String LOOK_CHAR = "lookChar";
  private static final String LOOK_VARCHAR = "lookVarchar";
  private static final String LOOK_TEXT = "lookText";
  private static final String LOOK_SMALLINT = "lookSmallint";
  private static final String LOOK_INTEGER = "lookInteger";
  private static final String LOOK_BIGINT = "lookBigint";
  private static final String LOOK_REAL = "lookReal";
  private static final String LOOK_DOUBLE = "lookDouble";
  private static final String LOOK_DECIMAL = "lookDecimal";
  private static final String LOOK_BOOLEAN = "lookBoolean";
  private static final String LOOK_DATE = "lookDate";
  private static final String LOOK_DATE_MIN = LOOK_DATE + "Min";
  private static final String LOOK_DATE_MAX = LOOK_DATE + "Max";
  private static final String LOOK_TIMESTAMP = "lookTimestamp";
  private static final String LOOK_TIMESTAMP_MIN = LOOK_TIMESTAMP + "Min";
  private static final String LOOK_TIMESTAMP_MAX = LOOK_TIMESTAMP + "Max";

  private static final String LOOK_ORG_IS_NULL = "lookOrgIsNull";
  private static final String LOOK_ID_IS_NULL = "lookIdIsNull";
  private static final String LOOK_CHAR_IS_NULL = "lookCharIsNull";
  private static final String LOOK_VARCHAR_IS_NULL = "lookVarcharIsNull";
  private static final String LOOK_TEXT_IS_NULL = "lookTextIsNull";
  private static final String LOOK_SMALLINT_IS_NULL = "lookSmallintIsNull";
  private static final String LOOK_INTEGER_IS_NULL = "lookIntegerIsNull";
  private static final String LOOK_BIGINT_IS_NULL = "lookBigintIsNull";
  private static final String LOOK_REAL_IS_NULL = "lookRealIsNull";
  private static final String LOOK_DOUBLE_IS_NULL = "lookDoubleIsNull";
  private static final String LOOK_DECIMAL_IS_NULL = "lookDecimalIsNull";
  private static final String LOOK_BOOLEAN_IS_NULL = "lookBooleanIsNull";
  private static final String LOOK_DATE_IS_NULL = "lookDateIsNull";
  private static final String LOOK_TIMESTAMP_IS_NULL = "lookTimestampIsNull";

  private static final String LOOK_CHAR_UPDATED = "lookCharUpdated";
  private static final String LOOK_VARCHAR_UPDATED = "lookVarcharUpdated";
  private static final String LOOK_TEXT_UPDATED = "lookTextUpdated";
  private static final String LOOK_SMALLINT_UPDATED = "lookSmallintUpdated";
  private static final String LOOK_INTEGER_UPDATED = "lookIntegerUpdated";
  private static final String LOOK_BIGINT_UPDATED = "lookBigintUpdated";
  private static final String LOOK_REAL_UPDATED = "lookRealUpdated";
  private static final String LOOK_DOUBLE_UPDATED = "lookDoubleUpdated";
  private static final String LOOK_DECIMAL_UPDATED = "lookDecimalUpdated";
  private static final String LOOK_BOOLEAN_UPDATED = "lookBooleanUpdated";
  private static final String LOOK_DATE_UPDATED = "lookDateUpdated";
  private static final String LOOK_TIMESTAMP_UPDATED = "lookTimestampUpdated";

  private static final HashMap<LookPropertyName, String> SORT_PROPERTIES;

  static {
    SORT_PROPERTIES = new HashMap<>();
    SORT_PROPERTIES.put(LookPropertyName.LOOK_ORG, "look.look_org");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_ID, "look.look_id");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_CHAR, "look.look_char");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_VARCHAR, "look.look_varchar");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_TEXT, "look.look_text");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_SMALLINT, "look.look_smallint");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_INTEGER, "look.look_integer");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_BIGINT, "look.look_bigint");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_REAL, "look.look_real");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_DOUBLE, "look.look_double");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_DECIMAL, "look.look_decimal");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_BOOLEAN, "look.look_boolean");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_DATE, "look.look_date");
    SORT_PROPERTIES.put(LookPropertyName.LOOK_TIMESTAMP, "look.look_timestamp");
  }

  public static MapSqlParameterSource insertValues(Look look) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue(LOOK_ORG, look.getLookOrg());
    paramMap.addValue(LOOK_ID, look.getLookId());
    paramMap.addValue(LOOK_CHAR, look.getLookChar());
    paramMap.addValue(LOOK_VARCHAR, look.getLookVarchar());
    paramMap.addValue(LOOK_TEXT, look.getLookText());
    paramMap.addValue(LOOK_SMALLINT, look.getLookSmallint());
    paramMap.addValue(LOOK_INTEGER, look.getLookInteger());
    paramMap.addValue(LOOK_BIGINT, look.getLookBigint());
    paramMap.addValue(LOOK_REAL, look.getLookReal());
    paramMap.addValue(LOOK_DOUBLE, look.getLookDouble());
    paramMap.addValue(LOOK_DECIMAL, look.getLookDecimal());
    paramMap.addValue(LOOK_BOOLEAN, look.getLookBoolean());
    paramMap.addValue(LOOK_DATE, look.getLookDate());
    paramMap.addValue(LOOK_TIMESTAMP, look.getLookTimestamp());
    return paramMap;
  }

  public static MapSqlParameterSource whereCondition(LookQuery lookQuery) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(LOOK_ORG_IS_NULL, isNullValue(lookQuery.getLookOrg()));
    paramMap.addValue(LOOK_ORG, getQueryValue(lookQuery.getLookOrg()));

    paramMap.addValue(LOOK_ID_IS_NULL, isNullValue(lookQuery.getLookId()));
    paramMap.addValue(LOOK_ID, getQueryValue(lookQuery.getLookId()));

    paramMap.addValue(LOOK_CHAR_IS_NULL, isNullValue(lookQuery.getLookChar()));
    paramMap.addValue(LOOK_CHAR, getQueryValue(lookQuery.getLookChar()));

    paramMap.addValue(LOOK_VARCHAR_IS_NULL, isNullValue(lookQuery.getLookVarchar()));
    paramMap.addValue(LOOK_VARCHAR, getQueryValue(lookQuery.getLookVarchar()));

    paramMap.addValue(LOOK_TEXT_IS_NULL, isNullValue(lookQuery.getLookText()));
    paramMap.addValue(LOOK_TEXT, getQueryValue(lookQuery.getLookText()));

    paramMap.addValue(LOOK_SMALLINT_IS_NULL, isNullValue(lookQuery.getLookSmallint()));
    paramMap.addValue(LOOK_SMALLINT, getQueryValue(lookQuery.getLookSmallint()));

    paramMap.addValue(LOOK_INTEGER_IS_NULL, isNullValue(lookQuery.getLookInteger()));
    paramMap.addValue(LOOK_INTEGER, getQueryValue(lookQuery.getLookInteger()));

    paramMap.addValue(LOOK_BIGINT_IS_NULL, isNullValue(lookQuery.getLookBigint()));
    paramMap.addValue(LOOK_BIGINT, getQueryValue(lookQuery.getLookBigint()));

    paramMap.addValue(LOOK_REAL_IS_NULL, isNullValue(lookQuery.getLookReal()));
    paramMap.addValue(LOOK_REAL, getQueryValue(lookQuery.getLookReal()));

    paramMap.addValue(LOOK_DOUBLE_IS_NULL, isNullValue(lookQuery.getLookDouble()));
    paramMap.addValue(LOOK_DOUBLE, getQueryValue(lookQuery.getLookDouble()));

    paramMap.addValue(LOOK_DECIMAL_IS_NULL, isNullValue(lookQuery.getLookDecimal()));
    paramMap.addValue(LOOK_DECIMAL, getQueryValue(lookQuery.getLookDecimal()));

    paramMap.addValue(LOOK_BOOLEAN_IS_NULL, isNullValue(lookQuery.getLookBoolean()));
    paramMap.addValue(LOOK_BOOLEAN, getQueryValue(lookQuery.getLookBoolean()));

    paramMap.addValue(LOOK_DATE_IS_NULL, isNullValue(lookQuery.getLookDate()));
    paramMap.addValue(LOOK_DATE, getQueryValue(lookQuery.getLookDate()));
    paramMap.addValue(LOOK_DATE_MIN, getQueryValue(lookQuery.getLookDateMin()));
    paramMap.addValue(LOOK_DATE_MAX, getQueryValue(lookQuery.getLookDateMax()));

    paramMap.addValue(LOOK_TIMESTAMP_IS_NULL, isNullValue(lookQuery.getLookTimestamp()));
    paramMap.addValue(LOOK_TIMESTAMP, getQueryValue(lookQuery.getLookTimestamp()));
    paramMap.addValue(LOOK_TIMESTAMP_MIN, getQueryValue(lookQuery.getLookTimestampMin()));
    paramMap.addValue(LOOK_TIMESTAMP_MAX, getQueryValue(lookQuery.getLookTimestampMax()));
    return paramMap;
  }

  public static String orderBy(String sql, List<SortRequest<LookPropertyName>> sorts) {
    List<String> orders = sorts.stream().map(sort -> {
      boolean asc = sort.isAscendant();
      if (SORT_PROPERTIES.containsKey(sort.getProperty())) {
        return new StringBuilder(SORT_PROPERTIES.get(sort.getProperty())).append(asc ? " ASC" : " DESC").toString();
      } else {
        return null;
      }
    }).filter(Objects::nonNull).collect(Collectors.toList());
    String orderBy = "";
    if (!orders.isEmpty()) {
      orderBy = new StringBuilder("ORDER BY ") //
          .append(orders.stream().collect(Collectors.joining(", "))) //
          .toString();
    }
    return StringUtils.replace(sql, "{{pageRequestOrder}}", orderBy);
  }

  public static MapSqlParameterSource updateValues(LookUpdate lookUpdate) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(LOOK_CHAR_UPDATED, lookUpdate.getLookChar() != null);
    paramMap.addValue(LOOK_CHAR, getUpdateValue(lookUpdate.getLookChar()));

    paramMap.addValue(LOOK_VARCHAR_UPDATED, lookUpdate.getLookVarchar() != null);
    paramMap.addValue(LOOK_VARCHAR, getUpdateValue(lookUpdate.getLookVarchar()));

    paramMap.addValue(LOOK_TEXT_UPDATED, lookUpdate.getLookText() != null);
    paramMap.addValue(LOOK_TEXT, getUpdateValue(lookUpdate.getLookText()));

    paramMap.addValue(LOOK_SMALLINT_UPDATED, lookUpdate.getLookSmallint() != null);
    paramMap.addValue(LOOK_SMALLINT, getUpdateValue(lookUpdate.getLookSmallint()));

    paramMap.addValue(LOOK_INTEGER_UPDATED, lookUpdate.getLookInteger() != null);
    paramMap.addValue(LOOK_INTEGER, getUpdateValue(lookUpdate.getLookInteger()));

    paramMap.addValue(LOOK_BIGINT_UPDATED, lookUpdate.getLookBigint() != null);
    paramMap.addValue(LOOK_BIGINT, getUpdateValue(lookUpdate.getLookBigint()));

    paramMap.addValue(LOOK_REAL_UPDATED, lookUpdate.getLookReal() != null);
    paramMap.addValue(LOOK_REAL, getUpdateValue(lookUpdate.getLookReal()));

    paramMap.addValue(LOOK_DOUBLE_UPDATED, lookUpdate.getLookDouble() != null);
    paramMap.addValue(LOOK_DOUBLE, getUpdateValue(lookUpdate.getLookDouble()));

    paramMap.addValue(LOOK_DECIMAL_UPDATED, lookUpdate.getLookDecimal() != null);
    paramMap.addValue(LOOK_DECIMAL, getUpdateValue(lookUpdate.getLookDecimal()));

    paramMap.addValue(LOOK_BOOLEAN_UPDATED, lookUpdate.getLookBoolean() != null);
    paramMap.addValue(LOOK_BOOLEAN, getUpdateValue(lookUpdate.getLookBoolean()));

    paramMap.addValue(LOOK_DATE_UPDATED, lookUpdate.getLookDate() != null);
    paramMap.addValue(LOOK_DATE, getUpdateValue(lookUpdate.getLookDate()));

    paramMap.addValue(LOOK_TIMESTAMP_UPDATED, lookUpdate.getLookTimestamp() != null);
    paramMap.addValue(LOOK_TIMESTAMP, getUpdateValue(lookUpdate.getLookTimestamp()));

    return paramMap;
  }

  private static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null || !queryValue.exist() ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue == null || !updateValue.exist() ? null : updateValue.get();
  }

  private LookJdbcHelper() {
  }
}
