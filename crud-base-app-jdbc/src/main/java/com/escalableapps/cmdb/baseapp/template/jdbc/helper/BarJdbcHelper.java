package com.escalableapps.cmdb.baseapp.template.jdbc.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class BarJdbcHelper {

  private static final String BAR_ID = "barId";
  private static final String BAR_CHAR = "barChar";
  private static final String BAR_VARCHAR = "barVarchar";
  private static final String BAR_TEXT = "barText";
  private static final String BAR_SMALLINT = "barSmallint";
  private static final String BAR_INTEGER = "barInteger";
  private static final String BAR_BIGINT = "barBigint";
  private static final String BAR_REAL = "barReal";
  private static final String BAR_DOUBLE = "barDouble";
  private static final String BAR_DECIMAL = "barDecimal";
  private static final String BAR_BOOLEAN = "barBoolean";
  private static final String BAR_DATE = "barDate";
  private static final String BAR_DATE_MIN = BAR_DATE + "Min";
  private static final String BAR_DATE_MAX = BAR_DATE + "Max";
  private static final String BAR_TIMESTAMP = "barTimestamp";
  private static final String BAR_TIMESTAMP_MIN = BAR_TIMESTAMP + "Min";
  private static final String BAR_TIMESTAMP_MAX = BAR_TIMESTAMP + "Max";

  private static final String BAR_ID_IS_NULL = "barIdIsNull";
  private static final String BAR_CHAR_IS_NULL = "barCharIsNull";
  private static final String BAR_VARCHAR_IS_NULL = "barVarcharIsNull";
  private static final String BAR_TEXT_IS_NULL = "barTextIsNull";
  private static final String BAR_SMALLINT_IS_NULL = "barSmallintIsNull";
  private static final String BAR_INTEGER_IS_NULL = "barIntegerIsNull";
  private static final String BAR_BIGINT_IS_NULL = "barBigintIsNull";
  private static final String BAR_REAL_IS_NULL = "barRealIsNull";
  private static final String BAR_DOUBLE_IS_NULL = "barDoubleIsNull";
  private static final String BAR_DECIMAL_IS_NULL = "barDecimalIsNull";
  private static final String BAR_BOOLEAN_IS_NULL = "barBooleanIsNull";
  private static final String BAR_DATE_IS_NULL = "barDateIsNull";
  private static final String BAR_TIMESTAMP_IS_NULL = "barTimestampIsNull";

  private static final String BAR_CHAR_UPDATED = "barCharUpdated";
  private static final String BAR_VARCHAR_UPDATED = "barVarcharUpdated";
  private static final String BAR_TEXT_UPDATED = "barTextUpdated";
  private static final String BAR_SMALLINT_UPDATED = "barSmallintUpdated";
  private static final String BAR_INTEGER_UPDATED = "barIntegerUpdated";
  private static final String BAR_BIGINT_UPDATED = "barBigintUpdated";
  private static final String BAR_REAL_UPDATED = "barRealUpdated";
  private static final String BAR_DOUBLE_UPDATED = "barDoubleUpdated";
  private static final String BAR_DECIMAL_UPDATED = "barDecimalUpdated";
  private static final String BAR_BOOLEAN_UPDATED = "barBooleanUpdated";
  private static final String BAR_DATE_UPDATED = "barDateUpdated";
  private static final String BAR_TIMESTAMP_UPDATED = "barTimestampUpdated";

  private static final HashMap<BarPropertyName, String> SORT_PROPERTIES;

  static {
    SORT_PROPERTIES = new HashMap<>();
    SORT_PROPERTIES.put(BarPropertyName.BAR_ID, "bar.bar_id");
    SORT_PROPERTIES.put(BarPropertyName.BAR_CHAR, "bar.bar_char");
    SORT_PROPERTIES.put(BarPropertyName.BAR_VARCHAR, "bar.bar_varchar");
    SORT_PROPERTIES.put(BarPropertyName.BAR_TEXT, "bar.bar_text");
    SORT_PROPERTIES.put(BarPropertyName.BAR_SMALLINT, "bar.bar_smallint");
    SORT_PROPERTIES.put(BarPropertyName.BAR_INTEGER, "bar.bar_integer");
    SORT_PROPERTIES.put(BarPropertyName.BAR_BIGINT, "bar.bar_bigint");
    SORT_PROPERTIES.put(BarPropertyName.BAR_REAL, "bar.bar_real");
    SORT_PROPERTIES.put(BarPropertyName.BAR_DOUBLE, "bar.bar_double");
    SORT_PROPERTIES.put(BarPropertyName.BAR_DECIMAL, "bar.bar_decimal");
    SORT_PROPERTIES.put(BarPropertyName.BAR_BOOLEAN, "bar.bar_boolean");
    SORT_PROPERTIES.put(BarPropertyName.BAR_DATE, "bar.bar_date");
    SORT_PROPERTIES.put(BarPropertyName.BAR_TIMESTAMP, "bar.bar_timestamp");
  }

  public static MapSqlParameterSource insertValues(Bar bar) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue(BAR_ID, bar.getBarId());
    paramMap.addValue(BAR_CHAR, bar.getBarChar());
    paramMap.addValue(BAR_VARCHAR, bar.getBarVarchar());
    paramMap.addValue(BAR_TEXT, bar.getBarText());
    paramMap.addValue(BAR_SMALLINT, bar.getBarSmallint());
    paramMap.addValue(BAR_INTEGER, bar.getBarInteger());
    paramMap.addValue(BAR_BIGINT, bar.getBarBigint());
    paramMap.addValue(BAR_REAL, bar.getBarReal());
    paramMap.addValue(BAR_DOUBLE, bar.getBarDouble());
    paramMap.addValue(BAR_DECIMAL, bar.getBarDecimal());
    paramMap.addValue(BAR_BOOLEAN, bar.getBarBoolean());
    paramMap.addValue(BAR_DATE, bar.getBarDate());
    paramMap.addValue(BAR_TIMESTAMP, bar.getBarTimestamp());
    return paramMap;
  }

  public static MapSqlParameterSource whereCondition(BarQuery barQuery) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(BAR_ID_IS_NULL, isNullValue(barQuery.getBarId()));
    paramMap.addValue(BAR_ID, getQueryValue(barQuery.getBarId()));

    paramMap.addValue(BAR_CHAR_IS_NULL, isNullValue(barQuery.getBarChar()));
    paramMap.addValue(BAR_CHAR, getQueryValue(barQuery.getBarChar()));

    paramMap.addValue(BAR_VARCHAR_IS_NULL, isNullValue(barQuery.getBarVarchar()));
    paramMap.addValue(BAR_VARCHAR, getQueryValue(barQuery.getBarVarchar()));

    paramMap.addValue(BAR_TEXT_IS_NULL, isNullValue(barQuery.getBarText()));
    paramMap.addValue(BAR_TEXT, getQueryValue(barQuery.getBarText()));

    paramMap.addValue(BAR_SMALLINT_IS_NULL, isNullValue(barQuery.getBarSmallint()));
    paramMap.addValue(BAR_SMALLINT, getQueryValue(barQuery.getBarSmallint()));

    paramMap.addValue(BAR_INTEGER_IS_NULL, isNullValue(barQuery.getBarInteger()));
    paramMap.addValue(BAR_INTEGER, getQueryValue(barQuery.getBarInteger()));

    paramMap.addValue(BAR_BIGINT_IS_NULL, isNullValue(barQuery.getBarBigint()));
    paramMap.addValue(BAR_BIGINT, getQueryValue(barQuery.getBarBigint()));

    paramMap.addValue(BAR_REAL_IS_NULL, isNullValue(barQuery.getBarReal()));
    paramMap.addValue(BAR_REAL, getQueryValue(barQuery.getBarReal()));

    paramMap.addValue(BAR_DOUBLE_IS_NULL, isNullValue(barQuery.getBarDouble()));
    paramMap.addValue(BAR_DOUBLE, getQueryValue(barQuery.getBarDouble()));

    paramMap.addValue(BAR_DECIMAL_IS_NULL, isNullValue(barQuery.getBarDecimal()));
    paramMap.addValue(BAR_DECIMAL, getQueryValue(barQuery.getBarDecimal()));

    paramMap.addValue(BAR_BOOLEAN_IS_NULL, isNullValue(barQuery.getBarBoolean()));
    paramMap.addValue(BAR_BOOLEAN, getQueryValue(barQuery.getBarBoolean()));

    paramMap.addValue(BAR_DATE_IS_NULL, isNullValue(barQuery.getBarDate()));
    paramMap.addValue(BAR_DATE, getQueryValue(barQuery.getBarDate()));
    paramMap.addValue(BAR_DATE_MIN, getQueryValue(barQuery.getBarDateMin()));
    paramMap.addValue(BAR_DATE_MAX, getQueryValue(barQuery.getBarDateMax()));

    paramMap.addValue(BAR_TIMESTAMP_IS_NULL, isNullValue(barQuery.getBarTimestamp()));
    paramMap.addValue(BAR_TIMESTAMP, getQueryValue(barQuery.getBarTimestamp()));
    paramMap.addValue(BAR_TIMESTAMP_MIN, getQueryValue(barQuery.getBarTimestampMin()));
    paramMap.addValue(BAR_TIMESTAMP_MAX, getQueryValue(barQuery.getBarTimestampMax()));
    return paramMap;
  }

  public static String orderBy(String sql, List<SortRequest<BarPropertyName>> sorts) {
    List<String> orders = sorts.stream().map(sort -> {
      boolean asc = sort.isAscendant();
      if (SORT_PROPERTIES.containsKey(sort.getProperty())) {
        return new StringBuilder(SORT_PROPERTIES.get(sort.getProperty())).append(asc ? " ASC" : " DESC").toString();
      } else {
        return null;
      }
    }).filter(Objects::nonNull).collect(Collectors.toList());
    String orderBy = "";
    if (!orders.isEmpty()) {
      orderBy = new StringBuilder("ORDER BY ") //
          .append(orders.stream().collect(Collectors.joining(", "))) //
          .toString();
    }
    return StringUtils.replace(sql, "{{pageRequestOrder}}", orderBy);
  }

  public static MapSqlParameterSource updateValues(BarUpdate barUpdate) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(BAR_CHAR_UPDATED, barUpdate.getBarChar() != null);
    paramMap.addValue(BAR_CHAR, getUpdateValue(barUpdate.getBarChar()));

    paramMap.addValue(BAR_VARCHAR_UPDATED, barUpdate.getBarVarchar() != null);
    paramMap.addValue(BAR_VARCHAR, getUpdateValue(barUpdate.getBarVarchar()));

    paramMap.addValue(BAR_TEXT_UPDATED, barUpdate.getBarText() != null);
    paramMap.addValue(BAR_TEXT, getUpdateValue(barUpdate.getBarText()));

    paramMap.addValue(BAR_SMALLINT_UPDATED, barUpdate.getBarSmallint() != null);
    paramMap.addValue(BAR_SMALLINT, getUpdateValue(barUpdate.getBarSmallint()));

    paramMap.addValue(BAR_INTEGER_UPDATED, barUpdate.getBarInteger() != null);
    paramMap.addValue(BAR_INTEGER, getUpdateValue(barUpdate.getBarInteger()));

    paramMap.addValue(BAR_BIGINT_UPDATED, barUpdate.getBarBigint() != null);
    paramMap.addValue(BAR_BIGINT, getUpdateValue(barUpdate.getBarBigint()));

    paramMap.addValue(BAR_REAL_UPDATED, barUpdate.getBarReal() != null);
    paramMap.addValue(BAR_REAL, getUpdateValue(barUpdate.getBarReal()));

    paramMap.addValue(BAR_DOUBLE_UPDATED, barUpdate.getBarDouble() != null);
    paramMap.addValue(BAR_DOUBLE, getUpdateValue(barUpdate.getBarDouble()));

    paramMap.addValue(BAR_DECIMAL_UPDATED, barUpdate.getBarDecimal() != null);
    paramMap.addValue(BAR_DECIMAL, getUpdateValue(barUpdate.getBarDecimal()));

    paramMap.addValue(BAR_BOOLEAN_UPDATED, barUpdate.getBarBoolean() != null);
    paramMap.addValue(BAR_BOOLEAN, getUpdateValue(barUpdate.getBarBoolean()));

    paramMap.addValue(BAR_DATE_UPDATED, barUpdate.getBarDate() != null);
    paramMap.addValue(BAR_DATE, getUpdateValue(barUpdate.getBarDate()));

    paramMap.addValue(BAR_TIMESTAMP_UPDATED, barUpdate.getBarTimestamp() != null);
    paramMap.addValue(BAR_TIMESTAMP, getUpdateValue(barUpdate.getBarTimestamp()));

    return paramMap;
  }

  private static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null || !queryValue.exist() ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue == null || !updateValue.exist() ? null : updateValue.get();
  }

  private BarJdbcHelper() {
  }
}
