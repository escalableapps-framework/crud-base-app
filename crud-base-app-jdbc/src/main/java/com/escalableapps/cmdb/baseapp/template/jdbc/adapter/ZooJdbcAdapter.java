package com.escalableapps.cmdb.baseapp.template.jdbc.adapter;

import static com.escalableapps.cmdb.baseapp.template.jdbc.helper.ZooJdbcHelper.*;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jdbc.mapper.ZooRowMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.ZooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class ZooJdbcAdapter implements ZooSpi {

  private final String countZoosSql;
  private final String createZooSql;
  private final String deleteZooSql;
  private final String existsZooIdSql;
  private final String existsZooSql;
  private final String findZooIdSql;
  private final String findZooIdsSql;
  private final String findZoosSql;
  private final String updateZooSql;

  private final DataSource dataSource;

  public ZooJdbcAdapter(DataSource dataSource) {
    createZooSql = ResourceUtils.readResource("classpath:template/zoo/createZoo.sql");
    findZooIdSql = ResourceUtils.readResource("classpath:template/zoo/findZooId.sql");
    findZooIdsSql = ResourceUtils.readResource("classpath:template/zoo/findZooIds.sql");
    findZoosSql = ResourceUtils.readResource("classpath:template/zoo/findZoos.sql");
    existsZooIdSql = ResourceUtils.readResource("classpath:template/zoo/existsZooId.sql");
    existsZooSql = ResourceUtils.readResource("classpath:template/zoo/existsZoo.sql");
    countZoosSql = ResourceUtils.readResource("classpath:template/zoo/countZoos.sql");
    updateZooSql = ResourceUtils.readResource("classpath:template/zoo/updateZoo.sql");
    deleteZooSql = ResourceUtils.readResource("classpath:template/zoo/deleteZoo.sql");

    this.dataSource = dataSource;
  }

  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoo:zoo={}", zoo);
    MapSqlParameterSource paramMap = insertValues(zoo);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    String[] generatedKeyColumn = {"zoo_id"};

    getTemplate().update(createZooSql, paramMap, keyHolder, generatedKeyColumn);

    zoo.setZooId(keyHolder.getKey().intValue());
    log.debug("createZoo:zoo={}", zoo);
    return zoo;
  }

  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooId", zooId);

    Zoo zoo;
    try {
      zoo = getTemplate().queryForObject(findZooIdSql, paramMap, new ZooRowMapper());
    } catch (EmptyResultDataAccessException e) {
      zoo = null;
    }

    log.debug("findZoo:zoo={}", zoo);
    return Optional.ofNullable(zoo);
  }

  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooIds", zooIds);

    List<Zoo> zoos = getTemplate().query(findZooIdsSql, paramMap, new ZooRowMapper());

    log.debug("findZoos:zoos={}", zoos);

    return zoos;
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery //
  ) {
    return findZoos(zooQuery, new PageRequest<>());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);
    MapSqlParameterSource paramMap = whereCondition(zooQuery);
    String findSortedZoosSql = orderBy(findZoosSql, paging.getSortRequests());
    paramMap.addValue("pageRequestOffset", paging.getOffset());
    paramMap.addValue("pageRequestLimit", paging.getPageSize());

    List<Zoo> zoos = getTemplate().query(findSortedZoosSql, paramMap, new ZooRowMapper());

    log.debug("findZoos:zoos={}", zoos);
    return zoos;
  }

  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooId", zooId);

    Boolean exists = getTemplate().queryForObject(existsZooIdSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoo:zooQuery={}", zooQuery);
    MapSqlParameterSource paramMap = whereCondition(zooQuery);

    Boolean exists = getTemplate().queryForObject(existsZooSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("countZoos:zooQuery={}", zooQuery);
    MapSqlParameterSource paramMap = whereCondition(zooQuery);

    Long count = getTemplate().queryForObject(countZoosSql, paramMap, new SingleColumnRowMapper<Long>());

    log.debug("countZoos:count={}", count);
    return count;
  }

  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("updateZoo:zooOrg={}", zooOrg);
    log.debug("updateZoo:zooId={}", zooId);
    log.debug("updateZoo:zooUpdate={}", zooUpdate);
    MapSqlParameterSource paramMap = updateValues(zooUpdate);
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooId", zooId);

    long updateCount = getTemplate().update(updateZooSql, paramMap);

    log.debug("updateZoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("deleteZoo:zooOrg={}", zooOrg);
    log.debug("deleteZoo:zooId={}", zooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("zooOrg", zooOrg);
    paramMap.addValue("zooId", zooId);

    long deleteCount = getTemplate().update(deleteZooSql, paramMap);

    log.debug("deleteZoo:deleteCount={}", deleteCount);
    return deleteCount;
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
