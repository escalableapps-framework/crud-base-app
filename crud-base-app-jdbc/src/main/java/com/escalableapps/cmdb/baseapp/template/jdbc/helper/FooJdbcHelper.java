package com.escalableapps.cmdb.baseapp.template.jdbc.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public final class FooJdbcHelper {

  private static final String FOO_ID = "fooId";
  private static final String FOO_CHAR = "fooChar";
  private static final String FOO_VARCHAR = "fooVarchar";
  private static final String FOO_TEXT = "fooText";
  private static final String FOO_SMALLINT = "fooSmallint";
  private static final String FOO_INTEGER = "fooInteger";
  private static final String FOO_BIGINT = "fooBigint";
  private static final String FOO_REAL = "fooReal";
  private static final String FOO_DOUBLE = "fooDouble";
  private static final String FOO_DECIMAL = "fooDecimal";
  private static final String FOO_BOOLEAN = "fooBoolean";
  private static final String FOO_DATE = "fooDate";
  private static final String FOO_DATE_MIN = FOO_DATE + "Min";
  private static final String FOO_DATE_MAX = FOO_DATE + "Max";
  private static final String FOO_TIMESTAMP = "fooTimestamp";
  private static final String FOO_TIMESTAMP_MIN = FOO_TIMESTAMP + "Min";
  private static final String FOO_TIMESTAMP_MAX = FOO_TIMESTAMP + "Max";

  private static final String FOO_ID_IS_NULL = "fooIdIsNull";
  private static final String FOO_CHAR_IS_NULL = "fooCharIsNull";
  private static final String FOO_VARCHAR_IS_NULL = "fooVarcharIsNull";
  private static final String FOO_TEXT_IS_NULL = "fooTextIsNull";
  private static final String FOO_SMALLINT_IS_NULL = "fooSmallintIsNull";
  private static final String FOO_INTEGER_IS_NULL = "fooIntegerIsNull";
  private static final String FOO_BIGINT_IS_NULL = "fooBigintIsNull";
  private static final String FOO_REAL_IS_NULL = "fooRealIsNull";
  private static final String FOO_DOUBLE_IS_NULL = "fooDoubleIsNull";
  private static final String FOO_DECIMAL_IS_NULL = "fooDecimalIsNull";
  private static final String FOO_BOOLEAN_IS_NULL = "fooBooleanIsNull";
  private static final String FOO_DATE_IS_NULL = "fooDateIsNull";
  private static final String FOO_TIMESTAMP_IS_NULL = "fooTimestampIsNull";

  private static final String FOO_CHAR_UPDATED = "fooCharUpdated";
  private static final String FOO_VARCHAR_UPDATED = "fooVarcharUpdated";
  private static final String FOO_TEXT_UPDATED = "fooTextUpdated";
  private static final String FOO_SMALLINT_UPDATED = "fooSmallintUpdated";
  private static final String FOO_INTEGER_UPDATED = "fooIntegerUpdated";
  private static final String FOO_BIGINT_UPDATED = "fooBigintUpdated";
  private static final String FOO_REAL_UPDATED = "fooRealUpdated";
  private static final String FOO_DOUBLE_UPDATED = "fooDoubleUpdated";
  private static final String FOO_DECIMAL_UPDATED = "fooDecimalUpdated";
  private static final String FOO_BOOLEAN_UPDATED = "fooBooleanUpdated";
  private static final String FOO_DATE_UPDATED = "fooDateUpdated";
  private static final String FOO_TIMESTAMP_UPDATED = "fooTimestampUpdated";

  private static final HashMap<FooPropertyName, String> SORT_PROPERTIES;

  static {
    SORT_PROPERTIES = new HashMap<>();
    SORT_PROPERTIES.put(FooPropertyName.FOO_ID, "foo.foo_id");
    SORT_PROPERTIES.put(FooPropertyName.FOO_CHAR, "foo.foo_char");
    SORT_PROPERTIES.put(FooPropertyName.FOO_VARCHAR, "foo.foo_varchar");
    SORT_PROPERTIES.put(FooPropertyName.FOO_TEXT, "foo.foo_text");
    SORT_PROPERTIES.put(FooPropertyName.FOO_SMALLINT, "foo.foo_smallint");
    SORT_PROPERTIES.put(FooPropertyName.FOO_INTEGER, "foo.foo_integer");
    SORT_PROPERTIES.put(FooPropertyName.FOO_BIGINT, "foo.foo_bigint");
    SORT_PROPERTIES.put(FooPropertyName.FOO_REAL, "foo.foo_real");
    SORT_PROPERTIES.put(FooPropertyName.FOO_DOUBLE, "foo.foo_double");
    SORT_PROPERTIES.put(FooPropertyName.FOO_DECIMAL, "foo.foo_decimal");
    SORT_PROPERTIES.put(FooPropertyName.FOO_BOOLEAN, "foo.foo_boolean");
    SORT_PROPERTIES.put(FooPropertyName.FOO_DATE, "foo.foo_date");
    SORT_PROPERTIES.put(FooPropertyName.FOO_TIMESTAMP, "foo.foo_timestamp");
  }

  public static MapSqlParameterSource insertValues(Foo foo) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue(FOO_ID, foo.getFooId());
    paramMap.addValue(FOO_CHAR, foo.getFooChar());
    paramMap.addValue(FOO_VARCHAR, foo.getFooVarchar());
    paramMap.addValue(FOO_TEXT, foo.getFooText());
    paramMap.addValue(FOO_SMALLINT, foo.getFooSmallint());
    paramMap.addValue(FOO_INTEGER, foo.getFooInteger());
    paramMap.addValue(FOO_BIGINT, foo.getFooBigint());
    paramMap.addValue(FOO_REAL, foo.getFooReal());
    paramMap.addValue(FOO_DOUBLE, foo.getFooDouble());
    paramMap.addValue(FOO_DECIMAL, foo.getFooDecimal());
    paramMap.addValue(FOO_BOOLEAN, foo.getFooBoolean());
    paramMap.addValue(FOO_DATE, foo.getFooDate());
    paramMap.addValue(FOO_TIMESTAMP, foo.getFooTimestamp());
    return paramMap;
  }

  public static MapSqlParameterSource whereCondition(FooQuery fooQuery) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(FOO_ID_IS_NULL, isNullValue(fooQuery.getFooId()));
    paramMap.addValue(FOO_ID, getQueryValue(fooQuery.getFooId()));

    paramMap.addValue(FOO_CHAR_IS_NULL, isNullValue(fooQuery.getFooChar()));
    paramMap.addValue(FOO_CHAR, getQueryValue(fooQuery.getFooChar()));

    paramMap.addValue(FOO_VARCHAR_IS_NULL, isNullValue(fooQuery.getFooVarchar()));
    paramMap.addValue(FOO_VARCHAR, getQueryValue(fooQuery.getFooVarchar()));

    paramMap.addValue(FOO_TEXT_IS_NULL, isNullValue(fooQuery.getFooText()));
    paramMap.addValue(FOO_TEXT, getQueryValue(fooQuery.getFooText()));

    paramMap.addValue(FOO_SMALLINT_IS_NULL, isNullValue(fooQuery.getFooSmallint()));
    paramMap.addValue(FOO_SMALLINT, getQueryValue(fooQuery.getFooSmallint()));

    paramMap.addValue(FOO_INTEGER_IS_NULL, isNullValue(fooQuery.getFooInteger()));
    paramMap.addValue(FOO_INTEGER, getQueryValue(fooQuery.getFooInteger()));

    paramMap.addValue(FOO_BIGINT_IS_NULL, isNullValue(fooQuery.getFooBigint()));
    paramMap.addValue(FOO_BIGINT, getQueryValue(fooQuery.getFooBigint()));

    paramMap.addValue(FOO_REAL_IS_NULL, isNullValue(fooQuery.getFooReal()));
    paramMap.addValue(FOO_REAL, getQueryValue(fooQuery.getFooReal()));

    paramMap.addValue(FOO_DOUBLE_IS_NULL, isNullValue(fooQuery.getFooDouble()));
    paramMap.addValue(FOO_DOUBLE, getQueryValue(fooQuery.getFooDouble()));

    paramMap.addValue(FOO_DECIMAL_IS_NULL, isNullValue(fooQuery.getFooDecimal()));
    paramMap.addValue(FOO_DECIMAL, getQueryValue(fooQuery.getFooDecimal()));

    paramMap.addValue(FOO_BOOLEAN_IS_NULL, isNullValue(fooQuery.getFooBoolean()));
    paramMap.addValue(FOO_BOOLEAN, getQueryValue(fooQuery.getFooBoolean()));

    paramMap.addValue(FOO_DATE_IS_NULL, isNullValue(fooQuery.getFooDate()));
    paramMap.addValue(FOO_DATE, getQueryValue(fooQuery.getFooDate()));
    paramMap.addValue(FOO_DATE_MIN, getQueryValue(fooQuery.getFooDateMin()));
    paramMap.addValue(FOO_DATE_MAX, getQueryValue(fooQuery.getFooDateMax()));

    paramMap.addValue(FOO_TIMESTAMP_IS_NULL, isNullValue(fooQuery.getFooTimestamp()));
    paramMap.addValue(FOO_TIMESTAMP, getQueryValue(fooQuery.getFooTimestamp()));
    paramMap.addValue(FOO_TIMESTAMP_MIN, getQueryValue(fooQuery.getFooTimestampMin()));
    paramMap.addValue(FOO_TIMESTAMP_MAX, getQueryValue(fooQuery.getFooTimestampMax()));
    return paramMap;
  }

  public static String orderBy(String sql, List<SortRequest<FooPropertyName>> sorts) {
    List<String> orders = sorts.stream().map(sort -> {
      boolean asc = sort.isAscendant();
      if (SORT_PROPERTIES.containsKey(sort.getProperty())) {
        return new StringBuilder(SORT_PROPERTIES.get(sort.getProperty())).append(asc ? " ASC" : " DESC").toString();
      } else {
        return null;
      }
    }).filter(Objects::nonNull).collect(Collectors.toList());
    String orderBy = "";
    if (!orders.isEmpty()) {
      orderBy = new StringBuilder("ORDER BY ") //
          .append(orders.stream().collect(Collectors.joining(", "))) //
          .toString();
    }
    return StringUtils.replace(sql, "{{pageRequestOrder}}", orderBy);
  }

  public static MapSqlParameterSource updateValues(FooUpdate fooUpdate) {
    MapSqlParameterSource paramMap = new MapSqlParameterSource();

    paramMap.addValue(FOO_CHAR_UPDATED, fooUpdate.getFooChar() != null);
    paramMap.addValue(FOO_CHAR, getUpdateValue(fooUpdate.getFooChar()));

    paramMap.addValue(FOO_VARCHAR_UPDATED, fooUpdate.getFooVarchar() != null);
    paramMap.addValue(FOO_VARCHAR, getUpdateValue(fooUpdate.getFooVarchar()));

    paramMap.addValue(FOO_TEXT_UPDATED, fooUpdate.getFooText() != null);
    paramMap.addValue(FOO_TEXT, getUpdateValue(fooUpdate.getFooText()));

    paramMap.addValue(FOO_SMALLINT_UPDATED, fooUpdate.getFooSmallint() != null);
    paramMap.addValue(FOO_SMALLINT, getUpdateValue(fooUpdate.getFooSmallint()));

    paramMap.addValue(FOO_INTEGER_UPDATED, fooUpdate.getFooInteger() != null);
    paramMap.addValue(FOO_INTEGER, getUpdateValue(fooUpdate.getFooInteger()));

    paramMap.addValue(FOO_BIGINT_UPDATED, fooUpdate.getFooBigint() != null);
    paramMap.addValue(FOO_BIGINT, getUpdateValue(fooUpdate.getFooBigint()));

    paramMap.addValue(FOO_REAL_UPDATED, fooUpdate.getFooReal() != null);
    paramMap.addValue(FOO_REAL, getUpdateValue(fooUpdate.getFooReal()));

    paramMap.addValue(FOO_DOUBLE_UPDATED, fooUpdate.getFooDouble() != null);
    paramMap.addValue(FOO_DOUBLE, getUpdateValue(fooUpdate.getFooDouble()));

    paramMap.addValue(FOO_DECIMAL_UPDATED, fooUpdate.getFooDecimal() != null);
    paramMap.addValue(FOO_DECIMAL, getUpdateValue(fooUpdate.getFooDecimal()));

    paramMap.addValue(FOO_BOOLEAN_UPDATED, fooUpdate.getFooBoolean() != null);
    paramMap.addValue(FOO_BOOLEAN, getUpdateValue(fooUpdate.getFooBoolean()));

    paramMap.addValue(FOO_DATE_UPDATED, fooUpdate.getFooDate() != null);
    paramMap.addValue(FOO_DATE, getUpdateValue(fooUpdate.getFooDate()));

    paramMap.addValue(FOO_TIMESTAMP_UPDATED, fooUpdate.getFooTimestamp() != null);
    paramMap.addValue(FOO_TIMESTAMP, getUpdateValue(fooUpdate.getFooTimestamp()));

    return paramMap;
  }

  private static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  private static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null || !queryValue.exist() ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue == null || !updateValue.exist() ? null : updateValue.get();
  }

  private FooJdbcHelper() {
  }
}
