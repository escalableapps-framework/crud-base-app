package com.escalableapps.cmdb.baseapp.template.jdbc.mapper;

import java.math.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;

import org.springframework.jdbc.core.RowMapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public class LookRowMapper implements RowMapper<Look> {

  @Override
  public Look mapRow(ResultSet rs, int rowNum) throws SQLException {
    Look look = new Look();
    look.setLookOrg(rs.getObject("lookOrg", Long.class));
    look.setLookId(rs.getObject("lookId", Long.class));
    look.setLookChar(rs.getObject("lookChar", String.class));
    look.setLookVarchar(rs.getObject("lookVarchar", String.class));
    look.setLookText(rs.getObject("lookText", String.class));
    look.setLookSmallint(rs.getObject("lookSmallint", Short.class));
    look.setLookInteger(rs.getObject("lookInteger", Integer.class));
    look.setLookBigint(rs.getObject("lookBigint", Long.class));
    look.setLookReal(rs.getObject("lookReal", Float.class));
    look.setLookDouble(rs.getObject("lookDouble", Double.class));
    look.setLookDecimal(rs.getObject("lookDecimal", BigDecimal.class));
    look.setLookBoolean(rs.getObject("lookBoolean", Boolean.class));
    look.setLookDate(rs.getObject("lookDate", LocalDate.class));
    look.setLookTimestamp(rs.getObject("lookTimestamp", OffsetDateTime.class));
    return look;
  }
}
