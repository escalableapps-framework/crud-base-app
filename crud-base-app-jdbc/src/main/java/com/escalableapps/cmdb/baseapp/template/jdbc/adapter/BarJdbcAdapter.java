package com.escalableapps.cmdb.baseapp.template.jdbc.adapter;

import static com.escalableapps.cmdb.baseapp.template.jdbc.helper.BarJdbcHelper.*;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jdbc.mapper.BarRowMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class BarJdbcAdapter implements BarSpi {

  private final String countBarsSql;
  private final String createBarSql;
  private final String deleteBarSql;
  private final String existsBarIdSql;
  private final String existsBarSql;
  private final String findBarIdSql;
  private final String findBarIdsSql;
  private final String findBarsSql;
  private final String updateBarSql;

  private final DataSource dataSource;

  public BarJdbcAdapter(DataSource dataSource) {
    createBarSql = ResourceUtils.readResource("classpath:template/bar/createBar.sql");
    findBarIdSql = ResourceUtils.readResource("classpath:template/bar/findBarId.sql");
    findBarIdsSql = ResourceUtils.readResource("classpath:template/bar/findBarIds.sql");
    findBarsSql = ResourceUtils.readResource("classpath:template/bar/findBars.sql");
    existsBarIdSql = ResourceUtils.readResource("classpath:template/bar/existsBarId.sql");
    existsBarSql = ResourceUtils.readResource("classpath:template/bar/existsBar.sql");
    countBarsSql = ResourceUtils.readResource("classpath:template/bar/countBars.sql");
    updateBarSql = ResourceUtils.readResource("classpath:template/bar/updateBar.sql");
    deleteBarSql = ResourceUtils.readResource("classpath:template/bar/deleteBar.sql");

    this.dataSource = dataSource;
  }

  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBar:bar={}", bar);
    MapSqlParameterSource paramMap = insertValues(bar);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    String[] generatedKeyColumn = {"bar_id"};

    getTemplate().update(createBarSql, paramMap, keyHolder, generatedKeyColumn);

    bar.setBarId(keyHolder.getKey().intValue());
    log.debug("createBar:bar={}", bar);
    return bar;
  }

  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("barId", barId);

    Bar bar;
    try {
      bar = getTemplate().queryForObject(findBarIdSql, paramMap, new BarRowMapper());
    } catch (EmptyResultDataAccessException e) {
      bar = null;
    }

    log.debug("findBar:bar={}", bar);
    return Optional.ofNullable(bar);
  }

  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("barIds", barIds);

    List<Bar> bars = getTemplate().query(findBarIdsSql, paramMap, new BarRowMapper());

    log.debug("findBars:bars={}", bars);

    return bars;
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery //
  ) {
    return findBars(barQuery, new PageRequest<>());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);
    MapSqlParameterSource paramMap = whereCondition(barQuery);
    String findSortedBarsSql = orderBy(findBarsSql, paging.getSortRequests());
    paramMap.addValue("pageRequestOffset", paging.getOffset());
    paramMap.addValue("pageRequestLimit", paging.getPageSize());

    List<Bar> bars = getTemplate().query(findSortedBarsSql, paramMap, new BarRowMapper());

    log.debug("findBars:bars={}", bars);
    return bars;
  }

  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("barId", barId);

    Boolean exists = getTemplate().queryForObject(existsBarIdSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBar:barQuery={}", barQuery);
    MapSqlParameterSource paramMap = whereCondition(barQuery);

    Boolean exists = getTemplate().queryForObject(existsBarSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("countBars:barQuery={}", barQuery);
    MapSqlParameterSource paramMap = whereCondition(barQuery);

    Long count = getTemplate().queryForObject(countBarsSql, paramMap, new SingleColumnRowMapper<Long>());

    log.debug("countBars:count={}", count);
    return count;
  }

  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("updateBar:barId={}", barId);
    log.debug("updateBar:barUpdate={}", barUpdate);
    MapSqlParameterSource paramMap = updateValues(barUpdate);
    paramMap.addValue("barId", barId);

    long updateCount = getTemplate().update(updateBarSql, paramMap);

    log.debug("updateBar:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("deleteBar:barId={}", barId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("barId", barId);

    long deleteCount = getTemplate().update(deleteBarSql, paramMap);

    log.debug("deleteBar:deleteCount={}", deleteCount);
    return deleteCount;
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
