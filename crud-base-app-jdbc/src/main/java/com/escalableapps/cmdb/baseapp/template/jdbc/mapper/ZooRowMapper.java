package com.escalableapps.cmdb.baseapp.template.jdbc.mapper;

import java.math.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;

import org.springframework.jdbc.core.RowMapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;

public class ZooRowMapper implements RowMapper<Zoo> {

  @Override
  public Zoo mapRow(ResultSet rs, int rowNum) throws SQLException {
    Zoo zoo = new Zoo();
    zoo.setZooOrg(rs.getObject("zooOrg", Long.class));
    zoo.setZooId(rs.getObject("zooId", Integer.class));
    zoo.setZooChar(rs.getObject("zooChar", String.class));
    zoo.setZooVarchar(rs.getObject("zooVarchar", String.class));
    zoo.setZooText(rs.getObject("zooText", String.class));
    zoo.setZooSmallint(rs.getObject("zooSmallint", Short.class));
    zoo.setZooInteger(rs.getObject("zooInteger", Integer.class));
    zoo.setZooBigint(rs.getObject("zooBigint", Long.class));
    zoo.setZooReal(rs.getObject("zooReal", Float.class));
    zoo.setZooDouble(rs.getObject("zooDouble", Double.class));
    zoo.setZooDecimal(rs.getObject("zooDecimal", BigDecimal.class));
    zoo.setZooBoolean(rs.getObject("zooBoolean", Boolean.class));
    zoo.setZooDate(rs.getObject("zooDate", LocalDate.class));
    zoo.setZooTimestamp(rs.getObject("zooTimestamp", OffsetDateTime.class));
    return zoo;
  }
}
