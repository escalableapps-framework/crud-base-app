package com.escalableapps.cmdb.baseapp.template.jdbc.adapter;

import static com.escalableapps.cmdb.baseapp.template.jdbc.helper.FooJdbcHelper.*;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jdbc.mapper.FooRowMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class FooJdbcAdapter implements FooSpi {

  private final String countFoosSql;
  private final String createFooSql;
  private final String deleteFooSql;
  private final String existsFooIdSql;
  private final String existsFooSql;
  private final String findFooIdSql;
  private final String findFooIdsSql;
  private final String findFoosSql;
  private final String updateFooSql;

  private final DataSource dataSource;

  public FooJdbcAdapter(DataSource dataSource) {
    createFooSql = ResourceUtils.readResource("classpath:template/foo/createFoo.sql");
    findFooIdSql = ResourceUtils.readResource("classpath:template/foo/findFooId.sql");
    findFooIdsSql = ResourceUtils.readResource("classpath:template/foo/findFooIds.sql");
    findFoosSql = ResourceUtils.readResource("classpath:template/foo/findFoos.sql");
    existsFooIdSql = ResourceUtils.readResource("classpath:template/foo/existsFooId.sql");
    existsFooSql = ResourceUtils.readResource("classpath:template/foo/existsFoo.sql");
    countFoosSql = ResourceUtils.readResource("classpath:template/foo/countFoos.sql");
    updateFooSql = ResourceUtils.readResource("classpath:template/foo/updateFoo.sql");
    deleteFooSql = ResourceUtils.readResource("classpath:template/foo/deleteFoo.sql");

    this.dataSource = dataSource;
  }

  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoo:foo={}", foo);
    MapSqlParameterSource paramMap = insertValues(foo);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    String[] generatedKeyColumn = {"foo_id"};

    getTemplate().update(createFooSql, paramMap, keyHolder, generatedKeyColumn);

    foo.setFooId(keyHolder.getKey().intValue());
    log.debug("createFoo:foo={}", foo);
    return foo;
  }

  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooId", fooId);

    Foo foo;
    try {
      foo = getTemplate().queryForObject(findFooIdSql, paramMap, new FooRowMapper());
    } catch (EmptyResultDataAccessException e) {
      foo = null;
    }

    log.debug("findFoo:foo={}", foo);
    return Optional.ofNullable(foo);
  }

  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooIds", fooIds);

    List<Foo> foos = getTemplate().query(findFooIdsSql, paramMap, new FooRowMapper());

    log.debug("findFoos:foos={}", foos);

    return foos;
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery //
  ) {
    return findFoos(fooQuery, new PageRequest<>());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);
    MapSqlParameterSource paramMap = whereCondition(fooQuery);
    String findSortedFoosSql = orderBy(findFoosSql, paging.getSortRequests());
    paramMap.addValue("pageRequestOffset", paging.getOffset());
    paramMap.addValue("pageRequestLimit", paging.getPageSize());

    List<Foo> foos = getTemplate().query(findSortedFoosSql, paramMap, new FooRowMapper());

    log.debug("findFoos:foos={}", foos);
    return foos;
  }

  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooId", fooId);

    Boolean exists = getTemplate().queryForObject(existsFooIdSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoo:fooQuery={}", fooQuery);
    MapSqlParameterSource paramMap = whereCondition(fooQuery);

    Boolean exists = getTemplate().queryForObject(existsFooSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("countFoos:fooQuery={}", fooQuery);
    MapSqlParameterSource paramMap = whereCondition(fooQuery);

    Long count = getTemplate().queryForObject(countFoosSql, paramMap, new SingleColumnRowMapper<Long>());

    log.debug("countFoos:count={}", count);
    return count;
  }

  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("updateFoo:fooId={}", fooId);
    log.debug("updateFoo:fooUpdate={}", fooUpdate);
    MapSqlParameterSource paramMap = updateValues(fooUpdate);
    paramMap.addValue("fooId", fooId);

    long updateCount = getTemplate().update(updateFooSql, paramMap);

    log.debug("updateFoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("deleteFoo:fooId={}", fooId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("fooId", fooId);

    long deleteCount = getTemplate().update(deleteFooSql, paramMap);

    log.debug("deleteFoo:deleteCount={}", deleteCount);
    return deleteCount;
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
