package com.escalableapps.cmdb.baseapp.template.jdbc.adapter;

import static com.escalableapps.cmdb.baseapp.template.jdbc.helper.LookJdbcHelper.*;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.util.ResourceUtils;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jdbc.mapper.LookRowMapper;
import com.escalableapps.cmdb.baseapp.template.port.spi.LookSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class LookJdbcAdapter implements LookSpi {

  private final String countLooksSql;
  private final String createLookSql;
  private final String deleteLookSql;
  private final String existsLookIdSql;
  private final String existsLookSql;
  private final String findLookIdSql;
  private final String findLookIdsSql;
  private final String findLooksSql;
  private final String updateLookSql;

  private final DataSource dataSource;

  public LookJdbcAdapter(DataSource dataSource) {
    createLookSql = ResourceUtils.readResource("classpath:template/look/createLook.sql");
    findLookIdSql = ResourceUtils.readResource("classpath:template/look/findLookId.sql");
    findLookIdsSql = ResourceUtils.readResource("classpath:template/look/findLookIds.sql");
    findLooksSql = ResourceUtils.readResource("classpath:template/look/findLooks.sql");
    existsLookIdSql = ResourceUtils.readResource("classpath:template/look/existsLookId.sql");
    existsLookSql = ResourceUtils.readResource("classpath:template/look/existsLook.sql");
    countLooksSql = ResourceUtils.readResource("classpath:template/look/countLooks.sql");
    updateLookSql = ResourceUtils.readResource("classpath:template/look/updateLook.sql");
    deleteLookSql = ResourceUtils.readResource("classpath:template/look/deleteLook.sql");

    this.dataSource = dataSource;
  }

  @Override
  public Look createLook( //
      Look look //
  ) {
    log.debug("createLook:look={}", look);
    MapSqlParameterSource paramMap = insertValues(look);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    String[] generatedKeyColumn = {""};

    getTemplate().update(createLookSql, paramMap, keyHolder, generatedKeyColumn);

    look.setLookId(keyHolder.getKey().longValue());
    log.debug("createLook:look={}", look);
    return look;
  }

  @Override
  public Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookId", lookId);

    Look look;
    try {
      look = getTemplate().queryForObject(findLookIdSql, paramMap, new LookRowMapper());
    } catch (EmptyResultDataAccessException e) {
      look = null;
    }

    log.debug("findLook:look={}", look);
    return Optional.ofNullable(look);
  }

  @Override
  public List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  ) {
    log.debug("findLooks:lookOrg={}", lookOrg);
    log.debug("findLooks:lookIds={}", lookIds);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookIds", lookIds);

    List<Look> looks = getTemplate().query(findLookIdsSql, paramMap, new LookRowMapper());

    log.debug("findLooks:looks={}", looks);

    return looks;
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery //
  ) {
    return findLooks(lookQuery, new PageRequest<>());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  ) {
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:paging={}", paging);
    MapSqlParameterSource paramMap = whereCondition(lookQuery);
    String findSortedLooksSql = orderBy(findLooksSql, paging.getSortRequests());
    paramMap.addValue("pageRequestOffset", paging.getOffset());
    paramMap.addValue("pageRequestLimit", paging.getPageSize());

    List<Look> looks = getTemplate().query(findSortedLooksSql, paramMap, new LookRowMapper());

    log.debug("findLooks:looks={}", looks);
    return looks;
  }

  @Override
  public boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookId", lookId);

    Boolean exists = getTemplate().queryForObject(existsLookIdSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("existsLook:lookQuery={}", lookQuery);
    MapSqlParameterSource paramMap = whereCondition(lookQuery);

    Boolean exists = getTemplate().queryForObject(existsLookSql, paramMap, new SingleColumnRowMapper<Boolean>());

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Override
  public long countLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("countLooks:lookQuery={}", lookQuery);
    MapSqlParameterSource paramMap = whereCondition(lookQuery);

    Long count = getTemplate().queryForObject(countLooksSql, paramMap, new SingleColumnRowMapper<Long>());

    log.debug("countLooks:count={}", count);
    return count;
  }

  @Override
  public long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  ) {
    log.debug("updateLook:lookOrg={}", lookOrg);
    log.debug("updateLook:lookId={}", lookId);
    log.debug("updateLook:lookUpdate={}", lookUpdate);
    MapSqlParameterSource paramMap = updateValues(lookUpdate);
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookId", lookId);

    long updateCount = getTemplate().update(updateLookSql, paramMap);

    log.debug("updateLook:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("deleteLook:lookOrg={}", lookOrg);
    log.debug("deleteLook:lookId={}", lookId);
    MapSqlParameterSource paramMap = new MapSqlParameterSource();
    paramMap.addValue("lookOrg", lookOrg);
    paramMap.addValue("lookId", lookId);

    long deleteCount = getTemplate().update(deleteLookSql, paramMap);

    log.debug("deleteLook:deleteCount={}", deleteCount);
    return deleteCount;
  }

  private NamedParameterJdbcTemplate getTemplate() {
    return new NamedParameterJdbcTemplate(dataSource);
  }
}
