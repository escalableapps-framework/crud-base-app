SELECT EXISTS(
  SELECT 1
  FROM crud_base_db.look AS look
  WHERE look.look_org = :lookOrg
    AND look.look_id = :lookId
  LIMIT 1
)
