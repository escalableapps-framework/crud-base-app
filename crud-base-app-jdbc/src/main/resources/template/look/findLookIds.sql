SELECT
  look.look_org AS lookOrg,
  look.look_id AS lookId,
  look.look_char AS lookChar,
  look.look_varchar AS lookVarchar,
  look.look_text AS lookText,
  look.look_smallint AS lookSmallint,
  look.look_integer AS lookInteger,
  look.look_bigint AS lookBigint,
  look.look_real AS lookReal,
  look.look_double AS lookDouble,
  look.look_decimal AS lookDecimal,
  look.look_boolean AS lookBoolean,
  look.look_date AS lookDate,
  look.look_timestamp AS lookTimestamp
FROM crud_base_db.look AS look
WHERE look.look_org = :lookOrg
  AND look.look_id IN (:lookIds)
