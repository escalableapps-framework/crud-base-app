INSERT INTO crud_base_db.look (
  look_org,
  /* look_id, */
  look_char,
  look_varchar,
  look_text,
  look_smallint,
  look_integer,
  look_bigint,
  look_real,
  look_double,
  look_decimal,
  look_boolean,
  look_date,
  look_timestamp
) VALUES (
  :lookOrg,
  /* :lookId, */
  :lookChar,
  :lookVarchar,
  :lookText,
  :lookSmallint,
  :lookInteger,
  :lookBigint,
  :lookReal,
  :lookDouble,
  :lookDecimal,
  :lookBoolean,
  :lookDate,
  :lookTimestamp
)
