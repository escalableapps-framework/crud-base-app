SELECT COUNT(1)
FROM crud_base_db.look AS look
WHERE CASE WHEN :lookOrgIsNull THEN look.look_org IS NULL 
      ELSE CAST(:lookOrg AS BIGINT) IS NULL OR look.look_org = :lookOrg END
  AND CASE WHEN :lookIdIsNull THEN look.look_id IS NULL 
      ELSE CAST(:lookId AS BIGINT) IS NULL OR look.look_id = :lookId END
  AND CASE WHEN :lookCharIsNull THEN look.look_char IS NULL 
      ELSE CAST(:lookChar AS CHARACTER) IS NULL OR look.look_char = :lookChar END
  AND CASE WHEN :lookVarcharIsNull THEN look.look_varchar IS NULL 
      ELSE CAST(:lookVarchar AS CHARACTER VARYING) IS NULL OR look.look_varchar = :lookVarchar END
  AND CASE WHEN :lookTextIsNull THEN look.look_text IS NULL 
      ELSE CAST(:lookText AS TEXT) IS NULL OR look.look_text = :lookText END
  AND CASE WHEN :lookSmallintIsNull THEN look.look_smallint IS NULL 
      ELSE CAST(:lookSmallint AS SMALLINT) IS NULL OR look.look_smallint = :lookSmallint END
  AND CASE WHEN :lookIntegerIsNull THEN look.look_integer IS NULL 
      ELSE CAST(:lookInteger AS INTEGER) IS NULL OR look.look_integer = :lookInteger END
  AND CASE WHEN :lookBigintIsNull THEN look.look_bigint IS NULL 
      ELSE CAST(:lookBigint AS BIGINT) IS NULL OR look.look_bigint = :lookBigint END
  AND CASE WHEN :lookRealIsNull THEN look.look_real IS NULL 
      ELSE CAST(:lookReal AS REAL) IS NULL OR look.look_real = :lookReal END
  AND CASE WHEN :lookDoubleIsNull THEN look.look_double IS NULL 
      ELSE CAST(:lookDouble AS DOUBLE PRECISION) IS NULL OR look.look_double = :lookDouble END
  AND CASE WHEN :lookDecimalIsNull THEN look.look_decimal IS NULL 
      ELSE CAST(:lookDecimal AS NUMERIC) IS NULL OR look.look_decimal = :lookDecimal END
  AND CASE WHEN :lookBooleanIsNull THEN look.look_boolean IS NULL 
      ELSE CAST(:lookBoolean AS BOOLEAN) IS NULL OR look.look_boolean = :lookBoolean END
  AND CASE WHEN :lookDateIsNull THEN look.look_date IS NULL 
      WHEN CAST(:lookDateMin AS DATE) IS NOT NULL OR CAST(:lookDateMax AS DATE) IS NOT NULL THEN (CAST(:lookDateMin AS DATE) IS NULL OR :lookDateMin <= look.look_date) AND (CAST(:lookDateMax AS DATE) IS NULL OR look.look_date <= :lookDateMax)
      ELSE CAST(:lookDate AS DATE) IS NULL OR look.look_date = :lookDate END
  AND CASE WHEN :lookTimestampIsNull THEN look.look_timestamp IS NULL 
      WHEN CAST(:lookTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NOT NULL OR CAST(:lookTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NOT NULL THEN (CAST(:lookTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NULL OR :lookTimestampMin <= look.look_timestamp) AND (CAST(:lookTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NULL OR look.look_timestamp <= :lookTimestampMax)
      ELSE CAST(:lookTimestamp AS TIMESTAMP WITH TIME ZONE) IS NULL OR look.look_timestamp = :lookTimestamp END
