UPDATE crud_base_db.look SET
  look_char = CASE WHEN :lookCharUpdated THEN :lookChar ELSE look_char END,
  look_varchar = CASE WHEN :lookVarcharUpdated THEN :lookVarchar ELSE look_varchar END,
  look_text = CASE WHEN :lookTextUpdated THEN :lookText ELSE look_text END,
  look_smallint = CASE WHEN :lookSmallintUpdated THEN :lookSmallint ELSE look_smallint END,
  look_integer = CASE WHEN :lookIntegerUpdated THEN :lookInteger ELSE look_integer END,
  look_bigint = CASE WHEN :lookBigintUpdated THEN :lookBigint ELSE look_bigint END,
  look_real = CASE WHEN :lookRealUpdated THEN :lookReal ELSE look_real END,
  look_double = CASE WHEN :lookDoubleUpdated THEN :lookDouble ELSE look_double END,
  look_decimal = CASE WHEN :lookDecimalUpdated THEN :lookDecimal ELSE look_decimal END,
  look_boolean = CASE WHEN :lookBooleanUpdated THEN :lookBoolean ELSE look_boolean END,
  look_date = CASE WHEN :lookDateUpdated THEN :lookDate ELSE look_date END,
  look_timestamp = CASE WHEN :lookTimestampUpdated THEN :lookTimestamp ELSE look_timestamp END
WHERE look_org = :lookOrg
  AND look_id = :lookId
