UPDATE crud_base_db.bar SET
  bar_char = CASE WHEN :barCharUpdated THEN :barChar ELSE bar_char END,
  bar_varchar = CASE WHEN :barVarcharUpdated THEN :barVarchar ELSE bar_varchar END,
  bar_text = CASE WHEN :barTextUpdated THEN :barText ELSE bar_text END,
  bar_smallint = CASE WHEN :barSmallintUpdated THEN :barSmallint ELSE bar_smallint END,
  bar_integer = CASE WHEN :barIntegerUpdated THEN :barInteger ELSE bar_integer END,
  bar_bigint = CASE WHEN :barBigintUpdated THEN :barBigint ELSE bar_bigint END,
  bar_real = CASE WHEN :barRealUpdated THEN :barReal ELSE bar_real END,
  bar_double = CASE WHEN :barDoubleUpdated THEN :barDouble ELSE bar_double END,
  bar_decimal = CASE WHEN :barDecimalUpdated THEN :barDecimal ELSE bar_decimal END,
  bar_boolean = CASE WHEN :barBooleanUpdated THEN :barBoolean ELSE bar_boolean END,
  bar_date = CASE WHEN :barDateUpdated THEN :barDate ELSE bar_date END,
  bar_timestamp = CASE WHEN :barTimestampUpdated THEN :barTimestamp ELSE bar_timestamp END
WHERE bar_id = :barId
