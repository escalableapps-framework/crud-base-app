SELECT EXISTS(
  SELECT 1
  FROM crud_base_db.bar AS bar
  WHERE CASE WHEN :barIdIsNull THEN bar.bar_id IS NULL 
        ELSE CAST(:barId AS INTEGER) IS NULL OR bar.bar_id = :barId END
    AND CASE WHEN :barCharIsNull THEN bar.bar_char IS NULL 
        ELSE CAST(:barChar AS CHARACTER) IS NULL OR bar.bar_char = :barChar END
    AND CASE WHEN :barVarcharIsNull THEN bar.bar_varchar IS NULL 
        ELSE CAST(:barVarchar AS CHARACTER VARYING) IS NULL OR bar.bar_varchar = :barVarchar END
    AND CASE WHEN :barTextIsNull THEN bar.bar_text IS NULL 
        ELSE CAST(:barText AS TEXT) IS NULL OR bar.bar_text = :barText END
    AND CASE WHEN :barSmallintIsNull THEN bar.bar_smallint IS NULL 
        ELSE CAST(:barSmallint AS SMALLINT) IS NULL OR bar.bar_smallint = :barSmallint END
    AND CASE WHEN :barIntegerIsNull THEN bar.bar_integer IS NULL 
        ELSE CAST(:barInteger AS INTEGER) IS NULL OR bar.bar_integer = :barInteger END
    AND CASE WHEN :barBigintIsNull THEN bar.bar_bigint IS NULL 
        ELSE CAST(:barBigint AS BIGINT) IS NULL OR bar.bar_bigint = :barBigint END
    AND CASE WHEN :barRealIsNull THEN bar.bar_real IS NULL 
        ELSE CAST(:barReal AS REAL) IS NULL OR bar.bar_real = :barReal END
    AND CASE WHEN :barDoubleIsNull THEN bar.bar_double IS NULL 
        ELSE CAST(:barDouble AS DOUBLE PRECISION) IS NULL OR bar.bar_double = :barDouble END
    AND CASE WHEN :barDecimalIsNull THEN bar.bar_decimal IS NULL 
        ELSE CAST(:barDecimal AS NUMERIC) IS NULL OR bar.bar_decimal = :barDecimal END
    AND CASE WHEN :barBooleanIsNull THEN bar.bar_boolean IS NULL 
        ELSE CAST(:barBoolean AS BOOLEAN) IS NULL OR bar.bar_boolean = :barBoolean END
    AND CASE WHEN :barDateIsNull THEN bar.bar_date IS NULL 
        WHEN CAST(:barDateMin AS DATE) IS NOT NULL OR CAST(:barDateMax AS DATE) IS NOT NULL THEN (CAST(:barDateMin AS DATE) IS NULL OR :barDateMin <= bar.bar_date) AND (CAST(:barDateMax AS DATE) IS NULL OR bar.bar_date <= :barDateMax)
        ELSE CAST(:barDate AS DATE) IS NULL OR bar.bar_date = :barDate END
    AND CASE WHEN :barTimestampIsNull THEN bar.bar_timestamp IS NULL 
        WHEN CAST(:barTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NOT NULL OR CAST(:barTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NOT NULL THEN (CAST(:barTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NULL OR :barTimestampMin <= bar.bar_timestamp) AND (CAST(:barTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NULL OR bar.bar_timestamp <= :barTimestampMax)
        ELSE CAST(:barTimestamp AS TIMESTAMP WITH TIME ZONE) IS NULL OR bar.bar_timestamp = :barTimestamp END
  LIMIT 1
)
