INSERT INTO crud_base_db.bar (
  /* bar_id, */
  bar_char,
  bar_varchar,
  bar_text,
  bar_smallint,
  bar_integer,
  bar_bigint,
  bar_real,
  bar_double,
  bar_decimal,
  bar_boolean,
  bar_date,
  bar_timestamp
) VALUES (
  /* :barId, */
  :barChar,
  :barVarchar,
  :barText,
  :barSmallint,
  :barInteger,
  :barBigint,
  :barReal,
  :barDouble,
  :barDecimal,
  :barBoolean,
  :barDate,
  :barTimestamp
)
