SELECT
  bar.bar_id AS barId,
  bar.bar_char AS barChar,
  bar.bar_varchar AS barVarchar,
  bar.bar_text AS barText,
  bar.bar_smallint AS barSmallint,
  bar.bar_integer AS barInteger,
  bar.bar_bigint AS barBigint,
  bar.bar_real AS barReal,
  bar.bar_double AS barDouble,
  bar.bar_decimal AS barDecimal,
  bar.bar_boolean AS barBoolean,
  bar.bar_date AS barDate,
  bar.bar_timestamp AS barTimestamp
FROM crud_base_db.bar AS bar
WHERE bar.bar_id IN (:barIds)
