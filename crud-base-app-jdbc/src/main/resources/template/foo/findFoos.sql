SELECT
  foo.foo_id AS fooId,
  foo.foo_char AS fooChar,
  foo.foo_varchar AS fooVarchar,
  foo.foo_text AS fooText,
  foo.foo_smallint AS fooSmallint,
  foo.foo_integer AS fooInteger,
  foo.foo_bigint AS fooBigint,
  foo.foo_real AS fooReal,
  foo.foo_double AS fooDouble,
  foo.foo_decimal AS fooDecimal,
  foo.foo_boolean AS fooBoolean,
  foo.foo_date AS fooDate,
  foo.foo_timestamp AS fooTimestamp
FROM crud_base_db.foo AS foo
WHERE CASE WHEN :fooIdIsNull THEN foo.foo_id IS NULL 
      ELSE CAST(:fooId AS INTEGER) IS NULL OR foo.foo_id = :fooId END
  AND CASE WHEN :fooCharIsNull THEN foo.foo_char IS NULL 
      ELSE CAST(:fooChar AS CHARACTER) IS NULL OR foo.foo_char = :fooChar END
  AND CASE WHEN :fooVarcharIsNull THEN foo.foo_varchar IS NULL 
      ELSE CAST(:fooVarchar AS CHARACTER VARYING) IS NULL OR foo.foo_varchar = :fooVarchar END
  AND CASE WHEN :fooTextIsNull THEN foo.foo_text IS NULL 
      ELSE CAST(:fooText AS TEXT) IS NULL OR foo.foo_text = :fooText END
  AND CASE WHEN :fooSmallintIsNull THEN foo.foo_smallint IS NULL 
      ELSE CAST(:fooSmallint AS SMALLINT) IS NULL OR foo.foo_smallint = :fooSmallint END
  AND CASE WHEN :fooIntegerIsNull THEN foo.foo_integer IS NULL 
      ELSE CAST(:fooInteger AS INTEGER) IS NULL OR foo.foo_integer = :fooInteger END
  AND CASE WHEN :fooBigintIsNull THEN foo.foo_bigint IS NULL 
      ELSE CAST(:fooBigint AS BIGINT) IS NULL OR foo.foo_bigint = :fooBigint END
  AND CASE WHEN :fooRealIsNull THEN foo.foo_real IS NULL 
      ELSE CAST(:fooReal AS REAL) IS NULL OR foo.foo_real = :fooReal END
  AND CASE WHEN :fooDoubleIsNull THEN foo.foo_double IS NULL 
      ELSE CAST(:fooDouble AS DOUBLE PRECISION) IS NULL OR foo.foo_double = :fooDouble END
  AND CASE WHEN :fooDecimalIsNull THEN foo.foo_decimal IS NULL 
      ELSE CAST(:fooDecimal AS NUMERIC) IS NULL OR foo.foo_decimal = :fooDecimal END
  AND CASE WHEN :fooBooleanIsNull THEN foo.foo_boolean IS NULL 
      ELSE CAST(:fooBoolean AS BOOLEAN) IS NULL OR foo.foo_boolean = :fooBoolean END
  AND CASE WHEN :fooDateIsNull THEN foo.foo_date IS NULL 
      WHEN CAST(:fooDateMin AS DATE) IS NOT NULL OR CAST(:fooDateMax AS DATE) IS NOT NULL THEN (CAST(:fooDateMin AS DATE) IS NULL OR :fooDateMin <= foo.foo_date) AND (CAST(:fooDateMax AS DATE) IS NULL OR foo.foo_date <= :fooDateMax)
      ELSE CAST(:fooDate AS DATE) IS NULL OR foo.foo_date = :fooDate END
  AND CASE WHEN :fooTimestampIsNull THEN foo.foo_timestamp IS NULL 
      WHEN CAST(:fooTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NOT NULL OR CAST(:fooTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NOT NULL THEN (CAST(:fooTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NULL OR :fooTimestampMin <= foo.foo_timestamp) AND (CAST(:fooTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NULL OR foo.foo_timestamp <= :fooTimestampMax)
      ELSE CAST(:fooTimestamp AS TIMESTAMP WITH TIME ZONE) IS NULL OR foo.foo_timestamp = :fooTimestamp END
{{pageRequestOrder}}
LIMIT :pageRequestLimit
OFFSET :pageRequestOffset
