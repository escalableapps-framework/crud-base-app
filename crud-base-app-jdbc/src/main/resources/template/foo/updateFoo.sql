UPDATE crud_base_db.foo SET
  foo_char = CASE WHEN :fooCharUpdated THEN :fooChar ELSE foo_char END,
  foo_varchar = CASE WHEN :fooVarcharUpdated THEN :fooVarchar ELSE foo_varchar END,
  foo_text = CASE WHEN :fooTextUpdated THEN :fooText ELSE foo_text END,
  foo_smallint = CASE WHEN :fooSmallintUpdated THEN :fooSmallint ELSE foo_smallint END,
  foo_integer = CASE WHEN :fooIntegerUpdated THEN :fooInteger ELSE foo_integer END,
  foo_bigint = CASE WHEN :fooBigintUpdated THEN :fooBigint ELSE foo_bigint END,
  foo_real = CASE WHEN :fooRealUpdated THEN :fooReal ELSE foo_real END,
  foo_double = CASE WHEN :fooDoubleUpdated THEN :fooDouble ELSE foo_double END,
  foo_decimal = CASE WHEN :fooDecimalUpdated THEN :fooDecimal ELSE foo_decimal END,
  foo_boolean = CASE WHEN :fooBooleanUpdated THEN :fooBoolean ELSE foo_boolean END,
  foo_date = CASE WHEN :fooDateUpdated THEN :fooDate ELSE foo_date END,
  foo_timestamp = CASE WHEN :fooTimestampUpdated THEN :fooTimestamp ELSE foo_timestamp END
WHERE foo_id = :fooId
