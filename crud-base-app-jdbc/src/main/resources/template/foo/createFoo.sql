INSERT INTO crud_base_db.foo (
  /* foo_id, */
  foo_char,
  foo_varchar,
  foo_text,
  foo_smallint,
  foo_integer,
  foo_bigint,
  foo_real,
  foo_double,
  foo_decimal,
  foo_boolean,
  foo_date,
  foo_timestamp
) VALUES (
  /* :fooId, */
  :fooChar,
  :fooVarchar,
  :fooText,
  :fooSmallint,
  :fooInteger,
  :fooBigint,
  :fooReal,
  :fooDouble,
  :fooDecimal,
  :fooBoolean,
  :fooDate,
  :fooTimestamp
)
