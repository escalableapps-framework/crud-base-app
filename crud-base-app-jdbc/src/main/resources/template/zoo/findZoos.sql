SELECT
  zoo.zoo_org AS zooOrg,
  zoo.zoo_id AS zooId,
  zoo.zoo_char AS zooChar,
  zoo.zoo_varchar AS zooVarchar,
  zoo.zoo_text AS zooText,
  zoo.zoo_smallint AS zooSmallint,
  zoo.zoo_integer AS zooInteger,
  zoo.zoo_bigint AS zooBigint,
  zoo.zoo_real AS zooReal,
  zoo.zoo_double AS zooDouble,
  zoo.zoo_decimal AS zooDecimal,
  zoo.zoo_boolean AS zooBoolean,
  zoo.zoo_date AS zooDate,
  zoo.zoo_timestamp AS zooTimestamp
FROM crud_base_db.zoo AS zoo
WHERE CASE WHEN :zooOrgIsNull THEN zoo.zoo_org IS NULL 
      ELSE CAST(:zooOrg AS BIGINT) IS NULL OR zoo.zoo_org = :zooOrg END
  AND CASE WHEN :zooIdIsNull THEN zoo.zoo_id IS NULL 
      ELSE CAST(:zooId AS INTEGER) IS NULL OR zoo.zoo_id = :zooId END
  AND CASE WHEN :zooCharIsNull THEN zoo.zoo_char IS NULL 
      ELSE CAST(:zooChar AS CHARACTER) IS NULL OR zoo.zoo_char = :zooChar END
  AND CASE WHEN :zooVarcharIsNull THEN zoo.zoo_varchar IS NULL 
      ELSE CAST(:zooVarchar AS CHARACTER VARYING) IS NULL OR zoo.zoo_varchar = :zooVarchar END
  AND CASE WHEN :zooTextIsNull THEN zoo.zoo_text IS NULL 
      ELSE CAST(:zooText AS TEXT) IS NULL OR zoo.zoo_text = :zooText END
  AND CASE WHEN :zooSmallintIsNull THEN zoo.zoo_smallint IS NULL 
      ELSE CAST(:zooSmallint AS SMALLINT) IS NULL OR zoo.zoo_smallint = :zooSmallint END
  AND CASE WHEN :zooIntegerIsNull THEN zoo.zoo_integer IS NULL 
      ELSE CAST(:zooInteger AS INTEGER) IS NULL OR zoo.zoo_integer = :zooInteger END
  AND CASE WHEN :zooBigintIsNull THEN zoo.zoo_bigint IS NULL 
      ELSE CAST(:zooBigint AS BIGINT) IS NULL OR zoo.zoo_bigint = :zooBigint END
  AND CASE WHEN :zooRealIsNull THEN zoo.zoo_real IS NULL 
      ELSE CAST(:zooReal AS REAL) IS NULL OR zoo.zoo_real = :zooReal END
  AND CASE WHEN :zooDoubleIsNull THEN zoo.zoo_double IS NULL 
      ELSE CAST(:zooDouble AS DOUBLE PRECISION) IS NULL OR zoo.zoo_double = :zooDouble END
  AND CASE WHEN :zooDecimalIsNull THEN zoo.zoo_decimal IS NULL 
      ELSE CAST(:zooDecimal AS NUMERIC) IS NULL OR zoo.zoo_decimal = :zooDecimal END
  AND CASE WHEN :zooBooleanIsNull THEN zoo.zoo_boolean IS NULL 
      ELSE CAST(:zooBoolean AS BOOLEAN) IS NULL OR zoo.zoo_boolean = :zooBoolean END
  AND CASE WHEN :zooDateIsNull THEN zoo.zoo_date IS NULL 
      WHEN CAST(:zooDateMin AS DATE) IS NOT NULL OR CAST(:zooDateMax AS DATE) IS NOT NULL THEN (CAST(:zooDateMin AS DATE) IS NULL OR :zooDateMin <= zoo.zoo_date) AND (CAST(:zooDateMax AS DATE) IS NULL OR zoo.zoo_date <= :zooDateMax)
      ELSE CAST(:zooDate AS DATE) IS NULL OR zoo.zoo_date = :zooDate END
  AND CASE WHEN :zooTimestampIsNull THEN zoo.zoo_timestamp IS NULL 
      WHEN CAST(:zooTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NOT NULL OR CAST(:zooTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NOT NULL THEN (CAST(:zooTimestampMin AS TIMESTAMP WITH TIME ZONE) IS NULL OR :zooTimestampMin <= zoo.zoo_timestamp) AND (CAST(:zooTimestampMax AS TIMESTAMP WITH TIME ZONE) IS NULL OR zoo.zoo_timestamp <= :zooTimestampMax)
      ELSE CAST(:zooTimestamp AS TIMESTAMP WITH TIME ZONE) IS NULL OR zoo.zoo_timestamp = :zooTimestamp END
{{pageRequestOrder}}
LIMIT :pageRequestLimit
OFFSET :pageRequestOffset
