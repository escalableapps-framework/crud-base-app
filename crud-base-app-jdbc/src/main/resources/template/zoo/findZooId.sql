SELECT
  zoo.zoo_org AS zooOrg,
  zoo.zoo_id AS zooId,
  zoo.zoo_char AS zooChar,
  zoo.zoo_varchar AS zooVarchar,
  zoo.zoo_text AS zooText,
  zoo.zoo_smallint AS zooSmallint,
  zoo.zoo_integer AS zooInteger,
  zoo.zoo_bigint AS zooBigint,
  zoo.zoo_real AS zooReal,
  zoo.zoo_double AS zooDouble,
  zoo.zoo_decimal AS zooDecimal,
  zoo.zoo_boolean AS zooBoolean,
  zoo.zoo_date AS zooDate,
  zoo.zoo_timestamp AS zooTimestamp
FROM crud_base_db.zoo AS zoo
WHERE zoo.zoo_org = :zooOrg
  AND zoo.zoo_id = :zooId
