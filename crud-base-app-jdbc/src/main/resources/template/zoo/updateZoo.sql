UPDATE crud_base_db.zoo SET
  zoo_char = CASE WHEN :zooCharUpdated THEN :zooChar ELSE zoo_char END,
  zoo_varchar = CASE WHEN :zooVarcharUpdated THEN :zooVarchar ELSE zoo_varchar END,
  zoo_text = CASE WHEN :zooTextUpdated THEN :zooText ELSE zoo_text END,
  zoo_smallint = CASE WHEN :zooSmallintUpdated THEN :zooSmallint ELSE zoo_smallint END,
  zoo_integer = CASE WHEN :zooIntegerUpdated THEN :zooInteger ELSE zoo_integer END,
  zoo_bigint = CASE WHEN :zooBigintUpdated THEN :zooBigint ELSE zoo_bigint END,
  zoo_real = CASE WHEN :zooRealUpdated THEN :zooReal ELSE zoo_real END,
  zoo_double = CASE WHEN :zooDoubleUpdated THEN :zooDouble ELSE zoo_double END,
  zoo_decimal = CASE WHEN :zooDecimalUpdated THEN :zooDecimal ELSE zoo_decimal END,
  zoo_boolean = CASE WHEN :zooBooleanUpdated THEN :zooBoolean ELSE zoo_boolean END,
  zoo_date = CASE WHEN :zooDateUpdated THEN :zooDate ELSE zoo_date END,
  zoo_timestamp = CASE WHEN :zooTimestampUpdated THEN :zooTimestamp ELSE zoo_timestamp END
WHERE zoo_org = :zooOrg
  AND zoo_id = :zooId
