SELECT EXISTS(
  SELECT 1
  FROM crud_base_db.zoo AS zoo
  WHERE zoo.zoo_org = :zooOrg
    AND zoo.zoo_id = :zooId
  LIMIT 1
)
