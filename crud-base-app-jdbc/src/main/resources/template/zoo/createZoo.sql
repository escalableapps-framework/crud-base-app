INSERT INTO crud_base_db.zoo (
  zoo_org,
  /* zoo_id, */
  zoo_char,
  zoo_varchar,
  zoo_text,
  zoo_smallint,
  zoo_integer,
  zoo_bigint,
  zoo_real,
  zoo_double,
  zoo_decimal,
  zoo_boolean,
  zoo_date,
  zoo_timestamp
) VALUES (
  :zooOrg,
  /* :zooId, */
  :zooChar,
  :zooVarchar,
  :zooText,
  :zooSmallint,
  :zooInteger,
  :zooBigint,
  :zooReal,
  :zooDouble,
  :zooDecimal,
  :zooBoolean,
  :zooDate,
  :zooTimestamp
)
