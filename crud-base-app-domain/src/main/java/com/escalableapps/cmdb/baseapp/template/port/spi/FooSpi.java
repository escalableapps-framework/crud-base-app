package com.escalableapps.cmdb.baseapp.template.port.spi;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface FooSpi {

  Foo createFoo( //
      @NotNull @Valid Foo foo //
  );

  Optional<Foo> findFoo( //
      @NotNull @Min(1) Integer fooId //
  );

  List<@NotNull @Valid Foo> findFoos( //
      @NotNull @Size(min = 1) List<@NotNull @Min(1) Integer> fooIds //
  );

  List<@NotNull @Valid Foo> findFoos( //
      @NotNull @Valid FooQuery fooQuery //
  );

  List<@NotNull @Valid Foo> findFoos( //
      @NotNull @Valid FooQuery fooQuery, //
      @NotNull @Valid PageRequest<FooPropertyName> paging //
  );

  boolean existsFoo( //
      @NotNull @Min(1) Integer fooId //
  );

  boolean existsFoos( //
      @NotNull @Valid FooQuery fooQuery //
  );

  long countFoos( //
      @NotNull @Valid FooQuery fooQuery //
  );

  long updateFoo( //
      @NotNull @Min(1) Integer fooId, //
      @NotNull @Valid FooUpdate fooUpdate //
  );

  long deleteFoo( //
      @NotNull @Min(1) Integer fooId //
  );
}
