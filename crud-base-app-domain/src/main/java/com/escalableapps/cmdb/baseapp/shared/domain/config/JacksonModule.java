package com.escalableapps.cmdb.baseapp.shared.domain.config;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import com.escalableapps.cmdb.baseapp.shared.domain.util.DateUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public final class JacksonModule extends SimpleModule {

  private static final long serialVersionUID = 1L;

  public JacksonModule() {
    addDeserializer(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {

      @Override
      public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String iso = null;
        try {
          JsonNode node = jp.getCodec().readTree(jp);
          iso = node.asText();
          OffsetDateTime offsetDatetime = OffsetDateTime.parse(iso);
          LocalDateTime localDateTime = DateUtils.convertToLocalDateTime(offsetDatetime);
          return localDateTime;
        } catch (Exception e) {
          throw new InvalidFormatException(jp, e.getMessage(), iso, LocalDateTime.class);
        }
      }

    });

    addSerializer(new StdSerializer<LocalDateTime>(LocalDateTime.class) {

      private static final long serialVersionUID = 1L;

      @Override
      public void serialize(LocalDateTime localDateTime, JsonGenerator gen, SerializerProvider provider) throws IOException {
        OffsetDateTime offsetDateTime = DateUtils.convertToOffsetDateTime(localDateTime);
        String iso = offsetDateTime.format(ISO_OFFSET_DATE_TIME);
        gen.writeString(iso);
      }

    });
  }
}
