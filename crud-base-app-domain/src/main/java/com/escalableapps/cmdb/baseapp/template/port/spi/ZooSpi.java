package com.escalableapps.cmdb.baseapp.template.port.spi;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface ZooSpi {

  Zoo createZoo( //
      @NotNull @Valid Zoo zoo //
  );

  Optional<Zoo> findZoo( //
      @NotNull @Min(1) Long zooOrg, //
      @NotNull @Min(1) Integer zooId //
  );

  List<@NotNull @Valid Zoo> findZoos( //
      @NotNull @Min(1) Long zooOrg, //
      @NotNull @Size(min = 1) List<@NotNull @Min(1) Integer> zooIds //
  );

  List<@NotNull @Valid Zoo> findZoos( //
      @NotNull @Valid ZooQuery zooQuery //
  );

  List<@NotNull @Valid Zoo> findZoos( //
      @NotNull @Valid ZooQuery zooQuery, //
      @NotNull @Valid PageRequest<ZooPropertyName> paging //
  );

  boolean existsZoo( //
      @NotNull @Min(1) Long zooOrg, //
      @NotNull @Min(1) Integer zooId //
  );

  boolean existsZoos( //
      @NotNull @Valid ZooQuery zooQuery //
  );

  long countZoos( //
      @NotNull @Valid ZooQuery zooQuery //
  );

  long updateZoo( //
      @NotNull @Min(1) Long zooOrg, //
      @NotNull @Min(1) Integer zooId, //
      @NotNull @Valid ZooUpdate zooUpdate //
  );

  long deleteZoo( //
      @NotNull @Min(1) Long zooOrg, //
      @NotNull @Min(1) Integer zooId //
  );
}
