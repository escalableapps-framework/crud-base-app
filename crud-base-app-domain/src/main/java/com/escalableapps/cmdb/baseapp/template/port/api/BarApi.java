package com.escalableapps.cmdb.baseapp.template.port.api;

import java.util.List;
import java.util.Optional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface BarApi {

  Bar createBar( //
      Bar bar //
  );

  Optional<Bar> findBar( //
      Integer barId //
  );

  List<Bar> findBars( //
      List<Integer> barIds //
  );

  PageResponse<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  );

  boolean existsBar( //
      Integer barId //
  );

  boolean existsBars( //
      BarQuery barQuery //
  );

  long countBars( //
      BarQuery barQuery //
  );

  long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  );

  long deleteBar( //
      Integer barId //
  );
}
