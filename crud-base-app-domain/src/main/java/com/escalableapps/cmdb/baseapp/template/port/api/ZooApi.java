package com.escalableapps.cmdb.baseapp.template.port.api;

import java.util.List;
import java.util.Optional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface ZooApi {

  Zoo createZoo( //
      Zoo zoo //
  );

  Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  );

  List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  );

  PageResponse<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  );

  boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  );

  boolean existsZoos( //
      ZooQuery zooQuery //
  );

  long countZoos( //
      ZooQuery zooQuery //
  );

  long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  );

  long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  );
}
