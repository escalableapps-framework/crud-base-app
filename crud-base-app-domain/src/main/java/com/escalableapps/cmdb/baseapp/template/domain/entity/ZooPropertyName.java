package com.escalableapps.cmdb.baseapp.template.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor()
public enum ZooPropertyName {
  ZOO_ORG("zooOrg"), //
  ZOO_ID("zooId"), //
  ZOO_CHAR("zooChar"), //
  ZOO_VARCHAR("zooVarchar"), //
  ZOO_TEXT("zooText"), //
  ZOO_SMALLINT("zooSmallint"), //
  ZOO_INTEGER("zooInteger"), //
  ZOO_BIGINT("zooBigint"), //
  ZOO_REAL("zooReal"), //
  ZOO_DOUBLE("zooDouble"), //
  ZOO_DECIMAL("zooDecimal"), //
  ZOO_BOOLEAN("zooBoolean"), //
  ZOO_DATE("zooDate"), //
  ZOO_TIMESTAMP("zooTimestamp");

  @Getter
  private final String propertyName;
}
