package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.UpdateValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Zoo<br>
 * <strong>zoo</strong>.
 */
@Data
public class ZooUpdate {

  /**
   * Description for zooChar<br>
   * <strong>zoo_char character</strong>.
   */
  private UpdateValue<@Size(max = 2) String> zooChar;

  /**
   * Description for zooVarchar<br>
   * <strong>zoo_varchar character varying</strong>.
   */
  private UpdateValue<@Size(max = 4) String> zooVarchar;

  /**
   * Description for zooText<br>
   * <strong>zoo_text text</strong>.
   */
  private UpdateValue<@Size(max = TEXT) String> zooText;

  /**
   * Description for zooSmallint<br>
   * <strong>zoo_smallint smallint</strong>.
   */
  private UpdateValue<Short> zooSmallint;

  /**
   * Description for zooInteger<br>
   * <strong>zoo_integer integer</strong>.
   */
  private UpdateValue<Integer> zooInteger;

  /**
   * Description for zooBigint<br>
   * <strong>zoo_bigint bigint</strong>.
   */
  private UpdateValue<Long> zooBigint;

  /**
   * Description for zooReal<br>
   * <strong>zoo_real real</strong>.
   */
  private UpdateValue<Float> zooReal;

  /**
   * Description for zooDouble<br>
   * <strong>zoo_double double precision</strong>.
   */
  private UpdateValue<Double> zooDouble;

  /**
   * Description for zooDecimal<br>
   * <strong>zoo_decimal numeric</strong>.
   */
  private UpdateValue<BigDecimal> zooDecimal;

  /**
   * Description for zooBoolean<br>
   * <strong>zoo_boolean boolean</strong>.
   */
  private UpdateValue<Boolean> zooBoolean;

  /**
   * Description for zooDate<br>
   * <strong>zoo_date date</strong>.
   */
  private UpdateValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> zooDate;

  /**
   * Description for zooTimestamp<br>
   * <strong>zoo_timestamp timestamp with time zone</strong>.
   */
  private UpdateValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> zooTimestamp;

  public void setZooChar(String zooChar) {
    this.zooChar = new UpdateValue<>(zooChar);
  }

  public void setZooVarchar(String zooVarchar) {
    this.zooVarchar = new UpdateValue<>(zooVarchar);
  }

  public void setZooText(String zooText) {
    this.zooText = new UpdateValue<>(zooText);
  }

  public void setZooSmallint(Short zooSmallint) {
    this.zooSmallint = new UpdateValue<>(zooSmallint);
  }

  public void setZooInteger(Integer zooInteger) {
    this.zooInteger = new UpdateValue<>(zooInteger);
  }

  public void setZooBigint(Long zooBigint) {
    this.zooBigint = new UpdateValue<>(zooBigint);
  }

  public void setZooReal(Float zooReal) {
    this.zooReal = new UpdateValue<>(zooReal);
  }

  public void setZooDouble(Double zooDouble) {
    this.zooDouble = new UpdateValue<>(zooDouble);
  }

  public void setZooDecimal(BigDecimal zooDecimal) {
    this.zooDecimal = new UpdateValue<>(zooDecimal);
  }

  public void setZooBoolean(Boolean zooBoolean) {
    this.zooBoolean = new UpdateValue<>(zooBoolean);
  }

  public void setZooDate(LocalDate zooDate) {
    this.zooDate = new UpdateValue<>(zooDate);
  }

  public void setZooTimestamp(OffsetDateTime zooTimestamp) {
    this.zooTimestamp = new UpdateValue<>(zooTimestamp);
  }
}
