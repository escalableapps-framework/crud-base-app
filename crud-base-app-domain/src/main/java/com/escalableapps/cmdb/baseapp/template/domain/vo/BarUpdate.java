package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.UpdateValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Bar<br>
 * <strong>bar</strong>.
 */
@Data
public class BarUpdate {

  /**
   * Description for barChar<br>
   * <strong>bar_char character NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = 1) String> barChar;

  /**
   * Description for barVarchar<br>
   * <strong>bar_varchar character varying NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = VARCHAR) String> barVarchar;

  /**
   * Description for barText<br>
   * <strong>bar_text text NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = TEXT) String> barText;

  /**
   * Description for barSmallint<br>
   * <strong>bar_smallint smallint NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Short> barSmallint;

  /**
   * Description for barInteger<br>
   * <strong>bar_integer integer NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Integer> barInteger;

  /**
   * Description for barBigint<br>
   * <strong>bar_bigint bigint NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Long> barBigint;

  /**
   * Description for barReal<br>
   * <strong>bar_real real NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Float> barReal;

  /**
   * Description for barDouble<br>
   * <strong>bar_double double precision NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Double> barDouble;

  /**
   * Description for barDecimal<br>
   * <strong>bar_decimal numeric NOT NULL</strong>.
   */
  private UpdateValue<@NotNull BigDecimal> barDecimal;

  /**
   * Description for barBoolean<br>
   * <strong>bar_boolean boolean NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Boolean> barBoolean;

  /**
   * Description for barDate<br>
   * <strong>bar_date date NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> barDate;

  /**
   * Description for barTimestamp<br>
   * <strong>bar_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> barTimestamp;

  public void setBarChar(String barChar) {
    this.barChar = new UpdateValue<>(barChar);
  }

  public void setBarVarchar(String barVarchar) {
    this.barVarchar = new UpdateValue<>(barVarchar);
  }

  public void setBarText(String barText) {
    this.barText = new UpdateValue<>(barText);
  }

  public void setBarSmallint(Short barSmallint) {
    this.barSmallint = new UpdateValue<>(barSmallint);
  }

  public void setBarInteger(Integer barInteger) {
    this.barInteger = new UpdateValue<>(barInteger);
  }

  public void setBarBigint(Long barBigint) {
    this.barBigint = new UpdateValue<>(barBigint);
  }

  public void setBarReal(Float barReal) {
    this.barReal = new UpdateValue<>(barReal);
  }

  public void setBarDouble(Double barDouble) {
    this.barDouble = new UpdateValue<>(barDouble);
  }

  public void setBarDecimal(BigDecimal barDecimal) {
    this.barDecimal = new UpdateValue<>(barDecimal);
  }

  public void setBarBoolean(Boolean barBoolean) {
    this.barBoolean = new UpdateValue<>(barBoolean);
  }

  public void setBarDate(LocalDate barDate) {
    this.barDate = new UpdateValue<>(barDate);
  }

  public void setBarTimestamp(OffsetDateTime barTimestamp) {
    this.barTimestamp = new UpdateValue<>(barTimestamp);
  }
}
