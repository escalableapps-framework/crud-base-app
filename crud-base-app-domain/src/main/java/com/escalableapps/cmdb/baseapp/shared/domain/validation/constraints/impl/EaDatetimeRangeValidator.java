package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.escalableapps.cmdb.baseapp.shared.domain.util.DateUtils;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.DatetimeRange;

public class EaDatetimeRangeValidator implements ConstraintValidator<DatetimeRange, Object> {

  private Instant min;
  private Instant max;

  @Override
  public void initialize(DatetimeRange constraintAnnotation) {
    min = OffsetDateTime.parse(constraintAnnotation.min()).toInstant();
    max = OffsetDateTime.parse(constraintAnnotation.max()).toInstant();
  }

  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    Instant instant;
    if (value instanceof OffsetDateTime) {
      instant = ((OffsetDateTime) value).toInstant();
    } else if (value instanceof LocalDateTime) {
      instant = DateUtils.convertToInstant((LocalDateTime) value);
    } else {
      return false;
    }
    return (min.isBefore(instant) || min.equals(instant)) && (instant.isBefore(max) || instant.equals(max));
  }
}
