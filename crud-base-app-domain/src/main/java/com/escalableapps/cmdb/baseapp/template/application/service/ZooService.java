package com.escalableapps.cmdb.baseapp.template.application.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.*;
import com.escalableapps.cmdb.baseapp.template.port.spi.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ZooService implements ZooApi {

  private final ZooSpi zooSpi;

  public ZooService( //
      ZooSpi zooSpi //
  ) {
    this.zooSpi = zooSpi;
  }

  @Transactional
  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoo:zoo={}", zoo);

    zooSpi.createZoo(zoo);

    log.debug("createZoo:zoo={}", zoo);
    return zoo;
  }

  @Transactional
  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    Optional<Zoo> zoo = zooSpi.findZoo(zooOrg, zooId);

    log.debug("findZoo:zoo={}", zoo);
    return zoo;
  }

  @Transactional
  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);

    List<Zoo> zoos = zooSpi.findZoos(zooOrg, zooIds);

    log.debug("findZoos:zoos={}", zoos);
    return zoos;
  }

  @Transactional
  @Override
  public PageResponse<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);

    List<Zoo> zoos = zooSpi.findZoos(zooQuery, paging);
    long count = zooSpi.countZoos(zooQuery);
    log.debug("findZoos:zoos={}", zoos);
    log.debug("findZoos:count={}", count);

    return new PageResponse<>(zoos, count, paging);
  }

  @Transactional
  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = zooSpi.existsZoo(zooOrg, zooId);

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoos:zooQuery={}", zooQuery);

    boolean exists = zooSpi.existsZoos(zooQuery);

    log.debug("existsZoos:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("countZoos:zooQuery={}", zooQuery);

    long count = zooSpi.countZoos(zooQuery);

    log.debug("countZoos:count={}", count);
    return count;
  }

  @Transactional
  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("updateZoo:zooOrg={}", zooOrg);
    log.debug("updateZoo:zooId={}", zooId);
    log.debug("updateZoo:zooUpdate={}", zooUpdate);

    long updateCount = zooSpi.updateZoo(zooOrg, zooId, zooUpdate);

    log.debug("updateZoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Transactional
  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("deleteZoo:zooOrg={}", zooOrg);
    log.debug("deleteZoo:zooId={}", zooId);

    long deleteCount = zooSpi.deleteZoo(zooOrg, zooId);

    log.debug("deleteZoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
