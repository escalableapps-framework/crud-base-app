package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.QueryValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Zoo<br>
 * <strong>zoo</strong>.
 */
@Data
public class ZooQuery {

  /**
   * Description for zooOrg<br>
   * <strong>zoo_org bigint NOT NULL</strong>.
   */
  private QueryValue<Long> zooOrg;

  /**
   * Description for zooId<br>
   * <strong>zoo_id integer NOT NULL</strong>.
   */
  private QueryValue<Integer> zooId;

  /**
   * Description for zooChar<br>
   * <strong>zoo_char character</strong>.
   */
  private QueryValue<String> zooChar;

  /**
   * Description for zooVarchar<br>
   * <strong>zoo_varchar character varying</strong>.
   */
  private QueryValue<String> zooVarchar;

  /**
   * Description for zooText<br>
   * <strong>zoo_text text</strong>.
   */
  private QueryValue<String> zooText;

  /**
   * Description for zooSmallint<br>
   * <strong>zoo_smallint smallint</strong>.
   */
  private QueryValue<Short> zooSmallint;

  /**
   * Description for zooInteger<br>
   * <strong>zoo_integer integer</strong>.
   */
  private QueryValue<Integer> zooInteger;

  /**
   * Description for zooBigint<br>
   * <strong>zoo_bigint bigint</strong>.
   */
  private QueryValue<Long> zooBigint;

  /**
   * Description for zooReal<br>
   * <strong>zoo_real real</strong>.
   */
  private QueryValue<Float> zooReal;

  /**
   * Description for zooDouble<br>
   * <strong>zoo_double double precision</strong>.
   */
  private QueryValue<Double> zooDouble;

  /**
   * Description for zooDecimal<br>
   * <strong>zoo_decimal numeric</strong>.
   */
  private QueryValue<BigDecimal> zooDecimal;

  /**
   * Description for zooBoolean<br>
   * <strong>zoo_boolean boolean</strong>.
   */
  private QueryValue<Boolean> zooBoolean;

  /**
   * Description for zooDate<br>
   * <strong>zoo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> zooDate;

  /**
   * Description for zooDate<br>
   * <strong>zoo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> zooDateMin;

  /**
   * Description for zooDate<br>
   * <strong>zoo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> zooDateMax;

  /**
   * Description for zooTimestamp<br>
   * <strong>zoo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> zooTimestamp;

  /**
   * Description for zooTimestamp<br>
   * <strong>zoo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> zooTimestampMin;

  /**
   * Description for zooTimestamp<br>
   * <strong>zoo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> zooTimestampMax;

  public void setZooOrg(Long zooOrg) {
    this.zooOrg = new QueryValue<>(zooOrg);
  }

  public void setZooId(Integer zooId) {
    this.zooId = new QueryValue<>(zooId);
  }

  public void setZooChar(String zooChar) {
    this.zooChar = new QueryValue<>(zooChar);
  }

  public void setZooVarchar(String zooVarchar) {
    this.zooVarchar = new QueryValue<>(zooVarchar);
  }

  public void setZooText(String zooText) {
    this.zooText = new QueryValue<>(zooText);
  }

  public void setZooSmallint(Short zooSmallint) {
    this.zooSmallint = new QueryValue<>(zooSmallint);
  }

  public void setZooInteger(Integer zooInteger) {
    this.zooInteger = new QueryValue<>(zooInteger);
  }

  public void setZooBigint(Long zooBigint) {
    this.zooBigint = new QueryValue<>(zooBigint);
  }

  public void setZooReal(Float zooReal) {
    this.zooReal = new QueryValue<>(zooReal);
  }

  public void setZooDouble(Double zooDouble) {
    this.zooDouble = new QueryValue<>(zooDouble);
  }

  public void setZooDecimal(BigDecimal zooDecimal) {
    this.zooDecimal = new QueryValue<>(zooDecimal);
  }

  public void setZooBoolean(Boolean zooBoolean) {
    this.zooBoolean = new QueryValue<>(zooBoolean);
  }

  public void setZooDate(LocalDate zooDate) {
    this.zooDate = new QueryValue<>(zooDate);
  }

  public void setZooDateMin(LocalDate zooDateMin) {
    this.zooDateMin = new QueryValue<>(zooDateMin);
  }

  public void setZooDateMax(LocalDate zooDateMax) {
    this.zooDateMax = new QueryValue<>(zooDateMax);
  }

  public void setZooTimestamp(OffsetDateTime zooTimestamp) {
    this.zooTimestamp = new QueryValue<>(zooTimestamp);
  }

  public void setZooTimestampMin(OffsetDateTime zooTimestampMin) {
    this.zooTimestampMin = new QueryValue<>(zooTimestampMin);
  }

  public void setZooTimestampMax(OffsetDateTime zooTimestampMax) {
    this.zooTimestampMax = new QueryValue<>(zooTimestampMax);
  }
}
