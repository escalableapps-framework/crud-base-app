package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.QueryValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Look<br>
 * <strong>look</strong>.
 */
@Data
public class LookQuery {

  /**
   * Description for lookOrg<br>
   * <strong>look_org bigint NOT NULL</strong>.
   */
  private QueryValue<Long> lookOrg;

  /**
   * Description for lookId<br>
   * <strong>look_id bigint NOT NULL</strong>.
   */
  private QueryValue<Long> lookId;

  /**
   * Description for lookChar<br>
   * <strong>look_char character NOT NULL</strong>.
   */
  private QueryValue<String> lookChar;

  /**
   * Description for lookVarchar<br>
   * <strong>look_varchar character varying NOT NULL</strong>.
   */
  private QueryValue<String> lookVarchar;

  /**
   * Description for lookText<br>
   * <strong>look_text text NOT NULL</strong>.
   */
  private QueryValue<String> lookText;

  /**
   * Description for lookSmallint<br>
   * <strong>look_smallint smallint NOT NULL</strong>.
   */
  private QueryValue<Short> lookSmallint;

  /**
   * Description for lookInteger<br>
   * <strong>look_integer integer NOT NULL</strong>.
   */
  private QueryValue<Integer> lookInteger;

  /**
   * Description for lookBigint<br>
   * <strong>look_bigint bigint NOT NULL</strong>.
   */
  private QueryValue<Long> lookBigint;

  /**
   * Description for lookReal<br>
   * <strong>look_real real NOT NULL</strong>.
   */
  private QueryValue<Float> lookReal;

  /**
   * Description for lookDouble<br>
   * <strong>look_double double precision NOT NULL</strong>.
   */
  private QueryValue<Double> lookDouble;

  /**
   * Description for lookDecimal<br>
   * <strong>look_decimal numeric NOT NULL</strong>.
   */
  private QueryValue<BigDecimal> lookDecimal;

  /**
   * Description for lookBoolean<br>
   * <strong>look_boolean boolean NOT NULL</strong>.
   */
  private QueryValue<Boolean> lookBoolean;

  /**
   * Description for lookDate<br>
   * <strong>look_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> lookDate;

  /**
   * Description for lookDate<br>
   * <strong>look_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> lookDateMin;

  /**
   * Description for lookDate<br>
   * <strong>look_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> lookDateMax;

  /**
   * Description for lookTimestamp<br>
   * <strong>look_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> lookTimestamp;

  /**
   * Description for lookTimestamp<br>
   * <strong>look_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> lookTimestampMin;

  /**
   * Description for lookTimestamp<br>
   * <strong>look_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> lookTimestampMax;

  public void setLookOrg(Long lookOrg) {
    this.lookOrg = new QueryValue<>(lookOrg);
  }

  public void setLookId(Long lookId) {
    this.lookId = new QueryValue<>(lookId);
  }

  public void setLookChar(String lookChar) {
    this.lookChar = new QueryValue<>(lookChar);
  }

  public void setLookVarchar(String lookVarchar) {
    this.lookVarchar = new QueryValue<>(lookVarchar);
  }

  public void setLookText(String lookText) {
    this.lookText = new QueryValue<>(lookText);
  }

  public void setLookSmallint(Short lookSmallint) {
    this.lookSmallint = new QueryValue<>(lookSmallint);
  }

  public void setLookInteger(Integer lookInteger) {
    this.lookInteger = new QueryValue<>(lookInteger);
  }

  public void setLookBigint(Long lookBigint) {
    this.lookBigint = new QueryValue<>(lookBigint);
  }

  public void setLookReal(Float lookReal) {
    this.lookReal = new QueryValue<>(lookReal);
  }

  public void setLookDouble(Double lookDouble) {
    this.lookDouble = new QueryValue<>(lookDouble);
  }

  public void setLookDecimal(BigDecimal lookDecimal) {
    this.lookDecimal = new QueryValue<>(lookDecimal);
  }

  public void setLookBoolean(Boolean lookBoolean) {
    this.lookBoolean = new QueryValue<>(lookBoolean);
  }

  public void setLookDate(LocalDate lookDate) {
    this.lookDate = new QueryValue<>(lookDate);
  }

  public void setLookDateMin(LocalDate lookDateMin) {
    this.lookDateMin = new QueryValue<>(lookDateMin);
  }

  public void setLookDateMax(LocalDate lookDateMax) {
    this.lookDateMax = new QueryValue<>(lookDateMax);
  }

  public void setLookTimestamp(OffsetDateTime lookTimestamp) {
    this.lookTimestamp = new QueryValue<>(lookTimestamp);
  }

  public void setLookTimestampMin(OffsetDateTime lookTimestampMin) {
    this.lookTimestampMin = new QueryValue<>(lookTimestampMin);
  }

  public void setLookTimestampMax(OffsetDateTime lookTimestampMax) {
    this.lookTimestampMax = new QueryValue<>(lookTimestampMax);
  }
}
