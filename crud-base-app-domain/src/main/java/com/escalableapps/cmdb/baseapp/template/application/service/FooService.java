package com.escalableapps.cmdb.baseapp.template.application.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.*;
import com.escalableapps.cmdb.baseapp.template.port.spi.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FooService implements FooApi {

  private final FooSpi fooSpi;

  public FooService( //
      FooSpi fooSpi //
  ) {
    this.fooSpi = fooSpi;
  }

  @Transactional
  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoo:foo={}", foo);

    fooSpi.createFoo(foo);

    log.debug("createFoo:foo={}", foo);
    return foo;
  }

  @Transactional
  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    Optional<Foo> foo = fooSpi.findFoo(fooId);

    log.debug("findFoo:foo={}", foo);
    return foo;
  }

  @Transactional
  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);

    List<Foo> foos = fooSpi.findFoos(fooIds);

    log.debug("findFoos:foos={}", foos);
    return foos;
  }

  @Transactional
  @Override
  public PageResponse<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);

    List<Foo> foos = fooSpi.findFoos(fooQuery, paging);
    long count = fooSpi.countFoos(fooQuery);
    log.debug("findFoos:foos={}", foos);
    log.debug("findFoos:count={}", count);

    return new PageResponse<>(foos, count, paging);
  }

  @Transactional
  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = fooSpi.existsFoo(fooId);

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoos:fooQuery={}", fooQuery);

    boolean exists = fooSpi.existsFoos(fooQuery);

    log.debug("existsFoos:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("countFoos:fooQuery={}", fooQuery);

    long count = fooSpi.countFoos(fooQuery);

    log.debug("countFoos:count={}", count);
    return count;
  }

  @Transactional
  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("updateFoo:fooId={}", fooId);
    log.debug("updateFoo:fooUpdate={}", fooUpdate);

    long updateCount = fooSpi.updateFoo(fooId, fooUpdate);

    log.debug("updateFoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Transactional
  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("deleteFoo:fooId={}", fooId);

    long deleteCount = fooSpi.deleteFoo(fooId);

    log.debug("deleteFoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
