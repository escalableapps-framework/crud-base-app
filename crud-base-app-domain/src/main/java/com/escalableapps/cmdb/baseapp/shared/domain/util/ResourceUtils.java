package com.escalableapps.cmdb.baseapp.shared.domain.util;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;

public final class ResourceUtils {

  private static final String RESOURCE_LOCATION_BLANK_ERROR_MESSAGE = "resourceLocation: must not be blank";
  private static final String RESOURCE_LOCATION_CLASSPATH_ERROR_MESSAGE = "resourceLocation: must belong to the classpath protocol";
  private static final String RESOURCE_PATH_BLANK_ERROR_MESSAGE = "resourcePath: must not be blank";
  private static final String RESOURCE_PATH_CLASSPATH_ERROR_MESSAGE = "resourcePath: must belong to the classpath protocol";
  private static final String FILENAME_ERROR_MESSAGE = "filename: must not be blank";

  private static final String CLASSPATH_RESOURCE_PREFIX = "^(classpath(\\*)?:.*)$";

  private static ResourceLoader resourceLoader;

  private static ResourcePatternResolver resourcePatternResolver;

  static {
    resourceLoader = new DefaultResourceLoader();
    resourcePatternResolver = new PathMatchingResourcePatternResolver();
  }

  public static String readResource(String resourceLocation) {
    if (isBlank(resourceLocation)) {
      throw new IllegalArgumentException(RESOURCE_LOCATION_BLANK_ERROR_MESSAGE);
    }
    if (!resourceLocation.matches(CLASSPATH_RESOURCE_PREFIX)) {
      throw new IllegalArgumentException(RESOURCE_LOCATION_CLASSPATH_ERROR_MESSAGE);
    }

    try {
      String resourceUrl = resourcePatternResolver.getResources(resourceLocation)[0].getURL().toString();
      Resource resource = resourceLoader.getResource(resourceUrl);
      return new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"));
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String readResource(String resourcePath, String filename) {
    if (isBlank(resourcePath)) {
      throw new IllegalArgumentException(RESOURCE_PATH_BLANK_ERROR_MESSAGE);
    }
    if (!resourcePath.matches(CLASSPATH_RESOURCE_PREFIX)) {
      throw new IllegalArgumentException(RESOURCE_PATH_CLASSPATH_ERROR_MESSAGE);
    }
    if (isBlank(filename)) {
      throw new IllegalArgumentException(FILENAME_ERROR_MESSAGE);
    }

    StringBuilder resourceLocation = new StringBuilder();
    resourceLocation.append(resourcePath);
    if (!resourcePath.endsWith("/")) {
      resourceLocation.append("/");
    }
    if (filename.startsWith("/")) {
      filename = filename.substring(1);
    }
    resourceLocation.append(filename);
    return readResource(resourceLocation.toString());
  }

  private ResourceUtils() {
  }
}
