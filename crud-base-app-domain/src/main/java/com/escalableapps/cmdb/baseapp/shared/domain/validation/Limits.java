package com.escalableapps.cmdb.baseapp.shared.domain.validation;

public final class Limits {

  public static final int CHAR = 10485760;
  public static final int VARCHAR = 10485760;
  public static final int TEXT = 2147483647;

  public static final String DATE_MIN = "-4713-01-01";
  public static final String DATE_MAX = "+5874897-12-31";

  public static final String TIMESTAMP_MIN = "-4713-01-01T00:00:00Z";
  public static final String TIMESTAMP_MAX = "+294276-12-31T23:59:59.999Z";

  private Limits() {
  }
}
