package com.escalableapps.cmdb.baseapp.shared.domain.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class JacksonConfig {

  private static ObjectMapper objectMapper = new ObjectMapper() //
      .registerModule(new JavaTimeModule()) //
      .registerModule(new Jdk8Module()) //
      .registerModule(new JacksonModule()) //
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) //
      .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE) //
      .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS) //
      .setSerializationInclusion(Include.NON_NULL);

  private static ObjectMapper objectMapperIncludeNull = new ObjectMapper() //
      .registerModule(new JavaTimeModule()) //
      .registerModule(new Jdk8Module()) //
      .registerModule(new JacksonModule()) //
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) //
      .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE) //
      .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

  public static ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public static ObjectMapper getObjectMapperIncludeNull() {
    return objectMapperIncludeNull;
  }

  @Bean
  public ObjectMapper objectMapper() {
    return getObjectMapper();
  }
}
