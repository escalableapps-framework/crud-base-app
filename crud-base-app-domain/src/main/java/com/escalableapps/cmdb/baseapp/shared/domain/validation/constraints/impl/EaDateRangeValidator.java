package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.DateRange;

public class EaDateRangeValidator implements ConstraintValidator<DateRange, LocalDate> {

  private LocalDate min;
  private LocalDate max;

  @Override
  public void initialize(DateRange constraintAnnotation) {
    min = LocalDate.parse(constraintAnnotation.min());
    max = LocalDate.parse(constraintAnnotation.max());
  }

  @Override
  public boolean isValid(LocalDate current, ConstraintValidatorContext context) {
    if (current == null) {
      return true;
    }
    return (min.isBefore(current) || min.equals(current)) && (current.isBefore(max) || current.equals(max));
  }
}
