package com.escalableapps.cmdb.baseapp.shared.domain.model.vo;

import java.io.Serializable;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
public class QueryValue<T extends Serializable> implements Serializable {

  private static final long serialVersionUID = 1L;

  private final T value;

  public T get() {
    return value == null ? null : value;
  }

  public boolean exist() {
    return value != null;
  }
}
