package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl;

import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.jsoup.safety.Whitelist.none;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.jsoup.Jsoup;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextPlain;

public class EaTextPlainValidator implements ConstraintValidator<TextPlain, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (value == null || trimToNull(value) == null) {
      return true;
    }

    return Jsoup.isValid(value, none());
  }
}
