package com.escalableapps.cmdb.baseapp.template.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor()
public enum LookPropertyName {
  LOOK_ORG("lookOrg"), //
  LOOK_ID("lookId"), //
  LOOK_CHAR("lookChar"), //
  LOOK_VARCHAR("lookVarchar"), //
  LOOK_TEXT("lookText"), //
  LOOK_SMALLINT("lookSmallint"), //
  LOOK_INTEGER("lookInteger"), //
  LOOK_BIGINT("lookBigint"), //
  LOOK_REAL("lookReal"), //
  LOOK_DOUBLE("lookDouble"), //
  LOOK_DECIMAL("lookDecimal"), //
  LOOK_BOOLEAN("lookBoolean"), //
  LOOK_DATE("lookDate"), //
  LOOK_TIMESTAMP("lookTimestamp");

  @Getter
  private final String propertyName;
}
