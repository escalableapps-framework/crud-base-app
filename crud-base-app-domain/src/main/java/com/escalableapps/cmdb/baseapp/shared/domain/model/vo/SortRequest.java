package com.escalableapps.cmdb.baseapp.shared.domain.model.vo;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class SortRequest<E extends Enum<E>> implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final String PROPERTY_MUST_ERROR_MESSAGE = "property: must not be null";

  private E property;

  private boolean ascendant;

  public SortRequest(E property) {
    this(property, true);
  }

  public SortRequest(E property, boolean ascendant) {
    if (property == null) {
      throw new IllegalArgumentException(PROPERTY_MUST_ERROR_MESSAGE);
    }

    this.property = property;
    this.ascendant = ascendant;
  }
}
