package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.UpdateValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Look<br>
 * <strong>look</strong>.
 */
@Data
public class LookUpdate {

  /**
   * Description for lookChar<br>
   * <strong>look_char character NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = 1) String> lookChar;

  /**
   * Description for lookVarchar<br>
   * <strong>look_varchar character varying NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = VARCHAR) String> lookVarchar;

  /**
   * Description for lookText<br>
   * <strong>look_text text NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @Size(max = TEXT) String> lookText;

  /**
   * Description for lookSmallint<br>
   * <strong>look_smallint smallint NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Short> lookSmallint;

  /**
   * Description for lookInteger<br>
   * <strong>look_integer integer NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Integer> lookInteger;

  /**
   * Description for lookBigint<br>
   * <strong>look_bigint bigint NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Long> lookBigint;

  /**
   * Description for lookReal<br>
   * <strong>look_real real NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Float> lookReal;

  /**
   * Description for lookDouble<br>
   * <strong>look_double double precision NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Double> lookDouble;

  /**
   * Description for lookDecimal<br>
   * <strong>look_decimal numeric NOT NULL</strong>.
   */
  private UpdateValue<@NotNull BigDecimal> lookDecimal;

  /**
   * Description for lookBoolean<br>
   * <strong>look_boolean boolean NOT NULL</strong>.
   */
  private UpdateValue<@NotNull Boolean> lookBoolean;

  /**
   * Description for lookDate<br>
   * <strong>look_date date NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> lookDate;

  /**
   * Description for lookTimestamp<br>
   * <strong>look_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private UpdateValue<@NotNull @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> lookTimestamp;

  public void setLookChar(String lookChar) {
    this.lookChar = new UpdateValue<>(lookChar);
  }

  public void setLookVarchar(String lookVarchar) {
    this.lookVarchar = new UpdateValue<>(lookVarchar);
  }

  public void setLookText(String lookText) {
    this.lookText = new UpdateValue<>(lookText);
  }

  public void setLookSmallint(Short lookSmallint) {
    this.lookSmallint = new UpdateValue<>(lookSmallint);
  }

  public void setLookInteger(Integer lookInteger) {
    this.lookInteger = new UpdateValue<>(lookInteger);
  }

  public void setLookBigint(Long lookBigint) {
    this.lookBigint = new UpdateValue<>(lookBigint);
  }

  public void setLookReal(Float lookReal) {
    this.lookReal = new UpdateValue<>(lookReal);
  }

  public void setLookDouble(Double lookDouble) {
    this.lookDouble = new UpdateValue<>(lookDouble);
  }

  public void setLookDecimal(BigDecimal lookDecimal) {
    this.lookDecimal = new UpdateValue<>(lookDecimal);
  }

  public void setLookBoolean(Boolean lookBoolean) {
    this.lookBoolean = new UpdateValue<>(lookBoolean);
  }

  public void setLookDate(LocalDate lookDate) {
    this.lookDate = new UpdateValue<>(lookDate);
  }

  public void setLookTimestamp(OffsetDateTime lookTimestamp) {
    this.lookTimestamp = new UpdateValue<>(lookTimestamp);
  }
}
