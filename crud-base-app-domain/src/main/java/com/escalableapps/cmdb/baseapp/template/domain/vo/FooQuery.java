package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.QueryValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Foo<br>
 * <strong>foo</strong>.
 */
@Data
public class FooQuery {

  /**
   * Description for fooId<br>
   * <strong>foo_id integer NOT NULL</strong>.
   */
  private QueryValue<Integer> fooId;

  /**
   * Description for fooChar<br>
   * <strong>foo_char character</strong>.
   */
  private QueryValue<String> fooChar;

  /**
   * Description for fooVarchar<br>
   * <strong>foo_varchar character varying</strong>.
   */
  private QueryValue<String> fooVarchar;

  /**
   * Description for fooText<br>
   * <strong>foo_text text</strong>.
   */
  private QueryValue<String> fooText;

  /**
   * Description for fooSmallint<br>
   * <strong>foo_smallint smallint</strong>.
   */
  private QueryValue<Short> fooSmallint;

  /**
   * Description for fooInteger<br>
   * <strong>foo_integer integer</strong>.
   */
  private QueryValue<Integer> fooInteger;

  /**
   * Description for fooBigint<br>
   * <strong>foo_bigint bigint</strong>.
   */
  private QueryValue<Long> fooBigint;

  /**
   * Description for fooReal<br>
   * <strong>foo_real real</strong>.
   */
  private QueryValue<Float> fooReal;

  /**
   * Description for fooDouble<br>
   * <strong>foo_double double precision</strong>.
   */
  private QueryValue<Double> fooDouble;

  /**
   * Description for fooDecimal<br>
   * <strong>foo_decimal numeric</strong>.
   */
  private QueryValue<BigDecimal> fooDecimal;

  /**
   * Description for fooBoolean<br>
   * <strong>foo_boolean boolean</strong>.
   */
  private QueryValue<Boolean> fooBoolean;

  /**
   * Description for fooDate<br>
   * <strong>foo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> fooDate;

  /**
   * Description for fooDate<br>
   * <strong>foo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> fooDateMin;

  /**
   * Description for fooDate<br>
   * <strong>foo_date date</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> fooDateMax;

  /**
   * Description for fooTimestamp<br>
   * <strong>foo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> fooTimestamp;

  /**
   * Description for fooTimestamp<br>
   * <strong>foo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> fooTimestampMin;

  /**
   * Description for fooTimestamp<br>
   * <strong>foo_timestamp timestamp with time zone</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> fooTimestampMax;

  public void setFooId(Integer fooId) {
    this.fooId = new QueryValue<>(fooId);
  }

  public void setFooChar(String fooChar) {
    this.fooChar = new QueryValue<>(fooChar);
  }

  public void setFooVarchar(String fooVarchar) {
    this.fooVarchar = new QueryValue<>(fooVarchar);
  }

  public void setFooText(String fooText) {
    this.fooText = new QueryValue<>(fooText);
  }

  public void setFooSmallint(Short fooSmallint) {
    this.fooSmallint = new QueryValue<>(fooSmallint);
  }

  public void setFooInteger(Integer fooInteger) {
    this.fooInteger = new QueryValue<>(fooInteger);
  }

  public void setFooBigint(Long fooBigint) {
    this.fooBigint = new QueryValue<>(fooBigint);
  }

  public void setFooReal(Float fooReal) {
    this.fooReal = new QueryValue<>(fooReal);
  }

  public void setFooDouble(Double fooDouble) {
    this.fooDouble = new QueryValue<>(fooDouble);
  }

  public void setFooDecimal(BigDecimal fooDecimal) {
    this.fooDecimal = new QueryValue<>(fooDecimal);
  }

  public void setFooBoolean(Boolean fooBoolean) {
    this.fooBoolean = new QueryValue<>(fooBoolean);
  }

  public void setFooDate(LocalDate fooDate) {
    this.fooDate = new QueryValue<>(fooDate);
  }

  public void setFooDateMin(LocalDate fooDateMin) {
    this.fooDateMin = new QueryValue<>(fooDateMin);
  }

  public void setFooDateMax(LocalDate fooDateMax) {
    this.fooDateMax = new QueryValue<>(fooDateMax);
  }

  public void setFooTimestamp(OffsetDateTime fooTimestamp) {
    this.fooTimestamp = new QueryValue<>(fooTimestamp);
  }

  public void setFooTimestampMin(OffsetDateTime fooTimestampMin) {
    this.fooTimestampMin = new QueryValue<>(fooTimestampMin);
  }

  public void setFooTimestampMax(OffsetDateTime fooTimestampMax) {
    this.fooTimestampMax = new QueryValue<>(fooTimestampMax);
  }
}
