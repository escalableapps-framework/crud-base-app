package com.escalableapps.cmdb.baseapp.template.domain.entity;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description for Look<br>
 * <strong>look</strong>.
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Look implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Description for lookOrg<br>
   * <strong>look_org bigint NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Long lookOrg;

  /**
   * Description for lookId<br>
   * <strong>look_id bigint NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Long lookId;

  /**
   * Description for lookChar<br>
   * <strong>look_char character NOT NULL</strong>.
   */
  @NotNull
  @Size(max = 1)
  private String lookChar;

  /**
   * Description for lookVarchar<br>
   * <strong>look_varchar character varying NOT NULL</strong>.
   */
  @NotNull
  @Size(max = VARCHAR)
  private String lookVarchar;

  /**
   * Description for lookText<br>
   * <strong>look_text text NOT NULL</strong>.
   */
  @NotNull
  @Size(max = TEXT)
  private String lookText;

  /**
   * Description for lookSmallint<br>
   * <strong>look_smallint smallint NOT NULL</strong>.
   */
  @NotNull
  private Short lookSmallint;

  /**
   * Description for lookInteger<br>
   * <strong>look_integer integer NOT NULL</strong>.
   */
  @NotNull
  private Integer lookInteger;

  /**
   * Description for lookBigint<br>
   * <strong>look_bigint bigint NOT NULL</strong>.
   */
  @NotNull
  private Long lookBigint;

  /**
   * Description for lookReal<br>
   * <strong>look_real real NOT NULL</strong>.
   */
  @NotNull
  private Float lookReal;

  /**
   * Description for lookDouble<br>
   * <strong>look_double double precision NOT NULL</strong>.
   */
  @NotNull
  private Double lookDouble;

  /**
   * Description for lookDecimal<br>
   * <strong>look_decimal numeric NOT NULL</strong>.
   */
  @NotNull
  private BigDecimal lookDecimal;

  /**
   * Description for lookBoolean<br>
   * <strong>look_boolean boolean NOT NULL</strong>.
   */
  @NotNull
  private Boolean lookBoolean;

  /**
   * Description for lookDate<br>
   * <strong>look_date date NOT NULL</strong>.
   */
  @NotNull
  @DateRange(min = DATE_MIN, max = DATE_MAX)
  private LocalDate lookDate;

  /**
   * Description for lookTimestamp<br>
   * <strong>look_timestamp timestamp with time zone NOT NULL</strong>.
   */
  @NotNull
  @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX)
  private OffsetDateTime lookTimestamp;
}
