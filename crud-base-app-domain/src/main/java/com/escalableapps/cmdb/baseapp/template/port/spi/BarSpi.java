package com.escalableapps.cmdb.baseapp.template.port.spi;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface BarSpi {

  Bar createBar( //
      @NotNull @Valid Bar bar //
  );

  Optional<Bar> findBar( //
      @NotNull @Min(1) Integer barId //
  );

  List<@NotNull @Valid Bar> findBars( //
      @NotNull @Size(min = 1) List<@NotNull @Min(1) Integer> barIds //
  );

  List<@NotNull @Valid Bar> findBars( //
      @NotNull @Valid BarQuery barQuery //
  );

  List<@NotNull @Valid Bar> findBars( //
      @NotNull @Valid BarQuery barQuery, //
      @NotNull @Valid PageRequest<BarPropertyName> paging //
  );

  boolean existsBar( //
      @NotNull @Min(1) Integer barId //
  );

  boolean existsBars( //
      @NotNull @Valid BarQuery barQuery //
  );

  long countBars( //
      @NotNull @Valid BarQuery barQuery //
  );

  long updateBar( //
      @NotNull @Min(1) Integer barId, //
      @NotNull @Valid BarUpdate barUpdate //
  );

  long deleteBar( //
      @NotNull @Min(1) Integer barId //
  );
}
