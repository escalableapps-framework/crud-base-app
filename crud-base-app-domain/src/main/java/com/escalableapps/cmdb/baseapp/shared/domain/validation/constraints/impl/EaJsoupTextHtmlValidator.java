package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType.HTML_BASIC;
import static com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType.HTML_DATA_IMAGES;
import static com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType.HTML_FULL;
import static com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType.HTML_IMAGES;
import static com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType.HTML_SIMPLE;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.jsoup.safety.Whitelist.basic;
import static org.jsoup.safety.Whitelist.basicWithImages;
import static org.jsoup.safety.Whitelist.none;
import static org.jsoup.safety.Whitelist.relaxed;
import static org.jsoup.safety.Whitelist.simpleText;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.jsoup.Jsoup;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.TextHtml.TextType;

public class EaJsoupTextHtmlValidator implements ConstraintValidator<TextHtml, String> {

  private TextType htmlType;

  @Override
  public void initialize(TextHtml constraintAnnotation) {
    this.htmlType = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (value == null || trimToNull(value) == null) {
      return true;
    }

    if (htmlType == HTML_FULL) {
      return validateHtmlFull(value);
    } else if (htmlType == HTML_DATA_IMAGES) {
      return validateHtmlDataImages(value);
    } else if (htmlType == HTML_IMAGES) {
      return validateHtmlImages(value);
    } else if (htmlType == HTML_BASIC) {
      return validateHtmlBasic(value);
    } else if (htmlType == HTML_SIMPLE) {
      return validateHtmlSimple(value);
    } else /* (htmlType == TextType.TEXT_PLAIN) */ {
      return validateTextPlain(value);
    }
  }

  private boolean validateHtmlFull(String value) {
    return Jsoup.isValid(value, relaxed());
  }

  private boolean validateHtmlDataImages(String value) {
    return Jsoup.isValid(value, basicWithImages() //
        .addProtocols("img", "src", "data") //
        .addAttributes("img", "style") //
    );
  }

  private boolean validateHtmlImages(String value) {
    return Jsoup.isValid(value, basicWithImages());
  }

  private boolean validateHtmlBasic(String value) {
    return Jsoup.isValid(value, basic());
  }

  private boolean validateHtmlSimple(String value) {
    return Jsoup.isValid(value, simpleText());
  }

  private boolean validateTextPlain(String value) {
    return Jsoup.isValid(value, none());
  }
}
