package com.escalableapps.cmdb.baseapp.template.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor()
public enum FooPropertyName {
  FOO_ID("fooId"), //
  FOO_CHAR("fooChar"), //
  FOO_VARCHAR("fooVarchar"), //
  FOO_TEXT("fooText"), //
  FOO_SMALLINT("fooSmallint"), //
  FOO_INTEGER("fooInteger"), //
  FOO_BIGINT("fooBigint"), //
  FOO_REAL("fooReal"), //
  FOO_DOUBLE("fooDouble"), //
  FOO_DECIMAL("fooDecimal"), //
  FOO_BOOLEAN("fooBoolean"), //
  FOO_DATE("fooDate"), //
  FOO_TIMESTAMP("fooTimestamp");

  @Getter
  private final String propertyName;
}
