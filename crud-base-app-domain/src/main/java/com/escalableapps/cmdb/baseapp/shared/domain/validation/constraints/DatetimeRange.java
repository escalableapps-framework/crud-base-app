package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.DatetimeRange.List;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl.EaDatetimeRangeValidator;

@Documented
@Constraint(validatedBy = EaDatetimeRangeValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface DatetimeRange {

  String min() default "-999999999-01-01T00:00:00Z";

  String max() default "+999999999-12-31T23:59:59.999Z";

  String message() default "{com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.DatetimeRange.fail}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
  @Retention(RUNTIME)
  @Documented
  public @interface List {
    DatetimeRange[] value();
  }
}
