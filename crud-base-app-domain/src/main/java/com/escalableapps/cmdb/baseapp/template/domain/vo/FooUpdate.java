package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.UpdateValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Foo<br>
 * <strong>foo</strong>.
 */
@Data
public class FooUpdate {

  /**
   * Description for fooChar<br>
   * <strong>foo_char character</strong>.
   */
  private UpdateValue<@Size(max = 2) String> fooChar;

  /**
   * Description for fooVarchar<br>
   * <strong>foo_varchar character varying</strong>.
   */
  private UpdateValue<@Size(max = 4) String> fooVarchar;

  /**
   * Description for fooText<br>
   * <strong>foo_text text</strong>.
   */
  private UpdateValue<@Size(max = TEXT) String> fooText;

  /**
   * Description for fooSmallint<br>
   * <strong>foo_smallint smallint</strong>.
   */
  private UpdateValue<Short> fooSmallint;

  /**
   * Description for fooInteger<br>
   * <strong>foo_integer integer</strong>.
   */
  private UpdateValue<Integer> fooInteger;

  /**
   * Description for fooBigint<br>
   * <strong>foo_bigint bigint</strong>.
   */
  private UpdateValue<Long> fooBigint;

  /**
   * Description for fooReal<br>
   * <strong>foo_real real</strong>.
   */
  private UpdateValue<Float> fooReal;

  /**
   * Description for fooDouble<br>
   * <strong>foo_double double precision</strong>.
   */
  private UpdateValue<Double> fooDouble;

  /**
   * Description for fooDecimal<br>
   * <strong>foo_decimal numeric</strong>.
   */
  private UpdateValue<BigDecimal> fooDecimal;

  /**
   * Description for fooBoolean<br>
   * <strong>foo_boolean boolean</strong>.
   */
  private UpdateValue<Boolean> fooBoolean;

  /**
   * Description for fooDate<br>
   * <strong>foo_date date</strong>.
   */
  private UpdateValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> fooDate;

  /**
   * Description for fooTimestamp<br>
   * <strong>foo_timestamp timestamp with time zone</strong>.
   */
  private UpdateValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> fooTimestamp;

  public void setFooChar(String fooChar) {
    this.fooChar = new UpdateValue<>(fooChar);
  }

  public void setFooVarchar(String fooVarchar) {
    this.fooVarchar = new UpdateValue<>(fooVarchar);
  }

  public void setFooText(String fooText) {
    this.fooText = new UpdateValue<>(fooText);
  }

  public void setFooSmallint(Short fooSmallint) {
    this.fooSmallint = new UpdateValue<>(fooSmallint);
  }

  public void setFooInteger(Integer fooInteger) {
    this.fooInteger = new UpdateValue<>(fooInteger);
  }

  public void setFooBigint(Long fooBigint) {
    this.fooBigint = new UpdateValue<>(fooBigint);
  }

  public void setFooReal(Float fooReal) {
    this.fooReal = new UpdateValue<>(fooReal);
  }

  public void setFooDouble(Double fooDouble) {
    this.fooDouble = new UpdateValue<>(fooDouble);
  }

  public void setFooDecimal(BigDecimal fooDecimal) {
    this.fooDecimal = new UpdateValue<>(fooDecimal);
  }

  public void setFooBoolean(Boolean fooBoolean) {
    this.fooBoolean = new UpdateValue<>(fooBoolean);
  }

  public void setFooDate(LocalDate fooDate) {
    this.fooDate = new UpdateValue<>(fooDate);
  }

  public void setFooTimestamp(OffsetDateTime fooTimestamp) {
    this.fooTimestamp = new UpdateValue<>(fooTimestamp);
  }
}
