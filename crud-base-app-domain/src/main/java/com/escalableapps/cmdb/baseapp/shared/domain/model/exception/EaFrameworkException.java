package com.escalableapps.cmdb.baseapp.shared.domain.model.exception;

public class EaFrameworkException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EaFrameworkException(Exception cause) {
    super(cause.getMessage(), cause);
  }

  public EaFrameworkException(String message, Exception cause) {
    super(message, cause);
  }

}
