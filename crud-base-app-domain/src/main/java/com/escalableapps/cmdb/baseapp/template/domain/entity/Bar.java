package com.escalableapps.cmdb.baseapp.template.domain.entity;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description for Bar<br>
 * <strong>bar</strong>.
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Bar implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Description for barId<br>
   * <strong>bar_id integer NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Integer barId;

  /**
   * Description for barChar<br>
   * <strong>bar_char character NOT NULL</strong>.
   */
  @NotNull
  @Size(max = 1)
  private String barChar;

  /**
   * Description for barVarchar<br>
   * <strong>bar_varchar character varying NOT NULL</strong>.
   */
  @NotNull
  @Size(max = VARCHAR)
  private String barVarchar;

  /**
   * Description for barText<br>
   * <strong>bar_text text NOT NULL</strong>.
   */
  @NotNull
  @Size(max = TEXT)
  private String barText;

  /**
   * Description for barSmallint<br>
   * <strong>bar_smallint smallint NOT NULL</strong>.
   */
  @NotNull
  private Short barSmallint;

  /**
   * Description for barInteger<br>
   * <strong>bar_integer integer NOT NULL</strong>.
   */
  @NotNull
  private Integer barInteger;

  /**
   * Description for barBigint<br>
   * <strong>bar_bigint bigint NOT NULL</strong>.
   */
  @NotNull
  private Long barBigint;

  /**
   * Description for barReal<br>
   * <strong>bar_real real NOT NULL</strong>.
   */
  @NotNull
  private Float barReal;

  /**
   * Description for barDouble<br>
   * <strong>bar_double double precision NOT NULL</strong>.
   */
  @NotNull
  private Double barDouble;

  /**
   * Description for barDecimal<br>
   * <strong>bar_decimal numeric NOT NULL</strong>.
   */
  @NotNull
  private BigDecimal barDecimal;

  /**
   * Description for barBoolean<br>
   * <strong>bar_boolean boolean NOT NULL</strong>.
   */
  @NotNull
  private Boolean barBoolean;

  /**
   * Description for barDate<br>
   * <strong>bar_date date NOT NULL</strong>.
   */
  @NotNull
  @DateRange(min = DATE_MIN, max = DATE_MAX)
  private LocalDate barDate;

  /**
   * Description for barTimestamp<br>
   * <strong>bar_timestamp timestamp with time zone NOT NULL</strong>.
   */
  @NotNull
  @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX)
  private OffsetDateTime barTimestamp;
}
