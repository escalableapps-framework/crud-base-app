package com.escalableapps.cmdb.baseapp.template.port.api;

import java.util.List;
import java.util.Optional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface FooApi {

  Foo createFoo( //
      Foo foo //
  );

  Optional<Foo> findFoo( //
      Integer fooId //
  );

  List<Foo> findFoos( //
      List<Integer> fooIds //
  );

  PageResponse<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  );

  boolean existsFoo( //
      Integer fooId //
  );

  boolean existsFoos( //
      FooQuery fooQuery //
  );

  long countFoos( //
      FooQuery fooQuery //
  );

  long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  );

  long deleteFoo( //
      Integer fooId //
  );
}
