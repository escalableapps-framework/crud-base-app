package com.escalableapps.cmdb.baseapp.template.port.api;

import java.util.List;
import java.util.Optional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface LookApi {

  Look createLook( //
      Look look //
  );

  Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  );

  List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  );

  PageResponse<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  );

  boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  );

  boolean existsLooks( //
      LookQuery lookQuery //
  );

  long countLooks( //
      LookQuery lookQuery //
  );

  long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  );

  long deleteLook( //
      Long lookOrg, //
      Long lookId //
  );
}
