package com.escalableapps.cmdb.baseapp.template.domain.entity;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description for Foo<br>
 * <strong>foo</strong>.
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Foo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Description for fooId<br>
   * <strong>foo_id integer NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Integer fooId;

  /**
   * Description for fooChar<br>
   * <strong>foo_char character</strong>.
   */
  @Size(max = 2)
  private String fooChar;

  /**
   * Description for fooVarchar<br>
   * <strong>foo_varchar character varying</strong>.
   */
  @Size(max = 4)
  private String fooVarchar;

  /**
   * Description for fooText<br>
   * <strong>foo_text text</strong>.
   */
  @Size(max = TEXT)
  private String fooText;

  /**
   * Description for fooSmallint<br>
   * <strong>foo_smallint smallint</strong>.
   */
  private Short fooSmallint;

  /**
   * Description for fooInteger<br>
   * <strong>foo_integer integer</strong>.
   */
  private Integer fooInteger;

  /**
   * Description for fooBigint<br>
   * <strong>foo_bigint bigint</strong>.
   */
  private Long fooBigint;

  /**
   * Description for fooReal<br>
   * <strong>foo_real real</strong>.
   */
  private Float fooReal;

  /**
   * Description for fooDouble<br>
   * <strong>foo_double double precision</strong>.
   */
  private Double fooDouble;

  /**
   * Description for fooDecimal<br>
   * <strong>foo_decimal numeric</strong>.
   */
  @DecimalMax("99.9999")
  private BigDecimal fooDecimal;

  /**
   * Description for fooBoolean<br>
   * <strong>foo_boolean boolean</strong>.
   */
  private Boolean fooBoolean;

  /**
   * Description for fooDate<br>
   * <strong>foo_date date</strong>.
   */
  @DateRange(min = DATE_MIN, max = DATE_MAX)
  private LocalDate fooDate;

  /**
   * Description for fooTimestamp<br>
   * <strong>foo_timestamp timestamp with time zone</strong>.
   */
  @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX)
  private OffsetDateTime fooTimestamp;
}
