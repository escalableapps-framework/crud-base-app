package com.escalableapps.cmdb.baseapp.shared.domain.model.vo;

import static java.util.Arrays.asList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class PageRequest<E extends Enum<E>> implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final String PAGE_NUMBER_ERROR_MESSAGE = "pageNumber: must be greater than or equal to 0";
  private static final String PAGE_SIZE_ERROR_MESSAGE = "pageSize: must be greater than or equal to 1";
  private static final String SORT_REQUEST_ERROR_MESSAGE = "sortRequest[%d]: must not be null";

  private int pageNumber;

  private int pageSize;

  private List<SortRequest<E>> sortRequests;

  public PageRequest() {
    this(0, Integer.MAX_VALUE);
  }

  @SafeVarargs
  public PageRequest(int pageNumber, int pageSize, SortRequest<E>... sortRequests) {
    if (pageNumber < 0) {
      throw new IllegalArgumentException(PAGE_NUMBER_ERROR_MESSAGE);
    }
    if (pageSize < 1) {
      throw new IllegalArgumentException(PAGE_SIZE_ERROR_MESSAGE);
    }
    for (int i = 0; i < sortRequests.length; i++) {
      if (sortRequests[i] == null) {
        throw new IllegalArgumentException(String.format(SORT_REQUEST_ERROR_MESSAGE, i));
      }
    }

    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.sortRequests = new ArrayList<>();
    this.sortRequests.addAll(asList(sortRequests));
  }

  public int getOffset() {
    return pageNumber * pageSize;
  }
}
