package com.escalableapps.cmdb.baseapp.shared.domain.model.exception;

import static java.lang.String.format;

public class ConflictException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ConflictException() {
    this((String) null);
  }

  public ConflictException(String message, Object... values) {
    super(message == null ? null : format(message, values));
  }

  public ConflictException(Exception cause) {
    super(cause.getMessage(), cause);
  }

}
