package com.escalableapps.cmdb.baseapp.shared.domain.util;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.escalableapps.cmdb.baseapp.shared.domain.config.JacksonConfig;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JsonUtils {

  private static final String JSON_ERROR_MESSAGE = "json: must not be blank";
  private static final String TYPE_MUST_ERROR_MESSAGE = "type: must not be null";
  private static final String KEY_CLASS_ERROR_MESSAGE = "keyClass: must not be null";
  private static final String VALUE_CLASS_ERROR_MESSAGE = "valueClass: must not be null";

  private static ObjectMapper objectMapper = JacksonConfig.getObjectMapper();

  private static ObjectMapper objectMapperIncludeNull = JacksonConfig.getObjectMapperIncludeNull();

  public static <T> T parseAsType(String json, Class<T> type) {
    if (isBlank(json)) {
      throw new IllegalArgumentException(JSON_ERROR_MESSAGE);
    }
    if (type == null) {
      throw new IllegalArgumentException(TYPE_MUST_ERROR_MESSAGE);
    }

    try {
      return objectMapper.readValue(json, type);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static <K, V> Map<K, V> parseAsMap(String json, Class<K> keyClass, Class<V> valueClass) {
    if (isBlank(json)) {
      throw new IllegalArgumentException(JSON_ERROR_MESSAGE);
    }
    if (keyClass == null) {
      throw new IllegalArgumentException(KEY_CLASS_ERROR_MESSAGE);
    }
    if (valueClass == null) {
      throw new IllegalArgumentException(VALUE_CLASS_ERROR_MESSAGE);
    }

    try {
      JavaType javaType = objectMapper.getTypeFactory().constructMapLikeType(HashMap.class, keyClass, valueClass);
      return objectMapper.readValue(json, javaType);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static <T> List<T> parseAsList(String json, Class<T> type) {
    if (isBlank(json)) {
      throw new IllegalArgumentException(JSON_ERROR_MESSAGE);
    }
    if (type == null) {
      throw new IllegalArgumentException(TYPE_MUST_ERROR_MESSAGE);
    }

    try {
      JavaType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, type);
      return objectMapper.readValue(json, javaType);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String asJson(Object source) {
    try {
      return objectMapper.writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String asJsonIncludeNull(Object source) {
    try {
      return objectMapperIncludeNull.writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  private JsonUtils() {
  }
}
