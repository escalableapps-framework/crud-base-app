package com.escalableapps.cmdb.baseapp.template.application.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.*;
import com.escalableapps.cmdb.baseapp.template.port.spi.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BarService implements BarApi {

  private final BarSpi barSpi;

  public BarService( //
      BarSpi barSpi //
  ) {
    this.barSpi = barSpi;
  }

  @Transactional
  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBar:bar={}", bar);

    barSpi.createBar(bar);

    log.debug("createBar:bar={}", bar);
    return bar;
  }

  @Transactional
  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    Optional<Bar> bar = barSpi.findBar(barId);

    log.debug("findBar:bar={}", bar);
    return bar;
  }

  @Transactional
  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);

    List<Bar> bars = barSpi.findBars(barIds);

    log.debug("findBars:bars={}", bars);
    return bars;
  }

  @Transactional
  @Override
  public PageResponse<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);

    List<Bar> bars = barSpi.findBars(barQuery, paging);
    long count = barSpi.countBars(barQuery);
    log.debug("findBars:bars={}", bars);
    log.debug("findBars:count={}", count);

    return new PageResponse<>(bars, count, paging);
  }

  @Transactional
  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = barSpi.existsBar(barId);

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBars:barQuery={}", barQuery);

    boolean exists = barSpi.existsBars(barQuery);

    log.debug("existsBars:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("countBars:barQuery={}", barQuery);

    long count = barSpi.countBars(barQuery);

    log.debug("countBars:count={}", count);
    return count;
  }

  @Transactional
  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("updateBar:barId={}", barId);
    log.debug("updateBar:barUpdate={}", barUpdate);

    long updateCount = barSpi.updateBar(barId, barUpdate);

    log.debug("updateBar:updateCount={}", updateCount);
    return updateCount;
  }

  @Transactional
  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("deleteBar:barId={}", barId);

    long deleteCount = barSpi.deleteBar(barId);

    log.debug("deleteBar:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
