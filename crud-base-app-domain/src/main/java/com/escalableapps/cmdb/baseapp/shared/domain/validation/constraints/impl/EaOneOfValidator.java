package com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.OneOf;

public class EaOneOfValidator implements ConstraintValidator<OneOf, String> {

  private String[] values;

  @Override
  public void initialize(OneOf constraintAnnotation) {
    this.values = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(String current, ConstraintValidatorContext context) {
    if (current == null) {
      return true;
    }
    for (String value : this.values) {
      if (value.equals(current)) {
        return true;
      }
    }
    return false;
  }
}
