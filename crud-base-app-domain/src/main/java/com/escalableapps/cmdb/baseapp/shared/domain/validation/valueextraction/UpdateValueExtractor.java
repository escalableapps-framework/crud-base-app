package com.escalableapps.cmdb.baseapp.shared.domain.validation.valueextraction;

import javax.validation.valueextraction.ExtractedValue;
import javax.validation.valueextraction.ValueExtractor;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.UpdateValue;

public class UpdateValueExtractor implements ValueExtractor<UpdateValue<@ExtractedValue ?>> {

  @Override
  public void extractValues(UpdateValue<@ExtractedValue ?> updateValue, ValueReceiver receiver) {
    Object object = updateValue.get();
    receiver.value("value", object);
  }
}
