package com.escalableapps.cmdb.baseapp.template.port.spi;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

public interface LookSpi {

  Look createLook( //
      @NotNull @Valid Look look //
  );

  Optional<Look> findLook( //
      @NotNull @Min(1) Long lookOrg, //
      @NotNull @Min(1) Long lookId //
  );

  List<@NotNull @Valid Look> findLooks( //
      @NotNull @Min(1) Long lookOrg, //
      @NotNull @Size(min = 1) List<@NotNull @Min(1) Long> lookIds //
  );

  List<@NotNull @Valid Look> findLooks( //
      @NotNull @Valid LookQuery lookQuery //
  );

  List<@NotNull @Valid Look> findLooks( //
      @NotNull @Valid LookQuery lookQuery, //
      @NotNull @Valid PageRequest<LookPropertyName> paging //
  );

  boolean existsLook( //
      @NotNull @Min(1) Long lookOrg, //
      @NotNull @Min(1) Long lookId //
  );

  boolean existsLooks( //
      @NotNull @Valid LookQuery lookQuery //
  );

  long countLooks( //
      @NotNull @Valid LookQuery lookQuery //
  );

  long updateLook( //
      @NotNull @Min(1) Long lookOrg, //
      @NotNull @Min(1) Long lookId, //
      @NotNull @Valid LookUpdate lookUpdate //
  );

  long deleteLook( //
      @NotNull @Min(1) Long lookOrg, //
      @NotNull @Min(1) Long lookId //
  );
}
