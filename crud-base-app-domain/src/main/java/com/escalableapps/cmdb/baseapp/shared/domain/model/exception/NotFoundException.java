package com.escalableapps.cmdb.baseapp.shared.domain.model.exception;

import static java.lang.String.format;

public class NotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public NotFoundException() {
    this((String) null);
  }

  public NotFoundException(String message, Object... values) {
    super(message == null ? null : format(message, values));
  }

  public NotFoundException(Exception cause) {
    super(cause.getMessage(), cause);
  }

}
