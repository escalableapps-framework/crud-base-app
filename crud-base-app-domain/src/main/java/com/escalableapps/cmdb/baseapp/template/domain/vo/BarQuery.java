package com.escalableapps.cmdb.baseapp.template.domain.vo;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.QueryValue;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;

/**
 * Description for Bar<br>
 * <strong>bar</strong>.
 */
@Data
public class BarQuery {

  /**
   * Description for barId<br>
   * <strong>bar_id integer NOT NULL</strong>.
   */
  private QueryValue<Integer> barId;

  /**
   * Description for barChar<br>
   * <strong>bar_char character NOT NULL</strong>.
   */
  private QueryValue<String> barChar;

  /**
   * Description for barVarchar<br>
   * <strong>bar_varchar character varying NOT NULL</strong>.
   */
  private QueryValue<String> barVarchar;

  /**
   * Description for barText<br>
   * <strong>bar_text text NOT NULL</strong>.
   */
  private QueryValue<String> barText;

  /**
   * Description for barSmallint<br>
   * <strong>bar_smallint smallint NOT NULL</strong>.
   */
  private QueryValue<Short> barSmallint;

  /**
   * Description for barInteger<br>
   * <strong>bar_integer integer NOT NULL</strong>.
   */
  private QueryValue<Integer> barInteger;

  /**
   * Description for barBigint<br>
   * <strong>bar_bigint bigint NOT NULL</strong>.
   */
  private QueryValue<Long> barBigint;

  /**
   * Description for barReal<br>
   * <strong>bar_real real NOT NULL</strong>.
   */
  private QueryValue<Float> barReal;

  /**
   * Description for barDouble<br>
   * <strong>bar_double double precision NOT NULL</strong>.
   */
  private QueryValue<Double> barDouble;

  /**
   * Description for barDecimal<br>
   * <strong>bar_decimal numeric NOT NULL</strong>.
   */
  private QueryValue<BigDecimal> barDecimal;

  /**
   * Description for barBoolean<br>
   * <strong>bar_boolean boolean NOT NULL</strong>.
   */
  private QueryValue<Boolean> barBoolean;

  /**
   * Description for barDate<br>
   * <strong>bar_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> barDate;

  /**
   * Description for barDate<br>
   * <strong>bar_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> barDateMin;

  /**
   * Description for barDate<br>
   * <strong>bar_date date NOT NULL</strong>.
   */
  private QueryValue<@DateRange(min = DATE_MIN, max = DATE_MAX) LocalDate> barDateMax;

  /**
   * Description for barTimestamp<br>
   * <strong>bar_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> barTimestamp;

  /**
   * Description for barTimestamp<br>
   * <strong>bar_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> barTimestampMin;

  /**
   * Description for barTimestamp<br>
   * <strong>bar_timestamp timestamp with time zone NOT NULL</strong>.
   */
  private QueryValue<@DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX) OffsetDateTime> barTimestampMax;

  public void setBarId(Integer barId) {
    this.barId = new QueryValue<>(barId);
  }

  public void setBarChar(String barChar) {
    this.barChar = new QueryValue<>(barChar);
  }

  public void setBarVarchar(String barVarchar) {
    this.barVarchar = new QueryValue<>(barVarchar);
  }

  public void setBarText(String barText) {
    this.barText = new QueryValue<>(barText);
  }

  public void setBarSmallint(Short barSmallint) {
    this.barSmallint = new QueryValue<>(barSmallint);
  }

  public void setBarInteger(Integer barInteger) {
    this.barInteger = new QueryValue<>(barInteger);
  }

  public void setBarBigint(Long barBigint) {
    this.barBigint = new QueryValue<>(barBigint);
  }

  public void setBarReal(Float barReal) {
    this.barReal = new QueryValue<>(barReal);
  }

  public void setBarDouble(Double barDouble) {
    this.barDouble = new QueryValue<>(barDouble);
  }

  public void setBarDecimal(BigDecimal barDecimal) {
    this.barDecimal = new QueryValue<>(barDecimal);
  }

  public void setBarBoolean(Boolean barBoolean) {
    this.barBoolean = new QueryValue<>(barBoolean);
  }

  public void setBarDate(LocalDate barDate) {
    this.barDate = new QueryValue<>(barDate);
  }

  public void setBarDateMin(LocalDate barDateMin) {
    this.barDateMin = new QueryValue<>(barDateMin);
  }

  public void setBarDateMax(LocalDate barDateMax) {
    this.barDateMax = new QueryValue<>(barDateMax);
  }

  public void setBarTimestamp(OffsetDateTime barTimestamp) {
    this.barTimestamp = new QueryValue<>(barTimestamp);
  }

  public void setBarTimestampMin(OffsetDateTime barTimestampMin) {
    this.barTimestampMin = new QueryValue<>(barTimestampMin);
  }

  public void setBarTimestampMax(OffsetDateTime barTimestampMax) {
    this.barTimestampMax = new QueryValue<>(barTimestampMax);
  }
}
