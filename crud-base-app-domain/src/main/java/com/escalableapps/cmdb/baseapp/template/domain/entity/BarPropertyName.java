package com.escalableapps.cmdb.baseapp.template.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor()
public enum BarPropertyName {
  BAR_ID("barId"), //
  BAR_CHAR("barChar"), //
  BAR_VARCHAR("barVarchar"), //
  BAR_TEXT("barText"), //
  BAR_SMALLINT("barSmallint"), //
  BAR_INTEGER("barInteger"), //
  BAR_BIGINT("barBigint"), //
  BAR_REAL("barReal"), //
  BAR_DOUBLE("barDouble"), //
  BAR_DECIMAL("barDecimal"), //
  BAR_BOOLEAN("barBoolean"), //
  BAR_DATE("barDate"), //
  BAR_TIMESTAMP("barTimestamp");

  @Getter
  private final String propertyName;
}
