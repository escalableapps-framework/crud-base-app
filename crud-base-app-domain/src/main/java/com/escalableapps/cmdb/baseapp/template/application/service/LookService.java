package com.escalableapps.cmdb.baseapp.template.application.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.*;
import com.escalableapps.cmdb.baseapp.template.port.spi.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LookService implements LookApi {

  private final LookSpi lookSpi;

  public LookService( //
      LookSpi lookSpi //
  ) {
    this.lookSpi = lookSpi;
  }

  @Transactional
  @Override
  public Look createLook( //
      Look look //
  ) {
    log.debug("createLook:look={}", look);

    lookSpi.createLook(look);

    log.debug("createLook:look={}", look);
    return look;
  }

  @Transactional
  @Override
  public Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);

    Optional<Look> look = lookSpi.findLook(lookOrg, lookId);

    log.debug("findLook:look={}", look);
    return look;
  }

  @Transactional
  @Override
  public List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  ) {
    log.debug("findLooks:lookOrg={}", lookOrg);
    log.debug("findLooks:lookIds={}", lookIds);

    List<Look> looks = lookSpi.findLooks(lookOrg, lookIds);

    log.debug("findLooks:looks={}", looks);
    return looks;
  }

  @Transactional
  @Override
  public PageResponse<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  ) {
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:paging={}", paging);

    List<Look> looks = lookSpi.findLooks(lookQuery, paging);
    long count = lookSpi.countLooks(lookQuery);
    log.debug("findLooks:looks={}", looks);
    log.debug("findLooks:count={}", count);

    return new PageResponse<>(looks, count, paging);
  }

  @Transactional
  @Override
  public boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);

    boolean exists = lookSpi.existsLook(lookOrg, lookId);

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public boolean existsLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("existsLooks:lookQuery={}", lookQuery);

    boolean exists = lookSpi.existsLooks(lookQuery);

    log.debug("existsLooks:exists={}", exists);
    return exists;
  }

  @Transactional
  @Override
  public long countLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("countLooks:lookQuery={}", lookQuery);

    long count = lookSpi.countLooks(lookQuery);

    log.debug("countLooks:count={}", count);
    return count;
  }

  @Transactional
  @Override
  public long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  ) {
    log.debug("updateLook:lookOrg={}", lookOrg);
    log.debug("updateLook:lookId={}", lookId);
    log.debug("updateLook:lookUpdate={}", lookUpdate);

    long updateCount = lookSpi.updateLook(lookOrg, lookId, lookUpdate);

    log.debug("updateLook:updateCount={}", updateCount);
    return updateCount;
  }

  @Transactional
  @Override
  public long deleteLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("deleteLook:lookOrg={}", lookOrg);
    log.debug("deleteLook:lookId={}", lookId);

    long deleteCount = lookSpi.deleteLook(lookOrg, lookId);

    log.debug("deleteLook:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
