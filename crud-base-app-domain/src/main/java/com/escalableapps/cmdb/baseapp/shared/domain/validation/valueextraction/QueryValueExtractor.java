package com.escalableapps.cmdb.baseapp.shared.domain.validation.valueextraction;

import javax.validation.valueextraction.ExtractedValue;
import javax.validation.valueextraction.ValueExtractor;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.QueryValue;

public class QueryValueExtractor implements ValueExtractor<QueryValue<@ExtractedValue ?>> {

  @Override
  public void extractValues(QueryValue<@ExtractedValue ?> queryValue, ValueReceiver receiver) {
    Object object = queryValue.get();
    receiver.value("value", object);
  }
}
