package com.escalableapps.cmdb.baseapp.shared.domain.util;

import static java.time.ZoneOffset.UTC;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public final class DateUtils {

  public static OffsetDateTime convertToOffsetDateTime(LocalDateTime localDateTime) {
    if (localDateTime == null) {
      return null;
    }
    Instant instant = convertToInstant(localDateTime);
    OffsetDateTime offsetDateTime = OffsetDateTime.ofInstant(instant, UTC);
    return offsetDateTime;
  }

  public static Instant convertToInstant(LocalDateTime localDateTime) {
    if (localDateTime == null) {
      return null;
    }
    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
    Instant instant = zonedDateTime.toInstant();
    return instant;
  }

  public static LocalDateTime convertToLocalDateTime(OffsetDateTime offsetDateTime) {
    if (offsetDateTime == null) {
      return null;
    }
    Instant instant = offsetDateTime.toInstant();
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    return localDateTime;
  }

  private DateUtils() {
  }

}
