package com.escalableapps.cmdb.baseapp.template.domain.entity;

import static com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description for Zoo<br>
 * <strong>zoo</strong>.
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Zoo implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Description for zooOrg<br>
   * <strong>zoo_org bigint NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Long zooOrg;

  /**
   * Description for zooId<br>
   * <strong>zoo_id integer NOT NULL</strong>.
   */
  @EqualsAndHashCode.Include
  @Min(1)
  private Integer zooId;

  /**
   * Description for zooChar<br>
   * <strong>zoo_char character</strong>.
   */
  @Size(max = 2)
  private String zooChar;

  /**
   * Description for zooVarchar<br>
   * <strong>zoo_varchar character varying</strong>.
   */
  @Size(max = 4)
  private String zooVarchar;

  /**
   * Description for zooText<br>
   * <strong>zoo_text text</strong>.
   */
  @Size(max = TEXT)
  private String zooText;

  /**
   * Description for zooSmallint<br>
   * <strong>zoo_smallint smallint</strong>.
   */
  private Short zooSmallint;

  /**
   * Description for zooInteger<br>
   * <strong>zoo_integer integer</strong>.
   */
  private Integer zooInteger;

  /**
   * Description for zooBigint<br>
   * <strong>zoo_bigint bigint</strong>.
   */
  private Long zooBigint;

  /**
   * Description for zooReal<br>
   * <strong>zoo_real real</strong>.
   */
  private Float zooReal;

  /**
   * Description for zooDouble<br>
   * <strong>zoo_double double precision</strong>.
   */
  private Double zooDouble;

  /**
   * Description for zooDecimal<br>
   * <strong>zoo_decimal numeric</strong>.
   */
  @DecimalMax("99.9999")
  private BigDecimal zooDecimal;

  /**
   * Description for zooBoolean<br>
   * <strong>zoo_boolean boolean</strong>.
   */
  private Boolean zooBoolean;

  /**
   * Description for zooDate<br>
   * <strong>zoo_date date</strong>.
   */
  @DateRange(min = DATE_MIN, max = DATE_MAX)
  private LocalDate zooDate;

  /**
   * Description for zooTimestamp<br>
   * <strong>zoo_timestamp timestamp with time zone</strong>.
   */
  @DatetimeRange(min = TIMESTAMP_MIN, max = TIMESTAMP_MAX)
  private OffsetDateTime zooTimestamp;
}
