package com.escalableapps.cmdb.baseapp.shared.domain.model.vo;

import java.io.Serializable;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class PageResponse<T extends Serializable> implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final String RESULTS_MUST_ERROR_MESSAGE = "results: must not be null";
  private static final String TOTAL_ELEMENTS_ERROR_MESSAGE = "totalElements: must be greater than or equal to 0";
  private static final String PAGE_REQUEST_ERROR_MESSAGE = "pageRequest: must not be null";
  private static final String PAGE_NUMBER_ERROR_MESSAGE = "pageNumber: must be greater than or equal to 0";
  private static final String PAGE_SIZE_ERROR_MESSAGE = "pageSize: must be greater than or equal to 1";

  private List<T> results;

  private long totalElements;

  private int pageNumber;

  private int pageSize;

  private int totalPages;

  private boolean first;

  private boolean last;

  public PageResponse(List<T> results, long totalElements, PageRequest<?> pageRequest) {
    if (results == null) {
      throw new IllegalArgumentException(RESULTS_MUST_ERROR_MESSAGE);
    }
    if (totalElements < 0) {
      throw new IllegalArgumentException(TOTAL_ELEMENTS_ERROR_MESSAGE);
    }
    if (pageRequest == null) {
      throw new IllegalArgumentException(PAGE_REQUEST_ERROR_MESSAGE);
    }

    buildPageResponse(results, totalElements, pageRequest.getPageNumber(), pageRequest.getPageSize());
  }

  public PageResponse(List<T> results, long totalElements, int pageNumber, int pageSize) {
    if (results == null) {
      throw new IllegalArgumentException(RESULTS_MUST_ERROR_MESSAGE);
    }
    if (totalElements < 0) {
      throw new IllegalArgumentException(TOTAL_ELEMENTS_ERROR_MESSAGE);
    }
    if (pageNumber < 0) {
      throw new IllegalArgumentException(PAGE_NUMBER_ERROR_MESSAGE);
    }
    if (pageSize < 1) {
      throw new IllegalArgumentException(PAGE_SIZE_ERROR_MESSAGE);
    }

    buildPageResponse(results, totalElements, pageNumber, pageSize);
  }

  private void buildPageResponse(List<T> results, long totalElements, int pageNumber, int pageSize) {
    this.results = results;
    this.totalElements = totalElements;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;

    this.totalPages = totalElements == 0 ? 0 : (int) Math.ceil((double) totalElements / (double) pageSize);
    this.first = totalElements == 0 || pageNumber == 0;
    this.last = totalElements == 0 || pageNumber >= (totalPages - 1);
  }
}
