package com.escalableapps.cmdb.baseapp.template.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.escalableapps.cmdb.baseapp.template.application.service.*;
import com.escalableapps.cmdb.baseapp.template.port.api.*;
import com.escalableapps.cmdb.baseapp.template.port.spi.*;

@Configuration
public class CrudBaseAppTemplateDomainConfig {

  @Bean
  public BarApi barService(BarSpi barSpi) {
    return new BarService(barSpi);
  }

  @Bean
  public FooApi fooService(FooSpi fooSpi) {
    return new FooService(fooSpi);
  }

  @Bean
  public LookApi lookService(LookSpi lookSpi) {
    return new LookService(lookSpi);
  }

  @Bean
  public ZooApi zooService(ZooSpi zooSpi) {
    return new ZooService(zooSpi);
  }
}
