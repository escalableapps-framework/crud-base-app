package com.escalableapps.cmdb.baseapp.template.rest.helper;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.ZooRestProperties.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

@Component
public class ZooRestHelper {

  private final HashMap<String, ZooPropertyName> sortProperties;

  public ZooRestHelper() {
    sortProperties = new HashMap<>();
    sortProperties.put(ZOO_ORG, ZooPropertyName.ZOO_ORG);
    sortProperties.put(ZOO_ID, ZooPropertyName.ZOO_ID);
    sortProperties.put(ZOO_CHAR, ZooPropertyName.ZOO_CHAR);
    sortProperties.put(ZOO_VARCHAR, ZooPropertyName.ZOO_VARCHAR);
    sortProperties.put(ZOO_TEXT, ZooPropertyName.ZOO_TEXT);
    sortProperties.put(ZOO_SMALLINT, ZooPropertyName.ZOO_SMALLINT);
    sortProperties.put(ZOO_INTEGER, ZooPropertyName.ZOO_INTEGER);
    sortProperties.put(ZOO_BIGINT, ZooPropertyName.ZOO_BIGINT);
    sortProperties.put(ZOO_REAL, ZooPropertyName.ZOO_REAL);
    sortProperties.put(ZOO_DOUBLE, ZooPropertyName.ZOO_DOUBLE);
    sortProperties.put(ZOO_DECIMAL, ZooPropertyName.ZOO_DECIMAL);
    sortProperties.put(ZOO_BOOLEAN, ZooPropertyName.ZOO_BOOLEAN);
    sortProperties.put(ZOO_DATE, ZooPropertyName.ZOO_DATE);
    sortProperties.put(ZOO_TIMESTAMP, ZooPropertyName.ZOO_TIMESTAMP);
  }

  public URI buildZooLocation(Zoo zoo) {
    return fromCurrentRequest().path("/{id}").buildAndExpand(zoo.getZooId()).toUri();
  }

  public ZooQuery queryCriteria( //
      Long zooOrg, //
      Integer zooId, //
      String zooChar, //
      String zooVarchar, //
      String zooText, //
      Short zooSmallint, //
      Integer zooInteger, //
      Long zooBigint, //
      Float zooReal, //
      Double zooDouble, //
      BigDecimal zooDecimal, //
      Boolean zooBoolean, //
      LocalDate zooDate, LocalDate zooDateMin, LocalDate zooDateMax, //
      OffsetDateTime zooTimestamp, OffsetDateTime zooTimestampMin, OffsetDateTime zooTimestampMax, //
      HttpServletRequest request //
  ) {
    ZooQuery query = new ZooQuery();
    queryCriteriaForZooOrg(query, zooOrg, request);
    queryCriteriaForZooId(query, zooId, request);
    queryCriteriaForZooChar(query, zooChar, request);
    queryCriteriaForZooVarchar(query, zooVarchar, request);
    queryCriteriaForZooText(query, zooText, request);
    queryCriteriaForZooSmallint(query, zooSmallint, request);
    queryCriteriaForZooInteger(query, zooInteger, request);
    queryCriteriaForZooBigint(query, zooBigint, request);
    queryCriteriaForZooReal(query, zooReal, request);
    queryCriteriaForZooDouble(query, zooDouble, request);
    queryCriteriaForZooDecimal(query, zooDecimal, request);
    queryCriteriaForZooBoolean(query, zooBoolean, request);
    queryCriteriaForZooDate(query, zooDate, zooDateMin, zooDateMax, request);
    queryCriteriaForZooTimestamp(query, zooTimestamp, zooTimestampMin, zooTimestampMax, request);
    return query;
  }

  private void queryCriteriaForZooOrg(ZooQuery query, Long zooOrg, HttpServletRequest request) {
    query.setZooOrg(zooOrg);
  }

  private void queryCriteriaForZooId(ZooQuery query, Integer zooId, HttpServletRequest request) {
    if (request.getParameter(ZOO_ID) != null) {
      query.setZooId(zooId);
    }
  }

  private void queryCriteriaForZooChar(ZooQuery query, String zooChar, HttpServletRequest request) {
    if (request.getParameter(ZOO_CHAR) != null) {
      query.setZooChar(StringUtils.EMPTY.equals(zooChar) ? null : zooChar);
    }
  }

  private void queryCriteriaForZooVarchar(ZooQuery query, String zooVarchar, HttpServletRequest request) {
    if (request.getParameter(ZOO_VARCHAR) != null) {
      query.setZooVarchar(StringUtils.EMPTY.equals(zooVarchar) ? null : zooVarchar);
    }
  }

  private void queryCriteriaForZooText(ZooQuery query, String zooText, HttpServletRequest request) {
    if (request.getParameter(ZOO_TEXT) != null) {
      query.setZooText(StringUtils.EMPTY.equals(zooText) ? null : zooText);
    }
  }

  private void queryCriteriaForZooSmallint(ZooQuery query, Short zooSmallint, HttpServletRequest request) {
    if (request.getParameter(ZOO_SMALLINT) != null) {
      query.setZooSmallint(zooSmallint);
    }
  }

  private void queryCriteriaForZooInteger(ZooQuery query, Integer zooInteger, HttpServletRequest request) {
    if (request.getParameter(ZOO_INTEGER) != null) {
      query.setZooInteger(zooInteger);
    }
  }

  private void queryCriteriaForZooBigint(ZooQuery query, Long zooBigint, HttpServletRequest request) {
    if (request.getParameter(ZOO_BIGINT) != null) {
      query.setZooBigint(zooBigint);
    }
  }

  private void queryCriteriaForZooReal(ZooQuery query, Float zooReal, HttpServletRequest request) {
    if (request.getParameter(ZOO_REAL) != null) {
      query.setZooReal(zooReal);
    }
  }

  private void queryCriteriaForZooDouble(ZooQuery query, Double zooDouble, HttpServletRequest request) {
    if (request.getParameter(ZOO_DOUBLE) != null) {
      query.setZooDouble(zooDouble);
    }
  }

  private void queryCriteriaForZooDecimal(ZooQuery query, BigDecimal zooDecimal, HttpServletRequest request) {
    if (request.getParameter(ZOO_DECIMAL) != null) {
      query.setZooDecimal(zooDecimal);
    }
  }

  private void queryCriteriaForZooBoolean(ZooQuery query, Boolean zooBoolean, HttpServletRequest request) {
    if (request.getParameter(ZOO_BOOLEAN) != null) {
      query.setZooBoolean(zooBoolean);
    }
  }

  private void queryCriteriaForZooDate(ZooQuery query, LocalDate zooDate, LocalDate zooDateMin, LocalDate zooDateMax, HttpServletRequest request) {
    if (request.getParameter(ZOO_DATE) != null) {
      query.setZooDate(zooDate);
    }
    if (request.getParameter(ZOO_DATE_MIN) != null) {
      query.setZooDateMin(zooDateMin);
    }
    if (request.getParameter(ZOO_DATE_MAX) != null) {
      query.setZooDateMax(zooDateMax);
    }
  }

  private void queryCriteriaForZooTimestamp(ZooQuery query, OffsetDateTime zooTimestamp, OffsetDateTime zooTimestampMin, OffsetDateTime zooTimestampMax, HttpServletRequest request) {
    if (request.getParameter(ZOO_TIMESTAMP) != null) {
      query.setZooTimestamp(zooTimestamp);
    }
    if (request.getParameter(ZOO_TIMESTAMP_MIN) != null) {
      query.setZooTimestampMin(zooTimestampMin);
    }
    if (request.getParameter(ZOO_TIMESTAMP_MAX) != null) {
      query.setZooTimestampMax(zooTimestampMax);
    }
  }

  public ZooPropertyName sortBy(String sortProperty) {
    return sortProperties.get(sortProperty);
  }
}
