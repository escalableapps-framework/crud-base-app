package com.escalableapps.cmdb.baseapp.shared.rest.config;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import javax.validation.ConstraintViolationException;

import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.EaErrorAttributes;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.EaErrorProperties;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ExceptionHandlerUtils;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Order(1)
@Slf4j
public class RestControllerExceptionHandler extends BasicErrorController {

  public RestControllerExceptionHandler() {
    super(new EaErrorAttributes(), new EaErrorProperties());
  }

  @ExceptionHandler({ ConstraintViolationException.class })
  public ResponseEntity<ErrorMessage> handleConstraintViolationException(ConstraintViolationException e) {
    if (ExceptionHandlerUtils.isConstraintViolationFromController(e)) {
      String message = ExceptionHandlerUtils.handleConstraintViolationException(e);
      return new ResponseError(message, BAD_REQUEST);
    } else {
      log.error(e.getMessage(), e);
      String message = ExceptionHandlerUtils.handleException();
      return new ResponseError(message, INTERNAL_SERVER_ERROR);
    }
  }

  @ExceptionHandler({ HttpMessageNotReadableException.class })
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
    if (e.getCause() instanceof InvalidFormatException) {
      String message = ExceptionHandlerUtils.handleInvalidFormatException((InvalidFormatException) e.getCause());
      return new ResponseError(message, BAD_REQUEST);
    } else if (e.getCause() instanceof JsonParseException) {
      String message = ExceptionHandlerUtils.handleJsonParseException((JsonParseException) e.getCause());
      return new ResponseError(message, BAD_REQUEST);
    } else {
      String message = ExceptionHandlerUtils.handleHttpMessageNotReadableException(e);
      return new ResponseError(message, BAD_REQUEST);
    }
  }

  @ExceptionHandler({ MethodArgumentNotValidException.class })
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    String message = ExceptionHandlerUtils.handleMethodArgumentNotValidException(e);
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
    String message = ExceptionHandlerUtils.handleMethodArgumentTypeMismatchException(e);
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({ MissingServletRequestParameterException.class })
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
    String message = ExceptionHandlerUtils.handleMissingServletRequestParameterException(e);
    return new ResponseError(message, BAD_REQUEST);
  }
}
