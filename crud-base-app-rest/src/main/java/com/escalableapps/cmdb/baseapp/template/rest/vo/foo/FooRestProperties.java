package com.escalableapps.cmdb.baseapp.template.rest.vo.foo;

public final class FooRestProperties {
  public static final String FOO_ID = "foo-id";
  public static final String FOO_CHAR = "foo-char";
  public static final String FOO_VARCHAR = "foo-varchar";
  public static final String FOO_TEXT = "foo-text";
  public static final String FOO_SMALLINT = "foo-smallint";
  public static final String FOO_INTEGER = "foo-integer";
  public static final String FOO_BIGINT = "foo-bigint";
  public static final String FOO_REAL = "foo-real";
  public static final String FOO_DOUBLE = "foo-double";
  public static final String FOO_DECIMAL = "foo-decimal";
  public static final String FOO_BOOLEAN = "foo-boolean";
  public static final String FOO_DATE = "foo-date";
  public static final String FOO_DATE_MIN = "foo-dateMin";
  public static final String FOO_DATE_MAX = "foo-dateMax";
  public static final String FOO_TIMESTAMP = "foo-timestamp";
  public static final String FOO_TIMESTAMP_MIN = "foo-timestampMin";
  public static final String FOO_TIMESTAMP_MAX = "foo-timestampMax";

  private FooRestProperties() {
  }
}
