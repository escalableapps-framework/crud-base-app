package com.escalableapps.cmdb.baseapp.template.rest.vo.foo;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.foo.FooRestProperties.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "Description for Foo")
@Data
public class FooResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  public static FooResponse fromEntity(Foo foo) {
    if (foo == null) {
      return null;
    }
    FooResponse fooResponse = new FooResponse();
    fooResponse.setFooId(foo.getFooId());
    fooResponse.setFooChar(foo.getFooChar());
    fooResponse.setFooVarchar(foo.getFooVarchar());
    fooResponse.setFooText(foo.getFooText());
    fooResponse.setFooSmallint(foo.getFooSmallint());
    fooResponse.setFooInteger(foo.getFooInteger());
    fooResponse.setFooBigint(foo.getFooBigint());
    fooResponse.setFooReal(foo.getFooReal());
    fooResponse.setFooDouble(foo.getFooDouble());
    fooResponse.setFooDecimal(foo.getFooDecimal());
    fooResponse.setFooBoolean(foo.getFooBoolean());
    fooResponse.setFooDate(foo.getFooDate());
    fooResponse.setFooTimestamp(foo.getFooTimestamp());
    return fooResponse;
  }

  @JsonProperty(value = FOO_ID, index = 1, required = true)
  @Schema(description = "Description for fooId")
  private Integer fooId;

  @JsonProperty(value = FOO_CHAR, index = 2, required = false)
  @Schema(description = "Description for fooChar")
  private String fooChar;

  @JsonProperty(value = FOO_VARCHAR, index = 3, required = false)
  @Schema(description = "Description for fooVarchar")
  private String fooVarchar;

  @JsonProperty(value = FOO_TEXT, index = 4, required = false)
  @Schema(description = "Description for fooText")
  private String fooText;

  @JsonProperty(value = FOO_SMALLINT, index = 5, required = false)
  @Schema(description = "Description for fooSmallint")
  private Short fooSmallint;

  @JsonProperty(value = FOO_INTEGER, index = 6, required = false)
  @Schema(description = "Description for fooInteger")
  private Integer fooInteger;

  @JsonProperty(value = FOO_BIGINT, index = 7, required = false)
  @Schema(description = "Description for fooBigint")
  private Long fooBigint;

  @JsonProperty(value = FOO_REAL, index = 8, required = false)
  @Schema(description = "Description for fooReal")
  private Float fooReal;

  @JsonProperty(value = FOO_DOUBLE, index = 9, required = false)
  @Schema(description = "Description for fooDouble")
  private Double fooDouble;

  @JsonProperty(value = FOO_DECIMAL, index = 10, required = false)
  @Schema(description = "Description for fooDecimal")
  private BigDecimal fooDecimal;

  @JsonProperty(value = FOO_BOOLEAN, index = 11, required = false)
  @Schema(description = "Description for fooBoolean")
  private Boolean fooBoolean;

  @JsonProperty(value = FOO_DATE, index = 12, required = false)
  @Schema(description = "Description for fooDate")
  private LocalDate fooDate;

  @JsonProperty(value = FOO_TIMESTAMP, index = 13, required = false)
  @Schema(description = "Description for fooTimestamp")
  private OffsetDateTime fooTimestamp;
}
