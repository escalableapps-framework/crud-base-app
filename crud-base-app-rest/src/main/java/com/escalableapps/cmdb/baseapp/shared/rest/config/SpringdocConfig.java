package com.escalableapps.cmdb.baseapp.shared.rest.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class SpringdocConfig {

  @Bean
  public OpenAPI springShopOpenAPI( //
      @Value("${info.app.name}") String name, //
      @Value("${info.app.description}") String description, //
      @Value("${info.app.version}") String version //
  ) {
    return new OpenAPI().info(new Info().title(name) //
        .description(description) //
        .version(version) //
        .license(new License() //
            .name("GNU General Public License v3") //
            .url("https://www.gnu.org/licenses/gpl-3.0.html")) //
    );
  }
}
