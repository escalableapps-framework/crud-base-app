package com.escalableapps.cmdb.baseapp.template.rest.helper;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.look.LookRestProperties.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

@Component
public class LookRestHelper {

  private final HashMap<String, LookPropertyName> sortProperties;

  public LookRestHelper() {
    sortProperties = new HashMap<>();
    sortProperties.put(LOOK_ORG, LookPropertyName.LOOK_ORG);
    sortProperties.put(LOOK_ID, LookPropertyName.LOOK_ID);
    sortProperties.put(LOOK_CHAR, LookPropertyName.LOOK_CHAR);
    sortProperties.put(LOOK_VARCHAR, LookPropertyName.LOOK_VARCHAR);
    sortProperties.put(LOOK_TEXT, LookPropertyName.LOOK_TEXT);
    sortProperties.put(LOOK_SMALLINT, LookPropertyName.LOOK_SMALLINT);
    sortProperties.put(LOOK_INTEGER, LookPropertyName.LOOK_INTEGER);
    sortProperties.put(LOOK_BIGINT, LookPropertyName.LOOK_BIGINT);
    sortProperties.put(LOOK_REAL, LookPropertyName.LOOK_REAL);
    sortProperties.put(LOOK_DOUBLE, LookPropertyName.LOOK_DOUBLE);
    sortProperties.put(LOOK_DECIMAL, LookPropertyName.LOOK_DECIMAL);
    sortProperties.put(LOOK_BOOLEAN, LookPropertyName.LOOK_BOOLEAN);
    sortProperties.put(LOOK_DATE, LookPropertyName.LOOK_DATE);
    sortProperties.put(LOOK_TIMESTAMP, LookPropertyName.LOOK_TIMESTAMP);
  }

  public URI buildLookLocation(Look look) {
    return fromCurrentRequest().path("/{id}").buildAndExpand(look.getLookId()).toUri();
  }

  public LookQuery queryCriteria( //
      Long lookOrg, //
      Long lookId, //
      String lookChar, //
      String lookVarchar, //
      String lookText, //
      Short lookSmallint, //
      Integer lookInteger, //
      Long lookBigint, //
      Float lookReal, //
      Double lookDouble, //
      BigDecimal lookDecimal, //
      Boolean lookBoolean, //
      LocalDate lookDate, LocalDate lookDateMin, LocalDate lookDateMax, //
      OffsetDateTime lookTimestamp, OffsetDateTime lookTimestampMin, OffsetDateTime lookTimestampMax, //
      HttpServletRequest request //
  ) {
    LookQuery query = new LookQuery();
    queryCriteriaForLookOrg(query, lookOrg, request);
    queryCriteriaForLookId(query, lookId, request);
    queryCriteriaForLookChar(query, lookChar, request);
    queryCriteriaForLookVarchar(query, lookVarchar, request);
    queryCriteriaForLookText(query, lookText, request);
    queryCriteriaForLookSmallint(query, lookSmallint, request);
    queryCriteriaForLookInteger(query, lookInteger, request);
    queryCriteriaForLookBigint(query, lookBigint, request);
    queryCriteriaForLookReal(query, lookReal, request);
    queryCriteriaForLookDouble(query, lookDouble, request);
    queryCriteriaForLookDecimal(query, lookDecimal, request);
    queryCriteriaForLookBoolean(query, lookBoolean, request);
    queryCriteriaForLookDate(query, lookDate, lookDateMin, lookDateMax, request);
    queryCriteriaForLookTimestamp(query, lookTimestamp, lookTimestampMin, lookTimestampMax, request);
    return query;
  }

  private void queryCriteriaForLookOrg(LookQuery query, Long lookOrg, HttpServletRequest request) {
    query.setLookOrg(lookOrg);
  }

  private void queryCriteriaForLookId(LookQuery query, Long lookId, HttpServletRequest request) {
    if (request.getParameter(LOOK_ID) != null) {
      query.setLookId(lookId);
    }
  }

  private void queryCriteriaForLookChar(LookQuery query, String lookChar, HttpServletRequest request) {
    if (request.getParameter(LOOK_CHAR) != null) {
      query.setLookChar(StringUtils.EMPTY.equals(lookChar) ? null : lookChar);
    }
  }

  private void queryCriteriaForLookVarchar(LookQuery query, String lookVarchar, HttpServletRequest request) {
    if (request.getParameter(LOOK_VARCHAR) != null) {
      query.setLookVarchar(StringUtils.EMPTY.equals(lookVarchar) ? null : lookVarchar);
    }
  }

  private void queryCriteriaForLookText(LookQuery query, String lookText, HttpServletRequest request) {
    if (request.getParameter(LOOK_TEXT) != null) {
      query.setLookText(StringUtils.EMPTY.equals(lookText) ? null : lookText);
    }
  }

  private void queryCriteriaForLookSmallint(LookQuery query, Short lookSmallint, HttpServletRequest request) {
    if (request.getParameter(LOOK_SMALLINT) != null) {
      query.setLookSmallint(lookSmallint);
    }
  }

  private void queryCriteriaForLookInteger(LookQuery query, Integer lookInteger, HttpServletRequest request) {
    if (request.getParameter(LOOK_INTEGER) != null) {
      query.setLookInteger(lookInteger);
    }
  }

  private void queryCriteriaForLookBigint(LookQuery query, Long lookBigint, HttpServletRequest request) {
    if (request.getParameter(LOOK_BIGINT) != null) {
      query.setLookBigint(lookBigint);
    }
  }

  private void queryCriteriaForLookReal(LookQuery query, Float lookReal, HttpServletRequest request) {
    if (request.getParameter(LOOK_REAL) != null) {
      query.setLookReal(lookReal);
    }
  }

  private void queryCriteriaForLookDouble(LookQuery query, Double lookDouble, HttpServletRequest request) {
    if (request.getParameter(LOOK_DOUBLE) != null) {
      query.setLookDouble(lookDouble);
    }
  }

  private void queryCriteriaForLookDecimal(LookQuery query, BigDecimal lookDecimal, HttpServletRequest request) {
    if (request.getParameter(LOOK_DECIMAL) != null) {
      query.setLookDecimal(lookDecimal);
    }
  }

  private void queryCriteriaForLookBoolean(LookQuery query, Boolean lookBoolean, HttpServletRequest request) {
    if (request.getParameter(LOOK_BOOLEAN) != null) {
      query.setLookBoolean(lookBoolean);
    }
  }

  private void queryCriteriaForLookDate(LookQuery query, LocalDate lookDate, LocalDate lookDateMin, LocalDate lookDateMax, HttpServletRequest request) {
    if (request.getParameter(LOOK_DATE) != null) {
      query.setLookDate(lookDate);
    }
    if (request.getParameter(LOOK_DATE_MIN) != null) {
      query.setLookDateMin(lookDateMin);
    }
    if (request.getParameter(LOOK_DATE_MAX) != null) {
      query.setLookDateMax(lookDateMax);
    }
  }

  private void queryCriteriaForLookTimestamp(LookQuery query, OffsetDateTime lookTimestamp, OffsetDateTime lookTimestampMin, OffsetDateTime lookTimestampMax, HttpServletRequest request) {
    if (request.getParameter(LOOK_TIMESTAMP) != null) {
      query.setLookTimestamp(lookTimestamp);
    }
    if (request.getParameter(LOOK_TIMESTAMP_MIN) != null) {
      query.setLookTimestampMin(lookTimestampMin);
    }
    if (request.getParameter(LOOK_TIMESTAMP_MAX) != null) {
      query.setLookTimestampMax(lookTimestampMax);
    }
  }

  public LookPropertyName sortBy(String sortProperty) {
    return sortProperties.get(sortProperty);
  }
}
