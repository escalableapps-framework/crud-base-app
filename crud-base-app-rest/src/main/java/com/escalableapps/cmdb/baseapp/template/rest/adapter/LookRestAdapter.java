package com.escalableapps.cmdb.baseapp.template.rest.adapter;

import static com.escalableapps.cmdb.baseapp.shared.rest.model.MediaType.APPLICATION_JSON;
import static com.escalableapps.cmdb.baseapp.template.rest.vo.look.LookRestProperties.*;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.SortRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.OneOf;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Patch;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Post;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Put;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Look;
import com.escalableapps.cmdb.baseapp.template.domain.entity.LookPropertyName;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.LookApi;
import com.escalableapps.cmdb.baseapp.template.rest.helper.LookRestHelper;
import com.escalableapps.cmdb.baseapp.template.rest.vo.look.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Validated
public class LookRestAdapter {

  private static final String NOT_FOUND_BY_CRITERIA = "look not found";
  private static final String NOT_FOUND_BY_ID = "look: id=%s not found";

  private final LookRestHelper lookRestHelper;
  private final LookApi lookApi;

  public LookRestAdapter(LookRestHelper lookRestHelper, LookApi lookApi) {
    this.lookRestHelper = lookRestHelper;
    this.lookApi = lookApi;
  }

  @Operation(description = "Create a new Look entity")
  @ApiResponse(responseCode = "201", headers = @Header(name = "Location"))
  @PostMapping(path = "/lookOrg/{lookOrg}/look", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Post.class })
  public ResponseEntity<LookResponse> createLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @RequestBody(required = true) @NotNull @Valid LookRequest lookRequest //
  ) {
    log.debug("createLook:lookRequest={}", lookRequest);

    Look look = lookApi.createLook(lookRequest.toEntity(lookOrg));

    URI location = lookRestHelper.buildLookLocation(look);
    return ResponseEntity.created(location).body(LookResponse.fromEntity(look));
  }

  @Operation(description = "Check if the entity Look exists through its identifier")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/lookOrg/{lookOrg}/look/{lookId}", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @Parameter(description = "Description for lookId") //
      @PathVariable(name = "lookId", required = true) @NotNull @Min(1) Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);

    boolean exists = lookApi.existsLook(lookOrg, lookId);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, lookId);
    }
  }

  @Operation(description = "Check if there is any Look entity through search filters")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/lookOrg/{lookOrg}/look", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsLooks( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) //
      @NotNull //
      @Min(1) //
      Long lookOrg, //

      @Parameter(description = "Description for lookId") //
      @RequestParam(name = LOOK_ID, required = false) //
      Long lookId, //

      @Parameter(description = "Description for lookChar") //
      @RequestParam(name = LOOK_CHAR, required = false) //
      String lookChar, //

      @Parameter(description = "Description for lookVarchar") //
      @RequestParam(name = LOOK_VARCHAR, required = false) //
      String lookVarchar, //

      @Parameter(description = "Description for lookText") //
      @RequestParam(name = LOOK_TEXT, required = false) //
      String lookText, //

      @Parameter(description = "Description for lookSmallint") //
      @RequestParam(name = LOOK_SMALLINT, required = false) //
      Short lookSmallint, //

      @Parameter(description = "Description for lookInteger") //
      @RequestParam(name = LOOK_INTEGER, required = false) //
      Integer lookInteger, //

      @Parameter(description = "Description for lookBigint") //
      @RequestParam(name = LOOK_BIGINT, required = false) //
      Long lookBigint, //

      @Parameter(description = "Description for lookReal") //
      @RequestParam(name = LOOK_REAL, required = false) //
      Float lookReal, //

      @Parameter(description = "Description for lookDouble") //
      @RequestParam(name = LOOK_DOUBLE, required = false) //
      Double lookDouble, //

      @Parameter(description = "Description for lookDecimal") //
      @RequestParam(name = LOOK_DECIMAL, required = false) //
      BigDecimal lookDecimal, //

      @Parameter(description = "Description for lookBoolean") //
      @RequestParam(name = LOOK_BOOLEAN, required = false) //
      Boolean lookBoolean, //

      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDate, //
      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDateMin, //
      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDateMax, //

      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestamp, //
      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestampMin, //
      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestampMax, //
      @Parameter(hidden = true) HttpServletRequest request //
  ) {
    LookQuery lookQuery = lookRestHelper.queryCriteria( //
        lookOrg, //
        lookId, //
        lookChar, //
        lookVarchar, //
        lookText, //
        lookSmallint, //
        lookInteger, //
        lookBigint, //
        lookReal, //
        lookDouble, //
        lookDecimal, //
        lookBoolean, //
        lookDate, lookDateMin, lookDateMax, //
        lookTimestamp, lookTimestampMin, lookTimestampMax, //
        request //
    );
    log.debug("existsLooks:lookQuery={}", lookQuery);

    boolean exists = lookApi.existsLooks(lookQuery);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_CRITERIA);
    }
  }

  @Operation(description = "Gets the Look entity by its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @GetMapping(path = "/lookOrg/{lookOrg}/look/{lookId}", produces = APPLICATION_JSON)
  public ResponseEntity<LookResponse> findLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @Parameter(description = "Description for lookId") //
      @PathVariable(name = "lookId", required = true) @NotNull @Min(1) Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);

    Optional<Look> look = lookApi.findLook(lookOrg, lookId);

    if (look.isPresent()) {
      return ResponseEntity.ok(LookResponse.fromEntity(look.get()));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, lookId);
    }
  }

  @Operation(description = "Get Look entities through search filters")
  @ApiResponse(responseCode = "200")
  @GetMapping(path = "/lookOrg/{lookOrg}/look", produces = APPLICATION_JSON)
  public ResponseEntity<PageResponse<LookResponse>> findLooks( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) //
      @NotNull //
      @Min(1) //
      Long lookOrg, //

      @Parameter(description = "Description for lookId") //
      @RequestParam(name = LOOK_ID, required = false) //
      Long lookId, //

      @Parameter(description = "Description for lookChar") //
      @RequestParam(name = LOOK_CHAR, required = false) //
      String lookChar, //

      @Parameter(description = "Description for lookVarchar") //
      @RequestParam(name = LOOK_VARCHAR, required = false) //
      String lookVarchar, //

      @Parameter(description = "Description for lookText") //
      @RequestParam(name = LOOK_TEXT, required = false) //
      String lookText, //

      @Parameter(description = "Description for lookSmallint") //
      @RequestParam(name = LOOK_SMALLINT, required = false) //
      Short lookSmallint, //

      @Parameter(description = "Description for lookInteger") //
      @RequestParam(name = LOOK_INTEGER, required = false) //
      Integer lookInteger, //

      @Parameter(description = "Description for lookBigint") //
      @RequestParam(name = LOOK_BIGINT, required = false) //
      Long lookBigint, //

      @Parameter(description = "Description for lookReal") //
      @RequestParam(name = LOOK_REAL, required = false) //
      Float lookReal, //

      @Parameter(description = "Description for lookDouble") //
      @RequestParam(name = LOOK_DOUBLE, required = false) //
      Double lookDouble, //

      @Parameter(description = "Description for lookDecimal") //
      @RequestParam(name = LOOK_DECIMAL, required = false) //
      BigDecimal lookDecimal, //

      @Parameter(description = "Description for lookBoolean") //
      @RequestParam(name = LOOK_BOOLEAN, required = false) //
      Boolean lookBoolean, //

      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDate, //
      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDateMin, //
      @Parameter(description = "Description for lookDate") //
      @RequestParam(name = LOOK_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate lookDateMax, //

      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestamp, //
      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestampMin, //
      @Parameter(description = "Description for lookTimestamp") //
      @RequestParam(name = LOOK_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime lookTimestampMax, //

      @Parameter(description = "Zero-based page index (0..n)") //
      @RequestParam(name = "pageNumber", required = false, defaultValue = "0") //
      @Min(0) //
      Integer pageNumber, //

      @Parameter(description = "The size of the page to be returned") //
      @RequestParam(name = "pageSize", required = false, defaultValue = "10") //
      @Min(1) //
      Integer pageSize, //

      @Parameter(description = "Property for ordering") //
      @RequestParam(name = "sortProperty", required = false) //
      @OneOf({ LOOK_ORG, LOOK_ID, LOOK_CHAR, LOOK_VARCHAR, LOOK_TEXT, LOOK_SMALLINT, LOOK_INTEGER, LOOK_BIGINT, LOOK_REAL, LOOK_DOUBLE, LOOK_DECIMAL, LOOK_BOOLEAN, LOOK_DATE, LOOK_TIMESTAMP }) //
      String sortProperty, //

      @Parameter(description = "Ascending (true) or descending (false) sort") //
      @RequestParam(name = "sortAscendant", required = false, defaultValue = "true") //
      Boolean sortAscendant, //

      @Parameter(hidden = true) //
      HttpServletRequest request //
  ) {
    LookQuery lookQuery = lookRestHelper.queryCriteria( //
        lookOrg, //
        lookId, //
        lookChar, //
        lookVarchar, //
        lookText, //
        lookSmallint, //
        lookInteger, //
        lookBigint, //
        lookReal, //
        lookDouble, //
        lookDecimal, //
        lookBoolean, //
        lookDate, lookDateMin, lookDateMax, //
        lookTimestamp, lookTimestampMin, lookTimestampMax, //
        request //
    );
    PageRequest<LookPropertyName> pagging;
    if (sortProperty != null) {
      pagging = new PageRequest<>( //
          pageNumber, //
          pageSize, //
          new SortRequest<>(lookRestHelper.sortBy(sortProperty), sortAscendant) //
      );
    } else {
      pagging = new PageRequest<>(pageNumber, pageSize);
    }
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:pagging={}", pagging);

    PageResponse<Look> looks = lookApi.findLooks(lookQuery, pagging);

    PageResponse<LookResponse> lookPageResponse = new PageResponse<>( //
        looks.getResults().stream().map(LookResponse::fromEntity).collect(Collectors.toList()), //
        looks.getTotalElements(), //
        pagging //
    );
    return ResponseEntity.ok(lookPageResponse);
  }

  @Operation(description = "Partially update a Look entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PatchMapping(path = "/lookOrg/{lookOrg}/look/{lookId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Patch.class })
  public ResponseEntity<LookResponse> updateLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @Parameter(description = "Description for lookId") //
      @PathVariable(name = "lookId", required = true) @NotNull @Min(1) Long lookId, //
      @RequestBody(required = true) @NotNull @Valid LookRequest lookRequest //
  ) {
    return update(lookOrg, lookId, lookRequest, false);
  }

  @Operation(description = "Fully update a Look entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PutMapping(path = "/lookOrg/{lookOrg}/look/{lookId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Put.class })
  public ResponseEntity<LookResponse> fullUpdateLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @Parameter(description = "Description for lookId") //
      @PathVariable(name = "lookId", required = true) @NotNull @Min(1) Long lookId, //
      @RequestBody(required = true) @NotNull @Valid LookRequest lookRequest //
  ) {
    return update(lookOrg, lookId, lookRequest, true);
  }

  private ResponseEntity<LookResponse> update( //
      Long lookOrg, //
      Long lookId, //
      LookRequest lookRequest, //
      boolean fullUpdate //
  ) {
    log.debug("update:lookOrg={}", lookOrg);
    log.debug("update:lookId={}", lookId);
    log.debug("update:lookRequest={}", lookRequest);

    long updateCount = lookApi.updateLook(lookOrg, lookId, lookRequest.toUpdate(fullUpdate));

    if (updateCount > 0) {
      Look look = lookApi.findLook(lookOrg, lookId).get();
      return ResponseEntity.ok(LookResponse.fromEntity(look));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, lookId);
    }
  }

  @Operation(description = "Delete a Look entity using its identifier")
  @ApiResponse(responseCode = "204")
  @DeleteMapping(path = "/lookOrg/{lookOrg}/look/{lookId}", produces = APPLICATION_JSON)
  public ResponseEntity<Void> deleteLook( //
      @Parameter(description = "Description for lookOrg") //
      @PathVariable(name = "lookOrg", required = true) @NotNull @Min(1) Long lookOrg, //
      @Parameter(description = "Description for lookId") //
      @PathVariable(name = "lookId", required = true) @NotNull @Min(1) Long lookId //
  ) {
    log.debug("deleteLook:lookOrg={}", lookOrg);
    log.debug("deleteLook:lookId={}", lookId);

    lookApi.deleteLook(lookOrg, lookId);

    return ResponseEntity.noContent().build();
  }
}
