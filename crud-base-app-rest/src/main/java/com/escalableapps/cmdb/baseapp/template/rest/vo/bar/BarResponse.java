package com.escalableapps.cmdb.baseapp.template.rest.vo.bar;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.bar.BarRestProperties.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "Description for Bar")
@Data
public class BarResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  public static BarResponse fromEntity(Bar bar) {
    if (bar == null) {
      return null;
    }
    BarResponse barResponse = new BarResponse();
    barResponse.setBarId(bar.getBarId());
    barResponse.setBarChar(bar.getBarChar());
    barResponse.setBarVarchar(bar.getBarVarchar());
    barResponse.setBarText(bar.getBarText());
    barResponse.setBarSmallint(bar.getBarSmallint());
    barResponse.setBarInteger(bar.getBarInteger());
    barResponse.setBarBigint(bar.getBarBigint());
    barResponse.setBarReal(bar.getBarReal());
    barResponse.setBarDouble(bar.getBarDouble());
    barResponse.setBarDecimal(bar.getBarDecimal());
    barResponse.setBarBoolean(bar.getBarBoolean());
    barResponse.setBarDate(bar.getBarDate());
    barResponse.setBarTimestamp(bar.getBarTimestamp());
    return barResponse;
  }

  @JsonProperty(value = BAR_ID, index = 1, required = true)
  @Schema(description = "Description for barId")
  private Integer barId;

  @JsonProperty(value = BAR_CHAR, index = 2, required = true)
  @Schema(description = "Description for barChar")
  private String barChar;

  @JsonProperty(value = BAR_VARCHAR, index = 3, required = true)
  @Schema(description = "Description for barVarchar")
  private String barVarchar;

  @JsonProperty(value = BAR_TEXT, index = 4, required = true)
  @Schema(description = "Description for barText")
  private String barText;

  @JsonProperty(value = BAR_SMALLINT, index = 5, required = true)
  @Schema(description = "Description for barSmallint")
  private Short barSmallint;

  @JsonProperty(value = BAR_INTEGER, index = 6, required = true)
  @Schema(description = "Description for barInteger")
  private Integer barInteger;

  @JsonProperty(value = BAR_BIGINT, index = 7, required = true)
  @Schema(description = "Description for barBigint")
  private Long barBigint;

  @JsonProperty(value = BAR_REAL, index = 8, required = true)
  @Schema(description = "Description for barReal")
  private Float barReal;

  @JsonProperty(value = BAR_DOUBLE, index = 9, required = true)
  @Schema(description = "Description for barDouble")
  private Double barDouble;

  @JsonProperty(value = BAR_DECIMAL, index = 10, required = true)
  @Schema(description = "Description for barDecimal")
  private BigDecimal barDecimal;

  @JsonProperty(value = BAR_BOOLEAN, index = 11, required = true)
  @Schema(description = "Description for barBoolean")
  private Boolean barBoolean;

  @JsonProperty(value = BAR_DATE, index = 12, required = true)
  @Schema(description = "Description for barDate")
  private LocalDate barDate;

  @JsonProperty(value = BAR_TIMESTAMP, index = 13, required = true)
  @Schema(description = "Description for barTimestamp")
  private OffsetDateTime barTimestamp;
}
