package com.escalableapps.cmdb.baseapp.shared.rest.config;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.ConflictException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ExceptionHandlerUtils;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Order(2)
@Slf4j
public class ComponentExceptionHandler {

  @ExceptionHandler({ ConflictException.class })
  public ResponseEntity<ErrorMessage> handleConflictException(ConflictException e) {
    String message = ExceptionHandlerUtils.handleConflictException(e);
    return new ResponseError(message, CONFLICT);
  }

  @ExceptionHandler({ NotFoundException.class })
  public ResponseEntity<ErrorMessage> handleNotFoundException(NotFoundException e) {
    String message = ExceptionHandlerUtils.handleNotFoundException(e);
    return new ResponseError(message, NOT_FOUND);
  }

  @ExceptionHandler({ Exception.class })
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  public ResponseEntity<ErrorMessage> handleException(Exception e) {
    log.error(e.getMessage(), e);
    String message = ExceptionHandlerUtils.handleException();
    return new ResponseError(message, INTERNAL_SERVER_ERROR);
  }
}
