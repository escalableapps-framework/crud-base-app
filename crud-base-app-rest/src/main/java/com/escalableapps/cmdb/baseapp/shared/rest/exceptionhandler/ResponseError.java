package com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler;

import static java.time.OffsetDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;

import lombok.Data;

public class ResponseError extends ResponseEntity<ErrorMessage> {

  @Data
  public static class ErrorMessage {

    private String timestamp;
    private Integer status;
    private String error;
    private String message;

    public ErrorMessage(String message, HttpStatus status) {
      this.timestamp = now(UTC).format(ISO_INSTANT);
      this.status = status.value();
      this.error = status.getReasonPhrase();
      this.message = message;
    }
  }

  public ResponseError(ErrorMessage body, HttpStatus status) {
    super(body, status);
  }

  public ResponseError(String message, HttpStatus status) {
    this(new ErrorMessage(message, status), status);
  }
}
