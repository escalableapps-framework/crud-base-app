package com.escalableapps.cmdb.baseapp.shared.rest.model;

public final class MediaType {
  public static final String APPLICATION_JSON = "application/json;charset=UTF-8";
  public static final String APPLICATION_PROBLEM_JSON = "application/problem+json;charset=UTF-8";

  private MediaType() {
  }
}
