package com.escalableapps.cmdb.baseapp.template.rest.vo.look;

public final class LookRestProperties {
  public static final String LOOK_ORG = "look-org";
  public static final String LOOK_ID = "look-id";
  public static final String LOOK_CHAR = "look-char";
  public static final String LOOK_VARCHAR = "look-varchar";
  public static final String LOOK_TEXT = "look-text";
  public static final String LOOK_SMALLINT = "look-smallint";
  public static final String LOOK_INTEGER = "look-integer";
  public static final String LOOK_BIGINT = "look-bigint";
  public static final String LOOK_REAL = "look-real";
  public static final String LOOK_DOUBLE = "look-double";
  public static final String LOOK_DECIMAL = "look-decimal";
  public static final String LOOK_BOOLEAN = "look-boolean";
  public static final String LOOK_DATE = "look-date";
  public static final String LOOK_DATE_MIN = "look-dateMin";
  public static final String LOOK_DATE_MAX = "look-dateMax";
  public static final String LOOK_TIMESTAMP = "look-timestamp";
  public static final String LOOK_TIMESTAMP_MIN = "look-timestampMin";
  public static final String LOOK_TIMESTAMP_MAX = "look-timestampMax";

  private LookRestProperties() {
  }
}
