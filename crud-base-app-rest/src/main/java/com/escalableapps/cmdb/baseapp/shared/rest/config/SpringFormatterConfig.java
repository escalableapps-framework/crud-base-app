package com.escalableapps.cmdb.baseapp.shared.rest.config;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.escalableapps.cmdb.baseapp.shared.domain.util.DateUtils;

@Configuration
public class SpringFormatterConfig implements WebMvcConfigurer {

  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(new Converter<String, LocalDateTime>() {

      @Override
      public LocalDateTime convert(String iso) {
        if (StringUtils.isEmpty(iso)) {
          return null;
        }
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(iso);
        return DateUtils.convertToLocalDateTime(offsetDateTime);
      }
    });
  }
}
