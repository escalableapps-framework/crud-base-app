package com.escalableapps.cmdb.baseapp.template.rest.vo.foo;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.foo.FooRestProperties.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Schema(description = "Description for Foo")
@ToString
@EqualsAndHashCode
public class FooRequest {

  @AssertTrue(groups = Patch.class, message = "No property has been set for update")
  @JsonIgnore
  private boolean hasPatchProperty;

  @Size(max = 2)
  @JsonProperty(value = FOO_CHAR, index = 1, required = false)
  @Schema(description = "Description for fooChar")
  private String fooChar;

  @JsonIgnore
  private boolean fooCharNull;

  @Size(max = 4)
  @JsonProperty(value = FOO_VARCHAR, index = 2, required = false)
  @Schema(description = "Description for fooVarchar")
  private String fooVarchar;

  @JsonIgnore
  private boolean fooVarcharNull;

  @Size(max = Limits.TEXT)
  @JsonProperty(value = FOO_TEXT, index = 3, required = false)
  @Schema(description = "Description for fooText")
  private String fooText;

  @JsonIgnore
  private boolean fooTextNull;

  @JsonProperty(value = FOO_SMALLINT, index = 4, required = false)
  @Schema(description = "Description for fooSmallint")
  private Short fooSmallint;

  @JsonIgnore
  private boolean fooSmallintNull;

  @JsonProperty(value = FOO_INTEGER, index = 5, required = false)
  @Schema(description = "Description for fooInteger")
  private Integer fooInteger;

  @JsonIgnore
  private boolean fooIntegerNull;

  @JsonProperty(value = FOO_BIGINT, index = 6, required = false)
  @Schema(description = "Description for fooBigint")
  private Long fooBigint;

  @JsonIgnore
  private boolean fooBigintNull;

  @JsonProperty(value = FOO_REAL, index = 7, required = false)
  @Schema(description = "Description for fooReal")
  private Float fooReal;

  @JsonIgnore
  private boolean fooRealNull;

  @JsonProperty(value = FOO_DOUBLE, index = 8, required = false)
  @Schema(description = "Description for fooDouble")
  private Double fooDouble;

  @JsonIgnore
  private boolean fooDoubleNull;

  @DecimalMax("99.9999")
  @JsonProperty(value = FOO_DECIMAL, index = 9, required = false)
  @Schema(description = "Description for fooDecimal")
  private BigDecimal fooDecimal;

  @JsonIgnore
  private boolean fooDecimalNull;

  @JsonProperty(value = FOO_BOOLEAN, index = 10, required = false)
  @Schema(description = "Description for fooBoolean")
  private Boolean fooBoolean;

  @JsonIgnore
  private boolean fooBooleanNull;

  @DateRange(min = Limits.DATE_MIN, max = Limits.DATE_MAX)
  @JsonProperty(value = FOO_DATE, index = 11, required = false)
  @Schema(description = "Description for fooDate")
  private LocalDate fooDate;

  @JsonIgnore
  private boolean fooDateNull;

  @DatetimeRange(min = Limits.TIMESTAMP_MIN, max = Limits.TIMESTAMP_MAX)
  @JsonProperty(value = FOO_TIMESTAMP, index = 12, required = false)
  @Schema(description = "Description for fooTimestamp")
  private OffsetDateTime fooTimestamp;

  @JsonIgnore
  private boolean fooTimestampNull;

  public Foo toEntity() {
    Foo foo = new Foo();
    foo.setFooChar(fooChar);
    foo.setFooVarchar(fooVarchar);
    foo.setFooText(fooText);
    foo.setFooSmallint(fooSmallint);
    foo.setFooInteger(fooInteger);
    foo.setFooBigint(fooBigint);
    foo.setFooReal(fooReal);
    foo.setFooDouble(fooDouble);
    foo.setFooDecimal(fooDecimal);
    foo.setFooBoolean(fooBoolean);
    foo.setFooDate(fooDate);
    foo.setFooTimestamp(fooTimestamp);
    return foo;
  }

  public FooUpdate toUpdate(boolean fullUpdate) {
    FooUpdate fooUpdate = new FooUpdate();
    toUpdateFooChar(fooUpdate, fullUpdate);
    toUpdateFooVarchar(fooUpdate, fullUpdate);
    toUpdateFooText(fooUpdate, fullUpdate);
    toUpdateFooSmallint(fooUpdate, fullUpdate);
    toUpdateFooInteger(fooUpdate, fullUpdate);
    toUpdateFooBigint(fooUpdate, fullUpdate);
    toUpdateFooReal(fooUpdate, fullUpdate);
    toUpdateFooDouble(fooUpdate, fullUpdate);
    toUpdateFooDecimal(fooUpdate, fullUpdate);
    toUpdateFooBoolean(fooUpdate, fullUpdate);
    toUpdateFooDate(fooUpdate, fullUpdate);
    toUpdateFooTimestamp(fooUpdate, fullUpdate);
    return fooUpdate;
  }

  private void toUpdateFooChar(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooChar != null || fooCharNull) {
      fooUpdate.setFooChar(fooChar);
    }
  }

  private void toUpdateFooVarchar(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooVarchar != null || fooVarcharNull) {
      fooUpdate.setFooVarchar(fooVarchar);
    }
  }

  private void toUpdateFooText(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooText != null || fooTextNull) {
      fooUpdate.setFooText(fooText);
    }
  }

  private void toUpdateFooSmallint(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooSmallint != null || fooSmallintNull) {
      fooUpdate.setFooSmallint(fooSmallint);
    }
  }

  private void toUpdateFooInteger(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooInteger != null || fooIntegerNull) {
      fooUpdate.setFooInteger(fooInteger);
    }
  }

  private void toUpdateFooBigint(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooBigint != null || fooBigintNull) {
      fooUpdate.setFooBigint(fooBigint);
    }
  }

  private void toUpdateFooReal(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooReal != null || fooRealNull) {
      fooUpdate.setFooReal(fooReal);
    }
  }

  private void toUpdateFooDouble(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooDouble != null || fooDoubleNull) {
      fooUpdate.setFooDouble(fooDouble);
    }
  }

  private void toUpdateFooDecimal(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooDecimal != null || fooDecimalNull) {
      fooUpdate.setFooDecimal(fooDecimal);
    }
  }

  private void toUpdateFooBoolean(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooBoolean != null || fooBooleanNull) {
      fooUpdate.setFooBoolean(fooBoolean);
    }
  }

  private void toUpdateFooDate(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooDate != null || fooDateNull) {
      fooUpdate.setFooDate(fooDate);
    }
  }

  private void toUpdateFooTimestamp(FooUpdate fooUpdate, boolean fullUpdate) {
    if (fullUpdate || fooTimestamp != null || fooTimestampNull) {
      fooUpdate.setFooTimestamp(fooTimestamp);
    }
  }

  public void setFooChar(String fooChar) {
    this.fooChar = fooChar;
    if (fooChar == null) {
      fooCharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooVarchar(String fooVarchar) {
    this.fooVarchar = fooVarchar;
    if (fooVarchar == null) {
      fooVarcharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooText(String fooText) {
    this.fooText = fooText;
    if (fooText == null) {
      fooTextNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooSmallint(Short fooSmallint) {
    this.fooSmallint = fooSmallint;
    if (fooSmallint == null) {
      fooSmallintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooInteger(Integer fooInteger) {
    this.fooInteger = fooInteger;
    if (fooInteger == null) {
      fooIntegerNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooBigint(Long fooBigint) {
    this.fooBigint = fooBigint;
    if (fooBigint == null) {
      fooBigintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooReal(Float fooReal) {
    this.fooReal = fooReal;
    if (fooReal == null) {
      fooRealNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooDouble(Double fooDouble) {
    this.fooDouble = fooDouble;
    if (fooDouble == null) {
      fooDoubleNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooDecimal(BigDecimal fooDecimal) {
    this.fooDecimal = fooDecimal;
    if (fooDecimal == null) {
      fooDecimalNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooBoolean(Boolean fooBoolean) {
    this.fooBoolean = fooBoolean;
    if (fooBoolean == null) {
      fooBooleanNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooDate(LocalDate fooDate) {
    this.fooDate = fooDate;
    if (fooDate == null) {
      fooDateNull = true;
    }
    hasPatchProperty = true;
  }

  public void setFooTimestamp(OffsetDateTime fooTimestamp) {
    this.fooTimestamp = fooTimestamp;
    if (fooTimestamp == null) {
      fooTimestampNull = true;
    }
    hasPatchProperty = true;
  }

}
