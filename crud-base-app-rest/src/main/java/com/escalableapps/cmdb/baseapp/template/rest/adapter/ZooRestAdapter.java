package com.escalableapps.cmdb.baseapp.template.rest.adapter;

import static com.escalableapps.cmdb.baseapp.shared.rest.model.MediaType.APPLICATION_JSON;
import static com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.ZooRestProperties.*;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.SortRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.OneOf;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Patch;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Post;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Put;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Zoo;
import com.escalableapps.cmdb.baseapp.template.domain.entity.ZooPropertyName;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.ZooApi;
import com.escalableapps.cmdb.baseapp.template.rest.helper.ZooRestHelper;
import com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Validated
public class ZooRestAdapter {

  private static final String NOT_FOUND_BY_CRITERIA = "zoo not found";
  private static final String NOT_FOUND_BY_ID = "zoo: id=%s not found";

  private final ZooRestHelper zooRestHelper;
  private final ZooApi zooApi;

  public ZooRestAdapter(ZooRestHelper zooRestHelper, ZooApi zooApi) {
    this.zooRestHelper = zooRestHelper;
    this.zooApi = zooApi;
  }

  @Operation(description = "Create a new Zoo entity")
  @ApiResponse(responseCode = "201", headers = @Header(name = "Location"))
  @PostMapping(path = "/zooOrg/{zooOrg}/zoo", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Post.class })
  public ResponseEntity<ZooResponse> createZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @RequestBody(required = true) @NotNull @Valid ZooRequest zooRequest //
  ) {
    log.debug("createZoo:zooRequest={}", zooRequest);

    Zoo zoo = zooApi.createZoo(zooRequest.toEntity(zooOrg));

    URI location = zooRestHelper.buildZooLocation(zoo);
    return ResponseEntity.created(location).body(ZooResponse.fromEntity(zoo));
  }

  @Operation(description = "Check if the entity Zoo exists through its identifier")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/zooOrg/{zooOrg}/zoo/{zooId}", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @Parameter(description = "Description for zooId") //
      @PathVariable(name = "zooId", required = true) @NotNull @Min(1) Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = zooApi.existsZoo(zooOrg, zooId);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, zooId);
    }
  }

  @Operation(description = "Check if there is any Zoo entity through search filters")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/zooOrg/{zooOrg}/zoo", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsZoos( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) //
      @NotNull //
      @Min(1) //
      Long zooOrg, //

      @Parameter(description = "Description for zooId") //
      @RequestParam(name = ZOO_ID, required = false) //
      Integer zooId, //

      @Parameter(description = "Description for zooChar") //
      @RequestParam(name = ZOO_CHAR, required = false) //
      String zooChar, //

      @Parameter(description = "Description for zooVarchar") //
      @RequestParam(name = ZOO_VARCHAR, required = false) //
      String zooVarchar, //

      @Parameter(description = "Description for zooText") //
      @RequestParam(name = ZOO_TEXT, required = false) //
      String zooText, //

      @Parameter(description = "Description for zooSmallint") //
      @RequestParam(name = ZOO_SMALLINT, required = false) //
      Short zooSmallint, //

      @Parameter(description = "Description for zooInteger") //
      @RequestParam(name = ZOO_INTEGER, required = false) //
      Integer zooInteger, //

      @Parameter(description = "Description for zooBigint") //
      @RequestParam(name = ZOO_BIGINT, required = false) //
      Long zooBigint, //

      @Parameter(description = "Description for zooReal") //
      @RequestParam(name = ZOO_REAL, required = false) //
      Float zooReal, //

      @Parameter(description = "Description for zooDouble") //
      @RequestParam(name = ZOO_DOUBLE, required = false) //
      Double zooDouble, //

      @Parameter(description = "Description for zooDecimal") //
      @RequestParam(name = ZOO_DECIMAL, required = false) //
      BigDecimal zooDecimal, //

      @Parameter(description = "Description for zooBoolean") //
      @RequestParam(name = ZOO_BOOLEAN, required = false) //
      Boolean zooBoolean, //

      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDate, //
      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDateMin, //
      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDateMax, //

      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestamp, //
      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestampMin, //
      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestampMax, //
      @Parameter(hidden = true) HttpServletRequest request //
  ) {
    ZooQuery zooQuery = zooRestHelper.queryCriteria( //
        zooOrg, //
        zooId, //
        zooChar, //
        zooVarchar, //
        zooText, //
        zooSmallint, //
        zooInteger, //
        zooBigint, //
        zooReal, //
        zooDouble, //
        zooDecimal, //
        zooBoolean, //
        zooDate, zooDateMin, zooDateMax, //
        zooTimestamp, zooTimestampMin, zooTimestampMax, //
        request //
    );
    log.debug("existsZoos:zooQuery={}", zooQuery);

    boolean exists = zooApi.existsZoos(zooQuery);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_CRITERIA);
    }
  }

  @Operation(description = "Gets the Zoo entity by its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @GetMapping(path = "/zooOrg/{zooOrg}/zoo/{zooId}", produces = APPLICATION_JSON)
  public ResponseEntity<ZooResponse> findZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @Parameter(description = "Description for zooId") //
      @PathVariable(name = "zooId", required = true) @NotNull @Min(1) Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    Optional<Zoo> zoo = zooApi.findZoo(zooOrg, zooId);

    if (zoo.isPresent()) {
      return ResponseEntity.ok(ZooResponse.fromEntity(zoo.get()));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, zooId);
    }
  }

  @Operation(description = "Get Zoo entities through search filters")
  @ApiResponse(responseCode = "200")
  @GetMapping(path = "/zooOrg/{zooOrg}/zoo", produces = APPLICATION_JSON)
  public ResponseEntity<PageResponse<ZooResponse>> findZoos( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) //
      @NotNull //
      @Min(1) //
      Long zooOrg, //

      @Parameter(description = "Description for zooId") //
      @RequestParam(name = ZOO_ID, required = false) //
      Integer zooId, //

      @Parameter(description = "Description for zooChar") //
      @RequestParam(name = ZOO_CHAR, required = false) //
      String zooChar, //

      @Parameter(description = "Description for zooVarchar") //
      @RequestParam(name = ZOO_VARCHAR, required = false) //
      String zooVarchar, //

      @Parameter(description = "Description for zooText") //
      @RequestParam(name = ZOO_TEXT, required = false) //
      String zooText, //

      @Parameter(description = "Description for zooSmallint") //
      @RequestParam(name = ZOO_SMALLINT, required = false) //
      Short zooSmallint, //

      @Parameter(description = "Description for zooInteger") //
      @RequestParam(name = ZOO_INTEGER, required = false) //
      Integer zooInteger, //

      @Parameter(description = "Description for zooBigint") //
      @RequestParam(name = ZOO_BIGINT, required = false) //
      Long zooBigint, //

      @Parameter(description = "Description for zooReal") //
      @RequestParam(name = ZOO_REAL, required = false) //
      Float zooReal, //

      @Parameter(description = "Description for zooDouble") //
      @RequestParam(name = ZOO_DOUBLE, required = false) //
      Double zooDouble, //

      @Parameter(description = "Description for zooDecimal") //
      @RequestParam(name = ZOO_DECIMAL, required = false) //
      BigDecimal zooDecimal, //

      @Parameter(description = "Description for zooBoolean") //
      @RequestParam(name = ZOO_BOOLEAN, required = false) //
      Boolean zooBoolean, //

      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDate, //
      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDateMin, //
      @Parameter(description = "Description for zooDate") //
      @RequestParam(name = ZOO_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate zooDateMax, //

      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestamp, //
      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestampMin, //
      @Parameter(description = "Description for zooTimestamp") //
      @RequestParam(name = ZOO_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime zooTimestampMax, //

      @Parameter(description = "Zero-based page index (0..n)") //
      @RequestParam(name = "pageNumber", required = false, defaultValue = "0") //
      @Min(0) //
      Integer pageNumber, //

      @Parameter(description = "The size of the page to be returned") //
      @RequestParam(name = "pageSize", required = false, defaultValue = "10") //
      @Min(1) //
      Integer pageSize, //

      @Parameter(description = "Property for ordering") //
      @RequestParam(name = "sortProperty", required = false) //
      @OneOf({ ZOO_ORG, ZOO_ID, ZOO_CHAR, ZOO_VARCHAR, ZOO_TEXT, ZOO_SMALLINT, ZOO_INTEGER, ZOO_BIGINT, ZOO_REAL, ZOO_DOUBLE, ZOO_DECIMAL, ZOO_BOOLEAN, ZOO_DATE, ZOO_TIMESTAMP }) //
      String sortProperty, //

      @Parameter(description = "Ascending (true) or descending (false) sort") //
      @RequestParam(name = "sortAscendant", required = false, defaultValue = "true") //
      Boolean sortAscendant, //

      @Parameter(hidden = true) //
      HttpServletRequest request //
  ) {
    ZooQuery zooQuery = zooRestHelper.queryCriteria( //
        zooOrg, //
        zooId, //
        zooChar, //
        zooVarchar, //
        zooText, //
        zooSmallint, //
        zooInteger, //
        zooBigint, //
        zooReal, //
        zooDouble, //
        zooDecimal, //
        zooBoolean, //
        zooDate, zooDateMin, zooDateMax, //
        zooTimestamp, zooTimestampMin, zooTimestampMax, //
        request //
    );
    PageRequest<ZooPropertyName> pagging;
    if (sortProperty != null) {
      pagging = new PageRequest<>( //
          pageNumber, //
          pageSize, //
          new SortRequest<>(zooRestHelper.sortBy(sortProperty), sortAscendant) //
      );
    } else {
      pagging = new PageRequest<>(pageNumber, pageSize);
    }
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:pagging={}", pagging);

    PageResponse<Zoo> zoos = zooApi.findZoos(zooQuery, pagging);

    PageResponse<ZooResponse> zooPageResponse = new PageResponse<>( //
        zoos.getResults().stream().map(ZooResponse::fromEntity).collect(Collectors.toList()), //
        zoos.getTotalElements(), //
        pagging //
    );
    return ResponseEntity.ok(zooPageResponse);
  }

  @Operation(description = "Partially update a Zoo entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PatchMapping(path = "/zooOrg/{zooOrg}/zoo/{zooId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Patch.class })
  public ResponseEntity<ZooResponse> updateZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @Parameter(description = "Description for zooId") //
      @PathVariable(name = "zooId", required = true) @NotNull @Min(1) Integer zooId, //
      @RequestBody(required = true) @NotNull @Valid ZooRequest zooRequest //
  ) {
    return update(zooOrg, zooId, zooRequest, false);
  }

  @Operation(description = "Fully update a Zoo entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PutMapping(path = "/zooOrg/{zooOrg}/zoo/{zooId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Put.class })
  public ResponseEntity<ZooResponse> fullUpdateZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @Parameter(description = "Description for zooId") //
      @PathVariable(name = "zooId", required = true) @NotNull @Min(1) Integer zooId, //
      @RequestBody(required = true) @NotNull @Valid ZooRequest zooRequest //
  ) {
    return update(zooOrg, zooId, zooRequest, true);
  }

  private ResponseEntity<ZooResponse> update( //
      Long zooOrg, //
      Integer zooId, //
      ZooRequest zooRequest, //
      boolean fullUpdate //
  ) {
    log.debug("update:zooOrg={}", zooOrg);
    log.debug("update:zooId={}", zooId);
    log.debug("update:zooRequest={}", zooRequest);

    long updateCount = zooApi.updateZoo(zooOrg, zooId, zooRequest.toUpdate(fullUpdate));

    if (updateCount > 0) {
      Zoo zoo = zooApi.findZoo(zooOrg, zooId).get();
      return ResponseEntity.ok(ZooResponse.fromEntity(zoo));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, zooId);
    }
  }

  @Operation(description = "Delete a Zoo entity using its identifier")
  @ApiResponse(responseCode = "204")
  @DeleteMapping(path = "/zooOrg/{zooOrg}/zoo/{zooId}", produces = APPLICATION_JSON)
  public ResponseEntity<Void> deleteZoo( //
      @Parameter(description = "Description for zooOrg") //
      @PathVariable(name = "zooOrg", required = true) @NotNull @Min(1) Long zooOrg, //
      @Parameter(description = "Description for zooId") //
      @PathVariable(name = "zooId", required = true) @NotNull @Min(1) Integer zooId //
  ) {
    log.debug("deleteZoo:zooOrg={}", zooOrg);
    log.debug("deleteZoo:zooId={}", zooId);

    zooApi.deleteZoo(zooOrg, zooId);

    return ResponseEntity.noContent().build();
  }
}
