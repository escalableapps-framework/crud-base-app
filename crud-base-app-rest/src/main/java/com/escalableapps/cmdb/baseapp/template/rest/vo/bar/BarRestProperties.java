package com.escalableapps.cmdb.baseapp.template.rest.vo.bar;

public final class BarRestProperties {
  public static final String BAR_ID = "bar-id";
  public static final String BAR_CHAR = "bar-char";
  public static final String BAR_VARCHAR = "bar-varchar";
  public static final String BAR_TEXT = "bar-text";
  public static final String BAR_SMALLINT = "bar-smallint";
  public static final String BAR_INTEGER = "bar-integer";
  public static final String BAR_BIGINT = "bar-bigint";
  public static final String BAR_REAL = "bar-real";
  public static final String BAR_DOUBLE = "bar-double";
  public static final String BAR_DECIMAL = "bar-decimal";
  public static final String BAR_BOOLEAN = "bar-boolean";
  public static final String BAR_DATE = "bar-date";
  public static final String BAR_DATE_MIN = "bar-dateMin";
  public static final String BAR_DATE_MAX = "bar-dateMax";
  public static final String BAR_TIMESTAMP = "bar-timestamp";
  public static final String BAR_TIMESTAMP_MIN = "bar-timestampMin";
  public static final String BAR_TIMESTAMP_MAX = "bar-timestampMax";

  private BarRestProperties() {
  }
}
