package com.escalableapps.cmdb.baseapp.template.rest.vo.zoo;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.ZooRestProperties.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Schema(description = "Description for Zoo")
@ToString
@EqualsAndHashCode
public class ZooRequest {

  @AssertTrue(groups = Patch.class, message = "No property has been set for update")
  @JsonIgnore
  private boolean hasPatchProperty;

  @Size(max = 2)
  @JsonProperty(value = ZOO_CHAR, index = 1, required = false)
  @Schema(description = "Description for zooChar")
  private String zooChar;

  @JsonIgnore
  private boolean zooCharNull;

  @Size(max = 4)
  @JsonProperty(value = ZOO_VARCHAR, index = 2, required = false)
  @Schema(description = "Description for zooVarchar")
  private String zooVarchar;

  @JsonIgnore
  private boolean zooVarcharNull;

  @Size(max = Limits.TEXT)
  @JsonProperty(value = ZOO_TEXT, index = 3, required = false)
  @Schema(description = "Description for zooText")
  private String zooText;

  @JsonIgnore
  private boolean zooTextNull;

  @JsonProperty(value = ZOO_SMALLINT, index = 4, required = false)
  @Schema(description = "Description for zooSmallint")
  private Short zooSmallint;

  @JsonIgnore
  private boolean zooSmallintNull;

  @JsonProperty(value = ZOO_INTEGER, index = 5, required = false)
  @Schema(description = "Description for zooInteger")
  private Integer zooInteger;

  @JsonIgnore
  private boolean zooIntegerNull;

  @JsonProperty(value = ZOO_BIGINT, index = 6, required = false)
  @Schema(description = "Description for zooBigint")
  private Long zooBigint;

  @JsonIgnore
  private boolean zooBigintNull;

  @JsonProperty(value = ZOO_REAL, index = 7, required = false)
  @Schema(description = "Description for zooReal")
  private Float zooReal;

  @JsonIgnore
  private boolean zooRealNull;

  @JsonProperty(value = ZOO_DOUBLE, index = 8, required = false)
  @Schema(description = "Description for zooDouble")
  private Double zooDouble;

  @JsonIgnore
  private boolean zooDoubleNull;

  @DecimalMax("99.9999")
  @JsonProperty(value = ZOO_DECIMAL, index = 9, required = false)
  @Schema(description = "Description for zooDecimal")
  private BigDecimal zooDecimal;

  @JsonIgnore
  private boolean zooDecimalNull;

  @JsonProperty(value = ZOO_BOOLEAN, index = 10, required = false)
  @Schema(description = "Description for zooBoolean")
  private Boolean zooBoolean;

  @JsonIgnore
  private boolean zooBooleanNull;

  @DateRange(min = Limits.DATE_MIN, max = Limits.DATE_MAX)
  @JsonProperty(value = ZOO_DATE, index = 11, required = false)
  @Schema(description = "Description for zooDate")
  private LocalDate zooDate;

  @JsonIgnore
  private boolean zooDateNull;

  @DatetimeRange(min = Limits.TIMESTAMP_MIN, max = Limits.TIMESTAMP_MAX)
  @JsonProperty(value = ZOO_TIMESTAMP, index = 12, required = false)
  @Schema(description = "Description for zooTimestamp")
  private OffsetDateTime zooTimestamp;

  @JsonIgnore
  private boolean zooTimestampNull;

  public Zoo toEntity(Long zooOrg) {
    Zoo zoo = new Zoo();
    zoo.setZooOrg(zooOrg);
    zoo.setZooChar(zooChar);
    zoo.setZooVarchar(zooVarchar);
    zoo.setZooText(zooText);
    zoo.setZooSmallint(zooSmallint);
    zoo.setZooInteger(zooInteger);
    zoo.setZooBigint(zooBigint);
    zoo.setZooReal(zooReal);
    zoo.setZooDouble(zooDouble);
    zoo.setZooDecimal(zooDecimal);
    zoo.setZooBoolean(zooBoolean);
    zoo.setZooDate(zooDate);
    zoo.setZooTimestamp(zooTimestamp);
    return zoo;
  }

  public ZooUpdate toUpdate(boolean fullUpdate) {
    ZooUpdate zooUpdate = new ZooUpdate();
    toUpdateZooChar(zooUpdate, fullUpdate);
    toUpdateZooVarchar(zooUpdate, fullUpdate);
    toUpdateZooText(zooUpdate, fullUpdate);
    toUpdateZooSmallint(zooUpdate, fullUpdate);
    toUpdateZooInteger(zooUpdate, fullUpdate);
    toUpdateZooBigint(zooUpdate, fullUpdate);
    toUpdateZooReal(zooUpdate, fullUpdate);
    toUpdateZooDouble(zooUpdate, fullUpdate);
    toUpdateZooDecimal(zooUpdate, fullUpdate);
    toUpdateZooBoolean(zooUpdate, fullUpdate);
    toUpdateZooDate(zooUpdate, fullUpdate);
    toUpdateZooTimestamp(zooUpdate, fullUpdate);
    return zooUpdate;
  }

  private void toUpdateZooChar(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooChar != null || zooCharNull) {
      zooUpdate.setZooChar(zooChar);
    }
  }

  private void toUpdateZooVarchar(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooVarchar != null || zooVarcharNull) {
      zooUpdate.setZooVarchar(zooVarchar);
    }
  }

  private void toUpdateZooText(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooText != null || zooTextNull) {
      zooUpdate.setZooText(zooText);
    }
  }

  private void toUpdateZooSmallint(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooSmallint != null || zooSmallintNull) {
      zooUpdate.setZooSmallint(zooSmallint);
    }
  }

  private void toUpdateZooInteger(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooInteger != null || zooIntegerNull) {
      zooUpdate.setZooInteger(zooInteger);
    }
  }

  private void toUpdateZooBigint(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooBigint != null || zooBigintNull) {
      zooUpdate.setZooBigint(zooBigint);
    }
  }

  private void toUpdateZooReal(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooReal != null || zooRealNull) {
      zooUpdate.setZooReal(zooReal);
    }
  }

  private void toUpdateZooDouble(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooDouble != null || zooDoubleNull) {
      zooUpdate.setZooDouble(zooDouble);
    }
  }

  private void toUpdateZooDecimal(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooDecimal != null || zooDecimalNull) {
      zooUpdate.setZooDecimal(zooDecimal);
    }
  }

  private void toUpdateZooBoolean(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooBoolean != null || zooBooleanNull) {
      zooUpdate.setZooBoolean(zooBoolean);
    }
  }

  private void toUpdateZooDate(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooDate != null || zooDateNull) {
      zooUpdate.setZooDate(zooDate);
    }
  }

  private void toUpdateZooTimestamp(ZooUpdate zooUpdate, boolean fullUpdate) {
    if (fullUpdate || zooTimestamp != null || zooTimestampNull) {
      zooUpdate.setZooTimestamp(zooTimestamp);
    }
  }

  public void setZooChar(String zooChar) {
    this.zooChar = zooChar;
    if (zooChar == null) {
      zooCharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooVarchar(String zooVarchar) {
    this.zooVarchar = zooVarchar;
    if (zooVarchar == null) {
      zooVarcharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooText(String zooText) {
    this.zooText = zooText;
    if (zooText == null) {
      zooTextNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooSmallint(Short zooSmallint) {
    this.zooSmallint = zooSmallint;
    if (zooSmallint == null) {
      zooSmallintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooInteger(Integer zooInteger) {
    this.zooInteger = zooInteger;
    if (zooInteger == null) {
      zooIntegerNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooBigint(Long zooBigint) {
    this.zooBigint = zooBigint;
    if (zooBigint == null) {
      zooBigintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooReal(Float zooReal) {
    this.zooReal = zooReal;
    if (zooReal == null) {
      zooRealNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooDouble(Double zooDouble) {
    this.zooDouble = zooDouble;
    if (zooDouble == null) {
      zooDoubleNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooDecimal(BigDecimal zooDecimal) {
    this.zooDecimal = zooDecimal;
    if (zooDecimal == null) {
      zooDecimalNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooBoolean(Boolean zooBoolean) {
    this.zooBoolean = zooBoolean;
    if (zooBoolean == null) {
      zooBooleanNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooDate(LocalDate zooDate) {
    this.zooDate = zooDate;
    if (zooDate == null) {
      zooDateNull = true;
    }
    hasPatchProperty = true;
  }

  public void setZooTimestamp(OffsetDateTime zooTimestamp) {
    this.zooTimestamp = zooTimestamp;
    if (zooTimestamp == null) {
      zooTimestampNull = true;
    }
    hasPatchProperty = true;
  }

}
