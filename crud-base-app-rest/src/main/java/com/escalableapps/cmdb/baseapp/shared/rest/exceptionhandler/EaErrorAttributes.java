package com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

public class EaErrorAttributes extends DefaultErrorAttributes {

  private static final String ATTRIBUTE_ERROR = "error";
  private static final String ATTRIBUTE_STATUS = "status";
  private static final String ATTRIBUTE_MESSAGE = "message";
  private static final String ATTRIBUTE_TIMESTAMP = "timestamp";

  /**
   * @deprecated
   */
  @Override
  @Deprecated
  public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
    Map<String, Object> errorAttributes = new LinkedHashMap<>();
    errorAttributes.put(ATTRIBUTE_TIMESTAMP, new Date());
    int status = (int) webRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE, RequestAttributes.SCOPE_REQUEST);
    errorAttributes.put(ATTRIBUTE_STATUS, status);
    errorAttributes.put(ATTRIBUTE_ERROR, HttpStatus.valueOf(status).getReasonPhrase());
    errorAttributes.put(ATTRIBUTE_MESSAGE, errorAttributes.get(ATTRIBUTE_ERROR));
    return errorAttributes;
  }
}
