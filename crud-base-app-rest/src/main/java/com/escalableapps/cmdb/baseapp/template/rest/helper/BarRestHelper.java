package com.escalableapps.cmdb.baseapp.template.rest.helper;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.bar.BarRestProperties.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

@Component
public class BarRestHelper {

  private final HashMap<String, BarPropertyName> sortProperties;

  public BarRestHelper() {
    sortProperties = new HashMap<>();
    sortProperties.put(BAR_ID, BarPropertyName.BAR_ID);
    sortProperties.put(BAR_CHAR, BarPropertyName.BAR_CHAR);
    sortProperties.put(BAR_VARCHAR, BarPropertyName.BAR_VARCHAR);
    sortProperties.put(BAR_TEXT, BarPropertyName.BAR_TEXT);
    sortProperties.put(BAR_SMALLINT, BarPropertyName.BAR_SMALLINT);
    sortProperties.put(BAR_INTEGER, BarPropertyName.BAR_INTEGER);
    sortProperties.put(BAR_BIGINT, BarPropertyName.BAR_BIGINT);
    sortProperties.put(BAR_REAL, BarPropertyName.BAR_REAL);
    sortProperties.put(BAR_DOUBLE, BarPropertyName.BAR_DOUBLE);
    sortProperties.put(BAR_DECIMAL, BarPropertyName.BAR_DECIMAL);
    sortProperties.put(BAR_BOOLEAN, BarPropertyName.BAR_BOOLEAN);
    sortProperties.put(BAR_DATE, BarPropertyName.BAR_DATE);
    sortProperties.put(BAR_TIMESTAMP, BarPropertyName.BAR_TIMESTAMP);
  }

  public URI buildBarLocation(Bar bar) {
    return fromCurrentRequest().path("/{id}").buildAndExpand(bar.getBarId()).toUri();
  }

  public BarQuery queryCriteria( //
      Integer barId, //
      String barChar, //
      String barVarchar, //
      String barText, //
      Short barSmallint, //
      Integer barInteger, //
      Long barBigint, //
      Float barReal, //
      Double barDouble, //
      BigDecimal barDecimal, //
      Boolean barBoolean, //
      LocalDate barDate, LocalDate barDateMin, LocalDate barDateMax, //
      OffsetDateTime barTimestamp, OffsetDateTime barTimestampMin, OffsetDateTime barTimestampMax, //
      HttpServletRequest request //
  ) {
    BarQuery query = new BarQuery();
    queryCriteriaForBarId(query, barId, request);
    queryCriteriaForBarChar(query, barChar, request);
    queryCriteriaForBarVarchar(query, barVarchar, request);
    queryCriteriaForBarText(query, barText, request);
    queryCriteriaForBarSmallint(query, barSmallint, request);
    queryCriteriaForBarInteger(query, barInteger, request);
    queryCriteriaForBarBigint(query, barBigint, request);
    queryCriteriaForBarReal(query, barReal, request);
    queryCriteriaForBarDouble(query, barDouble, request);
    queryCriteriaForBarDecimal(query, barDecimal, request);
    queryCriteriaForBarBoolean(query, barBoolean, request);
    queryCriteriaForBarDate(query, barDate, barDateMin, barDateMax, request);
    queryCriteriaForBarTimestamp(query, barTimestamp, barTimestampMin, barTimestampMax, request);
    return query;
  }

  private void queryCriteriaForBarId(BarQuery query, Integer barId, HttpServletRequest request) {
    if (request.getParameter(BAR_ID) != null) {
      query.setBarId(barId);
    }
  }

  private void queryCriteriaForBarChar(BarQuery query, String barChar, HttpServletRequest request) {
    if (request.getParameter(BAR_CHAR) != null) {
      query.setBarChar(StringUtils.EMPTY.equals(barChar) ? null : barChar);
    }
  }

  private void queryCriteriaForBarVarchar(BarQuery query, String barVarchar, HttpServletRequest request) {
    if (request.getParameter(BAR_VARCHAR) != null) {
      query.setBarVarchar(StringUtils.EMPTY.equals(barVarchar) ? null : barVarchar);
    }
  }

  private void queryCriteriaForBarText(BarQuery query, String barText, HttpServletRequest request) {
    if (request.getParameter(BAR_TEXT) != null) {
      query.setBarText(StringUtils.EMPTY.equals(barText) ? null : barText);
    }
  }

  private void queryCriteriaForBarSmallint(BarQuery query, Short barSmallint, HttpServletRequest request) {
    if (request.getParameter(BAR_SMALLINT) != null) {
      query.setBarSmallint(barSmallint);
    }
  }

  private void queryCriteriaForBarInteger(BarQuery query, Integer barInteger, HttpServletRequest request) {
    if (request.getParameter(BAR_INTEGER) != null) {
      query.setBarInteger(barInteger);
    }
  }

  private void queryCriteriaForBarBigint(BarQuery query, Long barBigint, HttpServletRequest request) {
    if (request.getParameter(BAR_BIGINT) != null) {
      query.setBarBigint(barBigint);
    }
  }

  private void queryCriteriaForBarReal(BarQuery query, Float barReal, HttpServletRequest request) {
    if (request.getParameter(BAR_REAL) != null) {
      query.setBarReal(barReal);
    }
  }

  private void queryCriteriaForBarDouble(BarQuery query, Double barDouble, HttpServletRequest request) {
    if (request.getParameter(BAR_DOUBLE) != null) {
      query.setBarDouble(barDouble);
    }
  }

  private void queryCriteriaForBarDecimal(BarQuery query, BigDecimal barDecimal, HttpServletRequest request) {
    if (request.getParameter(BAR_DECIMAL) != null) {
      query.setBarDecimal(barDecimal);
    }
  }

  private void queryCriteriaForBarBoolean(BarQuery query, Boolean barBoolean, HttpServletRequest request) {
    if (request.getParameter(BAR_BOOLEAN) != null) {
      query.setBarBoolean(barBoolean);
    }
  }

  private void queryCriteriaForBarDate(BarQuery query, LocalDate barDate, LocalDate barDateMin, LocalDate barDateMax, HttpServletRequest request) {
    if (request.getParameter(BAR_DATE) != null) {
      query.setBarDate(barDate);
    }
    if (request.getParameter(BAR_DATE_MIN) != null) {
      query.setBarDateMin(barDateMin);
    }
    if (request.getParameter(BAR_DATE_MAX) != null) {
      query.setBarDateMax(barDateMax);
    }
  }

  private void queryCriteriaForBarTimestamp(BarQuery query, OffsetDateTime barTimestamp, OffsetDateTime barTimestampMin, OffsetDateTime barTimestampMax, HttpServletRequest request) {
    if (request.getParameter(BAR_TIMESTAMP) != null) {
      query.setBarTimestamp(barTimestamp);
    }
    if (request.getParameter(BAR_TIMESTAMP_MIN) != null) {
      query.setBarTimestampMin(barTimestampMin);
    }
    if (request.getParameter(BAR_TIMESTAMP_MAX) != null) {
      query.setBarTimestampMax(barTimestampMax);
    }
  }

  public BarPropertyName sortBy(String sortProperty) {
    return sortProperties.get(sortProperty);
  }
}
