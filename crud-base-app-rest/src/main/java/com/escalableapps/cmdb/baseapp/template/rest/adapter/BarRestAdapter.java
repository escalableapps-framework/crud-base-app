package com.escalableapps.cmdb.baseapp.template.rest.adapter;

import static com.escalableapps.cmdb.baseapp.shared.rest.model.MediaType.APPLICATION_JSON;
import static com.escalableapps.cmdb.baseapp.template.rest.vo.bar.BarRestProperties.*;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.SortRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.OneOf;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Patch;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Post;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Put;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Bar;
import com.escalableapps.cmdb.baseapp.template.domain.entity.BarPropertyName;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.BarApi;
import com.escalableapps.cmdb.baseapp.template.rest.helper.BarRestHelper;
import com.escalableapps.cmdb.baseapp.template.rest.vo.bar.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Validated
public class BarRestAdapter {

  private static final String NOT_FOUND_BY_CRITERIA = "bar not found";
  private static final String NOT_FOUND_BY_ID = "bar: id=%s not found";

  private final BarRestHelper barRestHelper;
  private final BarApi barApi;

  public BarRestAdapter(BarRestHelper barRestHelper, BarApi barApi) {
    this.barRestHelper = barRestHelper;
    this.barApi = barApi;
  }

  @Operation(description = "Create a new Bar entity")
  @ApiResponse(responseCode = "201", headers = @Header(name = "Location"))
  @PostMapping(path = "/bar", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Post.class })
  public ResponseEntity<BarResponse> createBar( //
      @RequestBody(required = true) @NotNull @Valid BarRequest barRequest //
  ) {
    log.debug("createBar:barRequest={}", barRequest);

    Bar bar = barApi.createBar(barRequest.toEntity());

    URI location = barRestHelper.buildBarLocation(bar);
    return ResponseEntity.created(location).body(BarResponse.fromEntity(bar));
  }

  @Operation(description = "Check if the entity Bar exists through its identifier")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/bar/{barId}", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsBar( //
      @Parameter(description = "Description for barId") //
      @PathVariable(name = "barId", required = true) @NotNull @Min(1) Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = barApi.existsBar(barId);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, barId);
    }
  }

  @Operation(description = "Check if there is any Bar entity through search filters")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/bar", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsBars( //
      @Parameter(description = "Description for barId") //
      @RequestParam(name = BAR_ID, required = false) //
      Integer barId, //

      @Parameter(description = "Description for barChar") //
      @RequestParam(name = BAR_CHAR, required = false) //
      String barChar, //

      @Parameter(description = "Description for barVarchar") //
      @RequestParam(name = BAR_VARCHAR, required = false) //
      String barVarchar, //

      @Parameter(description = "Description for barText") //
      @RequestParam(name = BAR_TEXT, required = false) //
      String barText, //

      @Parameter(description = "Description for barSmallint") //
      @RequestParam(name = BAR_SMALLINT, required = false) //
      Short barSmallint, //

      @Parameter(description = "Description for barInteger") //
      @RequestParam(name = BAR_INTEGER, required = false) //
      Integer barInteger, //

      @Parameter(description = "Description for barBigint") //
      @RequestParam(name = BAR_BIGINT, required = false) //
      Long barBigint, //

      @Parameter(description = "Description for barReal") //
      @RequestParam(name = BAR_REAL, required = false) //
      Float barReal, //

      @Parameter(description = "Description for barDouble") //
      @RequestParam(name = BAR_DOUBLE, required = false) //
      Double barDouble, //

      @Parameter(description = "Description for barDecimal") //
      @RequestParam(name = BAR_DECIMAL, required = false) //
      BigDecimal barDecimal, //

      @Parameter(description = "Description for barBoolean") //
      @RequestParam(name = BAR_BOOLEAN, required = false) //
      Boolean barBoolean, //

      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDate, //
      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDateMin, //
      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDateMax, //

      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestamp, //
      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestampMin, //
      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestampMax, //
      @Parameter(hidden = true) HttpServletRequest request //
  ) {
    BarQuery barQuery = barRestHelper.queryCriteria( //
        barId, //
        barChar, //
        barVarchar, //
        barText, //
        barSmallint, //
        barInteger, //
        barBigint, //
        barReal, //
        barDouble, //
        barDecimal, //
        barBoolean, //
        barDate, barDateMin, barDateMax, //
        barTimestamp, barTimestampMin, barTimestampMax, //
        request //
    );
    log.debug("existsBars:barQuery={}", barQuery);

    boolean exists = barApi.existsBars(barQuery);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_CRITERIA);
    }
  }

  @Operation(description = "Gets the Bar entity by its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @GetMapping(path = "/bar/{barId}", produces = APPLICATION_JSON)
  public ResponseEntity<BarResponse> findBar( //
      @Parameter(description = "Description for barId") //
      @PathVariable(name = "barId", required = true) @NotNull @Min(1) Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    Optional<Bar> bar = barApi.findBar(barId);

    if (bar.isPresent()) {
      return ResponseEntity.ok(BarResponse.fromEntity(bar.get()));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, barId);
    }
  }

  @Operation(description = "Get Bar entities through search filters")
  @ApiResponse(responseCode = "200")
  @GetMapping(path = "/bar", produces = APPLICATION_JSON)
  public ResponseEntity<PageResponse<BarResponse>> findBars( //
      @Parameter(description = "Description for barId") //
      @RequestParam(name = BAR_ID, required = false) //
      Integer barId, //

      @Parameter(description = "Description for barChar") //
      @RequestParam(name = BAR_CHAR, required = false) //
      String barChar, //

      @Parameter(description = "Description for barVarchar") //
      @RequestParam(name = BAR_VARCHAR, required = false) //
      String barVarchar, //

      @Parameter(description = "Description for barText") //
      @RequestParam(name = BAR_TEXT, required = false) //
      String barText, //

      @Parameter(description = "Description for barSmallint") //
      @RequestParam(name = BAR_SMALLINT, required = false) //
      Short barSmallint, //

      @Parameter(description = "Description for barInteger") //
      @RequestParam(name = BAR_INTEGER, required = false) //
      Integer barInteger, //

      @Parameter(description = "Description for barBigint") //
      @RequestParam(name = BAR_BIGINT, required = false) //
      Long barBigint, //

      @Parameter(description = "Description for barReal") //
      @RequestParam(name = BAR_REAL, required = false) //
      Float barReal, //

      @Parameter(description = "Description for barDouble") //
      @RequestParam(name = BAR_DOUBLE, required = false) //
      Double barDouble, //

      @Parameter(description = "Description for barDecimal") //
      @RequestParam(name = BAR_DECIMAL, required = false) //
      BigDecimal barDecimal, //

      @Parameter(description = "Description for barBoolean") //
      @RequestParam(name = BAR_BOOLEAN, required = false) //
      Boolean barBoolean, //

      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDate, //
      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDateMin, //
      @Parameter(description = "Description for barDate") //
      @RequestParam(name = BAR_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate barDateMax, //

      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestamp, //
      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestampMin, //
      @Parameter(description = "Description for barTimestamp") //
      @RequestParam(name = BAR_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime barTimestampMax, //

      @Parameter(description = "Zero-based page index (0..n)") //
      @RequestParam(name = "pageNumber", required = false, defaultValue = "0") //
      @Min(0) //
      Integer pageNumber, //

      @Parameter(description = "The size of the page to be returned") //
      @RequestParam(name = "pageSize", required = false, defaultValue = "10") //
      @Min(1) //
      Integer pageSize, //

      @Parameter(description = "Property for ordering") //
      @RequestParam(name = "sortProperty", required = false) //
      @OneOf({ BAR_ID, BAR_CHAR, BAR_VARCHAR, BAR_TEXT, BAR_SMALLINT, BAR_INTEGER, BAR_BIGINT, BAR_REAL, BAR_DOUBLE, BAR_DECIMAL, BAR_BOOLEAN, BAR_DATE, BAR_TIMESTAMP }) //
      String sortProperty, //

      @Parameter(description = "Ascending (true) or descending (false) sort") //
      @RequestParam(name = "sortAscendant", required = false, defaultValue = "true") //
      Boolean sortAscendant, //

      @Parameter(hidden = true) //
      HttpServletRequest request //
  ) {
    BarQuery barQuery = barRestHelper.queryCriteria( //
        barId, //
        barChar, //
        barVarchar, //
        barText, //
        barSmallint, //
        barInteger, //
        barBigint, //
        barReal, //
        barDouble, //
        barDecimal, //
        barBoolean, //
        barDate, barDateMin, barDateMax, //
        barTimestamp, barTimestampMin, barTimestampMax, //
        request //
    );
    PageRequest<BarPropertyName> pagging;
    if (sortProperty != null) {
      pagging = new PageRequest<>( //
          pageNumber, //
          pageSize, //
          new SortRequest<>(barRestHelper.sortBy(sortProperty), sortAscendant) //
      );
    } else {
      pagging = new PageRequest<>(pageNumber, pageSize);
    }
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:pagging={}", pagging);

    PageResponse<Bar> bars = barApi.findBars(barQuery, pagging);

    PageResponse<BarResponse> barPageResponse = new PageResponse<>( //
        bars.getResults().stream().map(BarResponse::fromEntity).collect(Collectors.toList()), //
        bars.getTotalElements(), //
        pagging //
    );
    return ResponseEntity.ok(barPageResponse);
  }

  @Operation(description = "Partially update a Bar entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PatchMapping(path = "/bar/{barId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Patch.class })
  public ResponseEntity<BarResponse> updateBar( //
      @Parameter(description = "Description for barId") //
      @PathVariable(name = "barId", required = true) @NotNull @Min(1) Integer barId, //
      @RequestBody(required = true) @NotNull @Valid BarRequest barRequest //
  ) {
    return update(barId, barRequest, false);
  }

  @Operation(description = "Fully update a Bar entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PutMapping(path = "/bar/{barId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Put.class })
  public ResponseEntity<BarResponse> fullUpdateBar( //
      @Parameter(description = "Description for barId") //
      @PathVariable(name = "barId", required = true) @NotNull @Min(1) Integer barId, //
      @RequestBody(required = true) @NotNull @Valid BarRequest barRequest //
  ) {
    return update(barId, barRequest, true);
  }

  private ResponseEntity<BarResponse> update( //
      Integer barId, //
      BarRequest barRequest, //
      boolean fullUpdate //
  ) {
    log.debug("update:barId={}", barId);
    log.debug("update:barRequest={}", barRequest);

    long updateCount = barApi.updateBar(barId, barRequest.toUpdate(fullUpdate));

    if (updateCount > 0) {
      Bar bar = barApi.findBar(barId).get();
      return ResponseEntity.ok(BarResponse.fromEntity(bar));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, barId);
    }
  }

  @Operation(description = "Delete a Bar entity using its identifier")
  @ApiResponse(responseCode = "204")
  @DeleteMapping(path = "/bar/{barId}", produces = APPLICATION_JSON)
  public ResponseEntity<Void> deleteBar( //
      @Parameter(description = "Description for barId") //
      @PathVariable(name = "barId", required = true) @NotNull @Min(1) Integer barId //
  ) {
    log.debug("deleteBar:barId={}", barId);

    barApi.deleteBar(barId);

    return ResponseEntity.noContent().build();
  }
}
