package com.escalableapps.cmdb.baseapp.template.rest.vo.look;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.look.LookRestProperties.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Schema(description = "Description for Look")
@ToString
@EqualsAndHashCode
public class LookRequest {

  @AssertTrue(groups = Patch.class, message = "No property has been set for update")
  @JsonIgnore
  private boolean hasPatchProperty;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = 1)
  @JsonProperty(value = LOOK_CHAR, index = 1, required = true)
  @Schema(description = "Description for lookChar")
  private String lookChar;

  @JsonIgnore
  private boolean lookCharNull;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = Limits.VARCHAR)
  @JsonProperty(value = LOOK_VARCHAR, index = 2, required = true)
  @Schema(description = "Description for lookVarchar")
  private String lookVarchar;

  @JsonIgnore
  private boolean lookVarcharNull;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = Limits.TEXT)
  @JsonProperty(value = LOOK_TEXT, index = 3, required = true)
  @Schema(description = "Description for lookText")
  private String lookText;

  @JsonIgnore
  private boolean lookTextNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_SMALLINT, index = 4, required = true)
  @Schema(description = "Description for lookSmallint")
  private Short lookSmallint;

  @JsonIgnore
  private boolean lookSmallintNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_INTEGER, index = 5, required = true)
  @Schema(description = "Description for lookInteger")
  private Integer lookInteger;

  @JsonIgnore
  private boolean lookIntegerNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_BIGINT, index = 6, required = true)
  @Schema(description = "Description for lookBigint")
  private Long lookBigint;

  @JsonIgnore
  private boolean lookBigintNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_REAL, index = 7, required = true)
  @Schema(description = "Description for lookReal")
  private Float lookReal;

  @JsonIgnore
  private boolean lookRealNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_DOUBLE, index = 8, required = true)
  @Schema(description = "Description for lookDouble")
  private Double lookDouble;

  @JsonIgnore
  private boolean lookDoubleNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_DECIMAL, index = 9, required = true)
  @Schema(description = "Description for lookDecimal")
  private BigDecimal lookDecimal;

  @JsonIgnore
  private boolean lookDecimalNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = LOOK_BOOLEAN, index = 10, required = true)
  @Schema(description = "Description for lookBoolean")
  private Boolean lookBoolean;

  @JsonIgnore
  private boolean lookBooleanNull;

  @NotNull(groups = { Post.class, Put.class })
  @DateRange(min = Limits.DATE_MIN, max = Limits.DATE_MAX)
  @JsonProperty(value = LOOK_DATE, index = 11, required = true)
  @Schema(description = "Description for lookDate")
  private LocalDate lookDate;

  @JsonIgnore
  private boolean lookDateNull;

  @NotNull(groups = { Post.class, Put.class })
  @DatetimeRange(min = Limits.TIMESTAMP_MIN, max = Limits.TIMESTAMP_MAX)
  @JsonProperty(value = LOOK_TIMESTAMP, index = 12, required = true)
  @Schema(description = "Description for lookTimestamp")
  private OffsetDateTime lookTimestamp;

  @JsonIgnore
  private boolean lookTimestampNull;

  public Look toEntity(Long lookOrg) {
    Look look = new Look();
    look.setLookOrg(lookOrg);
    look.setLookChar(lookChar);
    look.setLookVarchar(lookVarchar);
    look.setLookText(lookText);
    look.setLookSmallint(lookSmallint);
    look.setLookInteger(lookInteger);
    look.setLookBigint(lookBigint);
    look.setLookReal(lookReal);
    look.setLookDouble(lookDouble);
    look.setLookDecimal(lookDecimal);
    look.setLookBoolean(lookBoolean);
    look.setLookDate(lookDate);
    look.setLookTimestamp(lookTimestamp);
    return look;
  }

  public LookUpdate toUpdate(boolean fullUpdate) {
    LookUpdate lookUpdate = new LookUpdate();
    toUpdateLookChar(lookUpdate, fullUpdate);
    toUpdateLookVarchar(lookUpdate, fullUpdate);
    toUpdateLookText(lookUpdate, fullUpdate);
    toUpdateLookSmallint(lookUpdate, fullUpdate);
    toUpdateLookInteger(lookUpdate, fullUpdate);
    toUpdateLookBigint(lookUpdate, fullUpdate);
    toUpdateLookReal(lookUpdate, fullUpdate);
    toUpdateLookDouble(lookUpdate, fullUpdate);
    toUpdateLookDecimal(lookUpdate, fullUpdate);
    toUpdateLookBoolean(lookUpdate, fullUpdate);
    toUpdateLookDate(lookUpdate, fullUpdate);
    toUpdateLookTimestamp(lookUpdate, fullUpdate);
    return lookUpdate;
  }

  private void toUpdateLookChar(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookChar != null || lookCharNull) {
      lookUpdate.setLookChar(lookChar);
    }
  }

  private void toUpdateLookVarchar(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookVarchar != null || lookVarcharNull) {
      lookUpdate.setLookVarchar(lookVarchar);
    }
  }

  private void toUpdateLookText(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookText != null || lookTextNull) {
      lookUpdate.setLookText(lookText);
    }
  }

  private void toUpdateLookSmallint(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookSmallint != null || lookSmallintNull) {
      lookUpdate.setLookSmallint(lookSmallint);
    }
  }

  private void toUpdateLookInteger(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookInteger != null || lookIntegerNull) {
      lookUpdate.setLookInteger(lookInteger);
    }
  }

  private void toUpdateLookBigint(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookBigint != null || lookBigintNull) {
      lookUpdate.setLookBigint(lookBigint);
    }
  }

  private void toUpdateLookReal(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookReal != null || lookRealNull) {
      lookUpdate.setLookReal(lookReal);
    }
  }

  private void toUpdateLookDouble(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookDouble != null || lookDoubleNull) {
      lookUpdate.setLookDouble(lookDouble);
    }
  }

  private void toUpdateLookDecimal(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookDecimal != null || lookDecimalNull) {
      lookUpdate.setLookDecimal(lookDecimal);
    }
  }

  private void toUpdateLookBoolean(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookBoolean != null || lookBooleanNull) {
      lookUpdate.setLookBoolean(lookBoolean);
    }
  }

  private void toUpdateLookDate(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookDate != null || lookDateNull) {
      lookUpdate.setLookDate(lookDate);
    }
  }

  private void toUpdateLookTimestamp(LookUpdate lookUpdate, boolean fullUpdate) {
    if (fullUpdate || lookTimestamp != null || lookTimestampNull) {
      lookUpdate.setLookTimestamp(lookTimestamp);
    }
  }

  public void setLookChar(String lookChar) {
    this.lookChar = lookChar;
    if (lookChar == null) {
      lookCharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookVarchar(String lookVarchar) {
    this.lookVarchar = lookVarchar;
    if (lookVarchar == null) {
      lookVarcharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookText(String lookText) {
    this.lookText = lookText;
    if (lookText == null) {
      lookTextNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookSmallint(Short lookSmallint) {
    this.lookSmallint = lookSmallint;
    if (lookSmallint == null) {
      lookSmallintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookInteger(Integer lookInteger) {
    this.lookInteger = lookInteger;
    if (lookInteger == null) {
      lookIntegerNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookBigint(Long lookBigint) {
    this.lookBigint = lookBigint;
    if (lookBigint == null) {
      lookBigintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookReal(Float lookReal) {
    this.lookReal = lookReal;
    if (lookReal == null) {
      lookRealNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookDouble(Double lookDouble) {
    this.lookDouble = lookDouble;
    if (lookDouble == null) {
      lookDoubleNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookDecimal(BigDecimal lookDecimal) {
    this.lookDecimal = lookDecimal;
    if (lookDecimal == null) {
      lookDecimalNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookBoolean(Boolean lookBoolean) {
    this.lookBoolean = lookBoolean;
    if (lookBoolean == null) {
      lookBooleanNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookDate(LocalDate lookDate) {
    this.lookDate = lookDate;
    if (lookDate == null) {
      lookDateNull = true;
    }
    hasPatchProperty = true;
  }

  public void setLookTimestamp(OffsetDateTime lookTimestamp) {
    this.lookTimestamp = lookTimestamp;
    if (lookTimestamp == null) {
      lookTimestampNull = true;
    }
    hasPatchProperty = true;
  }

}
