package com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler;

import org.springframework.boot.autoconfigure.web.ErrorProperties;

public class EaErrorProperties extends ErrorProperties {

  public EaErrorProperties() {
    setIncludeException(false);
    setIncludeStacktrace(IncludeStacktrace.NEVER);
    setIncludeBindingErrors(IncludeAttribute.NEVER);
    setIncludeMessage(IncludeAttribute.ALWAYS);
  }
}
