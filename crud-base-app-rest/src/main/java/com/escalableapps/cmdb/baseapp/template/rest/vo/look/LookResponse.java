package com.escalableapps.cmdb.baseapp.template.rest.vo.look;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.look.LookRestProperties.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "Description for Look")
@Data
public class LookResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  public static LookResponse fromEntity(Look look) {
    if (look == null) {
      return null;
    }
    LookResponse lookResponse = new LookResponse();
    lookResponse.setLookOrg(look.getLookOrg());
    lookResponse.setLookId(look.getLookId());
    lookResponse.setLookChar(look.getLookChar());
    lookResponse.setLookVarchar(look.getLookVarchar());
    lookResponse.setLookText(look.getLookText());
    lookResponse.setLookSmallint(look.getLookSmallint());
    lookResponse.setLookInteger(look.getLookInteger());
    lookResponse.setLookBigint(look.getLookBigint());
    lookResponse.setLookReal(look.getLookReal());
    lookResponse.setLookDouble(look.getLookDouble());
    lookResponse.setLookDecimal(look.getLookDecimal());
    lookResponse.setLookBoolean(look.getLookBoolean());
    lookResponse.setLookDate(look.getLookDate());
    lookResponse.setLookTimestamp(look.getLookTimestamp());
    return lookResponse;
  }

  @JsonProperty(value = LOOK_ORG, index = 1, required = true)
  @Schema(description = "Description for lookOrg")
  private Long lookOrg;

  @JsonProperty(value = LOOK_ID, index = 2, required = true)
  @Schema(description = "Description for lookId")
  private Long lookId;

  @JsonProperty(value = LOOK_CHAR, index = 3, required = true)
  @Schema(description = "Description for lookChar")
  private String lookChar;

  @JsonProperty(value = LOOK_VARCHAR, index = 4, required = true)
  @Schema(description = "Description for lookVarchar")
  private String lookVarchar;

  @JsonProperty(value = LOOK_TEXT, index = 5, required = true)
  @Schema(description = "Description for lookText")
  private String lookText;

  @JsonProperty(value = LOOK_SMALLINT, index = 6, required = true)
  @Schema(description = "Description for lookSmallint")
  private Short lookSmallint;

  @JsonProperty(value = LOOK_INTEGER, index = 7, required = true)
  @Schema(description = "Description for lookInteger")
  private Integer lookInteger;

  @JsonProperty(value = LOOK_BIGINT, index = 8, required = true)
  @Schema(description = "Description for lookBigint")
  private Long lookBigint;

  @JsonProperty(value = LOOK_REAL, index = 9, required = true)
  @Schema(description = "Description for lookReal")
  private Float lookReal;

  @JsonProperty(value = LOOK_DOUBLE, index = 10, required = true)
  @Schema(description = "Description for lookDouble")
  private Double lookDouble;

  @JsonProperty(value = LOOK_DECIMAL, index = 11, required = true)
  @Schema(description = "Description for lookDecimal")
  private BigDecimal lookDecimal;

  @JsonProperty(value = LOOK_BOOLEAN, index = 12, required = true)
  @Schema(description = "Description for lookBoolean")
  private Boolean lookBoolean;

  @JsonProperty(value = LOOK_DATE, index = 13, required = true)
  @Schema(description = "Description for lookDate")
  private LocalDate lookDate;

  @JsonProperty(value = LOOK_TIMESTAMP, index = 14, required = true)
  @Schema(description = "Description for lookTimestamp")
  private OffsetDateTime lookTimestamp;
}
