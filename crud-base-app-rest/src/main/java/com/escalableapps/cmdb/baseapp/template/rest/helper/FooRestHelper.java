package com.escalableapps.cmdb.baseapp.template.rest.helper;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.foo.FooRestProperties.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;

@Component
public class FooRestHelper {

  private final HashMap<String, FooPropertyName> sortProperties;

  public FooRestHelper() {
    sortProperties = new HashMap<>();
    sortProperties.put(FOO_ID, FooPropertyName.FOO_ID);
    sortProperties.put(FOO_CHAR, FooPropertyName.FOO_CHAR);
    sortProperties.put(FOO_VARCHAR, FooPropertyName.FOO_VARCHAR);
    sortProperties.put(FOO_TEXT, FooPropertyName.FOO_TEXT);
    sortProperties.put(FOO_SMALLINT, FooPropertyName.FOO_SMALLINT);
    sortProperties.put(FOO_INTEGER, FooPropertyName.FOO_INTEGER);
    sortProperties.put(FOO_BIGINT, FooPropertyName.FOO_BIGINT);
    sortProperties.put(FOO_REAL, FooPropertyName.FOO_REAL);
    sortProperties.put(FOO_DOUBLE, FooPropertyName.FOO_DOUBLE);
    sortProperties.put(FOO_DECIMAL, FooPropertyName.FOO_DECIMAL);
    sortProperties.put(FOO_BOOLEAN, FooPropertyName.FOO_BOOLEAN);
    sortProperties.put(FOO_DATE, FooPropertyName.FOO_DATE);
    sortProperties.put(FOO_TIMESTAMP, FooPropertyName.FOO_TIMESTAMP);
  }

  public URI buildFooLocation(Foo foo) {
    return fromCurrentRequest().path("/{id}").buildAndExpand(foo.getFooId()).toUri();
  }

  public FooQuery queryCriteria( //
      Integer fooId, //
      String fooChar, //
      String fooVarchar, //
      String fooText, //
      Short fooSmallint, //
      Integer fooInteger, //
      Long fooBigint, //
      Float fooReal, //
      Double fooDouble, //
      BigDecimal fooDecimal, //
      Boolean fooBoolean, //
      LocalDate fooDate, LocalDate fooDateMin, LocalDate fooDateMax, //
      OffsetDateTime fooTimestamp, OffsetDateTime fooTimestampMin, OffsetDateTime fooTimestampMax, //
      HttpServletRequest request //
  ) {
    FooQuery query = new FooQuery();
    queryCriteriaForFooId(query, fooId, request);
    queryCriteriaForFooChar(query, fooChar, request);
    queryCriteriaForFooVarchar(query, fooVarchar, request);
    queryCriteriaForFooText(query, fooText, request);
    queryCriteriaForFooSmallint(query, fooSmallint, request);
    queryCriteriaForFooInteger(query, fooInteger, request);
    queryCriteriaForFooBigint(query, fooBigint, request);
    queryCriteriaForFooReal(query, fooReal, request);
    queryCriteriaForFooDouble(query, fooDouble, request);
    queryCriteriaForFooDecimal(query, fooDecimal, request);
    queryCriteriaForFooBoolean(query, fooBoolean, request);
    queryCriteriaForFooDate(query, fooDate, fooDateMin, fooDateMax, request);
    queryCriteriaForFooTimestamp(query, fooTimestamp, fooTimestampMin, fooTimestampMax, request);
    return query;
  }

  private void queryCriteriaForFooId(FooQuery query, Integer fooId, HttpServletRequest request) {
    if (request.getParameter(FOO_ID) != null) {
      query.setFooId(fooId);
    }
  }

  private void queryCriteriaForFooChar(FooQuery query, String fooChar, HttpServletRequest request) {
    if (request.getParameter(FOO_CHAR) != null) {
      query.setFooChar(StringUtils.EMPTY.equals(fooChar) ? null : fooChar);
    }
  }

  private void queryCriteriaForFooVarchar(FooQuery query, String fooVarchar, HttpServletRequest request) {
    if (request.getParameter(FOO_VARCHAR) != null) {
      query.setFooVarchar(StringUtils.EMPTY.equals(fooVarchar) ? null : fooVarchar);
    }
  }

  private void queryCriteriaForFooText(FooQuery query, String fooText, HttpServletRequest request) {
    if (request.getParameter(FOO_TEXT) != null) {
      query.setFooText(StringUtils.EMPTY.equals(fooText) ? null : fooText);
    }
  }

  private void queryCriteriaForFooSmallint(FooQuery query, Short fooSmallint, HttpServletRequest request) {
    if (request.getParameter(FOO_SMALLINT) != null) {
      query.setFooSmallint(fooSmallint);
    }
  }

  private void queryCriteriaForFooInteger(FooQuery query, Integer fooInteger, HttpServletRequest request) {
    if (request.getParameter(FOO_INTEGER) != null) {
      query.setFooInteger(fooInteger);
    }
  }

  private void queryCriteriaForFooBigint(FooQuery query, Long fooBigint, HttpServletRequest request) {
    if (request.getParameter(FOO_BIGINT) != null) {
      query.setFooBigint(fooBigint);
    }
  }

  private void queryCriteriaForFooReal(FooQuery query, Float fooReal, HttpServletRequest request) {
    if (request.getParameter(FOO_REAL) != null) {
      query.setFooReal(fooReal);
    }
  }

  private void queryCriteriaForFooDouble(FooQuery query, Double fooDouble, HttpServletRequest request) {
    if (request.getParameter(FOO_DOUBLE) != null) {
      query.setFooDouble(fooDouble);
    }
  }

  private void queryCriteriaForFooDecimal(FooQuery query, BigDecimal fooDecimal, HttpServletRequest request) {
    if (request.getParameter(FOO_DECIMAL) != null) {
      query.setFooDecimal(fooDecimal);
    }
  }

  private void queryCriteriaForFooBoolean(FooQuery query, Boolean fooBoolean, HttpServletRequest request) {
    if (request.getParameter(FOO_BOOLEAN) != null) {
      query.setFooBoolean(fooBoolean);
    }
  }

  private void queryCriteriaForFooDate(FooQuery query, LocalDate fooDate, LocalDate fooDateMin, LocalDate fooDateMax, HttpServletRequest request) {
    if (request.getParameter(FOO_DATE) != null) {
      query.setFooDate(fooDate);
    }
    if (request.getParameter(FOO_DATE_MIN) != null) {
      query.setFooDateMin(fooDateMin);
    }
    if (request.getParameter(FOO_DATE_MAX) != null) {
      query.setFooDateMax(fooDateMax);
    }
  }

  private void queryCriteriaForFooTimestamp(FooQuery query, OffsetDateTime fooTimestamp, OffsetDateTime fooTimestampMin, OffsetDateTime fooTimestampMax, HttpServletRequest request) {
    if (request.getParameter(FOO_TIMESTAMP) != null) {
      query.setFooTimestamp(fooTimestamp);
    }
    if (request.getParameter(FOO_TIMESTAMP_MIN) != null) {
      query.setFooTimestampMin(fooTimestampMin);
    }
    if (request.getParameter(FOO_TIMESTAMP_MAX) != null) {
      query.setFooTimestampMax(fooTimestampMax);
    }
  }

  public FooPropertyName sortBy(String sortProperty) {
    return sortProperties.get(sortProperty);
  }
}
