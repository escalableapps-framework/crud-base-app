package com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.ConflictException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.EaFrameworkException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ExceptionHandlerUtils {

  private static final String MESSAGE_FIELD_MSG = "%s: %s";
  private static final String MESSAGE_MUST_BE_AN_VALID = "%s: must be an valid %s";
  private static final String REQUIRED_NOT_PRESENT = "Required %s parameter '%s' is not present";

  public static String handleConflictException(ConflictException e) {
    return e.getMessage();
  }

  public static String handleConstraintViolationException(ConstraintViolationException e) {
    ConstraintViolation<?> constraintViolation = e.getConstraintViolations().stream().findFirst().orElse(null);
    if (constraintViolation == null) {
      throw new EaFrameworkException("constraintViolation is null", e);
    }
    if (isDefaultMessage(constraintViolation)) {
      String code = constraintViolation.getPropertyPath().toString();
      String name = StringUtils.substring(code, StringUtils.ordinalIndexOf(code, ".", 1) + 1);
      return String.format(MESSAGE_FIELD_MSG, name, constraintViolation.getMessage());
    } else {
      return constraintViolation.getMessage();
    }
  }

  public static String handleException() {
    return INTERNAL_SERVER_ERROR.getReasonPhrase();
  }

  public static String handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
    log.warn(e.getMessage());
    return StringUtils.substringBefore(e.getMessage(), ":");
  }

  public static String handleInvalidFormatException(InvalidFormatException e) {
    StringBuilder name = new StringBuilder();
    List<Reference> references = e.getPath();
    references.forEach(reference -> {
      if (reference.getIndex() != -1) {
        name.append("[").append(reference.getIndex()).append("]");
      } else if (name.length() > 0) {
        name.append(".");
      }
      if (reference.getFieldName() != null) {
        name.append(reference.getFieldName());
      }
    });
    String type = getPropertyType(e.getTargetType().getName(), e.getTargetType().getSimpleName());
    return String.format(MESSAGE_MUST_BE_AN_VALID, name, type);
  }

  public static String handleJsonParseException(JsonParseException e) {
    log.warn(e.getMessage());
    return StringUtils.substringBefore(e.getOriginalMessage(), ":");
  }

  public static String handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    ObjectError error = e.getBindingResult().getAllErrors().get(0);
    if (isDefaultMessage(error.unwrap(ConstraintViolation.class))) {
      String[] codes = error.getCodes();
      if (codes == null) {
        throw new EaFrameworkException("codes is null", e);
      }
      String code = codes[0];
      String path = StringUtils.substring(code, StringUtils.ordinalIndexOf(code, ".", 2) + 1);
      return String.format(MESSAGE_FIELD_MSG, path, error.getDefaultMessage());
    } else {
      return error.getDefaultMessage();
    }
  }

  public static String handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
    String name = e.getName();
    Class<?> requiredType = e.getRequiredType();
    if (requiredType == null) {
      throw new EaFrameworkException("requiredType is null", e);
    }
    String type = getPropertyType(requiredType.getName(), requiredType.getSimpleName());
    return String.format(MESSAGE_MUST_BE_AN_VALID, name, type);
  }

  public static String handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
    String name = e.getParameterName();
    String type = getPropertyType(e.getParameterType(), e.getParameterType());
    return String.format(REQUIRED_NOT_PRESENT, type, name);
  }

  public static String handleNotFoundException(NotFoundException e) {
    return e.getMessage();
  }

  public static boolean isConstraintViolationFromController(ConstraintViolationException e) {
    ConstraintViolation<?> constraintViolation = e.getConstraintViolations().stream().findFirst().orElse(null);
    if (constraintViolation == null) {
      log.warn("constraintViolation is null");
      return false;
    }
    Class<?> rootBeanClass = constraintViolation.getRootBean().getClass();
    return rootBeanClass.isAnnotationPresent(RestController.class) || rootBeanClass.isAnnotationPresent(Controller.class);
  }

  private static String getPropertyType(String className, String defaultClassName) {
    switch (className) {
    case "String":
    case "java.lang.String":
      return "string";
    case "short":
    case "Short":
    case "java.lang.Short":
      return "number (int16)";
    case "int":
    case "Integer":
    case "java.lang.Integer":
      return "number (int32)";
    case "long":
    case "Long":
    case "java.lang.Long":
      return "number (int64)";
    case "float":
    case "Float":
    case "java.lang.Float":
      return "number (float)";
    case "double":
    case "Double":
    case "java.lang.Double":
      return "number (double)";
    case "BigDecimal":
    case "java.math.BigDecimal":
      return "number";
    case "boolean":
    case "Boolean":
    case "java.lang.Boolean":
      return "boolean";
    case "LocalDate":
    case "java.time.LocalDate":
      return "string (ISO-8601 date)";
    case "LocalDateTime":
    case "java.time.LocalDateTime":
    case "OffsetDateTime":
    case "java.time.OffsetDateTime":
      return "string (ISO-8601 date-time)";
    default:
      return defaultClassName;
    }
  }

  private static boolean isDefaultMessage(ConstraintViolation<?> constraintViolation) {
    return !constraintViolation.getMessageTemplate().equals(constraintViolation.getMessage());
  }

  private ExceptionHandlerUtils() {
  }
}
