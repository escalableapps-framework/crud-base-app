package com.escalableapps.cmdb.baseapp.template.rest.vo.bar;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.bar.BarRestProperties.*;

import java.math.*;
import java.time.*;

import javax.validation.constraints.*;

import com.escalableapps.cmdb.baseapp.shared.domain.validation.Limits;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.*;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Schema(description = "Description for Bar")
@ToString
@EqualsAndHashCode
public class BarRequest {

  @AssertTrue(groups = Patch.class, message = "No property has been set for update")
  @JsonIgnore
  private boolean hasPatchProperty;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = 1)
  @JsonProperty(value = BAR_CHAR, index = 1, required = true)
  @Schema(description = "Description for barChar")
  private String barChar;

  @JsonIgnore
  private boolean barCharNull;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = Limits.VARCHAR)
  @JsonProperty(value = BAR_VARCHAR, index = 2, required = true)
  @Schema(description = "Description for barVarchar")
  private String barVarchar;

  @JsonIgnore
  private boolean barVarcharNull;

  @NotNull(groups = { Post.class, Put.class })
  @Size(max = Limits.TEXT)
  @JsonProperty(value = BAR_TEXT, index = 3, required = true)
  @Schema(description = "Description for barText")
  private String barText;

  @JsonIgnore
  private boolean barTextNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_SMALLINT, index = 4, required = true)
  @Schema(description = "Description for barSmallint")
  private Short barSmallint;

  @JsonIgnore
  private boolean barSmallintNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_INTEGER, index = 5, required = true)
  @Schema(description = "Description for barInteger")
  private Integer barInteger;

  @JsonIgnore
  private boolean barIntegerNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_BIGINT, index = 6, required = true)
  @Schema(description = "Description for barBigint")
  private Long barBigint;

  @JsonIgnore
  private boolean barBigintNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_REAL, index = 7, required = true)
  @Schema(description = "Description for barReal")
  private Float barReal;

  @JsonIgnore
  private boolean barRealNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_DOUBLE, index = 8, required = true)
  @Schema(description = "Description for barDouble")
  private Double barDouble;

  @JsonIgnore
  private boolean barDoubleNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_DECIMAL, index = 9, required = true)
  @Schema(description = "Description for barDecimal")
  private BigDecimal barDecimal;

  @JsonIgnore
  private boolean barDecimalNull;

  @NotNull(groups = { Post.class, Put.class })
  @JsonProperty(value = BAR_BOOLEAN, index = 10, required = true)
  @Schema(description = "Description for barBoolean")
  private Boolean barBoolean;

  @JsonIgnore
  private boolean barBooleanNull;

  @NotNull(groups = { Post.class, Put.class })
  @DateRange(min = Limits.DATE_MIN, max = Limits.DATE_MAX)
  @JsonProperty(value = BAR_DATE, index = 11, required = true)
  @Schema(description = "Description for barDate")
  private LocalDate barDate;

  @JsonIgnore
  private boolean barDateNull;

  @NotNull(groups = { Post.class, Put.class })
  @DatetimeRange(min = Limits.TIMESTAMP_MIN, max = Limits.TIMESTAMP_MAX)
  @JsonProperty(value = BAR_TIMESTAMP, index = 12, required = true)
  @Schema(description = "Description for barTimestamp")
  private OffsetDateTime barTimestamp;

  @JsonIgnore
  private boolean barTimestampNull;

  public Bar toEntity() {
    Bar bar = new Bar();
    bar.setBarChar(barChar);
    bar.setBarVarchar(barVarchar);
    bar.setBarText(barText);
    bar.setBarSmallint(barSmallint);
    bar.setBarInteger(barInteger);
    bar.setBarBigint(barBigint);
    bar.setBarReal(barReal);
    bar.setBarDouble(barDouble);
    bar.setBarDecimal(barDecimal);
    bar.setBarBoolean(barBoolean);
    bar.setBarDate(barDate);
    bar.setBarTimestamp(barTimestamp);
    return bar;
  }

  public BarUpdate toUpdate(boolean fullUpdate) {
    BarUpdate barUpdate = new BarUpdate();
    toUpdateBarChar(barUpdate, fullUpdate);
    toUpdateBarVarchar(barUpdate, fullUpdate);
    toUpdateBarText(barUpdate, fullUpdate);
    toUpdateBarSmallint(barUpdate, fullUpdate);
    toUpdateBarInteger(barUpdate, fullUpdate);
    toUpdateBarBigint(barUpdate, fullUpdate);
    toUpdateBarReal(barUpdate, fullUpdate);
    toUpdateBarDouble(barUpdate, fullUpdate);
    toUpdateBarDecimal(barUpdate, fullUpdate);
    toUpdateBarBoolean(barUpdate, fullUpdate);
    toUpdateBarDate(barUpdate, fullUpdate);
    toUpdateBarTimestamp(barUpdate, fullUpdate);
    return barUpdate;
  }

  private void toUpdateBarChar(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barChar != null || barCharNull) {
      barUpdate.setBarChar(barChar);
    }
  }

  private void toUpdateBarVarchar(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barVarchar != null || barVarcharNull) {
      barUpdate.setBarVarchar(barVarchar);
    }
  }

  private void toUpdateBarText(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barText != null || barTextNull) {
      barUpdate.setBarText(barText);
    }
  }

  private void toUpdateBarSmallint(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barSmallint != null || barSmallintNull) {
      barUpdate.setBarSmallint(barSmallint);
    }
  }

  private void toUpdateBarInteger(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barInteger != null || barIntegerNull) {
      barUpdate.setBarInteger(barInteger);
    }
  }

  private void toUpdateBarBigint(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barBigint != null || barBigintNull) {
      barUpdate.setBarBigint(barBigint);
    }
  }

  private void toUpdateBarReal(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barReal != null || barRealNull) {
      barUpdate.setBarReal(barReal);
    }
  }

  private void toUpdateBarDouble(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barDouble != null || barDoubleNull) {
      barUpdate.setBarDouble(barDouble);
    }
  }

  private void toUpdateBarDecimal(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barDecimal != null || barDecimalNull) {
      barUpdate.setBarDecimal(barDecimal);
    }
  }

  private void toUpdateBarBoolean(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barBoolean != null || barBooleanNull) {
      barUpdate.setBarBoolean(barBoolean);
    }
  }

  private void toUpdateBarDate(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barDate != null || barDateNull) {
      barUpdate.setBarDate(barDate);
    }
  }

  private void toUpdateBarTimestamp(BarUpdate barUpdate, boolean fullUpdate) {
    if (fullUpdate || barTimestamp != null || barTimestampNull) {
      barUpdate.setBarTimestamp(barTimestamp);
    }
  }

  public void setBarChar(String barChar) {
    this.barChar = barChar;
    if (barChar == null) {
      barCharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarVarchar(String barVarchar) {
    this.barVarchar = barVarchar;
    if (barVarchar == null) {
      barVarcharNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarText(String barText) {
    this.barText = barText;
    if (barText == null) {
      barTextNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarSmallint(Short barSmallint) {
    this.barSmallint = barSmallint;
    if (barSmallint == null) {
      barSmallintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarInteger(Integer barInteger) {
    this.barInteger = barInteger;
    if (barInteger == null) {
      barIntegerNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarBigint(Long barBigint) {
    this.barBigint = barBigint;
    if (barBigint == null) {
      barBigintNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarReal(Float barReal) {
    this.barReal = barReal;
    if (barReal == null) {
      barRealNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarDouble(Double barDouble) {
    this.barDouble = barDouble;
    if (barDouble == null) {
      barDoubleNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarDecimal(BigDecimal barDecimal) {
    this.barDecimal = barDecimal;
    if (barDecimal == null) {
      barDecimalNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarBoolean(Boolean barBoolean) {
    this.barBoolean = barBoolean;
    if (barBoolean == null) {
      barBooleanNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarDate(LocalDate barDate) {
    this.barDate = barDate;
    if (barDate == null) {
      barDateNull = true;
    }
    hasPatchProperty = true;
  }

  public void setBarTimestamp(OffsetDateTime barTimestamp) {
    this.barTimestamp = barTimestamp;
    if (barTimestamp == null) {
      barTimestampNull = true;
    }
    hasPatchProperty = true;
  }

}
