package com.escalableapps.cmdb.baseapp.template.rest.vo.zoo;

import static com.escalableapps.cmdb.baseapp.template.rest.vo.zoo.ZooRestProperties.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "Description for Zoo")
@Data
public class ZooResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  public static ZooResponse fromEntity(Zoo zoo) {
    if (zoo == null) {
      return null;
    }
    ZooResponse zooResponse = new ZooResponse();
    zooResponse.setZooOrg(zoo.getZooOrg());
    zooResponse.setZooId(zoo.getZooId());
    zooResponse.setZooChar(zoo.getZooChar());
    zooResponse.setZooVarchar(zoo.getZooVarchar());
    zooResponse.setZooText(zoo.getZooText());
    zooResponse.setZooSmallint(zoo.getZooSmallint());
    zooResponse.setZooInteger(zoo.getZooInteger());
    zooResponse.setZooBigint(zoo.getZooBigint());
    zooResponse.setZooReal(zoo.getZooReal());
    zooResponse.setZooDouble(zoo.getZooDouble());
    zooResponse.setZooDecimal(zoo.getZooDecimal());
    zooResponse.setZooBoolean(zoo.getZooBoolean());
    zooResponse.setZooDate(zoo.getZooDate());
    zooResponse.setZooTimestamp(zoo.getZooTimestamp());
    return zooResponse;
  }

  @JsonProperty(value = ZOO_ORG, index = 1, required = true)
  @Schema(description = "Description for zooOrg")
  private Long zooOrg;

  @JsonProperty(value = ZOO_ID, index = 2, required = true)
  @Schema(description = "Description for zooId")
  private Integer zooId;

  @JsonProperty(value = ZOO_CHAR, index = 3, required = false)
  @Schema(description = "Description for zooChar")
  private String zooChar;

  @JsonProperty(value = ZOO_VARCHAR, index = 4, required = false)
  @Schema(description = "Description for zooVarchar")
  private String zooVarchar;

  @JsonProperty(value = ZOO_TEXT, index = 5, required = false)
  @Schema(description = "Description for zooText")
  private String zooText;

  @JsonProperty(value = ZOO_SMALLINT, index = 6, required = false)
  @Schema(description = "Description for zooSmallint")
  private Short zooSmallint;

  @JsonProperty(value = ZOO_INTEGER, index = 7, required = false)
  @Schema(description = "Description for zooInteger")
  private Integer zooInteger;

  @JsonProperty(value = ZOO_BIGINT, index = 8, required = false)
  @Schema(description = "Description for zooBigint")
  private Long zooBigint;

  @JsonProperty(value = ZOO_REAL, index = 9, required = false)
  @Schema(description = "Description for zooReal")
  private Float zooReal;

  @JsonProperty(value = ZOO_DOUBLE, index = 10, required = false)
  @Schema(description = "Description for zooDouble")
  private Double zooDouble;

  @JsonProperty(value = ZOO_DECIMAL, index = 11, required = false)
  @Schema(description = "Description for zooDecimal")
  private BigDecimal zooDecimal;

  @JsonProperty(value = ZOO_BOOLEAN, index = 12, required = false)
  @Schema(description = "Description for zooBoolean")
  private Boolean zooBoolean;

  @JsonProperty(value = ZOO_DATE, index = 13, required = false)
  @Schema(description = "Description for zooDate")
  private LocalDate zooDate;

  @JsonProperty(value = ZOO_TIMESTAMP, index = 14, required = false)
  @Schema(description = "Description for zooTimestamp")
  private OffsetDateTime zooTimestamp;
}
