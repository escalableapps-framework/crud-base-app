package com.escalableapps.cmdb.baseapp.template.rest.vo.zoo;

public final class ZooRestProperties {
  public static final String ZOO_ORG = "zoo-org";
  public static final String ZOO_ID = "zoo-id";
  public static final String ZOO_CHAR = "zoo-char";
  public static final String ZOO_VARCHAR = "zoo-varchar";
  public static final String ZOO_TEXT = "zoo-text";
  public static final String ZOO_SMALLINT = "zoo-smallint";
  public static final String ZOO_INTEGER = "zoo-integer";
  public static final String ZOO_BIGINT = "zoo-bigint";
  public static final String ZOO_REAL = "zoo-real";
  public static final String ZOO_DOUBLE = "zoo-double";
  public static final String ZOO_DECIMAL = "zoo-decimal";
  public static final String ZOO_BOOLEAN = "zoo-boolean";
  public static final String ZOO_DATE = "zoo-date";
  public static final String ZOO_DATE_MIN = "zoo-dateMin";
  public static final String ZOO_DATE_MAX = "zoo-dateMax";
  public static final String ZOO_TIMESTAMP = "zoo-timestamp";
  public static final String ZOO_TIMESTAMP_MIN = "zoo-timestampMin";
  public static final String ZOO_TIMESTAMP_MAX = "zoo-timestampMax";

  private ZooRestProperties() {
  }
}
