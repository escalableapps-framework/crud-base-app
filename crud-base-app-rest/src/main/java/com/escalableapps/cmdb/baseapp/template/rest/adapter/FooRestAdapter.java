package com.escalableapps.cmdb.baseapp.template.rest.adapter;

import static com.escalableapps.cmdb.baseapp.shared.rest.model.MediaType.APPLICATION_JSON;
import static com.escalableapps.cmdb.baseapp.template.rest.vo.foo.FooRestProperties.*;

import java.math.*;
import java.net.URI;
import java.time.*;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.cmdb.baseapp.shared.domain.model.exception.NotFoundException;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageResponse;
import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.SortRequest;
import com.escalableapps.cmdb.baseapp.shared.domain.validation.constraints.OneOf;
import com.escalableapps.cmdb.baseapp.shared.rest.exceptionhandler.ResponseError.ErrorMessage;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Patch;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Post;
import com.escalableapps.cmdb.baseapp.shared.rest.validation.group.Put;
import com.escalableapps.cmdb.baseapp.template.domain.entity.Foo;
import com.escalableapps.cmdb.baseapp.template.domain.entity.FooPropertyName;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.port.api.FooApi;
import com.escalableapps.cmdb.baseapp.template.rest.helper.FooRestHelper;
import com.escalableapps.cmdb.baseapp.template.rest.vo.foo.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Validated
public class FooRestAdapter {

  private static final String NOT_FOUND_BY_CRITERIA = "foo not found";
  private static final String NOT_FOUND_BY_ID = "foo: id=%s not found";

  private final FooRestHelper fooRestHelper;
  private final FooApi fooApi;

  public FooRestAdapter(FooRestHelper fooRestHelper, FooApi fooApi) {
    this.fooRestHelper = fooRestHelper;
    this.fooApi = fooApi;
  }

  @Operation(description = "Create a new Foo entity")
  @ApiResponse(responseCode = "201", headers = @Header(name = "Location"))
  @PostMapping(path = "/foo", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Post.class })
  public ResponseEntity<FooResponse> createFoo( //
      @RequestBody(required = true) @NotNull @Valid FooRequest fooRequest //
  ) {
    log.debug("createFoo:fooRequest={}", fooRequest);

    Foo foo = fooApi.createFoo(fooRequest.toEntity());

    URI location = fooRestHelper.buildFooLocation(foo);
    return ResponseEntity.created(location).body(FooResponse.fromEntity(foo));
  }

  @Operation(description = "Check if the entity Foo exists through its identifier")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/foo/{fooId}", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsFoo( //
      @Parameter(description = "Description for fooId") //
      @PathVariable(name = "fooId", required = true) @NotNull @Min(1) Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = fooApi.existsFoo(fooId);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, fooId);
    }
  }

  @Operation(description = "Check if there is any Foo entity through search filters")
  @ApiResponse(responseCode = "204")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @RequestMapping(path = "/foo", produces = APPLICATION_JSON, method = RequestMethod.HEAD)
  public ResponseEntity<Void> existsFoos( //
      @Parameter(description = "Description for fooId") //
      @RequestParam(name = FOO_ID, required = false) //
      Integer fooId, //

      @Parameter(description = "Description for fooChar") //
      @RequestParam(name = FOO_CHAR, required = false) //
      String fooChar, //

      @Parameter(description = "Description for fooVarchar") //
      @RequestParam(name = FOO_VARCHAR, required = false) //
      String fooVarchar, //

      @Parameter(description = "Description for fooText") //
      @RequestParam(name = FOO_TEXT, required = false) //
      String fooText, //

      @Parameter(description = "Description for fooSmallint") //
      @RequestParam(name = FOO_SMALLINT, required = false) //
      Short fooSmallint, //

      @Parameter(description = "Description for fooInteger") //
      @RequestParam(name = FOO_INTEGER, required = false) //
      Integer fooInteger, //

      @Parameter(description = "Description for fooBigint") //
      @RequestParam(name = FOO_BIGINT, required = false) //
      Long fooBigint, //

      @Parameter(description = "Description for fooReal") //
      @RequestParam(name = FOO_REAL, required = false) //
      Float fooReal, //

      @Parameter(description = "Description for fooDouble") //
      @RequestParam(name = FOO_DOUBLE, required = false) //
      Double fooDouble, //

      @Parameter(description = "Description for fooDecimal") //
      @RequestParam(name = FOO_DECIMAL, required = false) //
      BigDecimal fooDecimal, //

      @Parameter(description = "Description for fooBoolean") //
      @RequestParam(name = FOO_BOOLEAN, required = false) //
      Boolean fooBoolean, //

      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDate, //
      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDateMin, //
      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDateMax, //

      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestamp, //
      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestampMin, //
      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestampMax, //
      @Parameter(hidden = true) HttpServletRequest request //
  ) {
    FooQuery fooQuery = fooRestHelper.queryCriteria( //
        fooId, //
        fooChar, //
        fooVarchar, //
        fooText, //
        fooSmallint, //
        fooInteger, //
        fooBigint, //
        fooReal, //
        fooDouble, //
        fooDecimal, //
        fooBoolean, //
        fooDate, fooDateMin, fooDateMax, //
        fooTimestamp, fooTimestampMin, fooTimestampMax, //
        request //
    );
    log.debug("existsFoos:fooQuery={}", fooQuery);

    boolean exists = fooApi.existsFoos(fooQuery);

    if (exists) {
      return ResponseEntity.noContent().build();
    } else {
      throw new NotFoundException(NOT_FOUND_BY_CRITERIA);
    }
  }

  @Operation(description = "Gets the Foo entity by its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @GetMapping(path = "/foo/{fooId}", produces = APPLICATION_JSON)
  public ResponseEntity<FooResponse> findFoo( //
      @Parameter(description = "Description for fooId") //
      @PathVariable(name = "fooId", required = true) @NotNull @Min(1) Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    Optional<Foo> foo = fooApi.findFoo(fooId);

    if (foo.isPresent()) {
      return ResponseEntity.ok(FooResponse.fromEntity(foo.get()));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, fooId);
    }
  }

  @Operation(description = "Get Foo entities through search filters")
  @ApiResponse(responseCode = "200")
  @GetMapping(path = "/foo", produces = APPLICATION_JSON)
  public ResponseEntity<PageResponse<FooResponse>> findFoos( //
      @Parameter(description = "Description for fooId") //
      @RequestParam(name = FOO_ID, required = false) //
      Integer fooId, //

      @Parameter(description = "Description for fooChar") //
      @RequestParam(name = FOO_CHAR, required = false) //
      String fooChar, //

      @Parameter(description = "Description for fooVarchar") //
      @RequestParam(name = FOO_VARCHAR, required = false) //
      String fooVarchar, //

      @Parameter(description = "Description for fooText") //
      @RequestParam(name = FOO_TEXT, required = false) //
      String fooText, //

      @Parameter(description = "Description for fooSmallint") //
      @RequestParam(name = FOO_SMALLINT, required = false) //
      Short fooSmallint, //

      @Parameter(description = "Description for fooInteger") //
      @RequestParam(name = FOO_INTEGER, required = false) //
      Integer fooInteger, //

      @Parameter(description = "Description for fooBigint") //
      @RequestParam(name = FOO_BIGINT, required = false) //
      Long fooBigint, //

      @Parameter(description = "Description for fooReal") //
      @RequestParam(name = FOO_REAL, required = false) //
      Float fooReal, //

      @Parameter(description = "Description for fooDouble") //
      @RequestParam(name = FOO_DOUBLE, required = false) //
      Double fooDouble, //

      @Parameter(description = "Description for fooDecimal") //
      @RequestParam(name = FOO_DECIMAL, required = false) //
      BigDecimal fooDecimal, //

      @Parameter(description = "Description for fooBoolean") //
      @RequestParam(name = FOO_BOOLEAN, required = false) //
      Boolean fooBoolean, //

      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDate, //
      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDateMin, //
      @Parameter(description = "Description for fooDate") //
      @RequestParam(name = FOO_DATE_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE) //
      LocalDate fooDateMax, //

      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestamp, //
      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP_MIN, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestampMin, //
      @Parameter(description = "Description for fooTimestamp") //
      @RequestParam(name = FOO_TIMESTAMP_MAX, required = false) //
      @DateTimeFormat(iso = ISO.DATE_TIME) //
      OffsetDateTime fooTimestampMax, //

      @Parameter(description = "Zero-based page index (0..n)") //
      @RequestParam(name = "pageNumber", required = false, defaultValue = "0") //
      @Min(0) //
      Integer pageNumber, //

      @Parameter(description = "The size of the page to be returned") //
      @RequestParam(name = "pageSize", required = false, defaultValue = "10") //
      @Min(1) //
      Integer pageSize, //

      @Parameter(description = "Property for ordering") //
      @RequestParam(name = "sortProperty", required = false) //
      @OneOf({ FOO_ID, FOO_CHAR, FOO_VARCHAR, FOO_TEXT, FOO_SMALLINT, FOO_INTEGER, FOO_BIGINT, FOO_REAL, FOO_DOUBLE, FOO_DECIMAL, FOO_BOOLEAN, FOO_DATE, FOO_TIMESTAMP }) //
      String sortProperty, //

      @Parameter(description = "Ascending (true) or descending (false) sort") //
      @RequestParam(name = "sortAscendant", required = false, defaultValue = "true") //
      Boolean sortAscendant, //

      @Parameter(hidden = true) //
      HttpServletRequest request //
  ) {
    FooQuery fooQuery = fooRestHelper.queryCriteria( //
        fooId, //
        fooChar, //
        fooVarchar, //
        fooText, //
        fooSmallint, //
        fooInteger, //
        fooBigint, //
        fooReal, //
        fooDouble, //
        fooDecimal, //
        fooBoolean, //
        fooDate, fooDateMin, fooDateMax, //
        fooTimestamp, fooTimestampMin, fooTimestampMax, //
        request //
    );
    PageRequest<FooPropertyName> pagging;
    if (sortProperty != null) {
      pagging = new PageRequest<>( //
          pageNumber, //
          pageSize, //
          new SortRequest<>(fooRestHelper.sortBy(sortProperty), sortAscendant) //
      );
    } else {
      pagging = new PageRequest<>(pageNumber, pageSize);
    }
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:pagging={}", pagging);

    PageResponse<Foo> foos = fooApi.findFoos(fooQuery, pagging);

    PageResponse<FooResponse> fooPageResponse = new PageResponse<>( //
        foos.getResults().stream().map(FooResponse::fromEntity).collect(Collectors.toList()), //
        foos.getTotalElements(), //
        pagging //
    );
    return ResponseEntity.ok(fooPageResponse);
  }

  @Operation(description = "Partially update a Foo entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PatchMapping(path = "/foo/{fooId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Patch.class })
  public ResponseEntity<FooResponse> updateFoo( //
      @Parameter(description = "Description for fooId") //
      @PathVariable(name = "fooId", required = true) @NotNull @Min(1) Integer fooId, //
      @RequestBody(required = true) @NotNull @Valid FooRequest fooRequest //
  ) {
    return update(fooId, fooRequest, false);
  }

  @Operation(description = "Fully update a Foo entity using its identifier")
  @ApiResponse(responseCode = "200")
  @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(oneOf = ErrorMessage.class)))
  @PutMapping(path = "/foo/{fooId}", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
  @Validated({ Default.class, Put.class })
  public ResponseEntity<FooResponse> fullUpdateFoo( //
      @Parameter(description = "Description for fooId") //
      @PathVariable(name = "fooId", required = true) @NotNull @Min(1) Integer fooId, //
      @RequestBody(required = true) @NotNull @Valid FooRequest fooRequest //
  ) {
    return update(fooId, fooRequest, true);
  }

  private ResponseEntity<FooResponse> update( //
      Integer fooId, //
      FooRequest fooRequest, //
      boolean fullUpdate //
  ) {
    log.debug("update:fooId={}", fooId);
    log.debug("update:fooRequest={}", fooRequest);

    long updateCount = fooApi.updateFoo(fooId, fooRequest.toUpdate(fullUpdate));

    if (updateCount > 0) {
      Foo foo = fooApi.findFoo(fooId).get();
      return ResponseEntity.ok(FooResponse.fromEntity(foo));
    } else {
      throw new NotFoundException(NOT_FOUND_BY_ID, fooId);
    }
  }

  @Operation(description = "Delete a Foo entity using its identifier")
  @ApiResponse(responseCode = "204")
  @DeleteMapping(path = "/foo/{fooId}", produces = APPLICATION_JSON)
  public ResponseEntity<Void> deleteFoo( //
      @Parameter(description = "Description for fooId") //
      @PathVariable(name = "fooId", required = true) @NotNull @Min(1) Integer fooId //
  ) {
    log.debug("deleteFoo:fooId={}", fooId);

    fooApi.deleteFoo(fooId);

    return ResponseEntity.noContent().build();
  }
}
