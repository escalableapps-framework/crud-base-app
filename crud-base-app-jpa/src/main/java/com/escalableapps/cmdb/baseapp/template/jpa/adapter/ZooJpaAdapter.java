package com.escalableapps.cmdb.baseapp.template.jpa.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpa.helper.ZooJpaHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpa.mapper.ZooEntityMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.mapper.ZooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpa.repository.ZooJpaRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.ZooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class ZooJpaAdapter implements ZooSpi {

  private final ZooJpaRepository repository;
  private final EntityManager entityManager;

  public ZooJpaAdapter(ZooJpaRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoozoo={}", zoo);

    Integer zooId = repository.nextVal();
    ZooEntity zooEntity = repository.save(fromEntity(zoo, zooId));

    zoo.setZooId(zooEntity.getId().getZooId());
    log.debug("createZoozoo={}", zoo);
    return zoo;
  }

  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    ZooEntity zoo = repository.findById(new ZooPk(zooOrg, zooId)).orElse(null);

    log.debug("findZoo:zoo={}", zoo);

    return Optional.ofNullable(toEntity(zoo));
  }

  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);

    List<ZooEntity> zoos = repository.findAllById(zooIds.stream().map(zooId -> new ZooPk(zooOrg, zooId)).collect(Collectors.toList()));
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery //
  ) {
    return findZoos(zooQuery, new PageRequest<>());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<ZooEntity> query = criteriaBuilder.createQuery(ZooEntity.class);
    Root<ZooEntity> root = query.from(ZooEntity.class);

    List<ZooEntity> zoos = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, zooQuery)) //
            .orderBy(orderBy(criteriaBuilder, root, paging.getSortRequests())) //
        ) //
        .setFirstResult(paging.getOffset()) //
        .setMaxResults(paging.getPageSize()) //
        .getResultList();

    log.debug("findZoos:zoos={}", zoos);
    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = repository.existsById(new ZooPk(zooOrg, zooId));

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoos:zooQuery={}", zooQuery);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<ZooEntity> query = criteriaBuilder.createQuery(ZooEntity.class);
    Root<ZooEntity> root = query.from(ZooEntity.class);

    List<ZooEntity> zoos = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, zooQuery)) //
        ) //
        .setFirstResult(0) //
        .setMaxResults(1) //
        .getResultList();

    boolean exists = !zoos.isEmpty();

    log.debug("existsZoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("countZoos:zooQuery={}", zooQuery);

    long count = repository.count((root, query, criteriaBuilder) -> criteriaBuilder.and(whereCondition(criteriaBuilder, root, zooQuery)));

    log.debug("countZoos:count={}", count);
    return count;
  }

  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("updateZoo:zooOrg={}", zooOrg);
    log.debug("updateZoo:zooId={}", zooId);
    log.debug("updateZoo:zooUpdate={}", zooUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<ZooEntity> update = criteriaBuilder.createCriteriaUpdate(ZooEntity.class);
    Root<ZooEntity> root = update.from(ZooEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, zooUpdate) //
            .where(whereCondition(criteriaBuilder, root, zooOrg, zooId)) //
    ).executeUpdate();

    log.debug("updateZoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("deleteZoo:zooOrg={}", zooOrg);
    log.debug("deleteZoo:zooId={}", zooId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<ZooEntity> delete = criteriaBuilder.createCriteriaDelete(ZooEntity.class);
    Root<ZooEntity> root = delete.from(ZooEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, zooOrg, zooId)) //
    ).executeUpdate();

    log.debug("deleteZoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
