package com.escalableapps.cmdb.baseapp.template.jpa.persistence;

import static com.escalableapps.cmdb.baseapp.shared.jpa.SqlTypes.*;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Embeddable
public class LookPk implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "look_org", columnDefinition = BIGINT)
  private Long lookOrg;

  @Column(name = "look_id", columnDefinition = BIGINT)
  private Long lookId;
}
