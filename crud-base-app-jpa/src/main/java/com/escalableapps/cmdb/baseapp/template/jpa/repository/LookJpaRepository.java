package com.escalableapps.cmdb.baseapp.template.jpa.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public interface LookJpaRepository extends JpaRepository<LookEntity, LookPk>, JpaSpecificationExecutor<LookEntity> {

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.look_look_id_seq')")
  Long nextVal();
}
