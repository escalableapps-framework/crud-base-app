package com.escalableapps.cmdb.baseapp.template.jpa.persistence;

import static com.escalableapps.cmdb.baseapp.shared.jpa.SqlTypes.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "crud_base_db", name = "bar")
public class BarEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @EqualsAndHashCode.Include
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "bar_id", columnDefinition = INTEGER)
  private Integer barId;

  @Column(name = "bar_char", columnDefinition = CHAR)
  private String barChar;

  @Column(name = "bar_varchar", columnDefinition = VARCHAR)
  private String barVarchar;

  @Column(name = "bar_text", columnDefinition = TEXT)
  private String barText;

  @Column(name = "bar_smallint", columnDefinition = SMALLINT)
  private Short barSmallint;

  @Column(name = "bar_integer", columnDefinition = INTEGER)
  private Integer barInteger;

  @Column(name = "bar_bigint", columnDefinition = BIGINT)
  private Long barBigint;

  @Column(name = "bar_real", columnDefinition = REAL)
  private Float barReal;

  @Column(name = "bar_double", columnDefinition = DOUBLE)
  private Double barDouble;

  @Column(name = "bar_decimal", columnDefinition = DECIMAL)
  private BigDecimal barDecimal;

  @Column(name = "bar_boolean", columnDefinition = BOOLEAN)
  private Boolean barBoolean;

  @Column(name = "bar_date", columnDefinition = DATE)
  private LocalDate barDate;

  @Column(name = "bar_timestamp", columnDefinition = TIMESTAMP)
  private OffsetDateTime barTimestamp;
}
