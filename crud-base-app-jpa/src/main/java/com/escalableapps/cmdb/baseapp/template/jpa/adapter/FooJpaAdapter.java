package com.escalableapps.cmdb.baseapp.template.jpa.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpa.helper.FooJpaHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpa.mapper.FooEntityMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.mapper.FooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpa.repository.FooJpaRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class FooJpaAdapter implements FooSpi {

  private final FooJpaRepository repository;
  private final EntityManager entityManager;

  public FooJpaAdapter(FooJpaRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoofoo={}", foo);

    FooEntity fooEntity = repository.save(fromEntity(foo));

    foo.setFooId(fooEntity.getFooId());
    log.debug("createFoofoo={}", foo);
    return foo;
  }

  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    FooEntity foo = repository.findById(fooId).orElse(null);

    log.debug("findFoo:foo={}", foo);

    return Optional.ofNullable(toEntity(foo));
  }

  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);

    List<FooEntity> foos = repository.findAllById(fooIds);
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery //
  ) {
    return findFoos(fooQuery, new PageRequest<>());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<FooEntity> query = criteriaBuilder.createQuery(FooEntity.class);
    Root<FooEntity> root = query.from(FooEntity.class);

    List<FooEntity> foos = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, fooQuery)) //
            .orderBy(orderBy(criteriaBuilder, root, paging.getSortRequests())) //
        ) //
        .setFirstResult(paging.getOffset()) //
        .setMaxResults(paging.getPageSize()) //
        .getResultList();

    log.debug("findFoos:foos={}", foos);
    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = repository.existsById(fooId);

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoos:fooQuery={}", fooQuery);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<FooEntity> query = criteriaBuilder.createQuery(FooEntity.class);
    Root<FooEntity> root = query.from(FooEntity.class);

    List<FooEntity> foos = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, fooQuery)) //
        ) //
        .setFirstResult(0) //
        .setMaxResults(1) //
        .getResultList();

    boolean exists = !foos.isEmpty();

    log.debug("existsFoos:exists={}", exists);
    return exists;
  }

  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("countFoos:fooQuery={}", fooQuery);

    long count = repository.count((root, query, criteriaBuilder) -> criteriaBuilder.and(whereCondition(criteriaBuilder, root, fooQuery)));

    log.debug("countFoos:count={}", count);
    return count;
  }

  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("updateFoo:fooId={}", fooId);
    log.debug("updateFoo:fooUpdate={}", fooUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<FooEntity> update = criteriaBuilder.createCriteriaUpdate(FooEntity.class);
    Root<FooEntity> root = update.from(FooEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, fooUpdate) //
            .where(whereCondition(criteriaBuilder, root, fooId)) //
    ).executeUpdate();

    log.debug("updateFoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("deleteFoo:fooId={}", fooId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<FooEntity> delete = criteriaBuilder.createCriteriaDelete(FooEntity.class);
    Root<FooEntity> root = delete.from(FooEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, fooId)) //
    ).executeUpdate();

    log.debug("deleteFoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
