package com.escalableapps.cmdb.baseapp.template.jpa.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.FooPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public final class FooJpaHelper {

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<FooEntity> root, //
      Integer fooId //
  ) {
    FooQuery fooQuery = new FooQuery();
    fooQuery.setFooId(fooId);
    return whereCondition(criteriaBuilder, root, fooQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForFooId(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooChar(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooVarchar(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooText(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooSmallint(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooInteger(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooBigint(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooReal(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDouble(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDecimal(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooBoolean(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDate(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooTimestamp(predicates, criteriaBuilder, root, fooQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForFooId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooId() != null) {
      if (!fooQuery.getFooId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_ID.getPropertyName()), fooQuery.getFooId().get()));
      }
    }
  }

  private static void whereConditionForFooChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooChar() != null) {
      if (!fooQuery.getFooChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_CHAR.getPropertyName()), fooQuery.getFooChar().get()));
      }
    }
  }

  private static void whereConditionForFooVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooVarchar() != null) {
      if (!fooQuery.getFooVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_VARCHAR.getPropertyName()), fooQuery.getFooVarchar().get()));
      }
    }
  }

  private static void whereConditionForFooText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooText() != null) {
      if (!fooQuery.getFooText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_TEXT.getPropertyName()), fooQuery.getFooText().get()));
      }
    }
  }

  private static void whereConditionForFooSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooSmallint() != null) {
      if (!fooQuery.getFooSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_SMALLINT.getPropertyName()), fooQuery.getFooSmallint().get()));
      }
    }
  }

  private static void whereConditionForFooInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooInteger() != null) {
      if (!fooQuery.getFooInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_INTEGER.getPropertyName()), fooQuery.getFooInteger().get()));
      }
    }
  }

  private static void whereConditionForFooBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooBigint() != null) {
      if (!fooQuery.getFooBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_BIGINT.getPropertyName()), fooQuery.getFooBigint().get()));
      }
    }
  }

  private static void whereConditionForFooReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooReal() != null) {
      if (!fooQuery.getFooReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_REAL.getPropertyName()), fooQuery.getFooReal().get()));
      }
    }
  }

  private static void whereConditionForFooDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDouble() != null) {
      if (!fooQuery.getFooDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DOUBLE.getPropertyName()), fooQuery.getFooDouble().get()));
      }
    }
  }

  private static void whereConditionForFooDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDecimal() != null) {
      if (!fooQuery.getFooDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DECIMAL.getPropertyName()), fooQuery.getFooDecimal().get()));
      }
    }
  }

  private static void whereConditionForFooBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooBoolean() != null) {
      if (!fooQuery.getFooBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_BOOLEAN.getPropertyName()), fooQuery.getFooBoolean().get()));
      }
    }
  }

  private static void whereConditionForFooDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDate() != null) {
      if (!fooQuery.getFooDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DATE.getPropertyName()), fooQuery.getFooDate().get()));
      }
    } else if (fooQuery.getFooDateMin() != null || fooQuery.getFooDateMax() != null) {
      if (fooQuery.getFooDateMin() != null && fooQuery.getFooDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FOO_DATE.getPropertyName()), fooQuery.getFooDateMin().get()));
      }
      if (fooQuery.getFooDateMax() != null && fooQuery.getFooDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FOO_DATE.getPropertyName()), fooQuery.getFooDateMax().get()));
      }
    }
  }

  private static void whereConditionForFooTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooTimestamp() != null) {
      if (!fooQuery.getFooTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_TIMESTAMP.getPropertyName()), fooQuery.getFooTimestamp().get()));
      }
    } else if (fooQuery.getFooTimestampMin() != null || fooQuery.getFooTimestampMax() != null) {
      if (fooQuery.getFooTimestampMin() != null && fooQuery.getFooTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FOO_TIMESTAMP.getPropertyName()), fooQuery.getFooTimestampMin().get()));
      }
      if (fooQuery.getFooTimestampMax() != null && fooQuery.getFooTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FOO_TIMESTAMP.getPropertyName()), fooQuery.getFooTimestampMax().get()));
      }
    }
  }

  public static List<Order> orderBy(CriteriaBuilder criteriaBuilder, Root<FooEntity> root, List<SortRequest<FooPropertyName>> sorts) {
    List<Order> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForFooId(orders, criteriaBuilder, root, sort);
      orderByForFooChar(orders, criteriaBuilder, root, sort);
      orderByForFooVarchar(orders, criteriaBuilder, root, sort);
      orderByForFooText(orders, criteriaBuilder, root, sort);
      orderByForFooSmallint(orders, criteriaBuilder, root, sort);
      orderByForFooInteger(orders, criteriaBuilder, root, sort);
      orderByForFooBigint(orders, criteriaBuilder, root, sort);
      orderByForFooReal(orders, criteriaBuilder, root, sort);
      orderByForFooDouble(orders, criteriaBuilder, root, sort);
      orderByForFooDecimal(orders, criteriaBuilder, root, sort);
      orderByForFooBoolean(orders, criteriaBuilder, root, sort);
      orderByForFooDate(orders, criteriaBuilder, root, sort);
      orderByForFooTimestamp(orders, criteriaBuilder, root, sort);
    });
    return orders;
  }

  private static void orderByForFooId(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_ID) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_ID.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_ID.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooChar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_CHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_CHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_CHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooVarchar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_VARCHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_VARCHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooText(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TEXT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_TEXT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_TEXT.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooSmallint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_SMALLINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_SMALLINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooInteger(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_INTEGER) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_INTEGER.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_INTEGER.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooBigint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BIGINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_BIGINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_BIGINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooReal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_REAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_REAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_REAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooDouble(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_DOUBLE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_DOUBLE.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooDecimal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_DECIMAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_DECIMAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooBoolean(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_BOOLEAN.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_BOOLEAN.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooDate(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DATE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_DATE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_DATE.getPropertyName())) //
      );
    }
  }

  private static void orderByForFooTimestamp(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(FOO_TIMESTAMP.getPropertyName())) //
          : criteriaBuilder.desc(root.get(FOO_TIMESTAMP.getPropertyName())) //
      );
    }
  }

  public static CriteriaUpdate<FooEntity> updateValues(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    updateValuesForFooChar(update, root, fooUpdate);
    updateValuesForFooVarchar(update, root, fooUpdate);
    updateValuesForFooText(update, root, fooUpdate);
    updateValuesForFooSmallint(update, root, fooUpdate);
    updateValuesForFooInteger(update, root, fooUpdate);
    updateValuesForFooBigint(update, root, fooUpdate);
    updateValuesForFooReal(update, root, fooUpdate);
    updateValuesForFooDouble(update, root, fooUpdate);
    updateValuesForFooDecimal(update, root, fooUpdate);
    updateValuesForFooBoolean(update, root, fooUpdate);
    updateValuesForFooDate(update, root, fooUpdate);
    updateValuesForFooTimestamp(update, root, fooUpdate);
    return update;
  }

  private static void updateValuesForFooChar(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooChar() != null) {
      update.set(root.get(FOO_CHAR.getPropertyName()), getValue(fooUpdate.getFooChar()));
    }
  }

  private static void updateValuesForFooVarchar(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooVarchar() != null) {
      update.set(root.get(FOO_VARCHAR.getPropertyName()), getValue(fooUpdate.getFooVarchar()));
    }
  }

  private static void updateValuesForFooText(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooText() != null) {
      update.set(root.get(FOO_TEXT.getPropertyName()), getValue(fooUpdate.getFooText()));
    }
  }

  private static void updateValuesForFooSmallint(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooSmallint() != null) {
      update.set(root.get(FOO_SMALLINT.getPropertyName()), getValue(fooUpdate.getFooSmallint()));
    }
  }

  private static void updateValuesForFooInteger(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooInteger() != null) {
      update.set(root.get(FOO_INTEGER.getPropertyName()), getValue(fooUpdate.getFooInteger()));
    }
  }

  private static void updateValuesForFooBigint(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBigint() != null) {
      update.set(root.get(FOO_BIGINT.getPropertyName()), getValue(fooUpdate.getFooBigint()));
    }
  }

  private static void updateValuesForFooReal(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooReal() != null) {
      update.set(root.get(FOO_REAL.getPropertyName()), getValue(fooUpdate.getFooReal()));
    }
  }

  private static void updateValuesForFooDouble(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDouble() != null) {
      update.set(root.get(FOO_DOUBLE.getPropertyName()), getValue(fooUpdate.getFooDouble()));
    }
  }

  private static void updateValuesForFooDecimal(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDecimal() != null) {
      update.set(root.get(FOO_DECIMAL.getPropertyName()), getValue(fooUpdate.getFooDecimal()));
    }
  }

  private static void updateValuesForFooBoolean(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBoolean() != null) {
      update.set(root.get(FOO_BOOLEAN.getPropertyName()), getValue(fooUpdate.getFooBoolean()));
    }
  }

  private static void updateValuesForFooDate(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDate() != null) {
      update.set(root.get(FOO_DATE.getPropertyName()), getValue(fooUpdate.getFooDate()));
    }
  }

  private static void updateValuesForFooTimestamp(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooTimestamp() != null) {
      update.set(root.get(FOO_TIMESTAMP.getPropertyName()), getValue(fooUpdate.getFooTimestamp()));
    }
  }

  private static <T extends Serializable> T getValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private FooJpaHelper() {
  }
}
