package com.escalableapps.cmdb.baseapp.template.jpa.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.LookPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public final class LookJpaHelper {

  private static final String EMBEDDED_ID = "id";

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<LookEntity> root, //
      Long lookOrg, //
      Long lookId //
  ) {
    LookQuery lookQuery = new LookQuery();
    lookQuery.setLookOrg(lookOrg);
    lookQuery.setLookId(lookId);
    return whereCondition(criteriaBuilder, root, lookQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForLookOrg(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookId(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookChar(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookVarchar(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookText(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookSmallint(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookInteger(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookBigint(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookReal(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDouble(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDecimal(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookBoolean(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDate(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookTimestamp(predicates, criteriaBuilder, root, lookQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForLookOrg(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookOrg() != null) {
      if (!lookQuery.getLookOrg().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName()), lookQuery.getLookOrg().get()));
      }
    }
  }

  private static void whereConditionForLookId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookId() != null) {
      if (!lookQuery.getLookId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName()), lookQuery.getLookId().get()));
      }
    }
  }

  private static void whereConditionForLookChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookChar() != null) {
      if (!lookQuery.getLookChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_CHAR.getPropertyName()), lookQuery.getLookChar().get()));
      }
    }
  }

  private static void whereConditionForLookVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookVarchar() != null) {
      if (!lookQuery.getLookVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_VARCHAR.getPropertyName()), lookQuery.getLookVarchar().get()));
      }
    }
  }

  private static void whereConditionForLookText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookText() != null) {
      if (!lookQuery.getLookText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_TEXT.getPropertyName()), lookQuery.getLookText().get()));
      }
    }
  }

  private static void whereConditionForLookSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookSmallint() != null) {
      if (!lookQuery.getLookSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_SMALLINT.getPropertyName()), lookQuery.getLookSmallint().get()));
      }
    }
  }

  private static void whereConditionForLookInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookInteger() != null) {
      if (!lookQuery.getLookInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_INTEGER.getPropertyName()), lookQuery.getLookInteger().get()));
      }
    }
  }

  private static void whereConditionForLookBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookBigint() != null) {
      if (!lookQuery.getLookBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_BIGINT.getPropertyName()), lookQuery.getLookBigint().get()));
      }
    }
  }

  private static void whereConditionForLookReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookReal() != null) {
      if (!lookQuery.getLookReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_REAL.getPropertyName()), lookQuery.getLookReal().get()));
      }
    }
  }

  private static void whereConditionForLookDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDouble() != null) {
      if (!lookQuery.getLookDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DOUBLE.getPropertyName()), lookQuery.getLookDouble().get()));
      }
    }
  }

  private static void whereConditionForLookDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDecimal() != null) {
      if (!lookQuery.getLookDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DECIMAL.getPropertyName()), lookQuery.getLookDecimal().get()));
      }
    }
  }

  private static void whereConditionForLookBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookBoolean() != null) {
      if (!lookQuery.getLookBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_BOOLEAN.getPropertyName()), lookQuery.getLookBoolean().get()));
      }
    }
  }

  private static void whereConditionForLookDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDate() != null) {
      if (!lookQuery.getLookDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DATE.getPropertyName()), lookQuery.getLookDate().get()));
      }
    } else if (lookQuery.getLookDateMin() != null || lookQuery.getLookDateMax() != null) {
      if (lookQuery.getLookDateMin() != null && lookQuery.getLookDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LOOK_DATE.getPropertyName()), lookQuery.getLookDateMin().get()));
      }
      if (lookQuery.getLookDateMax() != null && lookQuery.getLookDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LOOK_DATE.getPropertyName()), lookQuery.getLookDateMax().get()));
      }
    }
  }

  private static void whereConditionForLookTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookTimestamp() != null) {
      if (!lookQuery.getLookTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_TIMESTAMP.getPropertyName()), lookQuery.getLookTimestamp().get()));
      }
    } else if (lookQuery.getLookTimestampMin() != null || lookQuery.getLookTimestampMax() != null) {
      if (lookQuery.getLookTimestampMin() != null && lookQuery.getLookTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LOOK_TIMESTAMP.getPropertyName()), lookQuery.getLookTimestampMin().get()));
      }
      if (lookQuery.getLookTimestampMax() != null && lookQuery.getLookTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LOOK_TIMESTAMP.getPropertyName()), lookQuery.getLookTimestampMax().get()));
      }
    }
  }

  public static List<Order> orderBy(CriteriaBuilder criteriaBuilder, Root<LookEntity> root, List<SortRequest<LookPropertyName>> sorts) {
    List<Order> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForLookOrg(orders, criteriaBuilder, root, sort);
      orderByForLookId(orders, criteriaBuilder, root, sort);
      orderByForLookChar(orders, criteriaBuilder, root, sort);
      orderByForLookVarchar(orders, criteriaBuilder, root, sort);
      orderByForLookText(orders, criteriaBuilder, root, sort);
      orderByForLookSmallint(orders, criteriaBuilder, root, sort);
      orderByForLookInteger(orders, criteriaBuilder, root, sort);
      orderByForLookBigint(orders, criteriaBuilder, root, sort);
      orderByForLookReal(orders, criteriaBuilder, root, sort);
      orderByForLookDouble(orders, criteriaBuilder, root, sort);
      orderByForLookDecimal(orders, criteriaBuilder, root, sort);
      orderByForLookBoolean(orders, criteriaBuilder, root, sort);
      orderByForLookDate(orders, criteriaBuilder, root, sort);
      orderByForLookTimestamp(orders, criteriaBuilder, root, sort);
    });
    return orders;
  }

  private static void orderByForLookOrg(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ORG) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName())) //
          : criteriaBuilder.desc(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookId(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ID) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName())) //
          : criteriaBuilder.desc(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookChar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_CHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_CHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_CHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookVarchar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_VARCHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_VARCHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookText(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TEXT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_TEXT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_TEXT.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookSmallint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_SMALLINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_SMALLINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookInteger(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_INTEGER) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_INTEGER.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_INTEGER.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookBigint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BIGINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_BIGINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_BIGINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookReal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_REAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_REAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_REAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookDouble(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_DOUBLE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_DOUBLE.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookDecimal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_DECIMAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_DECIMAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookBoolean(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_BOOLEAN.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_BOOLEAN.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookDate(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DATE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_DATE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_DATE.getPropertyName())) //
      );
    }
  }

  private static void orderByForLookTimestamp(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(LOOK_TIMESTAMP.getPropertyName())) //
          : criteriaBuilder.desc(root.get(LOOK_TIMESTAMP.getPropertyName())) //
      );
    }
  }

  public static CriteriaUpdate<LookEntity> updateValues(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    updateValuesForLookChar(update, root, lookUpdate);
    updateValuesForLookVarchar(update, root, lookUpdate);
    updateValuesForLookText(update, root, lookUpdate);
    updateValuesForLookSmallint(update, root, lookUpdate);
    updateValuesForLookInteger(update, root, lookUpdate);
    updateValuesForLookBigint(update, root, lookUpdate);
    updateValuesForLookReal(update, root, lookUpdate);
    updateValuesForLookDouble(update, root, lookUpdate);
    updateValuesForLookDecimal(update, root, lookUpdate);
    updateValuesForLookBoolean(update, root, lookUpdate);
    updateValuesForLookDate(update, root, lookUpdate);
    updateValuesForLookTimestamp(update, root, lookUpdate);
    return update;
  }

  private static void updateValuesForLookChar(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookChar() != null) {
      update.set(root.get(LOOK_CHAR.getPropertyName()), getValue(lookUpdate.getLookChar()));
    }
  }

  private static void updateValuesForLookVarchar(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookVarchar() != null) {
      update.set(root.get(LOOK_VARCHAR.getPropertyName()), getValue(lookUpdate.getLookVarchar()));
    }
  }

  private static void updateValuesForLookText(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookText() != null) {
      update.set(root.get(LOOK_TEXT.getPropertyName()), getValue(lookUpdate.getLookText()));
    }
  }

  private static void updateValuesForLookSmallint(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookSmallint() != null) {
      update.set(root.get(LOOK_SMALLINT.getPropertyName()), getValue(lookUpdate.getLookSmallint()));
    }
  }

  private static void updateValuesForLookInteger(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookInteger() != null) {
      update.set(root.get(LOOK_INTEGER.getPropertyName()), getValue(lookUpdate.getLookInteger()));
    }
  }

  private static void updateValuesForLookBigint(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBigint() != null) {
      update.set(root.get(LOOK_BIGINT.getPropertyName()), getValue(lookUpdate.getLookBigint()));
    }
  }

  private static void updateValuesForLookReal(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookReal() != null) {
      update.set(root.get(LOOK_REAL.getPropertyName()), getValue(lookUpdate.getLookReal()));
    }
  }

  private static void updateValuesForLookDouble(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDouble() != null) {
      update.set(root.get(LOOK_DOUBLE.getPropertyName()), getValue(lookUpdate.getLookDouble()));
    }
  }

  private static void updateValuesForLookDecimal(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDecimal() != null) {
      update.set(root.get(LOOK_DECIMAL.getPropertyName()), getValue(lookUpdate.getLookDecimal()));
    }
  }

  private static void updateValuesForLookBoolean(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBoolean() != null) {
      update.set(root.get(LOOK_BOOLEAN.getPropertyName()), getValue(lookUpdate.getLookBoolean()));
    }
  }

  private static void updateValuesForLookDate(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDate() != null) {
      update.set(root.get(LOOK_DATE.getPropertyName()), getValue(lookUpdate.getLookDate()));
    }
  }

  private static void updateValuesForLookTimestamp(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookTimestamp() != null) {
      update.set(root.get(LOOK_TIMESTAMP.getPropertyName()), getValue(lookUpdate.getLookTimestamp()));
    }
  }

  private static <T extends Serializable> T getValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private LookJpaHelper() {
  }
}
