package com.escalableapps.cmdb.baseapp.template.jpa.persistence;

import static com.escalableapps.cmdb.baseapp.shared.jpa.SqlTypes.*;

import java.io.Serializable;
import java.math.*;
import java.time.*;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "crud_base_db", name = "zoo")
public class ZooEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @EqualsAndHashCode.Include
  @EmbeddedId
  private ZooPk id;

  @Column(name = "zoo_char", columnDefinition = CHAR)
  private String zooChar;

  @Column(name = "zoo_varchar", columnDefinition = VARCHAR)
  private String zooVarchar;

  @Column(name = "zoo_text", columnDefinition = TEXT)
  private String zooText;

  @Column(name = "zoo_smallint", columnDefinition = SMALLINT)
  private Short zooSmallint;

  @Column(name = "zoo_integer", columnDefinition = INTEGER)
  private Integer zooInteger;

  @Column(name = "zoo_bigint", columnDefinition = BIGINT)
  private Long zooBigint;

  @Column(name = "zoo_real", columnDefinition = REAL)
  private Float zooReal;

  @Column(name = "zoo_double", columnDefinition = DOUBLE)
  private Double zooDouble;

  @Column(name = "zoo_decimal", columnDefinition = DECIMAL)
  private BigDecimal zooDecimal;

  @Column(name = "zoo_boolean", columnDefinition = BOOLEAN)
  private Boolean zooBoolean;

  @Column(name = "zoo_date", columnDefinition = DATE)
  private LocalDate zooDate;

  @Column(name = "zoo_timestamp", columnDefinition = TIMESTAMP)
  private OffsetDateTime zooTimestamp;
}
