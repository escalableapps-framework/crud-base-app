package com.escalableapps.cmdb.baseapp.template.jpa.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpa.helper.LookJpaHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpa.mapper.LookEntityMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.mapper.LookEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpa.repository.LookJpaRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.LookSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class LookJpaAdapter implements LookSpi {

  private final LookJpaRepository repository;
  private final EntityManager entityManager;

  public LookJpaAdapter(LookJpaRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Look createLook( //
      Look look //
  ) {
    log.debug("createLooklook={}", look);

    Long lookId = repository.nextVal();
    LookEntity lookEntity = repository.save(fromEntity(look, lookId));

    look.setLookId(lookEntity.getId().getLookId());
    log.debug("createLooklook={}", look);
    return look;
  }

  @Override
  public Optional<Look> findLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("findLook:lookOrg={}", lookOrg);
    log.debug("findLook:lookId={}", lookId);

    LookEntity look = repository.findById(new LookPk(lookOrg, lookId)).orElse(null);

    log.debug("findLook:look={}", look);

    return Optional.ofNullable(toEntity(look));
  }

  @Override
  public List<Look> findLooks( //
      Long lookOrg, //
      List<Long> lookIds //
  ) {
    log.debug("findLooks:lookOrg={}", lookOrg);
    log.debug("findLooks:lookIds={}", lookIds);

    List<LookEntity> looks = repository.findAllById(lookIds.stream().map(lookId -> new LookPk(lookOrg, lookId)).collect(Collectors.toList()));
    log.debug("findLooks:looks={}", looks);

    return looks.stream().map(LookEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery //
  ) {
    return findLooks(lookQuery, new PageRequest<>());
  }

  @Override
  public List<Look> findLooks( //
      LookQuery lookQuery, //
      PageRequest<LookPropertyName> paging //
  ) {
    log.debug("findLooks:lookQuery={}", lookQuery);
    log.debug("findLooks:paging={}", paging);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<LookEntity> query = criteriaBuilder.createQuery(LookEntity.class);
    Root<LookEntity> root = query.from(LookEntity.class);

    List<LookEntity> looks = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, lookQuery)) //
            .orderBy(orderBy(criteriaBuilder, root, paging.getSortRequests())) //
        ) //
        .setFirstResult(paging.getOffset()) //
        .setMaxResults(paging.getPageSize()) //
        .getResultList();

    log.debug("findLooks:looks={}", looks);
    return looks.stream().map(LookEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("existsLook:lookOrg={}", lookOrg);
    log.debug("existsLook:lookId={}", lookId);

    boolean exists = repository.existsById(new LookPk(lookOrg, lookId));

    log.debug("existsLook:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("existsLooks:lookQuery={}", lookQuery);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<LookEntity> query = criteriaBuilder.createQuery(LookEntity.class);
    Root<LookEntity> root = query.from(LookEntity.class);

    List<LookEntity> looks = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, lookQuery)) //
        ) //
        .setFirstResult(0) //
        .setMaxResults(1) //
        .getResultList();

    boolean exists = !looks.isEmpty();

    log.debug("existsLooks:exists={}", exists);
    return exists;
  }

  @Override
  public long countLooks( //
      LookQuery lookQuery //
  ) {
    log.debug("countLooks:lookQuery={}", lookQuery);

    long count = repository.count((root, query, criteriaBuilder) -> criteriaBuilder.and(whereCondition(criteriaBuilder, root, lookQuery)));

    log.debug("countLooks:count={}", count);
    return count;
  }

  @Override
  public long updateLook( //
      Long lookOrg, //
      Long lookId, //
      LookUpdate lookUpdate //
  ) {
    log.debug("updateLook:lookOrg={}", lookOrg);
    log.debug("updateLook:lookId={}", lookId);
    log.debug("updateLook:lookUpdate={}", lookUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<LookEntity> update = criteriaBuilder.createCriteriaUpdate(LookEntity.class);
    Root<LookEntity> root = update.from(LookEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, lookUpdate) //
            .where(whereCondition(criteriaBuilder, root, lookOrg, lookId)) //
    ).executeUpdate();

    log.debug("updateLook:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteLook( //
      Long lookOrg, //
      Long lookId //
  ) {
    log.debug("deleteLook:lookOrg={}", lookOrg);
    log.debug("deleteLook:lookId={}", lookId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<LookEntity> delete = criteriaBuilder.createCriteriaDelete(LookEntity.class);
    Root<LookEntity> root = delete.from(LookEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, lookOrg, lookId)) //
    ).executeUpdate();

    log.debug("deleteLook:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
