package com.escalableapps.cmdb.baseapp.template.jpa.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpa.helper.BarJpaHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpa.mapper.BarEntityMapper.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.mapper.BarEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpa.repository.BarJpaRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class BarJpaAdapter implements BarSpi {

  private final BarJpaRepository repository;
  private final EntityManager entityManager;

  public BarJpaAdapter(BarJpaRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBarbar={}", bar);

    BarEntity barEntity = repository.save(fromEntity(bar));

    bar.setBarId(barEntity.getBarId());
    log.debug("createBarbar={}", bar);
    return bar;
  }

  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    BarEntity bar = repository.findById(barId).orElse(null);

    log.debug("findBar:bar={}", bar);

    return Optional.ofNullable(toEntity(bar));
  }

  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);

    List<BarEntity> bars = repository.findAllById(barIds);
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery //
  ) {
    return findBars(barQuery, new PageRequest<>());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<BarEntity> query = criteriaBuilder.createQuery(BarEntity.class);
    Root<BarEntity> root = query.from(BarEntity.class);

    List<BarEntity> bars = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, barQuery)) //
            .orderBy(orderBy(criteriaBuilder, root, paging.getSortRequests())) //
        ) //
        .setFirstResult(paging.getOffset()) //
        .setMaxResults(paging.getPageSize()) //
        .getResultList();

    log.debug("findBars:bars={}", bars);
    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = repository.existsById(barId);

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBars:barQuery={}", barQuery);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaQuery<BarEntity> query = criteriaBuilder.createQuery(BarEntity.class);
    Root<BarEntity> root = query.from(BarEntity.class);

    List<BarEntity> bars = entityManager //
        .createQuery(query //
            .where(whereCondition(criteriaBuilder, root, barQuery)) //
        ) //
        .setFirstResult(0) //
        .setMaxResults(1) //
        .getResultList();

    boolean exists = !bars.isEmpty();

    log.debug("existsBars:exists={}", exists);
    return exists;
  }

  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("countBars:barQuery={}", barQuery);

    long count = repository.count((root, query, criteriaBuilder) -> criteriaBuilder.and(whereCondition(criteriaBuilder, root, barQuery)));

    log.debug("countBars:count={}", count);
    return count;
  }

  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("updateBar:barId={}", barId);
    log.debug("updateBar:barUpdate={}", barUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<BarEntity> update = criteriaBuilder.createCriteriaUpdate(BarEntity.class);
    Root<BarEntity> root = update.from(BarEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, barUpdate) //
            .where(whereCondition(criteriaBuilder, root, barId)) //
    ).executeUpdate();

    log.debug("updateBar:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("deleteBar:barId={}", barId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<BarEntity> delete = criteriaBuilder.createCriteriaDelete(BarEntity.class);
    Root<BarEntity> root = delete.from(BarEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, barId)) //
    ).executeUpdate();

    log.debug("deleteBar:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
