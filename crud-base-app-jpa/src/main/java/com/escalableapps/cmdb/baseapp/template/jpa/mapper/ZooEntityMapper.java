package com.escalableapps.cmdb.baseapp.template.jpa.mapper;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public final class ZooEntityMapper {

  public static ZooEntity fromEntity(Zoo zoo, Integer zooId) {
    if (zoo == null) {
      return null;
    }
    ZooPk id = new ZooPk();
    id.setZooOrg(zoo.getZooOrg());
    id.setZooId(zooId);
    ZooEntity zooEntity = new ZooEntity();
    zooEntity.setId(id);
    zooEntity.setZooChar(zoo.getZooChar());
    zooEntity.setZooVarchar(zoo.getZooVarchar());
    zooEntity.setZooText(zoo.getZooText());
    zooEntity.setZooSmallint(zoo.getZooSmallint());
    zooEntity.setZooInteger(zoo.getZooInteger());
    zooEntity.setZooBigint(zoo.getZooBigint());
    zooEntity.setZooReal(zoo.getZooReal());
    zooEntity.setZooDouble(zoo.getZooDouble());
    zooEntity.setZooDecimal(zoo.getZooDecimal());
    zooEntity.setZooBoolean(zoo.getZooBoolean());
    zooEntity.setZooDate(zoo.getZooDate());
    zooEntity.setZooTimestamp(zoo.getZooTimestamp());
    return zooEntity;
  }

  public static Zoo toEntity(ZooEntity zooEntity) {
    if (zooEntity == null) {
      return null;
    }
    Zoo zoo = new Zoo();
    zoo.setZooOrg(zooEntity.getId().getZooOrg());
    zoo.setZooId(zooEntity.getId().getZooId());
    zoo.setZooChar(zooEntity.getZooChar());
    zoo.setZooVarchar(zooEntity.getZooVarchar());
    zoo.setZooText(zooEntity.getZooText());
    zoo.setZooSmallint(zooEntity.getZooSmallint());
    zoo.setZooInteger(zooEntity.getZooInteger());
    zoo.setZooBigint(zooEntity.getZooBigint());
    zoo.setZooReal(zooEntity.getZooReal());
    zoo.setZooDouble(zooEntity.getZooDouble());
    zoo.setZooDecimal(zooEntity.getZooDecimal());
    zoo.setZooBoolean(zooEntity.getZooBoolean());
    zoo.setZooDate(zooEntity.getZooDate());
    zoo.setZooTimestamp(zooEntity.getZooTimestamp());
    return zoo;
  }

  private ZooEntityMapper() {
  }
}
