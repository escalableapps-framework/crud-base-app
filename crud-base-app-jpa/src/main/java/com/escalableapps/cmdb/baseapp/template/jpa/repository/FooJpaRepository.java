package com.escalableapps.cmdb.baseapp.template.jpa.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public interface FooJpaRepository extends JpaRepository<FooEntity, Integer>, JpaSpecificationExecutor<FooEntity> {

}
