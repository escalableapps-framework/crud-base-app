package com.escalableapps.cmdb.baseapp.template.jpa.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public interface BarJpaRepository extends JpaRepository<BarEntity, Integer>, JpaSpecificationExecutor<BarEntity> {

}
