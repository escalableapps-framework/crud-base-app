package com.escalableapps.cmdb.baseapp.template.jpa.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public interface ZooJpaRepository extends JpaRepository<ZooEntity, ZooPk>, JpaSpecificationExecutor<ZooEntity> {

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.zoo_zoo_id_seq')")
  Integer nextVal();
}
