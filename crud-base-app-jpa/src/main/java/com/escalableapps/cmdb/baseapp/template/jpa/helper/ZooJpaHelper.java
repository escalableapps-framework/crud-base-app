package com.escalableapps.cmdb.baseapp.template.jpa.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.ZooPropertyName.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpa.persistence.*;

public final class ZooJpaHelper {

  private static final String EMBEDDED_ID = "id";

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<ZooEntity> root, //
      Long zooOrg, //
      Integer zooId //
  ) {
    ZooQuery zooQuery = new ZooQuery();
    zooQuery.setZooOrg(zooOrg);
    zooQuery.setZooId(zooId);
    return whereCondition(criteriaBuilder, root, zooQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForZooOrg(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooId(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooChar(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooVarchar(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooText(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooSmallint(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooInteger(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooBigint(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooReal(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDouble(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDecimal(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooBoolean(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDate(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooTimestamp(predicates, criteriaBuilder, root, zooQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForZooOrg(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooOrg() != null) {
      if (!zooQuery.getZooOrg().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName()), zooQuery.getZooOrg().get()));
      }
    }
  }

  private static void whereConditionForZooId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooId() != null) {
      if (!zooQuery.getZooId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName()), zooQuery.getZooId().get()));
      }
    }
  }

  private static void whereConditionForZooChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooChar() != null) {
      if (!zooQuery.getZooChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_CHAR.getPropertyName()), zooQuery.getZooChar().get()));
      }
    }
  }

  private static void whereConditionForZooVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooVarchar() != null) {
      if (!zooQuery.getZooVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_VARCHAR.getPropertyName()), zooQuery.getZooVarchar().get()));
      }
    }
  }

  private static void whereConditionForZooText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooText() != null) {
      if (!zooQuery.getZooText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_TEXT.getPropertyName()), zooQuery.getZooText().get()));
      }
    }
  }

  private static void whereConditionForZooSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooSmallint() != null) {
      if (!zooQuery.getZooSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_SMALLINT.getPropertyName()), zooQuery.getZooSmallint().get()));
      }
    }
  }

  private static void whereConditionForZooInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooInteger() != null) {
      if (!zooQuery.getZooInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_INTEGER.getPropertyName()), zooQuery.getZooInteger().get()));
      }
    }
  }

  private static void whereConditionForZooBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooBigint() != null) {
      if (!zooQuery.getZooBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_BIGINT.getPropertyName()), zooQuery.getZooBigint().get()));
      }
    }
  }

  private static void whereConditionForZooReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooReal() != null) {
      if (!zooQuery.getZooReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_REAL.getPropertyName()), zooQuery.getZooReal().get()));
      }
    }
  }

  private static void whereConditionForZooDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDouble() != null) {
      if (!zooQuery.getZooDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DOUBLE.getPropertyName()), zooQuery.getZooDouble().get()));
      }
    }
  }

  private static void whereConditionForZooDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDecimal() != null) {
      if (!zooQuery.getZooDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DECIMAL.getPropertyName()), zooQuery.getZooDecimal().get()));
      }
    }
  }

  private static void whereConditionForZooBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooBoolean() != null) {
      if (!zooQuery.getZooBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_BOOLEAN.getPropertyName()), zooQuery.getZooBoolean().get()));
      }
    }
  }

  private static void whereConditionForZooDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDate() != null) {
      if (!zooQuery.getZooDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DATE.getPropertyName()), zooQuery.getZooDate().get()));
      }
    } else if (zooQuery.getZooDateMin() != null || zooQuery.getZooDateMax() != null) {
      if (zooQuery.getZooDateMin() != null && zooQuery.getZooDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(ZOO_DATE.getPropertyName()), zooQuery.getZooDateMin().get()));
      }
      if (zooQuery.getZooDateMax() != null && zooQuery.getZooDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(ZOO_DATE.getPropertyName()), zooQuery.getZooDateMax().get()));
      }
    }
  }

  private static void whereConditionForZooTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooTimestamp() != null) {
      if (!zooQuery.getZooTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_TIMESTAMP.getPropertyName()), zooQuery.getZooTimestamp().get()));
      }
    } else if (zooQuery.getZooTimestampMin() != null || zooQuery.getZooTimestampMax() != null) {
      if (zooQuery.getZooTimestampMin() != null && zooQuery.getZooTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(ZOO_TIMESTAMP.getPropertyName()), zooQuery.getZooTimestampMin().get()));
      }
      if (zooQuery.getZooTimestampMax() != null && zooQuery.getZooTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(ZOO_TIMESTAMP.getPropertyName()), zooQuery.getZooTimestampMax().get()));
      }
    }
  }

  public static List<Order> orderBy(CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, List<SortRequest<ZooPropertyName>> sorts) {
    List<Order> orders = new ArrayList<>();
    sorts.forEach(sort -> {
      orderByForZooOrg(orders, criteriaBuilder, root, sort);
      orderByForZooId(orders, criteriaBuilder, root, sort);
      orderByForZooChar(orders, criteriaBuilder, root, sort);
      orderByForZooVarchar(orders, criteriaBuilder, root, sort);
      orderByForZooText(orders, criteriaBuilder, root, sort);
      orderByForZooSmallint(orders, criteriaBuilder, root, sort);
      orderByForZooInteger(orders, criteriaBuilder, root, sort);
      orderByForZooBigint(orders, criteriaBuilder, root, sort);
      orderByForZooReal(orders, criteriaBuilder, root, sort);
      orderByForZooDouble(orders, criteriaBuilder, root, sort);
      orderByForZooDecimal(orders, criteriaBuilder, root, sort);
      orderByForZooBoolean(orders, criteriaBuilder, root, sort);
      orderByForZooDate(orders, criteriaBuilder, root, sort);
      orderByForZooTimestamp(orders, criteriaBuilder, root, sort);
    });
    return orders;
  }

  private static void orderByForZooOrg(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ORG) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName())) //
          : criteriaBuilder.desc(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooId(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ID) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName())) //
          : criteriaBuilder.desc(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooChar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_CHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_CHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_CHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooVarchar(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_VARCHAR.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_VARCHAR.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooText(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TEXT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_TEXT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_TEXT.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooSmallint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_SMALLINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_SMALLINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooInteger(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_INTEGER) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_INTEGER.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_INTEGER.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooBigint(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BIGINT) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_BIGINT.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_BIGINT.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooReal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_REAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_REAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_REAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooDouble(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_DOUBLE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_DOUBLE.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooDecimal(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_DECIMAL.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_DECIMAL.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooBoolean(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_BOOLEAN.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_BOOLEAN.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooDate(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DATE) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_DATE.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_DATE.getPropertyName())) //
      );
    }
  }

  private static void orderByForZooTimestamp(List<Order> orders, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? criteriaBuilder.asc(root.get(ZOO_TIMESTAMP.getPropertyName())) //
          : criteriaBuilder.desc(root.get(ZOO_TIMESTAMP.getPropertyName())) //
      );
    }
  }

  public static CriteriaUpdate<ZooEntity> updateValues(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    updateValuesForZooChar(update, root, zooUpdate);
    updateValuesForZooVarchar(update, root, zooUpdate);
    updateValuesForZooText(update, root, zooUpdate);
    updateValuesForZooSmallint(update, root, zooUpdate);
    updateValuesForZooInteger(update, root, zooUpdate);
    updateValuesForZooBigint(update, root, zooUpdate);
    updateValuesForZooReal(update, root, zooUpdate);
    updateValuesForZooDouble(update, root, zooUpdate);
    updateValuesForZooDecimal(update, root, zooUpdate);
    updateValuesForZooBoolean(update, root, zooUpdate);
    updateValuesForZooDate(update, root, zooUpdate);
    updateValuesForZooTimestamp(update, root, zooUpdate);
    return update;
  }

  private static void updateValuesForZooChar(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooChar() != null) {
      update.set(root.get(ZOO_CHAR.getPropertyName()), getValue(zooUpdate.getZooChar()));
    }
  }

  private static void updateValuesForZooVarchar(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooVarchar() != null) {
      update.set(root.get(ZOO_VARCHAR.getPropertyName()), getValue(zooUpdate.getZooVarchar()));
    }
  }

  private static void updateValuesForZooText(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooText() != null) {
      update.set(root.get(ZOO_TEXT.getPropertyName()), getValue(zooUpdate.getZooText()));
    }
  }

  private static void updateValuesForZooSmallint(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooSmallint() != null) {
      update.set(root.get(ZOO_SMALLINT.getPropertyName()), getValue(zooUpdate.getZooSmallint()));
    }
  }

  private static void updateValuesForZooInteger(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooInteger() != null) {
      update.set(root.get(ZOO_INTEGER.getPropertyName()), getValue(zooUpdate.getZooInteger()));
    }
  }

  private static void updateValuesForZooBigint(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBigint() != null) {
      update.set(root.get(ZOO_BIGINT.getPropertyName()), getValue(zooUpdate.getZooBigint()));
    }
  }

  private static void updateValuesForZooReal(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooReal() != null) {
      update.set(root.get(ZOO_REAL.getPropertyName()), getValue(zooUpdate.getZooReal()));
    }
  }

  private static void updateValuesForZooDouble(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDouble() != null) {
      update.set(root.get(ZOO_DOUBLE.getPropertyName()), getValue(zooUpdate.getZooDouble()));
    }
  }

  private static void updateValuesForZooDecimal(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDecimal() != null) {
      update.set(root.get(ZOO_DECIMAL.getPropertyName()), getValue(zooUpdate.getZooDecimal()));
    }
  }

  private static void updateValuesForZooBoolean(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBoolean() != null) {
      update.set(root.get(ZOO_BOOLEAN.getPropertyName()), getValue(zooUpdate.getZooBoolean()));
    }
  }

  private static void updateValuesForZooDate(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDate() != null) {
      update.set(root.get(ZOO_DATE.getPropertyName()), getValue(zooUpdate.getZooDate()));
    }
  }

  private static void updateValuesForZooTimestamp(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooTimestamp() != null) {
      update.set(root.get(ZOO_TIMESTAMP.getPropertyName()), getValue(zooUpdate.getZooTimestamp()));
    }
  }

  private static <T extends Serializable> T getValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private ZooJpaHelper() {
  }
}
