package com.escalableapps.cmdb.baseapp.template.jpql.mapper;

import java.util.Optional;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class BarEntityMapper {

  public static BarEntity fromEntity(Bar bar) {
    if (bar == null) {
      return null;
    }
    BarEntity barEntity = new BarEntity();
    barEntity.setBarId(bar.getBarId());
    barEntity.setBarChar(bar.getBarChar());
    barEntity.setBarVarchar(bar.getBarVarchar());
    barEntity.setBarText(bar.getBarText());
    barEntity.setBarSmallint(bar.getBarSmallint());
    barEntity.setBarInteger(bar.getBarInteger());
    barEntity.setBarBigint(bar.getBarBigint());
    barEntity.setBarReal(bar.getBarReal());
    barEntity.setBarDouble(bar.getBarDouble());
    barEntity.setBarDecimal(bar.getBarDecimal());
    barEntity.setBarBoolean(bar.getBarBoolean());
    barEntity.setBarDate(bar.getBarDate());
    barEntity.setBarTimestamp(bar.getBarTimestamp());
    return barEntity;
  }

  public static Optional<Bar> toEntity(Optional<BarEntity> optional) {
    if (!optional.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(toEntity(optional.get()));
  }

  public static Bar toEntity(BarEntity barEntity) {
    if (barEntity == null) {
      return null;
    }
    Bar bar = new Bar();
    bar.setBarId(barEntity.getBarId());
    bar.setBarChar(barEntity.getBarChar());
    bar.setBarVarchar(barEntity.getBarVarchar());
    bar.setBarText(barEntity.getBarText());
    bar.setBarSmallint(barEntity.getBarSmallint());
    bar.setBarInteger(barEntity.getBarInteger());
    bar.setBarBigint(barEntity.getBarBigint());
    bar.setBarReal(barEntity.getBarReal());
    bar.setBarDouble(barEntity.getBarDouble());
    bar.setBarDecimal(barEntity.getBarDecimal());
    bar.setBarBoolean(barEntity.getBarBoolean());
    bar.setBarDate(barEntity.getBarDate());
    bar.setBarTimestamp(barEntity.getBarTimestamp());
    return bar;
  }

  private BarEntityMapper() {
  }
}
