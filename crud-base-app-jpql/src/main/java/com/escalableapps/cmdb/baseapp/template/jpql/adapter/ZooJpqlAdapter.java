package com.escalableapps.cmdb.baseapp.template.jpql.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpql.helper.ZooJpqlHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpql.mapper.ZooEntityMapper.*;
import static org.springframework.data.domain.PageRequest.of;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.mapper.ZooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpql.repository.ZooJpqlRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.ZooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class ZooJpqlAdapter implements ZooSpi {

  private final ZooJpqlRepository repository;
  private final EntityManager entityManager;

  public ZooJpqlAdapter(ZooJpqlRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Zoo createZoo( //
      Zoo zoo //
  ) {
    log.debug("createZoo:zoo={}", zoo);

    Integer zooId = repository.nextVal();
    ZooEntity zooEntity = repository.save(fromEntity(zoo, zooId));

    zoo.setZooId(zooEntity.getId().getZooId());
    log.debug("createZoo:zoo={}", zoo);
    return zoo;
  }

  @Override
  public Optional<Zoo> findZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("findZoo:zooOrg={}", zooOrg);
    log.debug("findZoo:zooId={}", zooId);

    Optional<ZooEntity> zoo = repository.findById(new ZooPk(zooOrg, zooId));

    log.debug("findZoo:zoo={}", zoo);
    return toEntity(zoo);
  }

  @Override
  public List<Zoo> findZoos( //
      Long zooOrg, //
      List<Integer> zooIds //
  ) {
    log.debug("findZoos:zooOrg={}", zooOrg);
    log.debug("findZoos:zooIds={}", zooIds);

    List<ZooEntity> zoos = repository.findAllById(zooIds.stream().map(zooId -> new ZooPk(zooOrg, zooId)).collect(Collectors.toList()));
    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery //
  ) {
    return findZoos(zooQuery, new PageRequest<>());
  }

  @Override
  public List<Zoo> findZoos( //
      ZooQuery zooQuery, //
      PageRequest<ZooPropertyName> paging //
  ) {
    log.debug("findZoos:zooQuery={}", zooQuery);
    log.debug("findZoos:paging={}", paging);

    List<ZooEntity> zoos = repository.findZoos( //
        isNullValue(zooQuery.getZooOrg()), getQueryValue(zooQuery.getZooOrg()), //
        isNullValue(zooQuery.getZooId()), getQueryValue(zooQuery.getZooId()), //
        isNullValue(zooQuery.getZooChar()), getQueryValue(zooQuery.getZooChar()), //
        isNullValue(zooQuery.getZooVarchar()), getQueryValue(zooQuery.getZooVarchar()), //
        isNullValue(zooQuery.getZooText()), getQueryValue(zooQuery.getZooText()), //
        isNullValue(zooQuery.getZooSmallint()), getQueryValue(zooQuery.getZooSmallint()), //
        isNullValue(zooQuery.getZooInteger()), getQueryValue(zooQuery.getZooInteger()), //
        isNullValue(zooQuery.getZooBigint()), getQueryValue(zooQuery.getZooBigint()), //
        isNullValue(zooQuery.getZooReal()), getQueryValue(zooQuery.getZooReal()), //
        isNullValue(zooQuery.getZooDouble()), getQueryValue(zooQuery.getZooDouble()), //
        isNullValue(zooQuery.getZooDecimal()), getQueryValue(zooQuery.getZooDecimal()), //
        isNullValue(zooQuery.getZooBoolean()), getQueryValue(zooQuery.getZooBoolean()), //
        isNullValue(zooQuery.getZooDate()), getQueryValue(zooQuery.getZooDate()), //
        getQueryValue(zooQuery.getZooDateMin()), getQueryValue(zooQuery.getZooDateMax()), //
        isNullValue(zooQuery.getZooTimestamp()), getQueryValue(zooQuery.getZooTimestamp()), //
        getQueryValue(zooQuery.getZooTimestampMin()), getQueryValue(zooQuery.getZooTimestampMax()), //
        orderBy(paging));

    log.debug("findZoos:zoos={}", zoos);

    return zoos.stream().map(ZooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("existsZoo:zooOrg={}", zooOrg);
    log.debug("existsZoo:zooId={}", zooId);

    boolean exists = repository.existsById(new ZooPk(zooOrg, zooId));

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("existsZoo:zooQuery={}", zooQuery);

    boolean exists = !repository.existsZoos( //
        isNullValue(zooQuery.getZooOrg()), getQueryValue(zooQuery.getZooOrg()), //
        isNullValue(zooQuery.getZooId()), getQueryValue(zooQuery.getZooId()), //
        isNullValue(zooQuery.getZooChar()), getQueryValue(zooQuery.getZooChar()), //
        isNullValue(zooQuery.getZooVarchar()), getQueryValue(zooQuery.getZooVarchar()), //
        isNullValue(zooQuery.getZooText()), getQueryValue(zooQuery.getZooText()), //
        isNullValue(zooQuery.getZooSmallint()), getQueryValue(zooQuery.getZooSmallint()), //
        isNullValue(zooQuery.getZooInteger()), getQueryValue(zooQuery.getZooInteger()), //
        isNullValue(zooQuery.getZooBigint()), getQueryValue(zooQuery.getZooBigint()), //
        isNullValue(zooQuery.getZooReal()), getQueryValue(zooQuery.getZooReal()), //
        isNullValue(zooQuery.getZooDouble()), getQueryValue(zooQuery.getZooDouble()), //
        isNullValue(zooQuery.getZooDecimal()), getQueryValue(zooQuery.getZooDecimal()), //
        isNullValue(zooQuery.getZooBoolean()), getQueryValue(zooQuery.getZooBoolean()), //
        isNullValue(zooQuery.getZooDate()), getQueryValue(zooQuery.getZooDate()), //
        getQueryValue(zooQuery.getZooDateMin()), getQueryValue(zooQuery.getZooDateMax()), //
        isNullValue(zooQuery.getZooTimestamp()), getQueryValue(zooQuery.getZooTimestamp()), //
        getQueryValue(zooQuery.getZooTimestampMin()), getQueryValue(zooQuery.getZooTimestampMax()), //
        of(0, 1)).isEmpty();

    log.debug("existsZoo:exists={}", exists);
    return exists;
  }

  @Override
  public long countZoos( //
      ZooQuery zooQuery //
  ) {
    log.debug("countZoos:zooQuery={}", zooQuery);

    long count = repository.countZoos( //
        isNullValue(zooQuery.getZooOrg()), getQueryValue(zooQuery.getZooOrg()), //
        isNullValue(zooQuery.getZooId()), getQueryValue(zooQuery.getZooId()), //
        isNullValue(zooQuery.getZooChar()), getQueryValue(zooQuery.getZooChar()), //
        isNullValue(zooQuery.getZooVarchar()), getQueryValue(zooQuery.getZooVarchar()), //
        isNullValue(zooQuery.getZooText()), getQueryValue(zooQuery.getZooText()), //
        isNullValue(zooQuery.getZooSmallint()), getQueryValue(zooQuery.getZooSmallint()), //
        isNullValue(zooQuery.getZooInteger()), getQueryValue(zooQuery.getZooInteger()), //
        isNullValue(zooQuery.getZooBigint()), getQueryValue(zooQuery.getZooBigint()), //
        isNullValue(zooQuery.getZooReal()), getQueryValue(zooQuery.getZooReal()), //
        isNullValue(zooQuery.getZooDouble()), getQueryValue(zooQuery.getZooDouble()), //
        isNullValue(zooQuery.getZooDecimal()), getQueryValue(zooQuery.getZooDecimal()), //
        isNullValue(zooQuery.getZooBoolean()), getQueryValue(zooQuery.getZooBoolean()), //
        isNullValue(zooQuery.getZooDate()), getQueryValue(zooQuery.getZooDate()), //
        getQueryValue(zooQuery.getZooDateMin()), getQueryValue(zooQuery.getZooDateMax()), //
        isNullValue(zooQuery.getZooTimestamp()), getQueryValue(zooQuery.getZooTimestamp()), //
        getQueryValue(zooQuery.getZooTimestampMin()), getQueryValue(zooQuery.getZooTimestampMax()));

    log.debug("countZoos:count={}", count);
    return count;
  }

  @Override
  public long updateZoo( //
      Long zooOrg, //
      Integer zooId, //
      ZooUpdate zooUpdate //
  ) {
    log.debug("updateZoo:zooOrg={}", zooOrg);
    log.debug("updateZoo:zooId={}", zooId);
    log.debug("updateZoo:zooUpdate={}", zooUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<ZooEntity> update = criteriaBuilder.createCriteriaUpdate(ZooEntity.class);
    Root<ZooEntity> root = update.from(ZooEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, zooUpdate).where(whereCondition(criteriaBuilder, root, zooOrg, zooId)) //
    ).executeUpdate();

    log.debug("updateZoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteZoo( //
      Long zooOrg, //
      Integer zooId //
  ) {
    log.debug("deleteZoo:zooOrg={}", zooOrg);
    log.debug("deleteZoo:zooId={}", zooId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<ZooEntity> delete = criteriaBuilder.createCriteriaDelete(ZooEntity.class);
    Root<ZooEntity> root = delete.from(ZooEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, zooOrg, zooId)) //
    ).executeUpdate();

    log.debug("deleteZoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
