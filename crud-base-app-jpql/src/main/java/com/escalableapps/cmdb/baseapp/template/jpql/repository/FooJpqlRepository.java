package com.escalableapps.cmdb.baseapp.template.jpql.repository;

import java.math.*;
import java.time.*;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.escalableapps.cmdb.baseapp.shared.jpql.ExistsView;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public interface FooJpqlRepository extends JpaRepository<FooEntity, Integer> {

  String FOO_QUERY = //
      "FROM FooEntity AS foo " //
          + "WHERE ((TRUE = :fooIdIsNull AND foo.fooId IS NULL) OR (FALSE = :fooIdIsNull AND (:fooId IS NULL OR foo.fooId = :fooId))) " //
          + "  AND ((TRUE = :fooCharIsNull AND foo.fooChar IS NULL) OR (FALSE = :fooCharIsNull AND (:fooChar IS NULL OR foo.fooChar = :fooChar))) " //
          + "  AND ((TRUE = :fooVarcharIsNull AND foo.fooVarchar IS NULL) OR (FALSE = :fooVarcharIsNull AND (:fooVarchar IS NULL OR foo.fooVarchar = :fooVarchar))) " //
          + "  AND ((TRUE = :fooTextIsNull AND foo.fooText IS NULL) OR (FALSE = :fooTextIsNull AND (:fooText IS NULL OR foo.fooText = :fooText))) " //
          + "  AND ((TRUE = :fooSmallintIsNull AND foo.fooSmallint IS NULL) OR (FALSE = :fooSmallintIsNull AND (:fooSmallint IS NULL OR foo.fooSmallint = :fooSmallint))) " //
          + "  AND ((TRUE = :fooIntegerIsNull AND foo.fooInteger IS NULL) OR (FALSE = :fooIntegerIsNull AND (:fooInteger IS NULL OR foo.fooInteger = :fooInteger))) " //
          + "  AND ((TRUE = :fooBigintIsNull AND foo.fooBigint IS NULL) OR (FALSE = :fooBigintIsNull AND (:fooBigint IS NULL OR foo.fooBigint = :fooBigint))) " //
          + "  AND ((TRUE = :fooRealIsNull AND foo.fooReal IS NULL) OR (FALSE = :fooRealIsNull AND (:fooReal IS NULL OR foo.fooReal = :fooReal))) " //
          + "  AND ((TRUE = :fooDoubleIsNull AND foo.fooDouble IS NULL) OR (FALSE = :fooDoubleIsNull AND (:fooDouble IS NULL OR foo.fooDouble = :fooDouble))) " //
          + "  AND ((TRUE = :fooDecimalIsNull AND foo.fooDecimal IS NULL) OR (FALSE = :fooDecimalIsNull AND (:fooDecimal IS NULL OR foo.fooDecimal = :fooDecimal))) " //
          + "  AND ((TRUE = :fooBooleanIsNull AND foo.fooBoolean IS NULL) OR (FALSE = :fooBooleanIsNull AND (:fooBoolean IS NULL OR foo.fooBoolean = :fooBoolean))) " //
          + "  AND (" //
          + "        ((TRUE = :fooDateIsNull) AND (foo.fooDate IS NULL)) OR " //
          + "        ((FALSE = :fooDateIsNull AND (CAST(:fooDateMin AS java.time.LocalDate) IS NOT NULL OR CAST(:fooDateMax AS java.time.LocalDate) IS NOT NULL)) AND (CAST(:fooDateMin AS java.time.LocalDate) IS NULL OR :fooDateMin <= foo.fooDate) AND (CAST(:fooDateMax AS java.time.LocalDate) IS NULL OR foo.fooDate <= :fooDateMax)) OR " //
          + "        ((FALSE = :fooDateIsNull AND CAST(:fooDateMin AS java.time.LocalDate) IS NULL AND CAST(:fooDateMax AS java.time.LocalDate) IS NULL) AND (CAST(:fooDate AS java.time.LocalDate) IS NULL OR foo.fooDate = :fooDate))" //
          + "      ) " //
          + "  AND (" //
          + "        ((TRUE = :fooTimestampIsNull) AND (foo.fooTimestamp IS NULL)) OR " //
          + "        ((FALSE = :fooTimestampIsNull AND (CAST(:fooTimestampMin AS java.time.OffsetDateTime) IS NOT NULL OR CAST(:fooTimestampMax AS java.time.OffsetDateTime) IS NOT NULL)) AND (CAST(:fooTimestampMin AS java.time.OffsetDateTime) IS NULL OR :fooTimestampMin <= foo.fooTimestamp) AND (CAST(:fooTimestampMax AS java.time.OffsetDateTime) IS NULL OR foo.fooTimestamp <= :fooTimestampMax)) OR " //
          + "        ((FALSE = :fooTimestampIsNull AND CAST(:fooTimestampMin AS java.time.OffsetDateTime) IS NULL AND CAST(:fooTimestampMax AS java.time.OffsetDateTime) IS NULL) AND (CAST(:fooTimestamp AS java.time.OffsetDateTime) IS NULL OR foo.fooTimestamp = :fooTimestamp))" //
          + "      ) ";

  @Query(value = FOO_QUERY)
  List<FooEntity> findFoos( //
      @Param("fooIdIsNull") boolean fooIdIsNull, @Param("fooId") Integer fooId, //
      @Param("fooCharIsNull") boolean fooCharIsNull, @Param("fooChar") String fooChar, //
      @Param("fooVarcharIsNull") boolean fooVarcharIsNull, @Param("fooVarchar") String fooVarchar, //
      @Param("fooTextIsNull") boolean fooTextIsNull, @Param("fooText") String fooText, //
      @Param("fooSmallintIsNull") boolean fooSmallintIsNull, @Param("fooSmallint") Short fooSmallint, //
      @Param("fooIntegerIsNull") boolean fooIntegerIsNull, @Param("fooInteger") Integer fooInteger, //
      @Param("fooBigintIsNull") boolean fooBigintIsNull, @Param("fooBigint") Long fooBigint, //
      @Param("fooRealIsNull") boolean fooRealIsNull, @Param("fooReal") Float fooReal, //
      @Param("fooDoubleIsNull") boolean fooDoubleIsNull, @Param("fooDouble") Double fooDouble, //
      @Param("fooDecimalIsNull") boolean fooDecimalIsNull, @Param("fooDecimal") BigDecimal fooDecimal, //
      @Param("fooBooleanIsNull") boolean fooBooleanIsNull, @Param("fooBoolean") Boolean fooBoolean, //
      @Param("fooDateIsNull") boolean fooDateIsNull, @Param("fooDate") LocalDate fooDate, //
      @Param("fooDateMin") LocalDate fooDateMin, @Param("fooDateMax") LocalDate fooDateMax, //
      @Param("fooTimestampIsNull") boolean fooTimestampIsNull, @Param("fooTimestamp") OffsetDateTime fooTimestamp, //
      @Param("fooTimestampMin") OffsetDateTime fooTimestampMin, @Param("fooTimestampMax") OffsetDateTime fooTimestampMax, //
      Pageable pageable);

  @Query(value = FOO_QUERY)
  List<ExistsView> existsFoos( //
      @Param("fooIdIsNull") boolean fooIdIsNull, @Param("fooId") Integer fooId, //
      @Param("fooCharIsNull") boolean fooCharIsNull, @Param("fooChar") String fooChar, //
      @Param("fooVarcharIsNull") boolean fooVarcharIsNull, @Param("fooVarchar") String fooVarchar, //
      @Param("fooTextIsNull") boolean fooTextIsNull, @Param("fooText") String fooText, //
      @Param("fooSmallintIsNull") boolean fooSmallintIsNull, @Param("fooSmallint") Short fooSmallint, //
      @Param("fooIntegerIsNull") boolean fooIntegerIsNull, @Param("fooInteger") Integer fooInteger, //
      @Param("fooBigintIsNull") boolean fooBigintIsNull, @Param("fooBigint") Long fooBigint, //
      @Param("fooRealIsNull") boolean fooRealIsNull, @Param("fooReal") Float fooReal, //
      @Param("fooDoubleIsNull") boolean fooDoubleIsNull, @Param("fooDouble") Double fooDouble, //
      @Param("fooDecimalIsNull") boolean fooDecimalIsNull, @Param("fooDecimal") BigDecimal fooDecimal, //
      @Param("fooBooleanIsNull") boolean fooBooleanIsNull, @Param("fooBoolean") Boolean fooBoolean, //
      @Param("fooDateIsNull") boolean fooDateIsNull, @Param("fooDate") LocalDate fooDate, //
      @Param("fooDateMin") LocalDate fooDateMin, @Param("fooDateMax") LocalDate fooDateMax, //
      @Param("fooTimestampIsNull") boolean fooTimestampIsNull, @Param("fooTimestamp") OffsetDateTime fooTimestamp, //
      @Param("fooTimestampMin") OffsetDateTime fooTimestampMin, @Param("fooTimestampMax") OffsetDateTime fooTimestampMax, //
      Pageable pageable);

  @Query(value = "SELECT COUNT(foo) " + FOO_QUERY)
  long countFoos( //
      @Param("fooIdIsNull") boolean fooIdIsNull, @Param("fooId") Integer fooId, //
      @Param("fooCharIsNull") boolean fooCharIsNull, @Param("fooChar") String fooChar, //
      @Param("fooVarcharIsNull") boolean fooVarcharIsNull, @Param("fooVarchar") String fooVarchar, //
      @Param("fooTextIsNull") boolean fooTextIsNull, @Param("fooText") String fooText, //
      @Param("fooSmallintIsNull") boolean fooSmallintIsNull, @Param("fooSmallint") Short fooSmallint, //
      @Param("fooIntegerIsNull") boolean fooIntegerIsNull, @Param("fooInteger") Integer fooInteger, //
      @Param("fooBigintIsNull") boolean fooBigintIsNull, @Param("fooBigint") Long fooBigint, //
      @Param("fooRealIsNull") boolean fooRealIsNull, @Param("fooReal") Float fooReal, //
      @Param("fooDoubleIsNull") boolean fooDoubleIsNull, @Param("fooDouble") Double fooDouble, //
      @Param("fooDecimalIsNull") boolean fooDecimalIsNull, @Param("fooDecimal") BigDecimal fooDecimal, //
      @Param("fooBooleanIsNull") boolean fooBooleanIsNull, @Param("fooBoolean") Boolean fooBoolean, //
      @Param("fooDateIsNull") boolean fooDateIsNull, @Param("fooDate") LocalDate fooDate, //
      @Param("fooDateMin") LocalDate fooDateMin, @Param("fooDateMax") LocalDate fooDateMax, //
      @Param("fooTimestampIsNull") boolean fooTimestampIsNull, @Param("fooTimestamp") OffsetDateTime fooTimestamp, //
      @Param("fooTimestampMin") OffsetDateTime fooTimestampMin, @Param("fooTimestampMax") OffsetDateTime fooTimestampMax);
}
