package com.escalableapps.cmdb.baseapp.template.jpql.mapper;

import java.util.Optional;

import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class FooEntityMapper {

  public static FooEntity fromEntity(Foo foo) {
    if (foo == null) {
      return null;
    }
    FooEntity fooEntity = new FooEntity();
    fooEntity.setFooId(foo.getFooId());
    fooEntity.setFooChar(foo.getFooChar());
    fooEntity.setFooVarchar(foo.getFooVarchar());
    fooEntity.setFooText(foo.getFooText());
    fooEntity.setFooSmallint(foo.getFooSmallint());
    fooEntity.setFooInteger(foo.getFooInteger());
    fooEntity.setFooBigint(foo.getFooBigint());
    fooEntity.setFooReal(foo.getFooReal());
    fooEntity.setFooDouble(foo.getFooDouble());
    fooEntity.setFooDecimal(foo.getFooDecimal());
    fooEntity.setFooBoolean(foo.getFooBoolean());
    fooEntity.setFooDate(foo.getFooDate());
    fooEntity.setFooTimestamp(foo.getFooTimestamp());
    return fooEntity;
  }

  public static Optional<Foo> toEntity(Optional<FooEntity> optional) {
    if (!optional.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(toEntity(optional.get()));
  }

  public static Foo toEntity(FooEntity fooEntity) {
    if (fooEntity == null) {
      return null;
    }
    Foo foo = new Foo();
    foo.setFooId(fooEntity.getFooId());
    foo.setFooChar(fooEntity.getFooChar());
    foo.setFooVarchar(fooEntity.getFooVarchar());
    foo.setFooText(fooEntity.getFooText());
    foo.setFooSmallint(fooEntity.getFooSmallint());
    foo.setFooInteger(fooEntity.getFooInteger());
    foo.setFooBigint(fooEntity.getFooBigint());
    foo.setFooReal(fooEntity.getFooReal());
    foo.setFooDouble(fooEntity.getFooDouble());
    foo.setFooDecimal(fooEntity.getFooDecimal());
    foo.setFooBoolean(fooEntity.getFooBoolean());
    foo.setFooDate(fooEntity.getFooDate());
    foo.setFooTimestamp(fooEntity.getFooTimestamp());
    return foo;
  }

  private FooEntityMapper() {
  }
}
