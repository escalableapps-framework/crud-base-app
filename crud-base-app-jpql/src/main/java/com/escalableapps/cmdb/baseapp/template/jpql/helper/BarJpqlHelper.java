package com.escalableapps.cmdb.baseapp.template.jpql.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.BarPropertyName.*;
import static org.springframework.data.domain.PageRequest.of;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class BarJpqlHelper {

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<BarEntity> root, //
      Integer barId //
  ) {
    BarQuery barQuery = new BarQuery();
    barQuery.setBarId(barId);
    return whereCondition(criteriaBuilder, root, barQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForBarId(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarChar(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarVarchar(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarText(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarSmallint(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarInteger(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarBigint(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarReal(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarDouble(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarDecimal(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarBoolean(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarDate(predicates, criteriaBuilder, root, barQuery);
    whereConditionForBarTimestamp(predicates, criteriaBuilder, root, barQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForBarId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarId() != null) {
      if (!barQuery.getBarId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_ID.getPropertyName()), getQueryValue(barQuery.getBarId())));
      }
    }
  }

  private static void whereConditionForBarChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarChar() != null) {
      if (!barQuery.getBarChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_CHAR.getPropertyName()), getQueryValue(barQuery.getBarChar())));
      }
    }
  }

  private static void whereConditionForBarVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarVarchar() != null) {
      if (!barQuery.getBarVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_VARCHAR.getPropertyName()), getQueryValue(barQuery.getBarVarchar())));
      }
    }
  }

  private static void whereConditionForBarText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarText() != null) {
      if (!barQuery.getBarText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_TEXT.getPropertyName()), getQueryValue(barQuery.getBarText())));
      }
    }
  }

  private static void whereConditionForBarSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarSmallint() != null) {
      if (!barQuery.getBarSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_SMALLINT.getPropertyName()), getQueryValue(barQuery.getBarSmallint())));
      }
    }
  }

  private static void whereConditionForBarInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarInteger() != null) {
      if (!barQuery.getBarInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_INTEGER.getPropertyName()), getQueryValue(barQuery.getBarInteger())));
      }
    }
  }

  private static void whereConditionForBarBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarBigint() != null) {
      if (!barQuery.getBarBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_BIGINT.getPropertyName()), getQueryValue(barQuery.getBarBigint())));
      }
    }
  }

  private static void whereConditionForBarReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarReal() != null) {
      if (!barQuery.getBarReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_REAL.getPropertyName()), getQueryValue(barQuery.getBarReal())));
      }
    }
  }

  private static void whereConditionForBarDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarDouble() != null) {
      if (!barQuery.getBarDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_DOUBLE.getPropertyName()), getQueryValue(barQuery.getBarDouble())));
      }
    }
  }

  private static void whereConditionForBarDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarDecimal() != null) {
      if (!barQuery.getBarDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_DECIMAL.getPropertyName()), getQueryValue(barQuery.getBarDecimal())));
      }
    }
  }

  private static void whereConditionForBarBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarBoolean() != null) {
      if (!barQuery.getBarBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_BOOLEAN.getPropertyName()), getQueryValue(barQuery.getBarBoolean())));
      }
    }
  }

  private static void whereConditionForBarDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarDate() != null) {
      if (!barQuery.getBarDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_DATE.getPropertyName()), getQueryValue(barQuery.getBarDate())));
      }
    } else if (barQuery.getBarDateMin() != null || barQuery.getBarDateMax() != null) {
      if (barQuery.getBarDateMin() != null && barQuery.getBarDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(BAR_DATE.getPropertyName()), barQuery.getBarDateMin().get()));
      }
      if (barQuery.getBarDateMax() != null && barQuery.getBarDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(BAR_DATE.getPropertyName()), barQuery.getBarDateMax().get()));
      }
    }
  }

  private static void whereConditionForBarTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<BarEntity> root, BarQuery barQuery) {
    if (barQuery.getBarTimestamp() != null) {
      if (!barQuery.getBarTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(BAR_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(BAR_TIMESTAMP.getPropertyName()), getQueryValue(barQuery.getBarTimestamp())));
      }
    } else if (barQuery.getBarTimestampMin() != null || barQuery.getBarTimestampMax() != null) {
      if (barQuery.getBarTimestampMin() != null && barQuery.getBarTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(BAR_TIMESTAMP.getPropertyName()), barQuery.getBarTimestampMin().get()));
      }
      if (barQuery.getBarTimestampMax() != null && barQuery.getBarTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(BAR_TIMESTAMP.getPropertyName()), barQuery.getBarTimestampMax().get()));
      }
    }
  }

  public static Pageable orderBy(PageRequest<BarPropertyName> pageRequest) {
    List<Order> orders = new ArrayList<>();
    pageRequest.getSortRequests().forEach(sort -> {
      orderByForBarId(orders, sort);
      orderByForBarChar(orders, sort);
      orderByForBarVarchar(orders, sort);
      orderByForBarText(orders, sort);
      orderByForBarSmallint(orders, sort);
      orderByForBarInteger(orders, sort);
      orderByForBarBigint(orders, sort);
      orderByForBarReal(orders, sort);
      orderByForBarDouble(orders, sort);
      orderByForBarDecimal(orders, sort);
      orderByForBarBoolean(orders, sort);
      orderByForBarDate(orders, sort);
      orderByForBarTimestamp(orders, sort);
    });
    if (orders.isEmpty()) {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize());
    } else {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize(), Sort.by(orders));
    }
  }

  private static void orderByForBarId(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_ID) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarChar(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_CHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarVarchar(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarText(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_TEXT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarSmallint(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarInteger(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_INTEGER) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarBigint(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_BIGINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarReal(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_REAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarDouble(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarDecimal(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarBoolean(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarDate(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_DATE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForBarTimestamp(List<Order> orders, SortRequest<BarPropertyName> sort) {
    if (sort.getProperty() == BarPropertyName.BAR_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  public static CriteriaUpdate<BarEntity> updateValues(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    updateValuesForBarChar(update, root, barUpdate);
    updateValuesForBarVarchar(update, root, barUpdate);
    updateValuesForBarText(update, root, barUpdate);
    updateValuesForBarSmallint(update, root, barUpdate);
    updateValuesForBarInteger(update, root, barUpdate);
    updateValuesForBarBigint(update, root, barUpdate);
    updateValuesForBarReal(update, root, barUpdate);
    updateValuesForBarDouble(update, root, barUpdate);
    updateValuesForBarDecimal(update, root, barUpdate);
    updateValuesForBarBoolean(update, root, barUpdate);
    updateValuesForBarDate(update, root, barUpdate);
    updateValuesForBarTimestamp(update, root, barUpdate);
    return update;
  }

  private static void updateValuesForBarChar(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarChar() != null) {
      update.set(root.get(BAR_CHAR.getPropertyName()), getUpdateValue(barUpdate.getBarChar()));
    }
  }

  private static void updateValuesForBarVarchar(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarVarchar() != null) {
      update.set(root.get(BAR_VARCHAR.getPropertyName()), getUpdateValue(barUpdate.getBarVarchar()));
    }
  }

  private static void updateValuesForBarText(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarText() != null) {
      update.set(root.get(BAR_TEXT.getPropertyName()), getUpdateValue(barUpdate.getBarText()));
    }
  }

  private static void updateValuesForBarSmallint(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarSmallint() != null) {
      update.set(root.get(BAR_SMALLINT.getPropertyName()), getUpdateValue(barUpdate.getBarSmallint()));
    }
  }

  private static void updateValuesForBarInteger(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarInteger() != null) {
      update.set(root.get(BAR_INTEGER.getPropertyName()), getUpdateValue(barUpdate.getBarInteger()));
    }
  }

  private static void updateValuesForBarBigint(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarBigint() != null) {
      update.set(root.get(BAR_BIGINT.getPropertyName()), getUpdateValue(barUpdate.getBarBigint()));
    }
  }

  private static void updateValuesForBarReal(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarReal() != null) {
      update.set(root.get(BAR_REAL.getPropertyName()), getUpdateValue(barUpdate.getBarReal()));
    }
  }

  private static void updateValuesForBarDouble(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarDouble() != null) {
      update.set(root.get(BAR_DOUBLE.getPropertyName()), getUpdateValue(barUpdate.getBarDouble()));
    }
  }

  private static void updateValuesForBarDecimal(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarDecimal() != null) {
      update.set(root.get(BAR_DECIMAL.getPropertyName()), getUpdateValue(barUpdate.getBarDecimal()));
    }
  }

  private static void updateValuesForBarBoolean(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarBoolean() != null) {
      update.set(root.get(BAR_BOOLEAN.getPropertyName()), getUpdateValue(barUpdate.getBarBoolean()));
    }
  }

  private static void updateValuesForBarDate(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarDate() != null) {
      update.set(root.get(BAR_DATE.getPropertyName()), getUpdateValue(barUpdate.getBarDate()));
    }
  }

  private static void updateValuesForBarTimestamp(CriteriaUpdate<BarEntity> update, Root<BarEntity> root, BarUpdate barUpdate) {
    if (barUpdate.getBarTimestamp() != null) {
      update.set(root.get(BAR_TIMESTAMP.getPropertyName()), getUpdateValue(barUpdate.getBarTimestamp()));
    }
  }

  public static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  public static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private BarJpqlHelper() {
  }
}
