package com.escalableapps.cmdb.baseapp.template.jpql.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpql.helper.BarJpqlHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpql.mapper.BarEntityMapper.*;
import static org.springframework.data.domain.PageRequest.of;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.mapper.BarEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpql.repository.BarJpqlRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.BarSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class BarJpqlAdapter implements BarSpi {

  private final BarJpqlRepository repository;
  private final EntityManager entityManager;

  public BarJpqlAdapter(BarJpqlRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Bar createBar( //
      Bar bar //
  ) {
    log.debug("createBar:bar={}", bar);

    BarEntity barEntity = repository.save(fromEntity(bar));

    bar.setBarId(barEntity.getBarId());
    log.debug("createBar:bar={}", bar);
    return bar;
  }

  @Override
  public Optional<Bar> findBar( //
      Integer barId //
  ) {
    log.debug("findBar:barId={}", barId);

    Optional<BarEntity> bar = repository.findById(barId);

    log.debug("findBar:bar={}", bar);
    return toEntity(bar);
  }

  @Override
  public List<Bar> findBars( //
      List<Integer> barIds //
  ) {
    log.debug("findBars:barIds={}", barIds);

    List<BarEntity> bars = repository.findAllById(barIds);
    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery //
  ) {
    return findBars(barQuery, new PageRequest<>());
  }

  @Override
  public List<Bar> findBars( //
      BarQuery barQuery, //
      PageRequest<BarPropertyName> paging //
  ) {
    log.debug("findBars:barQuery={}", barQuery);
    log.debug("findBars:paging={}", paging);

    List<BarEntity> bars = repository.findBars( //
        isNullValue(barQuery.getBarId()), getQueryValue(barQuery.getBarId()), //
        isNullValue(barQuery.getBarChar()), getQueryValue(barQuery.getBarChar()), //
        isNullValue(barQuery.getBarVarchar()), getQueryValue(barQuery.getBarVarchar()), //
        isNullValue(barQuery.getBarText()), getQueryValue(barQuery.getBarText()), //
        isNullValue(barQuery.getBarSmallint()), getQueryValue(barQuery.getBarSmallint()), //
        isNullValue(barQuery.getBarInteger()), getQueryValue(barQuery.getBarInteger()), //
        isNullValue(barQuery.getBarBigint()), getQueryValue(barQuery.getBarBigint()), //
        isNullValue(barQuery.getBarReal()), getQueryValue(barQuery.getBarReal()), //
        isNullValue(barQuery.getBarDouble()), getQueryValue(barQuery.getBarDouble()), //
        isNullValue(barQuery.getBarDecimal()), getQueryValue(barQuery.getBarDecimal()), //
        isNullValue(barQuery.getBarBoolean()), getQueryValue(barQuery.getBarBoolean()), //
        isNullValue(barQuery.getBarDate()), getQueryValue(barQuery.getBarDate()), //
        getQueryValue(barQuery.getBarDateMin()), getQueryValue(barQuery.getBarDateMax()), //
        isNullValue(barQuery.getBarTimestamp()), getQueryValue(barQuery.getBarTimestamp()), //
        getQueryValue(barQuery.getBarTimestampMin()), getQueryValue(barQuery.getBarTimestampMax()), //
        orderBy(paging));

    log.debug("findBars:bars={}", bars);

    return bars.stream().map(BarEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsBar( //
      Integer barId //
  ) {
    log.debug("existsBar:barId={}", barId);

    boolean exists = repository.existsById(barId);

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsBars( //
      BarQuery barQuery //
  ) {
    log.debug("existsBar:barQuery={}", barQuery);

    boolean exists = !repository.existsBars( //
        isNullValue(barQuery.getBarId()), getQueryValue(barQuery.getBarId()), //
        isNullValue(barQuery.getBarChar()), getQueryValue(barQuery.getBarChar()), //
        isNullValue(barQuery.getBarVarchar()), getQueryValue(barQuery.getBarVarchar()), //
        isNullValue(barQuery.getBarText()), getQueryValue(barQuery.getBarText()), //
        isNullValue(barQuery.getBarSmallint()), getQueryValue(barQuery.getBarSmallint()), //
        isNullValue(barQuery.getBarInteger()), getQueryValue(barQuery.getBarInteger()), //
        isNullValue(barQuery.getBarBigint()), getQueryValue(barQuery.getBarBigint()), //
        isNullValue(barQuery.getBarReal()), getQueryValue(barQuery.getBarReal()), //
        isNullValue(barQuery.getBarDouble()), getQueryValue(barQuery.getBarDouble()), //
        isNullValue(barQuery.getBarDecimal()), getQueryValue(barQuery.getBarDecimal()), //
        isNullValue(barQuery.getBarBoolean()), getQueryValue(barQuery.getBarBoolean()), //
        isNullValue(barQuery.getBarDate()), getQueryValue(barQuery.getBarDate()), //
        getQueryValue(barQuery.getBarDateMin()), getQueryValue(barQuery.getBarDateMax()), //
        isNullValue(barQuery.getBarTimestamp()), getQueryValue(barQuery.getBarTimestamp()), //
        getQueryValue(barQuery.getBarTimestampMin()), getQueryValue(barQuery.getBarTimestampMax()), //
        of(0, 1)).isEmpty();

    log.debug("existsBar:exists={}", exists);
    return exists;
  }

  @Override
  public long countBars( //
      BarQuery barQuery //
  ) {
    log.debug("countBars:barQuery={}", barQuery);

    long count = repository.countBars( //
        isNullValue(barQuery.getBarId()), getQueryValue(barQuery.getBarId()), //
        isNullValue(barQuery.getBarChar()), getQueryValue(barQuery.getBarChar()), //
        isNullValue(barQuery.getBarVarchar()), getQueryValue(barQuery.getBarVarchar()), //
        isNullValue(barQuery.getBarText()), getQueryValue(barQuery.getBarText()), //
        isNullValue(barQuery.getBarSmallint()), getQueryValue(barQuery.getBarSmallint()), //
        isNullValue(barQuery.getBarInteger()), getQueryValue(barQuery.getBarInteger()), //
        isNullValue(barQuery.getBarBigint()), getQueryValue(barQuery.getBarBigint()), //
        isNullValue(barQuery.getBarReal()), getQueryValue(barQuery.getBarReal()), //
        isNullValue(barQuery.getBarDouble()), getQueryValue(barQuery.getBarDouble()), //
        isNullValue(barQuery.getBarDecimal()), getQueryValue(barQuery.getBarDecimal()), //
        isNullValue(barQuery.getBarBoolean()), getQueryValue(barQuery.getBarBoolean()), //
        isNullValue(barQuery.getBarDate()), getQueryValue(barQuery.getBarDate()), //
        getQueryValue(barQuery.getBarDateMin()), getQueryValue(barQuery.getBarDateMax()), //
        isNullValue(barQuery.getBarTimestamp()), getQueryValue(barQuery.getBarTimestamp()), //
        getQueryValue(barQuery.getBarTimestampMin()), getQueryValue(barQuery.getBarTimestampMax()));

    log.debug("countBars:count={}", count);
    return count;
  }

  @Override
  public long updateBar( //
      Integer barId, //
      BarUpdate barUpdate //
  ) {
    log.debug("updateBar:barId={}", barId);
    log.debug("updateBar:barUpdate={}", barUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<BarEntity> update = criteriaBuilder.createCriteriaUpdate(BarEntity.class);
    Root<BarEntity> root = update.from(BarEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, barUpdate).where(whereCondition(criteriaBuilder, root, barId)) //
    ).executeUpdate();

    log.debug("updateBar:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteBar( //
      Integer barId //
  ) {
    log.debug("deleteBar:barId={}", barId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<BarEntity> delete = criteriaBuilder.createCriteriaDelete(BarEntity.class);
    Root<BarEntity> root = delete.from(BarEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, barId)) //
    ).executeUpdate();

    log.debug("deleteBar:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
