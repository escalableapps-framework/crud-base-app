package com.escalableapps.cmdb.baseapp.template.jpql.repository;

import java.math.*;
import java.time.*;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.escalableapps.cmdb.baseapp.shared.jpql.ExistsView;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public interface ZooJpqlRepository extends JpaRepository<ZooEntity, ZooPk> {

  String ZOO_QUERY = //
      "FROM ZooEntity AS zoo " //
          + "WHERE ((TRUE = :zooOrgIsNull AND zoo.id.zooOrg IS NULL) OR (FALSE = :zooOrgIsNull AND (:zooOrg IS NULL OR zoo.id.zooOrg = :zooOrg))) " //
          + "  AND ((TRUE = :zooIdIsNull AND zoo.id.zooId IS NULL) OR (FALSE = :zooIdIsNull AND (:zooId IS NULL OR zoo.id.zooId = :zooId))) " //
          + "  AND ((TRUE = :zooCharIsNull AND zoo.zooChar IS NULL) OR (FALSE = :zooCharIsNull AND (:zooChar IS NULL OR zoo.zooChar = :zooChar))) " //
          + "  AND ((TRUE = :zooVarcharIsNull AND zoo.zooVarchar IS NULL) OR (FALSE = :zooVarcharIsNull AND (:zooVarchar IS NULL OR zoo.zooVarchar = :zooVarchar))) " //
          + "  AND ((TRUE = :zooTextIsNull AND zoo.zooText IS NULL) OR (FALSE = :zooTextIsNull AND (:zooText IS NULL OR zoo.zooText = :zooText))) " //
          + "  AND ((TRUE = :zooSmallintIsNull AND zoo.zooSmallint IS NULL) OR (FALSE = :zooSmallintIsNull AND (:zooSmallint IS NULL OR zoo.zooSmallint = :zooSmallint))) " //
          + "  AND ((TRUE = :zooIntegerIsNull AND zoo.zooInteger IS NULL) OR (FALSE = :zooIntegerIsNull AND (:zooInteger IS NULL OR zoo.zooInteger = :zooInteger))) " //
          + "  AND ((TRUE = :zooBigintIsNull AND zoo.zooBigint IS NULL) OR (FALSE = :zooBigintIsNull AND (:zooBigint IS NULL OR zoo.zooBigint = :zooBigint))) " //
          + "  AND ((TRUE = :zooRealIsNull AND zoo.zooReal IS NULL) OR (FALSE = :zooRealIsNull AND (:zooReal IS NULL OR zoo.zooReal = :zooReal))) " //
          + "  AND ((TRUE = :zooDoubleIsNull AND zoo.zooDouble IS NULL) OR (FALSE = :zooDoubleIsNull AND (:zooDouble IS NULL OR zoo.zooDouble = :zooDouble))) " //
          + "  AND ((TRUE = :zooDecimalIsNull AND zoo.zooDecimal IS NULL) OR (FALSE = :zooDecimalIsNull AND (:zooDecimal IS NULL OR zoo.zooDecimal = :zooDecimal))) " //
          + "  AND ((TRUE = :zooBooleanIsNull AND zoo.zooBoolean IS NULL) OR (FALSE = :zooBooleanIsNull AND (:zooBoolean IS NULL OR zoo.zooBoolean = :zooBoolean))) " //
          + "  AND (" //
          + "        ((TRUE = :zooDateIsNull) AND (zoo.zooDate IS NULL)) OR " //
          + "        ((FALSE = :zooDateIsNull AND (CAST(:zooDateMin AS java.time.LocalDate) IS NOT NULL OR CAST(:zooDateMax AS java.time.LocalDate) IS NOT NULL)) AND (CAST(:zooDateMin AS java.time.LocalDate) IS NULL OR :zooDateMin <= zoo.zooDate) AND (CAST(:zooDateMax AS java.time.LocalDate) IS NULL OR zoo.zooDate <= :zooDateMax)) OR " //
          + "        ((FALSE = :zooDateIsNull AND CAST(:zooDateMin AS java.time.LocalDate) IS NULL AND CAST(:zooDateMax AS java.time.LocalDate) IS NULL) AND (CAST(:zooDate AS java.time.LocalDate) IS NULL OR zoo.zooDate = :zooDate))" //
          + "      ) " //
          + "  AND (" //
          + "        ((TRUE = :zooTimestampIsNull) AND (zoo.zooTimestamp IS NULL)) OR " //
          + "        ((FALSE = :zooTimestampIsNull AND (CAST(:zooTimestampMin AS java.time.OffsetDateTime) IS NOT NULL OR CAST(:zooTimestampMax AS java.time.OffsetDateTime) IS NOT NULL)) AND (CAST(:zooTimestampMin AS java.time.OffsetDateTime) IS NULL OR :zooTimestampMin <= zoo.zooTimestamp) AND (CAST(:zooTimestampMax AS java.time.OffsetDateTime) IS NULL OR zoo.zooTimestamp <= :zooTimestampMax)) OR " //
          + "        ((FALSE = :zooTimestampIsNull AND CAST(:zooTimestampMin AS java.time.OffsetDateTime) IS NULL AND CAST(:zooTimestampMax AS java.time.OffsetDateTime) IS NULL) AND (CAST(:zooTimestamp AS java.time.OffsetDateTime) IS NULL OR zoo.zooTimestamp = :zooTimestamp))" //
          + "      ) ";

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.zoo_zoo_id_seq')")
  Integer nextVal();

  @Query(value = ZOO_QUERY)
  List<ZooEntity> findZoos( //
      @Param("zooOrgIsNull") boolean zooOrgIsNull, @Param("zooOrg") Long zooOrg, //
      @Param("zooIdIsNull") boolean zooIdIsNull, @Param("zooId") Integer zooId, //
      @Param("zooCharIsNull") boolean zooCharIsNull, @Param("zooChar") String zooChar, //
      @Param("zooVarcharIsNull") boolean zooVarcharIsNull, @Param("zooVarchar") String zooVarchar, //
      @Param("zooTextIsNull") boolean zooTextIsNull, @Param("zooText") String zooText, //
      @Param("zooSmallintIsNull") boolean zooSmallintIsNull, @Param("zooSmallint") Short zooSmallint, //
      @Param("zooIntegerIsNull") boolean zooIntegerIsNull, @Param("zooInteger") Integer zooInteger, //
      @Param("zooBigintIsNull") boolean zooBigintIsNull, @Param("zooBigint") Long zooBigint, //
      @Param("zooRealIsNull") boolean zooRealIsNull, @Param("zooReal") Float zooReal, //
      @Param("zooDoubleIsNull") boolean zooDoubleIsNull, @Param("zooDouble") Double zooDouble, //
      @Param("zooDecimalIsNull") boolean zooDecimalIsNull, @Param("zooDecimal") BigDecimal zooDecimal, //
      @Param("zooBooleanIsNull") boolean zooBooleanIsNull, @Param("zooBoolean") Boolean zooBoolean, //
      @Param("zooDateIsNull") boolean zooDateIsNull, @Param("zooDate") LocalDate zooDate, //
      @Param("zooDateMin") LocalDate zooDateMin, @Param("zooDateMax") LocalDate zooDateMax, //
      @Param("zooTimestampIsNull") boolean zooTimestampIsNull, @Param("zooTimestamp") OffsetDateTime zooTimestamp, //
      @Param("zooTimestampMin") OffsetDateTime zooTimestampMin, @Param("zooTimestampMax") OffsetDateTime zooTimestampMax, //
      Pageable pageable);

  @Query(value = ZOO_QUERY)
  List<ExistsView> existsZoos( //
      @Param("zooOrgIsNull") boolean zooOrgIsNull, @Param("zooOrg") Long zooOrg, //
      @Param("zooIdIsNull") boolean zooIdIsNull, @Param("zooId") Integer zooId, //
      @Param("zooCharIsNull") boolean zooCharIsNull, @Param("zooChar") String zooChar, //
      @Param("zooVarcharIsNull") boolean zooVarcharIsNull, @Param("zooVarchar") String zooVarchar, //
      @Param("zooTextIsNull") boolean zooTextIsNull, @Param("zooText") String zooText, //
      @Param("zooSmallintIsNull") boolean zooSmallintIsNull, @Param("zooSmallint") Short zooSmallint, //
      @Param("zooIntegerIsNull") boolean zooIntegerIsNull, @Param("zooInteger") Integer zooInteger, //
      @Param("zooBigintIsNull") boolean zooBigintIsNull, @Param("zooBigint") Long zooBigint, //
      @Param("zooRealIsNull") boolean zooRealIsNull, @Param("zooReal") Float zooReal, //
      @Param("zooDoubleIsNull") boolean zooDoubleIsNull, @Param("zooDouble") Double zooDouble, //
      @Param("zooDecimalIsNull") boolean zooDecimalIsNull, @Param("zooDecimal") BigDecimal zooDecimal, //
      @Param("zooBooleanIsNull") boolean zooBooleanIsNull, @Param("zooBoolean") Boolean zooBoolean, //
      @Param("zooDateIsNull") boolean zooDateIsNull, @Param("zooDate") LocalDate zooDate, //
      @Param("zooDateMin") LocalDate zooDateMin, @Param("zooDateMax") LocalDate zooDateMax, //
      @Param("zooTimestampIsNull") boolean zooTimestampIsNull, @Param("zooTimestamp") OffsetDateTime zooTimestamp, //
      @Param("zooTimestampMin") OffsetDateTime zooTimestampMin, @Param("zooTimestampMax") OffsetDateTime zooTimestampMax, //
      Pageable pageable);

  @Query(value = "SELECT COUNT(zoo) " + ZOO_QUERY)
  long countZoos( //
      @Param("zooOrgIsNull") boolean zooOrgIsNull, @Param("zooOrg") Long zooOrg, //
      @Param("zooIdIsNull") boolean zooIdIsNull, @Param("zooId") Integer zooId, //
      @Param("zooCharIsNull") boolean zooCharIsNull, @Param("zooChar") String zooChar, //
      @Param("zooVarcharIsNull") boolean zooVarcharIsNull, @Param("zooVarchar") String zooVarchar, //
      @Param("zooTextIsNull") boolean zooTextIsNull, @Param("zooText") String zooText, //
      @Param("zooSmallintIsNull") boolean zooSmallintIsNull, @Param("zooSmallint") Short zooSmallint, //
      @Param("zooIntegerIsNull") boolean zooIntegerIsNull, @Param("zooInteger") Integer zooInteger, //
      @Param("zooBigintIsNull") boolean zooBigintIsNull, @Param("zooBigint") Long zooBigint, //
      @Param("zooRealIsNull") boolean zooRealIsNull, @Param("zooReal") Float zooReal, //
      @Param("zooDoubleIsNull") boolean zooDoubleIsNull, @Param("zooDouble") Double zooDouble, //
      @Param("zooDecimalIsNull") boolean zooDecimalIsNull, @Param("zooDecimal") BigDecimal zooDecimal, //
      @Param("zooBooleanIsNull") boolean zooBooleanIsNull, @Param("zooBoolean") Boolean zooBoolean, //
      @Param("zooDateIsNull") boolean zooDateIsNull, @Param("zooDate") LocalDate zooDate, //
      @Param("zooDateMin") LocalDate zooDateMin, @Param("zooDateMax") LocalDate zooDateMax, //
      @Param("zooTimestampIsNull") boolean zooTimestampIsNull, @Param("zooTimestamp") OffsetDateTime zooTimestamp, //
      @Param("zooTimestampMin") OffsetDateTime zooTimestampMin, @Param("zooTimestampMax") OffsetDateTime zooTimestampMax);
}
