package com.escalableapps.cmdb.baseapp.template.jpql.repository;

import java.math.*;
import java.time.*;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.escalableapps.cmdb.baseapp.shared.jpql.ExistsView;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public interface LookJpqlRepository extends JpaRepository<LookEntity, LookPk> {

  String LOOK_QUERY = //
      "FROM LookEntity AS look " //
          + "WHERE ((TRUE = :lookOrgIsNull AND look.id.lookOrg IS NULL) OR (FALSE = :lookOrgIsNull AND (:lookOrg IS NULL OR look.id.lookOrg = :lookOrg))) " //
          + "  AND ((TRUE = :lookIdIsNull AND look.id.lookId IS NULL) OR (FALSE = :lookIdIsNull AND (:lookId IS NULL OR look.id.lookId = :lookId))) " //
          + "  AND ((TRUE = :lookCharIsNull AND look.lookChar IS NULL) OR (FALSE = :lookCharIsNull AND (:lookChar IS NULL OR look.lookChar = :lookChar))) " //
          + "  AND ((TRUE = :lookVarcharIsNull AND look.lookVarchar IS NULL) OR (FALSE = :lookVarcharIsNull AND (:lookVarchar IS NULL OR look.lookVarchar = :lookVarchar))) " //
          + "  AND ((TRUE = :lookTextIsNull AND look.lookText IS NULL) OR (FALSE = :lookTextIsNull AND (:lookText IS NULL OR look.lookText = :lookText))) " //
          + "  AND ((TRUE = :lookSmallintIsNull AND look.lookSmallint IS NULL) OR (FALSE = :lookSmallintIsNull AND (:lookSmallint IS NULL OR look.lookSmallint = :lookSmallint))) " //
          + "  AND ((TRUE = :lookIntegerIsNull AND look.lookInteger IS NULL) OR (FALSE = :lookIntegerIsNull AND (:lookInteger IS NULL OR look.lookInteger = :lookInteger))) " //
          + "  AND ((TRUE = :lookBigintIsNull AND look.lookBigint IS NULL) OR (FALSE = :lookBigintIsNull AND (:lookBigint IS NULL OR look.lookBigint = :lookBigint))) " //
          + "  AND ((TRUE = :lookRealIsNull AND look.lookReal IS NULL) OR (FALSE = :lookRealIsNull AND (:lookReal IS NULL OR look.lookReal = :lookReal))) " //
          + "  AND ((TRUE = :lookDoubleIsNull AND look.lookDouble IS NULL) OR (FALSE = :lookDoubleIsNull AND (:lookDouble IS NULL OR look.lookDouble = :lookDouble))) " //
          + "  AND ((TRUE = :lookDecimalIsNull AND look.lookDecimal IS NULL) OR (FALSE = :lookDecimalIsNull AND (:lookDecimal IS NULL OR look.lookDecimal = :lookDecimal))) " //
          + "  AND ((TRUE = :lookBooleanIsNull AND look.lookBoolean IS NULL) OR (FALSE = :lookBooleanIsNull AND (:lookBoolean IS NULL OR look.lookBoolean = :lookBoolean))) " //
          + "  AND (" //
          + "        ((TRUE = :lookDateIsNull) AND (look.lookDate IS NULL)) OR " //
          + "        ((FALSE = :lookDateIsNull AND (CAST(:lookDateMin AS java.time.LocalDate) IS NOT NULL OR CAST(:lookDateMax AS java.time.LocalDate) IS NOT NULL)) AND (CAST(:lookDateMin AS java.time.LocalDate) IS NULL OR :lookDateMin <= look.lookDate) AND (CAST(:lookDateMax AS java.time.LocalDate) IS NULL OR look.lookDate <= :lookDateMax)) OR " //
          + "        ((FALSE = :lookDateIsNull AND CAST(:lookDateMin AS java.time.LocalDate) IS NULL AND CAST(:lookDateMax AS java.time.LocalDate) IS NULL) AND (CAST(:lookDate AS java.time.LocalDate) IS NULL OR look.lookDate = :lookDate))" //
          + "      ) " //
          + "  AND (" //
          + "        ((TRUE = :lookTimestampIsNull) AND (look.lookTimestamp IS NULL)) OR " //
          + "        ((FALSE = :lookTimestampIsNull AND (CAST(:lookTimestampMin AS java.time.OffsetDateTime) IS NOT NULL OR CAST(:lookTimestampMax AS java.time.OffsetDateTime) IS NOT NULL)) AND (CAST(:lookTimestampMin AS java.time.OffsetDateTime) IS NULL OR :lookTimestampMin <= look.lookTimestamp) AND (CAST(:lookTimestampMax AS java.time.OffsetDateTime) IS NULL OR look.lookTimestamp <= :lookTimestampMax)) OR " //
          + "        ((FALSE = :lookTimestampIsNull AND CAST(:lookTimestampMin AS java.time.OffsetDateTime) IS NULL AND CAST(:lookTimestampMax AS java.time.OffsetDateTime) IS NULL) AND (CAST(:lookTimestamp AS java.time.OffsetDateTime) IS NULL OR look.lookTimestamp = :lookTimestamp))" //
          + "      ) ";

  @Query(nativeQuery = true, value = "select nextval('crud_base_db.look_look_id_seq')")
  Long nextVal();

  @Query(value = LOOK_QUERY)
  List<LookEntity> findLooks( //
      @Param("lookOrgIsNull") boolean lookOrgIsNull, @Param("lookOrg") Long lookOrg, //
      @Param("lookIdIsNull") boolean lookIdIsNull, @Param("lookId") Long lookId, //
      @Param("lookCharIsNull") boolean lookCharIsNull, @Param("lookChar") String lookChar, //
      @Param("lookVarcharIsNull") boolean lookVarcharIsNull, @Param("lookVarchar") String lookVarchar, //
      @Param("lookTextIsNull") boolean lookTextIsNull, @Param("lookText") String lookText, //
      @Param("lookSmallintIsNull") boolean lookSmallintIsNull, @Param("lookSmallint") Short lookSmallint, //
      @Param("lookIntegerIsNull") boolean lookIntegerIsNull, @Param("lookInteger") Integer lookInteger, //
      @Param("lookBigintIsNull") boolean lookBigintIsNull, @Param("lookBigint") Long lookBigint, //
      @Param("lookRealIsNull") boolean lookRealIsNull, @Param("lookReal") Float lookReal, //
      @Param("lookDoubleIsNull") boolean lookDoubleIsNull, @Param("lookDouble") Double lookDouble, //
      @Param("lookDecimalIsNull") boolean lookDecimalIsNull, @Param("lookDecimal") BigDecimal lookDecimal, //
      @Param("lookBooleanIsNull") boolean lookBooleanIsNull, @Param("lookBoolean") Boolean lookBoolean, //
      @Param("lookDateIsNull") boolean lookDateIsNull, @Param("lookDate") LocalDate lookDate, //
      @Param("lookDateMin") LocalDate lookDateMin, @Param("lookDateMax") LocalDate lookDateMax, //
      @Param("lookTimestampIsNull") boolean lookTimestampIsNull, @Param("lookTimestamp") OffsetDateTime lookTimestamp, //
      @Param("lookTimestampMin") OffsetDateTime lookTimestampMin, @Param("lookTimestampMax") OffsetDateTime lookTimestampMax, //
      Pageable pageable);

  @Query(value = LOOK_QUERY)
  List<ExistsView> existsLooks( //
      @Param("lookOrgIsNull") boolean lookOrgIsNull, @Param("lookOrg") Long lookOrg, //
      @Param("lookIdIsNull") boolean lookIdIsNull, @Param("lookId") Long lookId, //
      @Param("lookCharIsNull") boolean lookCharIsNull, @Param("lookChar") String lookChar, //
      @Param("lookVarcharIsNull") boolean lookVarcharIsNull, @Param("lookVarchar") String lookVarchar, //
      @Param("lookTextIsNull") boolean lookTextIsNull, @Param("lookText") String lookText, //
      @Param("lookSmallintIsNull") boolean lookSmallintIsNull, @Param("lookSmallint") Short lookSmallint, //
      @Param("lookIntegerIsNull") boolean lookIntegerIsNull, @Param("lookInteger") Integer lookInteger, //
      @Param("lookBigintIsNull") boolean lookBigintIsNull, @Param("lookBigint") Long lookBigint, //
      @Param("lookRealIsNull") boolean lookRealIsNull, @Param("lookReal") Float lookReal, //
      @Param("lookDoubleIsNull") boolean lookDoubleIsNull, @Param("lookDouble") Double lookDouble, //
      @Param("lookDecimalIsNull") boolean lookDecimalIsNull, @Param("lookDecimal") BigDecimal lookDecimal, //
      @Param("lookBooleanIsNull") boolean lookBooleanIsNull, @Param("lookBoolean") Boolean lookBoolean, //
      @Param("lookDateIsNull") boolean lookDateIsNull, @Param("lookDate") LocalDate lookDate, //
      @Param("lookDateMin") LocalDate lookDateMin, @Param("lookDateMax") LocalDate lookDateMax, //
      @Param("lookTimestampIsNull") boolean lookTimestampIsNull, @Param("lookTimestamp") OffsetDateTime lookTimestamp, //
      @Param("lookTimestampMin") OffsetDateTime lookTimestampMin, @Param("lookTimestampMax") OffsetDateTime lookTimestampMax, //
      Pageable pageable);

  @Query(value = "SELECT COUNT(look) " + LOOK_QUERY)
  long countLooks( //
      @Param("lookOrgIsNull") boolean lookOrgIsNull, @Param("lookOrg") Long lookOrg, //
      @Param("lookIdIsNull") boolean lookIdIsNull, @Param("lookId") Long lookId, //
      @Param("lookCharIsNull") boolean lookCharIsNull, @Param("lookChar") String lookChar, //
      @Param("lookVarcharIsNull") boolean lookVarcharIsNull, @Param("lookVarchar") String lookVarchar, //
      @Param("lookTextIsNull") boolean lookTextIsNull, @Param("lookText") String lookText, //
      @Param("lookSmallintIsNull") boolean lookSmallintIsNull, @Param("lookSmallint") Short lookSmallint, //
      @Param("lookIntegerIsNull") boolean lookIntegerIsNull, @Param("lookInteger") Integer lookInteger, //
      @Param("lookBigintIsNull") boolean lookBigintIsNull, @Param("lookBigint") Long lookBigint, //
      @Param("lookRealIsNull") boolean lookRealIsNull, @Param("lookReal") Float lookReal, //
      @Param("lookDoubleIsNull") boolean lookDoubleIsNull, @Param("lookDouble") Double lookDouble, //
      @Param("lookDecimalIsNull") boolean lookDecimalIsNull, @Param("lookDecimal") BigDecimal lookDecimal, //
      @Param("lookBooleanIsNull") boolean lookBooleanIsNull, @Param("lookBoolean") Boolean lookBoolean, //
      @Param("lookDateIsNull") boolean lookDateIsNull, @Param("lookDate") LocalDate lookDate, //
      @Param("lookDateMin") LocalDate lookDateMin, @Param("lookDateMax") LocalDate lookDateMax, //
      @Param("lookTimestampIsNull") boolean lookTimestampIsNull, @Param("lookTimestamp") OffsetDateTime lookTimestamp, //
      @Param("lookTimestampMin") OffsetDateTime lookTimestampMin, @Param("lookTimestampMax") OffsetDateTime lookTimestampMax);
}
