package com.escalableapps.cmdb.baseapp.template.jpql.adapter;

import static com.escalableapps.cmdb.baseapp.template.jpql.helper.FooJpqlHelper.*;
import static com.escalableapps.cmdb.baseapp.template.jpql.mapper.FooEntityMapper.*;
import static org.springframework.data.domain.PageRequest.of;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.PageRequest;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.mapper.FooEntityMapper;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;
import com.escalableapps.cmdb.baseapp.template.jpql.repository.FooJpqlRepository;
import com.escalableapps.cmdb.baseapp.template.port.spi.FooSpi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Validated
public class FooJpqlAdapter implements FooSpi {

  private final FooJpqlRepository repository;
  private final EntityManager entityManager;

  public FooJpqlAdapter(FooJpqlRepository repository, EntityManager entityManager) {
    this.repository = repository;
    this.entityManager = entityManager;
  }

  @Override
  public Foo createFoo( //
      Foo foo //
  ) {
    log.debug("createFoo:foo={}", foo);

    FooEntity fooEntity = repository.save(fromEntity(foo));

    foo.setFooId(fooEntity.getFooId());
    log.debug("createFoo:foo={}", foo);
    return foo;
  }

  @Override
  public Optional<Foo> findFoo( //
      Integer fooId //
  ) {
    log.debug("findFoo:fooId={}", fooId);

    Optional<FooEntity> foo = repository.findById(fooId);

    log.debug("findFoo:foo={}", foo);
    return toEntity(foo);
  }

  @Override
  public List<Foo> findFoos( //
      List<Integer> fooIds //
  ) {
    log.debug("findFoos:fooIds={}", fooIds);

    List<FooEntity> foos = repository.findAllById(fooIds);
    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery //
  ) {
    return findFoos(fooQuery, new PageRequest<>());
  }

  @Override
  public List<Foo> findFoos( //
      FooQuery fooQuery, //
      PageRequest<FooPropertyName> paging //
  ) {
    log.debug("findFoos:fooQuery={}", fooQuery);
    log.debug("findFoos:paging={}", paging);

    List<FooEntity> foos = repository.findFoos( //
        isNullValue(fooQuery.getFooId()), getQueryValue(fooQuery.getFooId()), //
        isNullValue(fooQuery.getFooChar()), getQueryValue(fooQuery.getFooChar()), //
        isNullValue(fooQuery.getFooVarchar()), getQueryValue(fooQuery.getFooVarchar()), //
        isNullValue(fooQuery.getFooText()), getQueryValue(fooQuery.getFooText()), //
        isNullValue(fooQuery.getFooSmallint()), getQueryValue(fooQuery.getFooSmallint()), //
        isNullValue(fooQuery.getFooInteger()), getQueryValue(fooQuery.getFooInteger()), //
        isNullValue(fooQuery.getFooBigint()), getQueryValue(fooQuery.getFooBigint()), //
        isNullValue(fooQuery.getFooReal()), getQueryValue(fooQuery.getFooReal()), //
        isNullValue(fooQuery.getFooDouble()), getQueryValue(fooQuery.getFooDouble()), //
        isNullValue(fooQuery.getFooDecimal()), getQueryValue(fooQuery.getFooDecimal()), //
        isNullValue(fooQuery.getFooBoolean()), getQueryValue(fooQuery.getFooBoolean()), //
        isNullValue(fooQuery.getFooDate()), getQueryValue(fooQuery.getFooDate()), //
        getQueryValue(fooQuery.getFooDateMin()), getQueryValue(fooQuery.getFooDateMax()), //
        isNullValue(fooQuery.getFooTimestamp()), getQueryValue(fooQuery.getFooTimestamp()), //
        getQueryValue(fooQuery.getFooTimestampMin()), getQueryValue(fooQuery.getFooTimestampMax()), //
        orderBy(paging));

    log.debug("findFoos:foos={}", foos);

    return foos.stream().map(FooEntityMapper::toEntity).collect(Collectors.toList());
  }

  @Override
  public boolean existsFoo( //
      Integer fooId //
  ) {
    log.debug("existsFoo:fooId={}", fooId);

    boolean exists = repository.existsById(fooId);

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public boolean existsFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("existsFoo:fooQuery={}", fooQuery);

    boolean exists = !repository.existsFoos( //
        isNullValue(fooQuery.getFooId()), getQueryValue(fooQuery.getFooId()), //
        isNullValue(fooQuery.getFooChar()), getQueryValue(fooQuery.getFooChar()), //
        isNullValue(fooQuery.getFooVarchar()), getQueryValue(fooQuery.getFooVarchar()), //
        isNullValue(fooQuery.getFooText()), getQueryValue(fooQuery.getFooText()), //
        isNullValue(fooQuery.getFooSmallint()), getQueryValue(fooQuery.getFooSmallint()), //
        isNullValue(fooQuery.getFooInteger()), getQueryValue(fooQuery.getFooInteger()), //
        isNullValue(fooQuery.getFooBigint()), getQueryValue(fooQuery.getFooBigint()), //
        isNullValue(fooQuery.getFooReal()), getQueryValue(fooQuery.getFooReal()), //
        isNullValue(fooQuery.getFooDouble()), getQueryValue(fooQuery.getFooDouble()), //
        isNullValue(fooQuery.getFooDecimal()), getQueryValue(fooQuery.getFooDecimal()), //
        isNullValue(fooQuery.getFooBoolean()), getQueryValue(fooQuery.getFooBoolean()), //
        isNullValue(fooQuery.getFooDate()), getQueryValue(fooQuery.getFooDate()), //
        getQueryValue(fooQuery.getFooDateMin()), getQueryValue(fooQuery.getFooDateMax()), //
        isNullValue(fooQuery.getFooTimestamp()), getQueryValue(fooQuery.getFooTimestamp()), //
        getQueryValue(fooQuery.getFooTimestampMin()), getQueryValue(fooQuery.getFooTimestampMax()), //
        of(0, 1)).isEmpty();

    log.debug("existsFoo:exists={}", exists);
    return exists;
  }

  @Override
  public long countFoos( //
      FooQuery fooQuery //
  ) {
    log.debug("countFoos:fooQuery={}", fooQuery);

    long count = repository.countFoos( //
        isNullValue(fooQuery.getFooId()), getQueryValue(fooQuery.getFooId()), //
        isNullValue(fooQuery.getFooChar()), getQueryValue(fooQuery.getFooChar()), //
        isNullValue(fooQuery.getFooVarchar()), getQueryValue(fooQuery.getFooVarchar()), //
        isNullValue(fooQuery.getFooText()), getQueryValue(fooQuery.getFooText()), //
        isNullValue(fooQuery.getFooSmallint()), getQueryValue(fooQuery.getFooSmallint()), //
        isNullValue(fooQuery.getFooInteger()), getQueryValue(fooQuery.getFooInteger()), //
        isNullValue(fooQuery.getFooBigint()), getQueryValue(fooQuery.getFooBigint()), //
        isNullValue(fooQuery.getFooReal()), getQueryValue(fooQuery.getFooReal()), //
        isNullValue(fooQuery.getFooDouble()), getQueryValue(fooQuery.getFooDouble()), //
        isNullValue(fooQuery.getFooDecimal()), getQueryValue(fooQuery.getFooDecimal()), //
        isNullValue(fooQuery.getFooBoolean()), getQueryValue(fooQuery.getFooBoolean()), //
        isNullValue(fooQuery.getFooDate()), getQueryValue(fooQuery.getFooDate()), //
        getQueryValue(fooQuery.getFooDateMin()), getQueryValue(fooQuery.getFooDateMax()), //
        isNullValue(fooQuery.getFooTimestamp()), getQueryValue(fooQuery.getFooTimestamp()), //
        getQueryValue(fooQuery.getFooTimestampMin()), getQueryValue(fooQuery.getFooTimestampMax()));

    log.debug("countFoos:count={}", count);
    return count;
  }

  @Override
  public long updateFoo( //
      Integer fooId, //
      FooUpdate fooUpdate //
  ) {
    log.debug("updateFoo:fooId={}", fooId);
    log.debug("updateFoo:fooUpdate={}", fooUpdate);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaUpdate<FooEntity> update = criteriaBuilder.createCriteriaUpdate(FooEntity.class);
    Root<FooEntity> root = update.from(FooEntity.class);

    long updateCount = session.createQuery( //
        updateValues(update, root, fooUpdate).where(whereCondition(criteriaBuilder, root, fooId)) //
    ).executeUpdate();

    log.debug("updateFoo:updateCount={}", updateCount);
    return updateCount;
  }

  @Override
  public long deleteFoo( //
      Integer fooId //
  ) {
    log.debug("deleteFoo:fooId={}", fooId);
    Session session = entityManager.unwrap(Session.class);
    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

    CriteriaDelete<FooEntity> delete = criteriaBuilder.createCriteriaDelete(FooEntity.class);
    Root<FooEntity> root = delete.from(FooEntity.class);

    long deleteCount = session.createQuery( //
        delete.where(whereCondition(criteriaBuilder, root, fooId)) //
    ).executeUpdate();

    log.debug("deleteFoo:deleteCount={}", deleteCount);
    return deleteCount;
  }
}
