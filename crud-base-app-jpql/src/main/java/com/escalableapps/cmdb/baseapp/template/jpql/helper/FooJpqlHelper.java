package com.escalableapps.cmdb.baseapp.template.jpql.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.FooPropertyName.*;
import static org.springframework.data.domain.PageRequest.of;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class FooJpqlHelper {

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<FooEntity> root, //
      Integer fooId //
  ) {
    FooQuery fooQuery = new FooQuery();
    fooQuery.setFooId(fooId);
    return whereCondition(criteriaBuilder, root, fooQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForFooId(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooChar(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooVarchar(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooText(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooSmallint(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooInteger(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooBigint(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooReal(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDouble(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDecimal(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooBoolean(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooDate(predicates, criteriaBuilder, root, fooQuery);
    whereConditionForFooTimestamp(predicates, criteriaBuilder, root, fooQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForFooId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooId() != null) {
      if (!fooQuery.getFooId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_ID.getPropertyName()), getQueryValue(fooQuery.getFooId())));
      }
    }
  }

  private static void whereConditionForFooChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooChar() != null) {
      if (!fooQuery.getFooChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_CHAR.getPropertyName()), getQueryValue(fooQuery.getFooChar())));
      }
    }
  }

  private static void whereConditionForFooVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooVarchar() != null) {
      if (!fooQuery.getFooVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_VARCHAR.getPropertyName()), getQueryValue(fooQuery.getFooVarchar())));
      }
    }
  }

  private static void whereConditionForFooText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooText() != null) {
      if (!fooQuery.getFooText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_TEXT.getPropertyName()), getQueryValue(fooQuery.getFooText())));
      }
    }
  }

  private static void whereConditionForFooSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooSmallint() != null) {
      if (!fooQuery.getFooSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_SMALLINT.getPropertyName()), getQueryValue(fooQuery.getFooSmallint())));
      }
    }
  }

  private static void whereConditionForFooInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooInteger() != null) {
      if (!fooQuery.getFooInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_INTEGER.getPropertyName()), getQueryValue(fooQuery.getFooInteger())));
      }
    }
  }

  private static void whereConditionForFooBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooBigint() != null) {
      if (!fooQuery.getFooBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_BIGINT.getPropertyName()), getQueryValue(fooQuery.getFooBigint())));
      }
    }
  }

  private static void whereConditionForFooReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooReal() != null) {
      if (!fooQuery.getFooReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_REAL.getPropertyName()), getQueryValue(fooQuery.getFooReal())));
      }
    }
  }

  private static void whereConditionForFooDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDouble() != null) {
      if (!fooQuery.getFooDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DOUBLE.getPropertyName()), getQueryValue(fooQuery.getFooDouble())));
      }
    }
  }

  private static void whereConditionForFooDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDecimal() != null) {
      if (!fooQuery.getFooDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DECIMAL.getPropertyName()), getQueryValue(fooQuery.getFooDecimal())));
      }
    }
  }

  private static void whereConditionForFooBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooBoolean() != null) {
      if (!fooQuery.getFooBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_BOOLEAN.getPropertyName()), getQueryValue(fooQuery.getFooBoolean())));
      }
    }
  }

  private static void whereConditionForFooDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooDate() != null) {
      if (!fooQuery.getFooDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_DATE.getPropertyName()), getQueryValue(fooQuery.getFooDate())));
      }
    } else if (fooQuery.getFooDateMin() != null || fooQuery.getFooDateMax() != null) {
      if (fooQuery.getFooDateMin() != null && fooQuery.getFooDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FOO_DATE.getPropertyName()), fooQuery.getFooDateMin().get()));
      }
      if (fooQuery.getFooDateMax() != null && fooQuery.getFooDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FOO_DATE.getPropertyName()), fooQuery.getFooDateMax().get()));
      }
    }
  }

  private static void whereConditionForFooTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<FooEntity> root, FooQuery fooQuery) {
    if (fooQuery.getFooTimestamp() != null) {
      if (!fooQuery.getFooTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(FOO_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(FOO_TIMESTAMP.getPropertyName()), getQueryValue(fooQuery.getFooTimestamp())));
      }
    } else if (fooQuery.getFooTimestampMin() != null || fooQuery.getFooTimestampMax() != null) {
      if (fooQuery.getFooTimestampMin() != null && fooQuery.getFooTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FOO_TIMESTAMP.getPropertyName()), fooQuery.getFooTimestampMin().get()));
      }
      if (fooQuery.getFooTimestampMax() != null && fooQuery.getFooTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FOO_TIMESTAMP.getPropertyName()), fooQuery.getFooTimestampMax().get()));
      }
    }
  }

  public static Pageable orderBy(PageRequest<FooPropertyName> pageRequest) {
    List<Order> orders = new ArrayList<>();
    pageRequest.getSortRequests().forEach(sort -> {
      orderByForFooId(orders, sort);
      orderByForFooChar(orders, sort);
      orderByForFooVarchar(orders, sort);
      orderByForFooText(orders, sort);
      orderByForFooSmallint(orders, sort);
      orderByForFooInteger(orders, sort);
      orderByForFooBigint(orders, sort);
      orderByForFooReal(orders, sort);
      orderByForFooDouble(orders, sort);
      orderByForFooDecimal(orders, sort);
      orderByForFooBoolean(orders, sort);
      orderByForFooDate(orders, sort);
      orderByForFooTimestamp(orders, sort);
    });
    if (orders.isEmpty()) {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize());
    } else {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize(), Sort.by(orders));
    }
  }

  private static void orderByForFooId(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_ID) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooChar(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_CHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooVarchar(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooText(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TEXT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooSmallint(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooInteger(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_INTEGER) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooBigint(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BIGINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooReal(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_REAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooDouble(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooDecimal(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooBoolean(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooDate(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_DATE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForFooTimestamp(List<Order> orders, SortRequest<FooPropertyName> sort) {
    if (sort.getProperty() == FooPropertyName.FOO_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  public static CriteriaUpdate<FooEntity> updateValues(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    updateValuesForFooChar(update, root, fooUpdate);
    updateValuesForFooVarchar(update, root, fooUpdate);
    updateValuesForFooText(update, root, fooUpdate);
    updateValuesForFooSmallint(update, root, fooUpdate);
    updateValuesForFooInteger(update, root, fooUpdate);
    updateValuesForFooBigint(update, root, fooUpdate);
    updateValuesForFooReal(update, root, fooUpdate);
    updateValuesForFooDouble(update, root, fooUpdate);
    updateValuesForFooDecimal(update, root, fooUpdate);
    updateValuesForFooBoolean(update, root, fooUpdate);
    updateValuesForFooDate(update, root, fooUpdate);
    updateValuesForFooTimestamp(update, root, fooUpdate);
    return update;
  }

  private static void updateValuesForFooChar(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooChar() != null) {
      update.set(root.get(FOO_CHAR.getPropertyName()), getUpdateValue(fooUpdate.getFooChar()));
    }
  }

  private static void updateValuesForFooVarchar(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooVarchar() != null) {
      update.set(root.get(FOO_VARCHAR.getPropertyName()), getUpdateValue(fooUpdate.getFooVarchar()));
    }
  }

  private static void updateValuesForFooText(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooText() != null) {
      update.set(root.get(FOO_TEXT.getPropertyName()), getUpdateValue(fooUpdate.getFooText()));
    }
  }

  private static void updateValuesForFooSmallint(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooSmallint() != null) {
      update.set(root.get(FOO_SMALLINT.getPropertyName()), getUpdateValue(fooUpdate.getFooSmallint()));
    }
  }

  private static void updateValuesForFooInteger(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooInteger() != null) {
      update.set(root.get(FOO_INTEGER.getPropertyName()), getUpdateValue(fooUpdate.getFooInteger()));
    }
  }

  private static void updateValuesForFooBigint(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBigint() != null) {
      update.set(root.get(FOO_BIGINT.getPropertyName()), getUpdateValue(fooUpdate.getFooBigint()));
    }
  }

  private static void updateValuesForFooReal(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooReal() != null) {
      update.set(root.get(FOO_REAL.getPropertyName()), getUpdateValue(fooUpdate.getFooReal()));
    }
  }

  private static void updateValuesForFooDouble(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDouble() != null) {
      update.set(root.get(FOO_DOUBLE.getPropertyName()), getUpdateValue(fooUpdate.getFooDouble()));
    }
  }

  private static void updateValuesForFooDecimal(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDecimal() != null) {
      update.set(root.get(FOO_DECIMAL.getPropertyName()), getUpdateValue(fooUpdate.getFooDecimal()));
    }
  }

  private static void updateValuesForFooBoolean(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooBoolean() != null) {
      update.set(root.get(FOO_BOOLEAN.getPropertyName()), getUpdateValue(fooUpdate.getFooBoolean()));
    }
  }

  private static void updateValuesForFooDate(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooDate() != null) {
      update.set(root.get(FOO_DATE.getPropertyName()), getUpdateValue(fooUpdate.getFooDate()));
    }
  }

  private static void updateValuesForFooTimestamp(CriteriaUpdate<FooEntity> update, Root<FooEntity> root, FooUpdate fooUpdate) {
    if (fooUpdate.getFooTimestamp() != null) {
      update.set(root.get(FOO_TIMESTAMP.getPropertyName()), getUpdateValue(fooUpdate.getFooTimestamp()));
    }
  }

  public static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  public static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private FooJpqlHelper() {
  }
}
