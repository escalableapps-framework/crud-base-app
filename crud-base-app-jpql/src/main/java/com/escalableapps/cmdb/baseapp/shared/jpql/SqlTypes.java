package com.escalableapps.cmdb.baseapp.shared.jpql;

public final class SqlTypes {
  public static final String CHAR = "BPCHAR";
  public static final String VARCHAR = "VARCHAR";
  public static final String TEXT = "TEXT";
  public static final String SMALLINT = "INT2";
  public static final String INTEGER = "INT4";
  public static final String BIGINT = "INT8";
  public static final String REAL = "FLOAT4";
  public static final String DOUBLE = "FLOAT8";
  public static final String DECIMAL = "NUMERIC";
  public static final String BOOLEAN = "BOOL";
  public static final String DATE = "DATE";
  public static final String TIMESTAMP = "TIMESTAMPTZ";

  private SqlTypes() {
  }
}
