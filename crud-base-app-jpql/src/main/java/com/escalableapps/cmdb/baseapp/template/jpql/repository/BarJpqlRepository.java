package com.escalableapps.cmdb.baseapp.template.jpql.repository;

import java.math.*;
import java.time.*;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.escalableapps.cmdb.baseapp.shared.jpql.ExistsView;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public interface BarJpqlRepository extends JpaRepository<BarEntity, Integer> {

  String BAR_QUERY = //
      "FROM BarEntity AS bar " //
          + "WHERE ((TRUE = :barIdIsNull AND bar.barId IS NULL) OR (FALSE = :barIdIsNull AND (:barId IS NULL OR bar.barId = :barId))) " //
          + "  AND ((TRUE = :barCharIsNull AND bar.barChar IS NULL) OR (FALSE = :barCharIsNull AND (:barChar IS NULL OR bar.barChar = :barChar))) " //
          + "  AND ((TRUE = :barVarcharIsNull AND bar.barVarchar IS NULL) OR (FALSE = :barVarcharIsNull AND (:barVarchar IS NULL OR bar.barVarchar = :barVarchar))) " //
          + "  AND ((TRUE = :barTextIsNull AND bar.barText IS NULL) OR (FALSE = :barTextIsNull AND (:barText IS NULL OR bar.barText = :barText))) " //
          + "  AND ((TRUE = :barSmallintIsNull AND bar.barSmallint IS NULL) OR (FALSE = :barSmallintIsNull AND (:barSmallint IS NULL OR bar.barSmallint = :barSmallint))) " //
          + "  AND ((TRUE = :barIntegerIsNull AND bar.barInteger IS NULL) OR (FALSE = :barIntegerIsNull AND (:barInteger IS NULL OR bar.barInteger = :barInteger))) " //
          + "  AND ((TRUE = :barBigintIsNull AND bar.barBigint IS NULL) OR (FALSE = :barBigintIsNull AND (:barBigint IS NULL OR bar.barBigint = :barBigint))) " //
          + "  AND ((TRUE = :barRealIsNull AND bar.barReal IS NULL) OR (FALSE = :barRealIsNull AND (:barReal IS NULL OR bar.barReal = :barReal))) " //
          + "  AND ((TRUE = :barDoubleIsNull AND bar.barDouble IS NULL) OR (FALSE = :barDoubleIsNull AND (:barDouble IS NULL OR bar.barDouble = :barDouble))) " //
          + "  AND ((TRUE = :barDecimalIsNull AND bar.barDecimal IS NULL) OR (FALSE = :barDecimalIsNull AND (:barDecimal IS NULL OR bar.barDecimal = :barDecimal))) " //
          + "  AND ((TRUE = :barBooleanIsNull AND bar.barBoolean IS NULL) OR (FALSE = :barBooleanIsNull AND (:barBoolean IS NULL OR bar.barBoolean = :barBoolean))) " //
          + "  AND (" //
          + "        ((TRUE = :barDateIsNull) AND (bar.barDate IS NULL)) OR " //
          + "        ((FALSE = :barDateIsNull AND (CAST(:barDateMin AS java.time.LocalDate) IS NOT NULL OR CAST(:barDateMax AS java.time.LocalDate) IS NOT NULL)) AND (CAST(:barDateMin AS java.time.LocalDate) IS NULL OR :barDateMin <= bar.barDate) AND (CAST(:barDateMax AS java.time.LocalDate) IS NULL OR bar.barDate <= :barDateMax)) OR " //
          + "        ((FALSE = :barDateIsNull AND CAST(:barDateMin AS java.time.LocalDate) IS NULL AND CAST(:barDateMax AS java.time.LocalDate) IS NULL) AND (CAST(:barDate AS java.time.LocalDate) IS NULL OR bar.barDate = :barDate))" //
          + "      ) " //
          + "  AND (" //
          + "        ((TRUE = :barTimestampIsNull) AND (bar.barTimestamp IS NULL)) OR " //
          + "        ((FALSE = :barTimestampIsNull AND (CAST(:barTimestampMin AS java.time.OffsetDateTime) IS NOT NULL OR CAST(:barTimestampMax AS java.time.OffsetDateTime) IS NOT NULL)) AND (CAST(:barTimestampMin AS java.time.OffsetDateTime) IS NULL OR :barTimestampMin <= bar.barTimestamp) AND (CAST(:barTimestampMax AS java.time.OffsetDateTime) IS NULL OR bar.barTimestamp <= :barTimestampMax)) OR " //
          + "        ((FALSE = :barTimestampIsNull AND CAST(:barTimestampMin AS java.time.OffsetDateTime) IS NULL AND CAST(:barTimestampMax AS java.time.OffsetDateTime) IS NULL) AND (CAST(:barTimestamp AS java.time.OffsetDateTime) IS NULL OR bar.barTimestamp = :barTimestamp))" //
          + "      ) ";

  @Query(value = BAR_QUERY)
  List<BarEntity> findBars( //
      @Param("barIdIsNull") boolean barIdIsNull, @Param("barId") Integer barId, //
      @Param("barCharIsNull") boolean barCharIsNull, @Param("barChar") String barChar, //
      @Param("barVarcharIsNull") boolean barVarcharIsNull, @Param("barVarchar") String barVarchar, //
      @Param("barTextIsNull") boolean barTextIsNull, @Param("barText") String barText, //
      @Param("barSmallintIsNull") boolean barSmallintIsNull, @Param("barSmallint") Short barSmallint, //
      @Param("barIntegerIsNull") boolean barIntegerIsNull, @Param("barInteger") Integer barInteger, //
      @Param("barBigintIsNull") boolean barBigintIsNull, @Param("barBigint") Long barBigint, //
      @Param("barRealIsNull") boolean barRealIsNull, @Param("barReal") Float barReal, //
      @Param("barDoubleIsNull") boolean barDoubleIsNull, @Param("barDouble") Double barDouble, //
      @Param("barDecimalIsNull") boolean barDecimalIsNull, @Param("barDecimal") BigDecimal barDecimal, //
      @Param("barBooleanIsNull") boolean barBooleanIsNull, @Param("barBoolean") Boolean barBoolean, //
      @Param("barDateIsNull") boolean barDateIsNull, @Param("barDate") LocalDate barDate, //
      @Param("barDateMin") LocalDate barDateMin, @Param("barDateMax") LocalDate barDateMax, //
      @Param("barTimestampIsNull") boolean barTimestampIsNull, @Param("barTimestamp") OffsetDateTime barTimestamp, //
      @Param("barTimestampMin") OffsetDateTime barTimestampMin, @Param("barTimestampMax") OffsetDateTime barTimestampMax, //
      Pageable pageable);

  @Query(value = BAR_QUERY)
  List<ExistsView> existsBars( //
      @Param("barIdIsNull") boolean barIdIsNull, @Param("barId") Integer barId, //
      @Param("barCharIsNull") boolean barCharIsNull, @Param("barChar") String barChar, //
      @Param("barVarcharIsNull") boolean barVarcharIsNull, @Param("barVarchar") String barVarchar, //
      @Param("barTextIsNull") boolean barTextIsNull, @Param("barText") String barText, //
      @Param("barSmallintIsNull") boolean barSmallintIsNull, @Param("barSmallint") Short barSmallint, //
      @Param("barIntegerIsNull") boolean barIntegerIsNull, @Param("barInteger") Integer barInteger, //
      @Param("barBigintIsNull") boolean barBigintIsNull, @Param("barBigint") Long barBigint, //
      @Param("barRealIsNull") boolean barRealIsNull, @Param("barReal") Float barReal, //
      @Param("barDoubleIsNull") boolean barDoubleIsNull, @Param("barDouble") Double barDouble, //
      @Param("barDecimalIsNull") boolean barDecimalIsNull, @Param("barDecimal") BigDecimal barDecimal, //
      @Param("barBooleanIsNull") boolean barBooleanIsNull, @Param("barBoolean") Boolean barBoolean, //
      @Param("barDateIsNull") boolean barDateIsNull, @Param("barDate") LocalDate barDate, //
      @Param("barDateMin") LocalDate barDateMin, @Param("barDateMax") LocalDate barDateMax, //
      @Param("barTimestampIsNull") boolean barTimestampIsNull, @Param("barTimestamp") OffsetDateTime barTimestamp, //
      @Param("barTimestampMin") OffsetDateTime barTimestampMin, @Param("barTimestampMax") OffsetDateTime barTimestampMax, //
      Pageable pageable);

  @Query(value = "SELECT COUNT(bar) " + BAR_QUERY)
  long countBars( //
      @Param("barIdIsNull") boolean barIdIsNull, @Param("barId") Integer barId, //
      @Param("barCharIsNull") boolean barCharIsNull, @Param("barChar") String barChar, //
      @Param("barVarcharIsNull") boolean barVarcharIsNull, @Param("barVarchar") String barVarchar, //
      @Param("barTextIsNull") boolean barTextIsNull, @Param("barText") String barText, //
      @Param("barSmallintIsNull") boolean barSmallintIsNull, @Param("barSmallint") Short barSmallint, //
      @Param("barIntegerIsNull") boolean barIntegerIsNull, @Param("barInteger") Integer barInteger, //
      @Param("barBigintIsNull") boolean barBigintIsNull, @Param("barBigint") Long barBigint, //
      @Param("barRealIsNull") boolean barRealIsNull, @Param("barReal") Float barReal, //
      @Param("barDoubleIsNull") boolean barDoubleIsNull, @Param("barDouble") Double barDouble, //
      @Param("barDecimalIsNull") boolean barDecimalIsNull, @Param("barDecimal") BigDecimal barDecimal, //
      @Param("barBooleanIsNull") boolean barBooleanIsNull, @Param("barBoolean") Boolean barBoolean, //
      @Param("barDateIsNull") boolean barDateIsNull, @Param("barDate") LocalDate barDate, //
      @Param("barDateMin") LocalDate barDateMin, @Param("barDateMax") LocalDate barDateMax, //
      @Param("barTimestampIsNull") boolean barTimestampIsNull, @Param("barTimestamp") OffsetDateTime barTimestamp, //
      @Param("barTimestampMin") OffsetDateTime barTimestampMin, @Param("barTimestampMax") OffsetDateTime barTimestampMax);
}
