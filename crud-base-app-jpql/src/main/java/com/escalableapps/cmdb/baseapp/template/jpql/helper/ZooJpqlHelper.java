package com.escalableapps.cmdb.baseapp.template.jpql.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.ZooPropertyName.*;
import static org.springframework.data.domain.PageRequest.of;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class ZooJpqlHelper {

  private static final String EMBEDDED_ID = "id";

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<ZooEntity> root, //
      Long zooOrg, //
      Integer zooId //
  ) {
    ZooQuery zooQuery = new ZooQuery();
    zooQuery.setZooOrg(zooOrg);
    zooQuery.setZooId(zooId);
    return whereCondition(criteriaBuilder, root, zooQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForZooOrg(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooId(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooChar(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooVarchar(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooText(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooSmallint(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooInteger(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooBigint(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooReal(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDouble(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDecimal(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooBoolean(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooDate(predicates, criteriaBuilder, root, zooQuery);
    whereConditionForZooTimestamp(predicates, criteriaBuilder, root, zooQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForZooOrg(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooOrg() != null) {
      if (!zooQuery.getZooOrg().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(ZOO_ORG.getPropertyName()), getQueryValue(zooQuery.getZooOrg())));
      }
    }
  }

  private static void whereConditionForZooId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooId() != null) {
      if (!zooQuery.getZooId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(ZOO_ID.getPropertyName()), getQueryValue(zooQuery.getZooId())));
      }
    }
  }

  private static void whereConditionForZooChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooChar() != null) {
      if (!zooQuery.getZooChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_CHAR.getPropertyName()), getQueryValue(zooQuery.getZooChar())));
      }
    }
  }

  private static void whereConditionForZooVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooVarchar() != null) {
      if (!zooQuery.getZooVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_VARCHAR.getPropertyName()), getQueryValue(zooQuery.getZooVarchar())));
      }
    }
  }

  private static void whereConditionForZooText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooText() != null) {
      if (!zooQuery.getZooText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_TEXT.getPropertyName()), getQueryValue(zooQuery.getZooText())));
      }
    }
  }

  private static void whereConditionForZooSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooSmallint() != null) {
      if (!zooQuery.getZooSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_SMALLINT.getPropertyName()), getQueryValue(zooQuery.getZooSmallint())));
      }
    }
  }

  private static void whereConditionForZooInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooInteger() != null) {
      if (!zooQuery.getZooInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_INTEGER.getPropertyName()), getQueryValue(zooQuery.getZooInteger())));
      }
    }
  }

  private static void whereConditionForZooBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooBigint() != null) {
      if (!zooQuery.getZooBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_BIGINT.getPropertyName()), getQueryValue(zooQuery.getZooBigint())));
      }
    }
  }

  private static void whereConditionForZooReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooReal() != null) {
      if (!zooQuery.getZooReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_REAL.getPropertyName()), getQueryValue(zooQuery.getZooReal())));
      }
    }
  }

  private static void whereConditionForZooDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDouble() != null) {
      if (!zooQuery.getZooDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DOUBLE.getPropertyName()), getQueryValue(zooQuery.getZooDouble())));
      }
    }
  }

  private static void whereConditionForZooDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDecimal() != null) {
      if (!zooQuery.getZooDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DECIMAL.getPropertyName()), getQueryValue(zooQuery.getZooDecimal())));
      }
    }
  }

  private static void whereConditionForZooBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooBoolean() != null) {
      if (!zooQuery.getZooBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_BOOLEAN.getPropertyName()), getQueryValue(zooQuery.getZooBoolean())));
      }
    }
  }

  private static void whereConditionForZooDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooDate() != null) {
      if (!zooQuery.getZooDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_DATE.getPropertyName()), getQueryValue(zooQuery.getZooDate())));
      }
    } else if (zooQuery.getZooDateMin() != null || zooQuery.getZooDateMax() != null) {
      if (zooQuery.getZooDateMin() != null && zooQuery.getZooDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(ZOO_DATE.getPropertyName()), zooQuery.getZooDateMin().get()));
      }
      if (zooQuery.getZooDateMax() != null && zooQuery.getZooDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(ZOO_DATE.getPropertyName()), zooQuery.getZooDateMax().get()));
      }
    }
  }

  private static void whereConditionForZooTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<ZooEntity> root, ZooQuery zooQuery) {
    if (zooQuery.getZooTimestamp() != null) {
      if (!zooQuery.getZooTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(ZOO_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(ZOO_TIMESTAMP.getPropertyName()), getQueryValue(zooQuery.getZooTimestamp())));
      }
    } else if (zooQuery.getZooTimestampMin() != null || zooQuery.getZooTimestampMax() != null) {
      if (zooQuery.getZooTimestampMin() != null && zooQuery.getZooTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(ZOO_TIMESTAMP.getPropertyName()), zooQuery.getZooTimestampMin().get()));
      }
      if (zooQuery.getZooTimestampMax() != null && zooQuery.getZooTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(ZOO_TIMESTAMP.getPropertyName()), zooQuery.getZooTimestampMax().get()));
      }
    }
  }

  public static Pageable orderBy(PageRequest<ZooPropertyName> pageRequest) {
    List<Order> orders = new ArrayList<>();
    pageRequest.getSortRequests().forEach(sort -> {
      orderByForZooOrg(orders, sort);
      orderByForZooId(orders, sort);
      orderByForZooChar(orders, sort);
      orderByForZooVarchar(orders, sort);
      orderByForZooText(orders, sort);
      orderByForZooSmallint(orders, sort);
      orderByForZooInteger(orders, sort);
      orderByForZooBigint(orders, sort);
      orderByForZooReal(orders, sort);
      orderByForZooDouble(orders, sort);
      orderByForZooDecimal(orders, sort);
      orderByForZooBoolean(orders, sort);
      orderByForZooDate(orders, sort);
      orderByForZooTimestamp(orders, sort);
    });
    if (orders.isEmpty()) {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize());
    } else {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize(), Sort.by(orders));
    }
  }

  private static void orderByForZooOrg(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ORG) {
      orders.add(sort.isAscendant() //
          ? Order.asc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
          : Order.desc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooId(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_ID) {
      orders.add(sort.isAscendant() //
          ? Order.asc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
          : Order.desc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooChar(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_CHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooVarchar(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooText(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TEXT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooSmallint(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooInteger(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_INTEGER) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooBigint(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BIGINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooReal(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_REAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooDouble(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooDecimal(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooBoolean(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooDate(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_DATE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForZooTimestamp(List<Order> orders, SortRequest<ZooPropertyName> sort) {
    if (sort.getProperty() == ZooPropertyName.ZOO_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  public static CriteriaUpdate<ZooEntity> updateValues(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    updateValuesForZooChar(update, root, zooUpdate);
    updateValuesForZooVarchar(update, root, zooUpdate);
    updateValuesForZooText(update, root, zooUpdate);
    updateValuesForZooSmallint(update, root, zooUpdate);
    updateValuesForZooInteger(update, root, zooUpdate);
    updateValuesForZooBigint(update, root, zooUpdate);
    updateValuesForZooReal(update, root, zooUpdate);
    updateValuesForZooDouble(update, root, zooUpdate);
    updateValuesForZooDecimal(update, root, zooUpdate);
    updateValuesForZooBoolean(update, root, zooUpdate);
    updateValuesForZooDate(update, root, zooUpdate);
    updateValuesForZooTimestamp(update, root, zooUpdate);
    return update;
  }

  private static void updateValuesForZooChar(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooChar() != null) {
      update.set(root.get(ZOO_CHAR.getPropertyName()), getUpdateValue(zooUpdate.getZooChar()));
    }
  }

  private static void updateValuesForZooVarchar(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooVarchar() != null) {
      update.set(root.get(ZOO_VARCHAR.getPropertyName()), getUpdateValue(zooUpdate.getZooVarchar()));
    }
  }

  private static void updateValuesForZooText(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooText() != null) {
      update.set(root.get(ZOO_TEXT.getPropertyName()), getUpdateValue(zooUpdate.getZooText()));
    }
  }

  private static void updateValuesForZooSmallint(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooSmallint() != null) {
      update.set(root.get(ZOO_SMALLINT.getPropertyName()), getUpdateValue(zooUpdate.getZooSmallint()));
    }
  }

  private static void updateValuesForZooInteger(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooInteger() != null) {
      update.set(root.get(ZOO_INTEGER.getPropertyName()), getUpdateValue(zooUpdate.getZooInteger()));
    }
  }

  private static void updateValuesForZooBigint(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBigint() != null) {
      update.set(root.get(ZOO_BIGINT.getPropertyName()), getUpdateValue(zooUpdate.getZooBigint()));
    }
  }

  private static void updateValuesForZooReal(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooReal() != null) {
      update.set(root.get(ZOO_REAL.getPropertyName()), getUpdateValue(zooUpdate.getZooReal()));
    }
  }

  private static void updateValuesForZooDouble(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDouble() != null) {
      update.set(root.get(ZOO_DOUBLE.getPropertyName()), getUpdateValue(zooUpdate.getZooDouble()));
    }
  }

  private static void updateValuesForZooDecimal(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDecimal() != null) {
      update.set(root.get(ZOO_DECIMAL.getPropertyName()), getUpdateValue(zooUpdate.getZooDecimal()));
    }
  }

  private static void updateValuesForZooBoolean(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooBoolean() != null) {
      update.set(root.get(ZOO_BOOLEAN.getPropertyName()), getUpdateValue(zooUpdate.getZooBoolean()));
    }
  }

  private static void updateValuesForZooDate(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooDate() != null) {
      update.set(root.get(ZOO_DATE.getPropertyName()), getUpdateValue(zooUpdate.getZooDate()));
    }
  }

  private static void updateValuesForZooTimestamp(CriteriaUpdate<ZooEntity> update, Root<ZooEntity> root, ZooUpdate zooUpdate) {
    if (zooUpdate.getZooTimestamp() != null) {
      update.set(root.get(ZOO_TIMESTAMP.getPropertyName()), getUpdateValue(zooUpdate.getZooTimestamp()));
    }
  }

  public static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  public static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private ZooJpqlHelper() {
  }
}
