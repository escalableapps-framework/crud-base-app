package com.escalableapps.cmdb.baseapp.template.jpql.helper;

import static com.escalableapps.cmdb.baseapp.template.domain.entity.LookPropertyName.*;
import static org.springframework.data.domain.PageRequest.of;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.escalableapps.cmdb.baseapp.shared.domain.model.vo.*;
import com.escalableapps.cmdb.baseapp.template.domain.entity.*;
import com.escalableapps.cmdb.baseapp.template.domain.vo.*;
import com.escalableapps.cmdb.baseapp.template.jpql.persistence.*;

public final class LookJpqlHelper {

  private static final String EMBEDDED_ID = "id";

  public static Predicate[] whereCondition( //
      CriteriaBuilder criteriaBuilder, //
      Root<LookEntity> root, //
      Long lookOrg, //
      Long lookId //
  ) {
    LookQuery lookQuery = new LookQuery();
    lookQuery.setLookOrg(lookOrg);
    lookQuery.setLookId(lookId);
    return whereCondition(criteriaBuilder, root, lookQuery);
  }

  public static Predicate[] whereCondition(CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    List<Predicate> predicates = new ArrayList<>();
    whereConditionForLookOrg(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookId(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookChar(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookVarchar(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookText(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookSmallint(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookInteger(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookBigint(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookReal(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDouble(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDecimal(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookBoolean(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookDate(predicates, criteriaBuilder, root, lookQuery);
    whereConditionForLookTimestamp(predicates, criteriaBuilder, root, lookQuery);
    return predicates.stream().toArray(size -> new Predicate[size]);
  }

  private static void whereConditionForLookOrg(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookOrg() != null) {
      if (!lookQuery.getLookOrg().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(LOOK_ORG.getPropertyName()), getQueryValue(lookQuery.getLookOrg())));
      }
    }
  }

  private static void whereConditionForLookId(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookId() != null) {
      if (!lookQuery.getLookId().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(EMBEDDED_ID).get(LOOK_ID.getPropertyName()), getQueryValue(lookQuery.getLookId())));
      }
    }
  }

  private static void whereConditionForLookChar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookChar() != null) {
      if (!lookQuery.getLookChar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_CHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_CHAR.getPropertyName()), getQueryValue(lookQuery.getLookChar())));
      }
    }
  }

  private static void whereConditionForLookVarchar(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookVarchar() != null) {
      if (!lookQuery.getLookVarchar().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_VARCHAR.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_VARCHAR.getPropertyName()), getQueryValue(lookQuery.getLookVarchar())));
      }
    }
  }

  private static void whereConditionForLookText(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookText() != null) {
      if (!lookQuery.getLookText().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_TEXT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_TEXT.getPropertyName()), getQueryValue(lookQuery.getLookText())));
      }
    }
  }

  private static void whereConditionForLookSmallint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookSmallint() != null) {
      if (!lookQuery.getLookSmallint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_SMALLINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_SMALLINT.getPropertyName()), getQueryValue(lookQuery.getLookSmallint())));
      }
    }
  }

  private static void whereConditionForLookInteger(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookInteger() != null) {
      if (!lookQuery.getLookInteger().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_INTEGER.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_INTEGER.getPropertyName()), getQueryValue(lookQuery.getLookInteger())));
      }
    }
  }

  private static void whereConditionForLookBigint(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookBigint() != null) {
      if (!lookQuery.getLookBigint().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_BIGINT.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_BIGINT.getPropertyName()), getQueryValue(lookQuery.getLookBigint())));
      }
    }
  }

  private static void whereConditionForLookReal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookReal() != null) {
      if (!lookQuery.getLookReal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_REAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_REAL.getPropertyName()), getQueryValue(lookQuery.getLookReal())));
      }
    }
  }

  private static void whereConditionForLookDouble(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDouble() != null) {
      if (!lookQuery.getLookDouble().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DOUBLE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DOUBLE.getPropertyName()), getQueryValue(lookQuery.getLookDouble())));
      }
    }
  }

  private static void whereConditionForLookDecimal(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDecimal() != null) {
      if (!lookQuery.getLookDecimal().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DECIMAL.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DECIMAL.getPropertyName()), getQueryValue(lookQuery.getLookDecimal())));
      }
    }
  }

  private static void whereConditionForLookBoolean(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookBoolean() != null) {
      if (!lookQuery.getLookBoolean().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_BOOLEAN.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_BOOLEAN.getPropertyName()), getQueryValue(lookQuery.getLookBoolean())));
      }
    }
  }

  private static void whereConditionForLookDate(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookDate() != null) {
      if (!lookQuery.getLookDate().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_DATE.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_DATE.getPropertyName()), getQueryValue(lookQuery.getLookDate())));
      }
    } else if (lookQuery.getLookDateMin() != null || lookQuery.getLookDateMax() != null) {
      if (lookQuery.getLookDateMin() != null && lookQuery.getLookDateMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LOOK_DATE.getPropertyName()), lookQuery.getLookDateMin().get()));
      }
      if (lookQuery.getLookDateMax() != null && lookQuery.getLookDateMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LOOK_DATE.getPropertyName()), lookQuery.getLookDateMax().get()));
      }
    }
  }

  private static void whereConditionForLookTimestamp(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Root<LookEntity> root, LookQuery lookQuery) {
    if (lookQuery.getLookTimestamp() != null) {
      if (!lookQuery.getLookTimestamp().exist()) {
        predicates.add(criteriaBuilder.isNull(root.get(LOOK_TIMESTAMP.getPropertyName())));
      } else {
        predicates.add(criteriaBuilder.equal(root.get(LOOK_TIMESTAMP.getPropertyName()), getQueryValue(lookQuery.getLookTimestamp())));
      }
    } else if (lookQuery.getLookTimestampMin() != null || lookQuery.getLookTimestampMax() != null) {
      if (lookQuery.getLookTimestampMin() != null && lookQuery.getLookTimestampMin().exist()) {
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LOOK_TIMESTAMP.getPropertyName()), lookQuery.getLookTimestampMin().get()));
      }
      if (lookQuery.getLookTimestampMax() != null && lookQuery.getLookTimestampMax().exist()) {
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LOOK_TIMESTAMP.getPropertyName()), lookQuery.getLookTimestampMax().get()));
      }
    }
  }

  public static Pageable orderBy(PageRequest<LookPropertyName> pageRequest) {
    List<Order> orders = new ArrayList<>();
    pageRequest.getSortRequests().forEach(sort -> {
      orderByForLookOrg(orders, sort);
      orderByForLookId(orders, sort);
      orderByForLookChar(orders, sort);
      orderByForLookVarchar(orders, sort);
      orderByForLookText(orders, sort);
      orderByForLookSmallint(orders, sort);
      orderByForLookInteger(orders, sort);
      orderByForLookBigint(orders, sort);
      orderByForLookReal(orders, sort);
      orderByForLookDouble(orders, sort);
      orderByForLookDecimal(orders, sort);
      orderByForLookBoolean(orders, sort);
      orderByForLookDate(orders, sort);
      orderByForLookTimestamp(orders, sort);
    });
    if (orders.isEmpty()) {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize());
    } else {
      return of(pageRequest.getPageNumber(), pageRequest.getPageSize(), Sort.by(orders));
    }
  }

  private static void orderByForLookOrg(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ORG) {
      orders.add(sort.isAscendant() //
          ? Order.asc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
          : Order.desc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookId(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_ID) {
      orders.add(sort.isAscendant() //
          ? Order.asc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
          : Order.desc(EMBEDDED_ID + "." + sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookChar(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_CHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookVarchar(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_VARCHAR) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookText(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TEXT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookSmallint(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_SMALLINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookInteger(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_INTEGER) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookBigint(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BIGINT) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookReal(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_REAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookDouble(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DOUBLE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookDecimal(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DECIMAL) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookBoolean(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_BOOLEAN) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookDate(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_DATE) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  private static void orderByForLookTimestamp(List<Order> orders, SortRequest<LookPropertyName> sort) {
    if (sort.getProperty() == LookPropertyName.LOOK_TIMESTAMP) {
      orders.add(sort.isAscendant() //
          ? Order.asc(sort.getProperty().getPropertyName()) //
          : Order.desc(sort.getProperty().getPropertyName()) //
      );
    }
  }

  public static CriteriaUpdate<LookEntity> updateValues(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    updateValuesForLookChar(update, root, lookUpdate);
    updateValuesForLookVarchar(update, root, lookUpdate);
    updateValuesForLookText(update, root, lookUpdate);
    updateValuesForLookSmallint(update, root, lookUpdate);
    updateValuesForLookInteger(update, root, lookUpdate);
    updateValuesForLookBigint(update, root, lookUpdate);
    updateValuesForLookReal(update, root, lookUpdate);
    updateValuesForLookDouble(update, root, lookUpdate);
    updateValuesForLookDecimal(update, root, lookUpdate);
    updateValuesForLookBoolean(update, root, lookUpdate);
    updateValuesForLookDate(update, root, lookUpdate);
    updateValuesForLookTimestamp(update, root, lookUpdate);
    return update;
  }

  private static void updateValuesForLookChar(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookChar() != null) {
      update.set(root.get(LOOK_CHAR.getPropertyName()), getUpdateValue(lookUpdate.getLookChar()));
    }
  }

  private static void updateValuesForLookVarchar(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookVarchar() != null) {
      update.set(root.get(LOOK_VARCHAR.getPropertyName()), getUpdateValue(lookUpdate.getLookVarchar()));
    }
  }

  private static void updateValuesForLookText(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookText() != null) {
      update.set(root.get(LOOK_TEXT.getPropertyName()), getUpdateValue(lookUpdate.getLookText()));
    }
  }

  private static void updateValuesForLookSmallint(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookSmallint() != null) {
      update.set(root.get(LOOK_SMALLINT.getPropertyName()), getUpdateValue(lookUpdate.getLookSmallint()));
    }
  }

  private static void updateValuesForLookInteger(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookInteger() != null) {
      update.set(root.get(LOOK_INTEGER.getPropertyName()), getUpdateValue(lookUpdate.getLookInteger()));
    }
  }

  private static void updateValuesForLookBigint(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBigint() != null) {
      update.set(root.get(LOOK_BIGINT.getPropertyName()), getUpdateValue(lookUpdate.getLookBigint()));
    }
  }

  private static void updateValuesForLookReal(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookReal() != null) {
      update.set(root.get(LOOK_REAL.getPropertyName()), getUpdateValue(lookUpdate.getLookReal()));
    }
  }

  private static void updateValuesForLookDouble(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDouble() != null) {
      update.set(root.get(LOOK_DOUBLE.getPropertyName()), getUpdateValue(lookUpdate.getLookDouble()));
    }
  }

  private static void updateValuesForLookDecimal(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDecimal() != null) {
      update.set(root.get(LOOK_DECIMAL.getPropertyName()), getUpdateValue(lookUpdate.getLookDecimal()));
    }
  }

  private static void updateValuesForLookBoolean(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookBoolean() != null) {
      update.set(root.get(LOOK_BOOLEAN.getPropertyName()), getUpdateValue(lookUpdate.getLookBoolean()));
    }
  }

  private static void updateValuesForLookDate(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookDate() != null) {
      update.set(root.get(LOOK_DATE.getPropertyName()), getUpdateValue(lookUpdate.getLookDate()));
    }
  }

  private static void updateValuesForLookTimestamp(CriteriaUpdate<LookEntity> update, Root<LookEntity> root, LookUpdate lookUpdate) {
    if (lookUpdate.getLookTimestamp() != null) {
      update.set(root.get(LOOK_TIMESTAMP.getPropertyName()), getUpdateValue(lookUpdate.getLookTimestamp()));
    }
  }

  public static boolean isNullValue(QueryValue<?> queryValue) {
    return queryValue != null && !queryValue.exist();
  }

  public static <T extends Serializable> T getQueryValue(QueryValue<T> queryValue) {
    return queryValue == null ? null : queryValue.get();
  }

  private static <T extends Serializable> T getUpdateValue(UpdateValue<T> updateValue) {
    return updateValue.get();
  }

  private LookJpqlHelper() {
  }
}
