package com.escalableapps.cmdb.baseapp.template.jpql.persistence;

import static com.escalableapps.cmdb.baseapp.shared.jpql.SqlTypes.*;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Embeddable
public class ZooPk implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "zoo_org", columnDefinition = BIGINT)
  private Long zooOrg;

  @Column(name = "zoo_id", columnDefinition = INTEGER)
  private Integer zooId;
}
